package com.spicedigital.offnet;

import java.io.*;
import java.text.*;
import java.net.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.security.*;
import javax.net.ssl.*;

import org.apache.log4j.*;

public class CGSubmit implements Runnable
{
	private static Logger logger				= Logger.getLogger(CGSubmit.class.getName());
	private static int thread_id				= 0;
	private DBConnectionManager connMgr			= null;
	private String dbpool_mysql					= "mysql";

	private static int sleep_timer_noselect		= 500;
	private static int sleep_timer_select		= 10;
	private static int response_top_select		= 100;

	private static final String cg_url			= "http://<consent_gateway_ip>:<port>/Offnet/cgengine?CGSystemId=<CGSystemId>&CGUserName=<CGUserName>&CGPassword=<CGPassword>&Msisdn=<msisdn>&OrigServiceCode=<OrigServiceCode>&FirstConsentMsg=<message_level_1>&FirstConsentResp=<FirstConsentResp>&OrigCircleCode=<circle>&ProductID=<PRODUCT_ID>&en_SecondConsentMsg=<message_level_2>&SecondConsentResp=9&user=Spice&pass=Hidvm58n&msisdn=<msisdn>&srvkey=<srvkey>&Refid=<ref_id>&mode=ussd&reqType=<req_type>&precharge=N&reqdate=<req_date>&reqtime=<req_time>&originator=Spice";

	private static String consent_gateway_ip	= "";
	private static String port					= "";
	private static String CGSystemId			= "";
	private static String CGUserName			= "";
	private static String CGPassword			= "";

	public CGSubmit()
	{
		try{PropertyConfigurator.configure("log4j.properties");}catch(Exception exp){}
		try{connMgr 	= DBConnectionManager.getInstance();}catch(Exception exp){logException("",exp);}
		try{trustAllHttpsCertificates();}catch(Exception ssl){}
		try
		{
			Properties prop 					= new Properties();
			prop.load(new DataInputStream(new FileInputStream("offnet.properties")));

			try{sleep_timer_noselect			= Integer.parseInt(prop.getProperty("offnet.submit.noselect.timer"));}catch(Exception exp){sleep_timer_noselect=500;}
			try{sleep_timer_select				= Integer.parseInt(prop.getProperty("offnet.submit.select.timer"));}catch(Exception exp){sleep_timer_select=10;}
			try{response_top_select				= Integer.parseInt(prop.getProperty("offnet.submit.top.select"));}catch(Exception exp){response_top_select=100;}
			consent_gateway_ip					= prop.getProperty("offnet.consent.gateway.ip");
			port								= prop.getProperty("offnet.consent.gateway.port");
			CGSystemId							= prop.getProperty("offnet.consent.gateway.CGSystemId");
			CGUserName							= prop.getProperty("offnet.consent.gateway.CGUserName");
			CGPassword							= prop.getProperty("offnet.consent.gateway.CGPassword");
		}catch(Exception exp){logException("",exp);}

		this.thread_id				= thread_id;
		Thread t = new Thread(this);	t.start();
	}

	public void run()
	{
		final int threadId					= thread_id;
		final String threadName				= String.format("[%-10s] ->(cg  :%02d) ",Thread.currentThread().getName(),threadId);
		try
		{
			int rowCtr						= 0;
			int sleepTimer					= 50;
			int prntCtr						= 0;
			while(true)
			{
				try
				{
					rowCtr	= processRequest(threadName,threadId);
					if(rowCtr>0)
					{
						sleepTimer		= sleep_timer_select;
					}else sleepTimer	= sleep_timer_noselect;
					if(prntCtr%250 == 0)
					{
						logger.debug(threadName+"Total records processed "+rowCtr+". Sleeping for "+sleepTimer+" msecs. prntCtr = "+prntCtr+" prntCtr%250 = "+prntCtr%250);
						prntCtr			= 0;
					}
					try{Thread.sleep(sleepTimer);}catch(Exception exp){}
				}catch(Exception exp){logException(threadName,exp);try{Thread.sleep(1000*5);}catch(Exception expTmr){}}
				prntCtr++;
			}
		}catch(Exception exp){logException(threadName,exp);}
	}

	private int processRequest(final String threadName,final int threadId)
	{
		int rowCtr = 0;

		String RESPONSE_LEVEL_STR					= "";
		int RESPONSE_LEVEL_IND						= 0;
		//logger.debug(threadName+"dbpool_mysql = "+dbpool_mysql+", dbpool_mysql = "+dbpool_mysql);
		Connection connSel 							= connMgr.getConnection(dbpool_mysql);
		Connection connDel 							= connMgr.getConnection(dbpool_mysql);
		try
		{
			if(connSel != null && connDel != null)
			{
				try
				{
					int thread_counter					= 0;
					Statement stmtSel					= connSel.createStatement();
					Statement stmtDel					= connDel.createStatement();

					String strSel						= "select refid,circle,shortcode,msisdn,productid,srvkey,reqtype,message_level_1,FirstConsentResp,message_level_2 from tbl_cgRequest where status = 0";
					//logger.debug(threadName+strSel);
					ResultSet rs						= stmtSel.executeQuery(strSel);

					while(rs.next())
					{
						String refid					= rs.getString("refid");
						String circle					= rs.getString("circle");
						String shortcode				= rs.getString("shortcode");
						String msisdn					= rs.getString("msisdn");
						String productid				= rs.getString("productid");
						String srvkey					= rs.getString("srvkey");
						String reqtype					= rs.getString("reqtype");
						String message_level_1			= rs.getString("message_level_1");
						String FirstConsentResp			= rs.getString("FirstConsentResp");
						String message_level_2			= rs.getString("message_level_2");
						String url						= cg_url;

						//"http://<consent_gateway_ip>:<port>/ConsentGateway?CGSystemId=<CGSystemId>&CGUserName=<CGUserName>&CGPassword=<CGPassword>&Msisdn=<msisdn>&OrigServiceCode=*999#&FirstConsentMsg=<message_level_1>&FirstConsentResp=<FirstConsentResp>&OrigCircleCode=CE&ProductID=<PRODUCT_ID>&SecondConsentMsg=<message_level_2>&SecondConsentResp=YES&User=spice&Pass=Mmrrp98u&msisdn=<msisdn>&srvkey=<srvkey>&Refid=<ref_id>&mode=ussd&reqType=<req_type>&precharge=N&reqdate=<req_date>&reqtime=<req_time>&originator=Spice";
						if(circle.equalsIgnoreCase("kl"))		{circle = "ka";}
						else if(circle.equalsIgnoreCase("mu"))	{circle = "mb";}
						else if(circle.equalsIgnoreCase("ne"))	{circle = "ns";}
						else if(circle.equalsIgnoreCase(""))	{circle = "ce";}

						shortcode						= shortcode.replace("*","");
						shortcode						= shortcode.replace("#","");

						url								= url.replace("<consent_gateway_ip>",consent_gateway_ip);
						url								= url.replace("<port>",port);
						url								= url.replace("<CGSystemId>",CGSystemId);
						url								= url.replace("<CGUserName>",CGUserName);
						url								= url.replace("<CGPassword>",CGPassword);
						url								= url.replace("<OrigServiceCode>",shortcode);
						url								= url.replace("<msisdn>",msisdn);
						url								= url.replace("<circle>",circle.toUpperCase());
						url								= url.replace("<message_level_1>",URLEncoder.encode(message_level_1,"UTF-8"));
						url								= url.replace("<FirstConsentResp>",FirstConsentResp);
						url								= url.replace("<PRODUCT_ID>",URLEncoder.encode(productid,"UTF-8"));
						url								= url.replace("<message_level_2>",URLEncoder.encode(message_level_2,"UTF-8"));
						url								= url.replace("<srvkey>",srvkey);
						url								= url.replace("<ref_id>",refid);
						url								= url.replace("<req_type>","sub");
						url								= url.replace("<req_date>",generateDate());
						url								= url.replace("<req_time>",generateTime());

						String retStr					= callHttp(threadName,url);
						//if(!retStr.equalsIgnoreCase("error"))
						{
							String stmDel				= "delete from tbl_cgRequest where refid = '"+refid+"'";
							logger.debug(threadName+stmDel+" = "+stmtDel.executeUpdate(stmDel));
						}
						rowCtr++;
						try{Thread.sleep(50);}catch(Exception exp){}
					}
				}catch(Exception exp){logException(threadName,exp);}
			}
			else{}
		}catch(Exception exp){logException(threadName,exp);}
		finally
		{
			if(connSel != null){connMgr.freeConnection(dbpool_mysql,connSel);}
			if(connDel != null){connMgr.freeConnection(dbpool_mysql,connDel);}
		}
		return rowCtr;
	}
	private String generateDate()
	{
		java.util.Date date 						= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT			= new SimpleDateFormat("yyyyMMdd");
		String logDate								= SQL_DATE_FORMAT.format(date.getTime());
		return logDate;
	}

	private String generateTime()
	{
		java.util.Date date 						= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT			= new SimpleDateFormat("HHmmss");
		String logDate								= SQL_DATE_FORMAT.format(date.getTime());
		return logDate;
	}
 	private String callHttps(final String threadName,final String url)
 	{
 		String retStr 								= "";
 		String header 								= "";
 		try
 		{
			HostnameVerifier hv 					= new HostnameVerifier(){public boolean verify(String urlHostName, SSLSession session){return true;}};
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
 			URL Url 								= new URL(url);
 			URLConnection conn 						= Url.openConnection();
 			conn.setReadTimeout(1000*20);
 			header 									= String.valueOf(conn.getHeaderFields());
 			logger.debug(threadName+"responseHeader: "+header);
 			if(header.indexOf("200 OK")>0) header = "[HTTP/1.1 200 OK]"; else retStr = header;
 			String str 								= null;
 			BufferedReader in 						= new BufferedReader(new InputStreamReader(conn.getInputStream()));
 			while ((str = in.readLine()) != null) retStr += str;
 			in.close();
 			if(retStr.length()<2) if(header.equalsIgnoreCase("[HTTP/1.1 200 OK]")){retStr="success";}
 			logger.debug(threadName+url+" : "+retStr);
 		}catch(Exception exp){retStr="error";logger.error(threadName+url);logException(threadName,exp);}
 		return retStr;
 	}

 	 	private String callHttp(final String threadName,final String url)
	 	{
	 		String retStr 								= "";
	 		String header 								= "";
	 		try
	 		{
	 			URL Url 								= new URL(url);
	 			URLConnection conn 						= Url.openConnection();
	 			conn.setReadTimeout(1000*20);
	 			header 									= String.valueOf(conn.getHeaderFields());
	 			logger.debug(threadName+"responseHeader: "+header);
	 			if(header.indexOf("200 OK")>0) header = "[HTTP/1.1 200 OK]"; else retStr = header;
	 			String str 								= null;
	 			BufferedReader in 						= new BufferedReader(new InputStreamReader(conn.getInputStream()));
	 			while ((str = in.readLine()) != null) retStr += str;
	 			in.close();
	 			if(retStr.length()<2) if(header.equalsIgnoreCase("[HTTP/1.1 200 OK]")){retStr="success";}
	 			logger.debug(threadName+url+" :"+retStr);
	 		}catch(Exception exp){retStr="error";logger.error(threadName+url);logException(threadName,exp);}
	 		return retStr;
 	}

	private void logException(final String threadName,Exception exp)
	{
		logger.error(threadName+getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}
	private static void trustAllHttpsCertificates() throws Exception
	{
		javax.net.ssl.TrustManager[] trustAllCerts	= new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm 				= new miTM();

		trustAllCerts[0] 							= tm;
		javax.net.ssl.SSLContext sc 				= javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	}

	public static class miTM implements javax.net.ssl.TrustManager,javax.net.ssl.X509TrustManager
	{
		public java.security.cert.X509Certificate[] getAcceptedIssuers()			{return null;}
		public boolean isServerTrusted(java.security.cert.X509Certificate[] certs)	{return true;}
		public boolean isClientTrusted(java.security.cert.X509Certificate[] certs)	{return true;}
		public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)	throws java.security.cert.CertificateException{return;}
		public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) throws java.security.cert.CertificateException{return;}
	}

}