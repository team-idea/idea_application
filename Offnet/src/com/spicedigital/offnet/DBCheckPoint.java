package com.spicedigital.offnet;

import java.io.*;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Timer;
import java.sql.*;
import org.apache.log4j.*;

class DBCheckPoint extends TimerTask
{
    private Timer timer;
    private int count=0;
	private static Logger logger 				= Logger.getLogger(DBCheckPoint.class.getName());
	private DBConnectionManager connMgr			= null;
	private String dbpool_hsql					= "hsql";

    public DBCheckPoint()
    {

    }
    public DBCheckPoint(Timer timer)
    {
		try{PropertyConfigurator.configure("log4j.properties");}catch(Exception exp){}
		try{connMgr 	= DBConnectionManager.getInstance();}catch(Exception exp){logException("",exp);}
        this.timer=timer;
    }
	private String dbCheckPoint(final String threadName)
	{
		String SQLQuery								= "CHECKPOINT DEFRAG";
		logger.debug(threadName+"dbCheckPoint() called.");
		String retStr								= "";
		return retStr.trim();
	}

    @Override
    public void run()
    {
		final String threadName						= String.format("[%-10s] ->(tmr : al) ",Thread.currentThread().getName());
		dbCheckPoint(threadName);
	}
	private void logException(final String threadName,Exception exp)
	{
		logger.error(threadName+getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}
}
