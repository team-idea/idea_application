package com.spicedigital.offnet;

import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import org.apache.log4j.*;

public class FetchRetryRecords implements Runnable
{
	private static Logger logger 				= Logger.getLogger(FetchRetryRecords.class.getName());
	private DBConnectionManager connMgr			= null;
	private String dbpool_mysql					= "mysql";
	private String dbpool_hsql					= "hsql";
	private String dbpool_mssql					= "mssql";
	private static int thread_cnt				= 0;
	private int thread_counter					= 0;
	private int thresholdCount					= 200;

	private static int sleep_timer_noselect		= 500;
	private static int sleep_timer_select		= 10;
	private static String retry_select_view		= "view_offnet_retry";
	private static int retry_top_select			= 100;

	public FetchRetryRecords(final int thread_counter)
	{
		try{PropertyConfigurator.configure("log4j.properties");}catch(Exception exp){}
		try{connMgr 	= DBConnectionManager.getInstance();}catch(Exception exp){logException("",exp);}
		try
		{
			Properties prop 					= new Properties();
			prop.load(new DataInputStream(new FileInputStream("offnet.properties")));
			retry_select_view					= prop.getProperty("offnet.retry.fetch.select.view");
			try{sleep_timer_noselect			= Integer.parseInt(prop.getProperty("offnet.retry.fetch.noselect.timer"));}catch(Exception exp){sleep_timer_noselect=500;}
			try{sleep_timer_select				= Integer.parseInt(prop.getProperty("offnet.retry.fetch.select.timer"));}catch(Exception exp){sleep_timer_select=10;}
			try{retry_top_select				= Integer.parseInt(prop.getProperty("offnet.retry.fetch.top.select"));}catch(Exception exp){retry_top_select=100;}
			try{thresholdCount					= Integer.parseInt(prop.getProperty("offnet.retry.fetch.threshold.limit"));}catch(Exception exp){thresholdCount=200;}

		}catch(Exception exp){logException("",exp);}

		thread_cnt		= thread_counter;

		Thread t = new Thread(this); t.start();
	}
	public void run()
	{
		final int thread_count					= this.thread_cnt;
		final String threadName					= String.format("[%-10s] ->(ret : al) ",Thread.currentThread().getName());
		int rowCtr								= 0;
		int prntCtr								= 0;
		int sleepTimer							= 0;
		flushQue(threadName);
		while(true)
		{
			int qCount							= getQueCounter(threadName);
			if(qCount > thresholdCount)
			{
				logger.debug(threadName+"Memory DB full to prescribed ["+thresholdCount+"] limit. Please re-check the application.");
				sleepTimer						= sleep_timer_noselect;
			}
			else
			{
				rowCtr	= insertRecords(threadName,thread_cnt);
				if(rowCtr>0) sleepTimer = sleep_timer_select; else sleepTimer = sleep_timer_noselect;
				if(prntCtr%500 == 0)
				{
					logger.debug(threadName+"Total records processed "+rowCtr+". Sleeping for "+sleepTimer+" msecs. prntCtr = "+prntCtr+" prntCtr%250 = "+prntCtr%250);
					prntCtr		= 0;
				}
			}
			try{Thread.sleep(sleepTimer);}catch(Exception exp){}
			prntCtr++;
		}
	}

	private int flushQue(String threadName)
	{
		int retRowCount								= 0;
		Connection connSel 							= connMgr.getConnection(dbpool_hsql);
		try
		{
			String truncStr							= "truncate table tbl_offnet_retry";
			if(connSel != null)
			{
				Statement stmtSel					= connSel.createStatement();
				int cntr							= stmtSel.executeUpdate(truncStr);
				logger.debug(threadName+truncStr+" : "+cntr);
			}else logger.debug(threadName+"No active connection for db pool ["+dbpool_hsql+"] to find the queue status.");
		}catch(Exception exp){}
		finally
		{
			if(connSel != null){connMgr.freeConnection(dbpool_hsql, connSel);}
		}
		return retRowCount;
	}

	private int getQueCounter(String threadName)
	{
		int retRowCount								= 0;
		Connection connSel 							= connMgr.getConnection(dbpool_hsql);
		try
		{
			String selStr							= "select count(1) cnt from "+retry_select_view+" ";
			if(connSel != null)
			{
				Statement stmtSel					= connSel.createStatement();
				ResultSet rs						= stmtSel.executeQuery(selStr);
				if(rs.next()) {retRowCount			= rs.getInt("cnt");try{rs.close();}catch(Exception exp){}}
			}else logger.debug(threadName+"No active connection for db pool ["+dbpool_hsql+"] to find the queue status.");
		}catch(Exception exp){}
		finally
		{
			if(connSel != null){connMgr.freeConnection(dbpool_hsql, connSel);}
		}
		return retRowCount;
	}

	private int insertRecords(final String threadName,final int thread_count)
	{
		int rowCtr = 0;
		//logger.debug(threadName+"dbpool_mysql = "+dbpool_mysql+", dbpool_hsql = "+dbpool_hsql);
		Connection connSel 							= connMgr.getConnection(dbpool_mssql);
		Connection connDel 							= connMgr.getConnection(dbpool_mssql);
		Connection connIns 							= connMgr.getConnection(dbpool_hsql);

		try
		{
			if(connSel != null && connDel != null && connIns != null)
			{

				try
				{
					Statement stmtSel					= connSel.createStatement();
					Statement stmtDel					= connDel.createStatement();
					Statement stmtIns					= connIns.createStatement();
					//String strSel						= "select top "+retry_top_select+" refid,circle,msisdn,contentid,mode from tbl_retryBase where convert(varchar,billdate,120) <= convert(varchar,getdate(),120) and convert(varchar,resubdate,120)>=convert(varchar,getdate(),120) and status = 0 order by billdate asc";
					String strSel						= "select top "+retry_top_select+" refid,circle,msisdn,contentid,mode from tbl_retryBase where convert(varchar,billdate,120) = convert(varchar,getdate(),120) and convert(varchar,resubdate,120)>=convert(varchar,getdate(),120) and circle='tn' and  srvkey='DEVO10' and contentid='A7D1007' and status = 0 order by billdate asc";
					ResultSet rs						= stmtSel.executeQuery(strSel);  
					while(rs.next())
					{
						if(thread_counter>=thread_count)thread_counter = 0; else thread_counter++;
						String refid		      		= rs.getString("refid");
						String circle		      		= rs.getString("circle");
						String msisdn		      		= rs.getString("msisdn");
						String content_id	      		= rs.getString("contentid");
						String mode		      			= rs.getString("mode");

						logger.debug(threadName+thread_counter+" - "+refid+" - "+circle+" - "+msisdn+" - "+content_id+" - "+mode);

						String strInsMem				= "insert into tbl_offnet_retry values ('"+refid+"','"+circle+"','"+msisdn+"','"+content_id+"','"+mode+"',0,'"+thread_counter+"')";
						int rowCount					= stmtIns.executeUpdate(strInsMem);
						if(rowCount == 1)
						{
							String updtStr			= "update tbl_retryBase set billDate = getdate(),status=1 where refid = '"+refid+"' and circle = '"+circle+"' and msisdn = '"+msisdn+"' and contentid = '"+content_id+"'";
							logger.debug(threadName+updtStr+" : "+stmtDel.executeUpdate(updtStr));
						}
						rowCtr++;
						try{Thread.sleep(25);}catch(Exception exp){}
					}
				}catch(Exception exp){logException(threadName,exp);try{Thread.sleep(10);}catch(Exception expTmr){}}
			}else {logger.debug(threadName+"connSel = "+connSel+" connDel = "+connDel+" connIns = "+connIns);try{Thread.sleep(10);}catch(Exception expTmr){}}
		}catch(Exception exp){logException(threadName,exp);try{Thread.sleep(10);}catch(Exception expTmr){}}
		finally
		{
			if(connSel != null){connMgr.freeConnection(dbpool_mssql,connSel);}
			if(connDel != null){connMgr.freeConnection(dbpool_mssql,connDel);}
			if(connIns != null){connMgr.freeConnection(dbpool_hsql, connIns);}
		}
		return rowCtr;
	}
	private void logException(final String threadName,Exception exp)
	{
		logger.error(threadName+getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}
}