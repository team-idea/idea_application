package com.spicedigital.offnet;

import java.io.*;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import org.apache.log4j.*;

public class CallbackSync extends Thread
{
	private static Logger logger 				= Logger.getLogger(CallbackSync.class.getName() );
	private DBConnectionManager connMgr			= null;
	private String conPool_mysql				= "mysql";
	private String conPool_mssql				= "mssql";
	private static String circle_name			= "xx";

	private static int select_top_rows			= 10;
	private static int thread_sleeptime			= 10;
	private static int idle_log_print			= 5;
	private static int callback_sync_flag		= 0;

	public CallbackSync(final String circle)
	{
		try
		{
			PropertyConfigurator.configure("log4j.properties");
			connMgr 							= DBConnectionManager.getInstance();
			Properties prop						= new Properties();
			prop.load(new DataInputStream(new FileInputStream("offnet.properties")));
			try{select_top_rows					= Integer.parseInt(prop.getProperty("offnet.CallbackSync.top.select"));}catch(Exception exp){select_top_rows=50;}
			try{thread_sleeptime				= Integer.parseInt(prop.getProperty("offnet.CallbackSync.sleeptime.select"));}catch(Exception exp){thread_sleeptime=5;}
			try{callback_sync_flag				= Integer.parseInt(prop.getProperty("offnet.CallbackSync.status.flag"));}catch(Exception exp){callback_sync_flag=1;}
			try{idle_log_print					= Integer.parseInt(prop.getProperty("offnet.idle.log.print"));}catch(Exception exp){idle_log_print=5;}
		}
		catch(Exception exp){exp.printStackTrace();}
		this.circle_name						= circle;
		this.start();
	}

	public void run()
	{
		final String circleName			= circle_name;
		final String threadName			= String.format("[%-10s] ->(syn :%3s) ",Thread.currentThread().getName(),circleName);

		int prntCtr						= 0;
		while(true)
		{
			if(callback_sync_flag == 1)
			{
				int processedRows 			= processCallbackReq(threadName,circleName);
				if(processedRows<=0)
				{
					prntCtr++;
					if(prntCtr%idle_log_print == 0)
					{
						logger.info(threadName+"No Records found to be processed. Sleeping for "+String.format("%3d",thread_sleeptime)+" secs. ");
						prntCtr = 0;
					}
					try{Thread.sleep(thread_sleeptime*1000);}catch(Exception expTmr){}
				}
				else{logger.info(threadName+"Total Records processed = "+processedRows+". Sleeping for 500 msecs.");try{Thread.sleep(500);}catch(Exception expTmr){}}
			}
			else{logger.info(threadName+"Callback Sync has been currently disabled. Sleeping for 5 mins.");try{Thread.sleep(1000*60*5);}catch(Exception expTmr){}}
		}
	}

	private int processCallbackReq(final String threadName,final String circleName)
	{
		int retCode = 0;
		try
		{
			Connection connSelect			= connMgr.getConnection(conPool_mysql);
			Connection connUpdate			= connMgr.getConnection(conPool_mysql);
			Connection connInsert			= connMgr.getConnection(conPool_mssql);
			try
			{
				if(connSelect!=null&&connUpdate!=null&&connInsert!=null)
				{
					logger.debug(threadName+"Into processCallbackReq() function. conPool_mysql= "+conPool_mysql+" conPool_mysql= "+conPool_mysql);
					Statement stmtSelect	= connSelect.createStatement();
					Statement stmtUpdate	= connUpdate.createStatement();
					Statement stmtInsert	= connInsert.createStatement();
					String stmSelect		= "select RefId, circle, msisdn, prepost, srvKey, mode, price, sdpStatus, status, action, precharge, startDateTime, endDateTime, originator, cp_user, cp_password, info, retry_num, callbackInsertDateTime, sync_status from sdpCentral_"+circleName+".dbo.tbl_callback_log with(nolock) where circle = '"+circleName+"' and sync_status =0 limit "+select_top_rows;
					logger.debug(threadName+""+stmSelect);
					ResultSet rs			= stmtSelect.executeQuery(stmSelect);
					logger.debug(threadName+"Into select ResultSet iteration");
					while(rs.next())
					{
						String RefId					= rs.getString("RefId");
						String circle					= rs.getString("circle");
						String msisdn					= rs.getString("msisdn");
						String prepost					= rs.getString("prepost");
						String srvKey					= rs.getString("srvKey");
						String mode						= rs.getString("mode");
						String price					= rs.getString("price");
						String sdpStatus				= rs.getString("sdpStatus");
						String status					= rs.getString("status");
						String action					= rs.getString("action");
						String precharge				= rs.getString("precharge");
						String startDateTime			= rs.getString("startDateTime");
						String endDateTime				= rs.getString("endDateTime");
						String originator				= rs.getString("originator");
						String cp_user					= rs.getString("cp_user");
						String cp_password				= rs.getString("cp_password");
						String info						= rs.getString("info");
						String retry_num				= rs.getString("retry_num");
						String callbackInsertDateTime	= rs.getString("callbackInsertDateTime");
						String sync_status				= rs.getString("sync_status");

						if(circle.length()>2){circle = circleName;}

						if(msisdn.length() == 10 || msisdn.length() == 12)
						{
							String insert_stmt			= "insert into sdpCentral_"+circleName+".dbo.tbl_callback(RefId, circle, msisdn, prepost, srvKey, mode, price, sdpStatus, status, action, precharge, startDateTime, endDateTime, originator, cp_user, cp_password, info, retry_num, callbackInsertDateTime, sync_status) values ('"+RefId+"','"+circle+"','"+msisdn+"','"+prepost+"','"+srvKey+"','"+mode+"','"+price+"','"+sdpStatus+"','0','"+action+"','"+precharge+"','"+startDateTime+"','"+endDateTime+"','"+originator+"','"+cp_user+"','"+cp_password+"','"+info+"','"+retry_num+"','"+callbackInsertDateTime+"','"+sync_status+"')";
							String update_stmt			= "";
							int rowCtr					= 0;
							try
							{
								rowCtr					= stmtInsert.executeUpdate(insert_stmt);
								update_stmt				= "update sdpCentral_"+circleName+".dbo.tbl_callback_log set sync_status = 1 where RefId='"+RefId+"' and srvKey = '"+srvKey+"' and msisdn='"+msisdn+"' and sync_status = 0";
							}catch(Exception exp){logger.error("["+threadName+"] ->pull0 :"+circleName+") "+insert_stmt+": error");logException(exp,circle,threadName);rowCtr=0;}

							logger.debug(threadName+""+insert_stmt+":"+rowCtr);
							if(rowCtr != 1)	{logger.error(threadName+"Not Able to submit the message correctly to central Database rowCtr = "+rowCtr);}
							else 			{stmtUpdate.executeUpdate(update_stmt); }
							try{Thread.sleep(10*2);}catch(Exception expT){}
							retCode++;
						}
						else
						{
							logger.debug(threadName+"Invalid msisdn ["+msisdn+"] received. Updating the entry as invalid entry.");
							try{Thread.sleep(10*5);}catch(Exception expT){}
							String update_stmt			= "update sdpCentral_"+circleName+".dbo.tbl_callback_log set sync_status = -1 where RefId='"+RefId+"' and srvKey = '"+srvKey+"' and msisdn='"+msisdn+"' and sync_status = 0";
							stmtUpdate.executeUpdate(update_stmt);
						}
					}
				}
				else
				{
					logger.info(threadName+"[ "+conPool_mysql+"<--->"+conPool_mysql+" ] Connection Cannot be established connSelect="+connSelect+" connUpdate="+connUpdate+" connInsert"+connInsert);
					try{Thread.sleep(1000*3);}catch(Exception expTmr){}
				}
			}catch(Exception expCon){logException(expCon,circleName,threadName);}
			finally
			{
				if(connSelect!=null) connMgr.freeConnection(conPool_mysql,connSelect);
				if(connUpdate!=null) connMgr.freeConnection(conPool_mysql,connUpdate);
				if(connInsert!=null) connMgr.freeConnection(conPool_mssql,connInsert);
			}
		}
		catch(Exception exp){logException(exp,circleName,threadName);}
		return retCode;
	}
	private void logException(Exception exp,final String circleName,final String threadName)
	{
		logger.error(threadName+getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}
}