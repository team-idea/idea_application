package com.spicedigital.offnet;

import java.io.*;
import java.text.*;
import java.net.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.security.*;
import javax.net.ssl.*;

import org.apache.log4j.*;

public class OffNetDeactivate implements Runnable
{
	private static Logger logger				= Logger.getLogger(OffNetDeactivate.class.getName());
	private static int thread_id				= 0;
	private DBConnectionManager connMgr			= null;
	private String dbpool_mssql					= "mssql";
	private static int sleep_timer_noselect		= 500;
	private static int sleep_timer_select		= 10;
	private static int response_top_select		= 100;


	public OffNetDeactivate()
	{
		try{PropertyConfigurator.configure("log4j.properties");}catch(Exception exp){}
		try{connMgr 	= DBConnectionManager.getInstance();}catch(Exception exp){logException("",exp);}
		try{trustAllHttpsCertificates();}catch(Exception ssl){}
		try
		{
			Properties prop 					= new Properties();
			prop.load(new DataInputStream(new FileInputStream("offnet.properties")));

			try{sleep_timer_noselect			= Integer.parseInt(prop.getProperty("sdp.offnet.dct.noselect.timer"));}catch(Exception exp){sleep_timer_noselect=5000;}
			try{sleep_timer_select				= Integer.parseInt(prop.getProperty("sdp.offnet.dct.select.timer"));}catch(Exception exp){sleep_timer_select=100;}
			try{response_top_select				= Integer.parseInt(prop.getProperty("sdp.offnet.dct.top.select"));}catch(Exception exp){response_top_select=100;}

		}catch(Exception exp){logException("",exp);}


		this.thread_id				= thread_id;
		Thread t = new Thread(this);	t.start();
	}

	public void run()
	{
		final int threadId					= thread_id;
		final String threadName				= String.format("[%-10s] ->(dct :%03d) ",Thread.currentThread().getName(),threadId);
		try
		{
			int rowCtr						= 0;
			int sleepTimer					= 50;
			int prntCtr						= 0;
			String table_info				= "active";
			while(true)
			{
				try
				{
					rowCtr	= processRequest(threadName,threadId,table_info);
					if(rowCtr>0)
					{
						sleepTimer		= sleep_timer_select;
					}else sleepTimer	= sleep_timer_noselect;
					if(prntCtr%500 == 0)
					{
						logger.debug(threadName+"Total records processed "+rowCtr+". Sleeping for "+sleepTimer+" msecs. prntCtr = "+prntCtr+" prntCtr%250 = "+prntCtr%250);
						prntCtr			= 0;
					}
					if(table_info.equalsIgnoreCase("active"))		table_info = "grace";
					else if(table_info.equalsIgnoreCase("grace"))	table_info = "retry";
					else 											table_info = "active";
					try{Thread.sleep(sleepTimer);}catch(Exception exp){}
				}catch(Exception exp){logException(threadName,exp);try{Thread.sleep(1000*5);}catch(Exception expTmr){}}
				prntCtr++;
			}
		}catch(Exception exp){logException(threadName,exp);}
	}

	private int processRequest(final String threadName,final int threadId,final String base)
	{
		int rowCtr = 0;
		Connection connSel 							= connMgr.getConnection(dbpool_mssql);
		Connection connUpd 							= connMgr.getConnection(dbpool_mssql);
		try
		{
			if(connSel != null && connUpd != null)
			{
				try
				{
					int thread_counter					= 0;
					Statement stmtSel					= connSel.createStatement();
					Statement stmtUpd					= connUpd.createStatement();
					String strSel						= "select top 50 circle,resubdate,msisdn,srvkey,mode from sdpCentral_ss.dbo.tbl_"+base+"Base where convert(varchar,resubDate,120) < convert(varchar,getdate(),120) and status not in (0,2)";
					ResultSet rs						= stmtSel.executeQuery(strSel);

					while(rs.next())
					{
						String circle					= rs.getString("circle").trim();
						String resubdate				= rs.getString("resubdate").trim();
						String msisdn					= rs.getString("msisdn").trim();
						String srvkey					= rs.getString("srvkey").trim();
						String mode						= rs.getString("mode").trim();
						logger.debug(threadName+"circle "+circle+", resubdate "+resubdate+", msisdn "+msisdn+", srvkey "+srvkey+", mode "+mode);
						String url						= "http://10.0.252.103/Offnet/DeactivateSubscription?circle="+circle+"&msisdn="+msisdn+"&srvKey="+srvkey+"&mode="+mode;
						String retStr					= "error";
						retStr							= callHttp(threadName,url);
						logger.debug(threadName+"url: "+url+" res: "+retStr);
						if(retStr.equalsIgnoreCase("200|success|Unsubscribed"))
						{
							String updt_str					= "update sdpCentral_ss.dbo.tbl_"+base+"Base set status = 2 where circle = '"+circle+"' and msisdn = '"+msisdn+"' and srvkey = '"+srvkey+"'";
							logger.debug(threadName+updt_str);
							int cntr_updt					= stmtUpd.executeUpdate(updt_str);
							rowCtr++;
						}else logger.error(threadName+"Could not deactivate subscriber "+msisdn+" for service "+srvkey+", circle = "+circle);
						try{Thread.sleep(50);}catch(Exception exp){}
					}
				}catch(Exception exp){logException(threadName,exp);}
			}
			else{}
		}catch(Exception exp){logException(threadName,exp);}
		finally
		{
			if(connSel != null){connMgr.freeConnection(dbpool_mssql,connSel);}
			if(connUpd != null){connMgr.freeConnection(dbpool_mssql,connUpd);}
		}
		return rowCtr;
	}
 	private String callHttps(final String threadName,final String url)
 	{
 		String retStr 								= "";
 		String header 								= "";
 		try
 		{
			HostnameVerifier hv 					= new HostnameVerifier(){public boolean verify(String urlHostName, SSLSession session){return true;}};
			HttpsURLConnection.setDefaultHostnameVerifier(hv);
 			URL Url 								= new URL(url);
 			URLConnection conn 						= Url.openConnection();
 			conn.setReadTimeout(1000*20);
 			header 									= String.valueOf(conn.getHeaderFields());
 			logger.debug(threadName+"responseHeader: "+header);
 			if(header.indexOf("200 OK")>0) header = "[HTTP/1.1 200 OK]"; else retStr = header;
 			String str 								= null;
 			BufferedReader in 						= new BufferedReader(new InputStreamReader(conn.getInputStream()));
 			while ((str = in.readLine()) != null) retStr += str;
 			in.close();
 			if(retStr.length()<2) if(header.equalsIgnoreCase("[HTTP/1.1 200 OK]")){retStr="success";}
 			logger.debug(threadName+url+retStr);
 		}catch(Exception exp){retStr="error";logger.error(threadName+url);logException(threadName,exp);}
 		return retStr;
 	}

	private String callHttp(final String threadName,final String url)
	{
		String retStr 								= "";
		String header 								= "";
		try
		{
			URL Url 								= new URL(url);
			URLConnection conn 						= Url.openConnection();
			conn.setReadTimeout(1000*20);
			header 									= String.valueOf(conn.getHeaderFields());
			logger.debug(threadName+"responseHeader: "+header);
			if(header.indexOf("200 OK")>0) header = "[HTTP/1.1 200 OK]"; else retStr = header;
			String str 								= null;
			BufferedReader in 						= new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while ((str = in.readLine()) != null) retStr += str;
			in.close();
			if(retStr.length()<2) if(header.equalsIgnoreCase("[HTTP/1.1 200 OK]")){retStr="success";}
			logger.debug(threadName+url+retStr);
		}catch(Exception exp){retStr="error";logger.error(threadName+url);logException(threadName,exp);}
		return retStr;
	}

	private void logException(final String threadName,Exception exp)
	{
		logger.error(threadName+getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}
	private static void trustAllHttpsCertificates() throws Exception
	{
		javax.net.ssl.TrustManager[] trustAllCerts	= new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm 				= new miTM();

		trustAllCerts[0] 							= tm;
		javax.net.ssl.SSLContext sc 				= javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	}

	public static class miTM implements javax.net.ssl.TrustManager,javax.net.ssl.X509TrustManager
	{
		public java.security.cert.X509Certificate[] getAcceptedIssuers()			{return null;}
		public boolean isServerTrusted(java.security.cert.X509Certificate[] certs)	{return true;}
		public boolean isClientTrusted(java.security.cert.X509Certificate[] certs)	{return true;}
		public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)	throws java.security.cert.CertificateException{return;}
		public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) throws java.security.cert.CertificateException{return;}
	}
}