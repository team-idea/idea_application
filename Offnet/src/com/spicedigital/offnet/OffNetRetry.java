package com.spicedigital.offnet;

import java.io.*;
import java.text.*;
import java.net.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import java.security.*;
import javax.net.ssl.*;

import org.apache.log4j.*;

public class OffNetRetry implements Runnable
{
	private static Logger logger				= Logger.getLogger(OffNetRetry.class.getName());
	private static int thread_id				= 0;
	private DBConnectionManager connMgr			= null;
	private String dbpool_hsql					= "hsql";
	private String dbpool_mysql					= "mysql";
	private String dbpool_mssql					= "mssql";
	private static int sleep_timer_noselect		= 500;
	private static int sleep_timer_select		= 10;
	private static int response_top_select		= 100;


	public OffNetRetry(final int threadid)
	{
		try{PropertyConfigurator.configure("log4j.properties");}catch(Exception exp){}
		try{connMgr 	= DBConnectionManager.getInstance();}catch(Exception exp){logException("",exp);}
		try{trustAllHttpsCertificates();}catch(Exception ssl){}
		try
		{
			Properties prop 					= new Properties();
			prop.load(new DataInputStream(new FileInputStream("offnet.properties")));

			try{sleep_timer_noselect			= Integer.parseInt(prop.getProperty("offnet.retry.noselect.timer"));}catch(Exception exp){sleep_timer_noselect=500;}
			try{sleep_timer_select				= Integer.parseInt(prop.getProperty("offnet.retry.select.timer"));}catch(Exception exp){sleep_timer_select=10;}
			try{response_top_select				= Integer.parseInt(prop.getProperty("offnet.retry.top.select"));}catch(Exception exp){response_top_select=100;}

		}catch(Exception exp){logException("",exp);}

		this.thread_id= threadid;
		Thread t = new Thread(this);	t.start();
	}

	public void run()
	{
		final int thread					= this.thread_id;
		final String threadName				= String.format("[%-10s] ->(ret :%03d) ",Thread.currentThread().getName(),thread);
		try
		{
			int rowCtr						= 0;
			int sleepTimer					= 50;
			int prntCtr						= 0;
			while(true)
			{
				try
				{
					rowCtr	= processRequest(threadName,thread);
					if(rowCtr>0)
					{
						sleepTimer		= sleep_timer_select;
					}else sleepTimer	= sleep_timer_noselect;
					if(prntCtr%500 == 0)
					{
						logger.debug(threadName+"Total records processed "+rowCtr+". Sleeping for "+sleepTimer+" msecs. prntCtr = "+prntCtr+" prntCtr%250 = "+prntCtr%250);
						prntCtr			= 0;
					}
					try{Thread.sleep(sleepTimer);}catch(Exception exp){}
				}catch(Exception exp){logException(threadName,exp);try{Thread.sleep(1000*5);}catch(Exception expTmr){}}
				prntCtr++;
			}
		}catch(Exception exp){logException(threadName,exp);}
	}

	private int processRequest(final String threadName,final int thread_id)
	{
		int rowCtr = 0;
		Connection connSel 							= connMgr.getConnection(dbpool_hsql);
		Connection connUpd 							= connMgr.getConnection(dbpool_hsql);
		Connection connIns 							= connMgr.getConnection(dbpool_hsql);
		try
		{
			if(connSel != null && connUpd != null && connIns != null)
			{
				try
				{
					int thread_counter					= 0;
					Statement stmtSel					= connSel.createStatement();
					Statement stmtUpd					= connUpd.createStatement();
					Statement stmtIns					= connIns.createStatement();

					String strSel						= "select refid,circle,msisdn,contentid,mode from tbl_offnet_retry where status=0 and thread_id = "+thread_id+" limit "+response_top_select;
					ResultSet rs						= stmtSel.executeQuery(strSel);
					while(rs.next())
					{
						String refid					= rs.getString("refid").trim();
						String circle					= rs.getString("circle").trim();
						String msisdn					= rs.getString("msisdn").trim();
						String contentid				= rs.getString("contentid").trim();
						String mode						= rs.getString("mode").trim();
						String retStr					= retryRequest(threadName,circle,msisdn,contentid,mode,"retry");
						String updtStr					= "delete from tbl_offnet_retry where refid = '"+refid+"' and circle = '"+circle+"' and msisdn = '"+msisdn+"' and contentid = '"+contentid+"'";
						int cntr_updt					= stmtUpd.executeUpdate(updtStr);
						logger.debug(threadName+updtStr+" = "+cntr_updt);
						rowCtr++;
						try{Thread.sleep(50);}catch(Exception exp){}
					}
				}catch(Exception exp){logException(threadName,exp);}
			}
			else{}
		}catch(Exception exp){logException(threadName,exp);}
		finally
		{
			if(connSel != null){connMgr.freeConnection(dbpool_hsql,connSel);}
			if(connUpd != null){connMgr.freeConnection(dbpool_hsql,connUpd);}
			if(connIns != null){connMgr.freeConnection(dbpool_hsql,connIns);}
		}
		return rowCtr;
	}

	private String generateTime()
	{
		java.util.Date date 						= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT			= new SimpleDateFormat("HH");
		String logDate								= SQL_DATE_FORMAT.format(date.getTime());
		return logDate;
	}

	private String retryRequest(final String threadName,final String circle,final String msisdn,final String contentId,final String mode,final String subinfo)
	{
		String retStr				= "";

		String res_id="",result="fail",ret_url="900|fail|Unknown Error",content_partner="A7",content_gid="",contentid = contentId,contentDesc="",price="0.00",srvKey="",contentType="",fallback_list="",succ_msg=null,offnet_user = "SPICEDIGITAL_01",offnet_password="U1BJQ0VESUdJVEFMQDEy",sub_info=subinfo;
		int days = 0,service_validDays=0,service_retryCount=0,service_retryDays=0;
		String remoteIp				= "127.0.0.1";
		String str_req				= circle+" - "+msisdn+" - "+contentid+" - "+mode+" - "+sub_info;
		try
		{
			logger.trace(threadName+str_req);
			if(isNumeric(msisdn))
			{
				String proc_res				= checkServiceInfo(threadName,remoteIp,circle,msisdn,contentid,mode);			//1|success|content_gid,contentType,contentDesc,price,srvKey,service_validDays,service_retryCount,service_retryDays
				String[] response			= proc_res.split("[|]");
				final String req_id			= response[1];
				result						= response[2];
				String[] proc_response		= response[3].split(",");
				try{content_gid				= proc_response[0];}catch(Exception exp){content_gid	= "error";}
				try{contentType				= proc_response[1];}catch(Exception exp){contentType	= "error";}
				try{contentDesc				= proc_response[2];}catch(Exception exp){contentDesc	= "error";}
				try{price					= proc_response[3];}catch(Exception exp){price			= "0";}
				try{srvKey					= proc_response[4];}catch(Exception exp){srvKey			= "error";}
				try{service_validDays		= Integer.parseInt(proc_response[5]);}catch(Exception exp){service_validDays	= 1;}
				try{fallback_list			= proc_response[6];}catch(Exception exp){fallback_list	= price;}
				try{content_partner			= proc_response[7];}catch(Exception exp){content_partner= "A7";}
				try{succ_msg				= proc_response[8];}catch(Exception exp){succ_msg		= null;}

				//if(content_partner.equalsIgnoreCase("A7"))		{offnet_user = "SPICEDIGITAL_01";	offnet_password = "U1BJQ0VESUdJVEFMQDEy";}
				//else if(content_partner.equalsIgnoreCase("F5"))	{offnet_user = "DIVINEVISTAAS_01";	offnet_password = "bWFubmF0QGY1";}
				//else											{offnet_user = "SPICEDIGITAL_01";	offnet_password = "U1BJQ0VESUdJVEFMQDEy";}

				  if(content_partner.equalsIgnoreCase("A7"))		{offnet_user = "SPICEDIGITAL_01";	offnet_password = "IxBcNl9m0LYnG/j4+ZTwAA==";}
				  else if(content_partner.equalsIgnoreCase("F5"))	{offnet_user = "DIVINEVISTAAS_01";	offnet_password = "08vv1q+g6XpPO7cGM0Ahjg==";}
				  else											{offnet_user = "SPICEDIGITAL_01";	offnet_password = "IxBcNl9m0LYnG/j4+ZTwAA==";}



				String content_id			= contentid;
				String price_local			= price;
				if(response[0].equalsIgnoreCase("1"))
				{
					logger.info(threadName+"start of the offnet check sub info, fallback info = "+fallback_list+" for content_id = "+content_id);
					String sub_flag					= checkSubInfo(threadName,circle,msisdn,content_gid,srvKey,sub_info);
					logger.trace(threadName+"checkSubInfo('"+circle+"','"+msisdn+"','"+content_gid+"','"+srvKey+"','"+sub_info+") = "+sub_flag);
					if(sub_flag.startsWith("0"))
					{
						String[] arr_fallback				= fallback_list.split("-");
						for(int i=0;i<arr_fallback.length;i++)
						{
							String[] fallback_detail		= arr_fallback[i].split(":");
							content_id						= contentid.substring(0,3);
							price_local						= String.format("%2s",fallback_detail[0]).replace(' ', '0');
							String validity	 				= String.format("%2s",fallback_detail[1]).replace(' ', '0');
							try{service_validDays			= Integer.parseInt(proc_response[5]);}catch(Exception exp){service_validDays	= 1;}
							content_id						= content_id+price_local+validity;

							if(content_id.length()>7)		{content_id = content_id.substring(0,7);}
							if(contentDesc.length()>25)		{contentDesc= contentDesc.substring(0,25);}

							price_local						= price_local+"00";
							logger.info(threadName+"msisdn:"+msisdn+", content_id:"+content_id+", price:"+price_local+", validity: "+validity+", service_validDays: "+service_validDays+", contentDesc: "+contentDesc);

							final String str_url			= "https://172.30.196.127:7777/offnet/offnet?msisdn=+"+msisdn+"&cpid="+content_partner+"&aggregatorid=3&contenttype="+contentType+"&contentid="+content_id+"&contentdesc="+URLEncoder.encode(contentDesc, "ISO-8859-1")+"&copyrightid=0&smskeyword=&eup="+price_local+"&contenturl=&accounttype=1&devicemodel=&contentsize=240&contentMimeType=&cptransid="+req_id+"&category=&producttype=SONG&Userid="+offnet_user+"&Password="+offnet_password+"";
							
							result							= callHttps(str_url).toLowerCase();
							logger.debug(threadName+str_url+" : "+result);
							try{res_id 						= result.split(",")[1];}catch(Exception exp){}
							if(result.indexOf("success")>0)
							{
								try{service_validDays		= Integer.parseInt(validity);}catch(Exception exp){service_validDays	= 1;}
								ret_url 					= "200|success|Charging request for "+contentDesc+" successfull. Amount deducted Rs. "+String.valueOf(Integer.parseInt(price_local)/100);
								result 						= "success";
								contentid 					= content_id;
								sub_info					= "act";
								break;
							}
							else if(result.indexOf("err_allocation_failed")>0)	{ret_url = "901|fail|Charging request for "+contentDesc+" failed due to low balance.";result = "fail";}
							else if(result.indexOf("invalid_subscriber")>0)		{ret_url = "904|fail|Subscriber not present.";result = "fail";}
							else 												{ret_url = "900|fail|Unknown System Error. Please try again later.";result = "fail";}
							if(result.equalsIgnoreCase("success"))	logger.info(threadName+"Charging successfull for "+msisdn+" for pricepoint "+price_local+" and content_id "+content_id+" fallback_detail[0] = "+fallback_detail[0]);
							else									logger.info(threadName+"Charging not successfull for "+msisdn+" for pricepoint "+price_local+" and content_id "+content_id+" fallback_detail[0] = "+fallback_detail[0]);
						}
					}
					else
					{
						try{price_local				= String.valueOf(Integer.parseInt(price_local)/100);}catch(Exception exp){price_local="0.0";}
						if(sub_flag.indexOf("RetryActive")>=0)				{ret_url = "902|fail|Charging Request is already in Queue for "+contentDesc+" @ Rs. "+price_local;}
						else if(sub_flag.indexOf("ActiveActive")>=0)		{ret_url = "903|fail|Charging already done for "+contentDesc;}
						else if(sub_flag.indexOf("GraceActive")>=0)			{ret_url = "903|fail|Charging already done for "+contentDesc;}
						else if(sub_flag.indexOf("CallbackActive")>=0)		{ret_url = "906|fail|Number pending in charging queue "+contentDesc;}
						else 												{ret_url = "900|fail|Unknown System Error. Please try again later.";}
						result = "fail";
					}
				}
				else if(response[0].equalsIgnoreCase("0"))
				{
					logger.trace(threadName+"User charging information cannot be fetched from the database. Sending DCT command");
					ret_url = "999|fail|Not a valid charging request.";
				}
				else if(response[0].equalsIgnoreCase("909"))
				{
					ret_url = "909|fail|IP Authentication Error.";
				}
				else{ret_url  = proc_res;}
				updateRecord(threadName,circle,remoteIp,msisdn,srvKey,content_gid,contentid,contentDesc,contentType,service_validDays,price_local,req_id,res_id,result,ret_url,mode,sub_info);
			}else ret_url = "905|fail|Invalid MSISDN. Not a Number";
		}catch(Exception exp){logException(threadName,exp);ret_url = "900|fail|Unknown System Error. Please try again later.";}
		return ret_url;
	}

	public boolean isNumeric(String str) {return str.matches("[-+]?\\d*\\.?\\d+");}

	private String checkServiceInfo(final String threadName,final String remoteIp,final String circle,final String msisdn,final String content_id,final String mode)
	{
		String retStr					= "0|fail|na";
		final Connection conn			= connMgr.getConnection(dbpool_mysql);
		try
		{
			CallableStatement cstmt		= null;
			try
			{
				//refid,msisdn,content_gid,content_id,content_desc,price,validDays,srvkey,content_type
				cstmt 	= conn.prepareCall("{call proc_OffnetCheck(?,?,?,?,?,?)}");
				cstmt.setString(1,remoteIp);
				cstmt.setString(2,circle);
				cstmt.setString(3,msisdn);
				cstmt.setString(4,content_id);
				cstmt.setString(5,mode);
				cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);
				cstmt.execute();
				retStr	= cstmt.getString(6);
				logger.info(threadName+"{call proc_OffnetCheck('"+remoteIp+"','"+circle+"','"+msisdn+"','"+content_id+"','"+mode+"',@res)} = "+retStr);
			}catch(Exception exp){logger.error(threadName,exp);}
			finally{try{cstmt.close();cstmt = null;}catch(Exception exp){logger.error(threadName,exp);}}
		}catch(Exception exp){}
		finally{connMgr.freeConnection(dbpool_mysql,conn);}
		return retStr;
	}

	private String checkSubInfo(final String threadName,final String circle,final String msisdn,final String srv_gid,final String srvKey,final String sub_info)
	{
		String retStr			= "";
		try
		{
			final Connection conn	= connMgr.getConnection(dbpool_mssql);
			try
			{
				if(conn != null)
				{
					CallableStatement cstmt		= null;
					try
					{
						cstmt 	= conn.prepareCall("{call sdpCentral_SS.dbo.proc_checkSubsciberStatus(?,?,?,?,?,?)}");
						cstmt.setString(1,circle);
						cstmt.setString(2,srv_gid);
						cstmt.setString(3,srvKey);
						cstmt.setString(4,msisdn);
						cstmt.setString(5,sub_info);
						cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);
						cstmt.execute();
						retStr	= cstmt.getString(6);
						logger.info(threadName+"{call sdpCentral_SS.dbo.proc_checkSubsciberStatus('"+circle+"','"+srv_gid+"','"+srvKey+"','"+msisdn+"','"+sub_info+"',@res)} = "+retStr);
					}catch(Exception exp){logger.error(threadName,exp);}
					finally{try{cstmt.close();cstmt = null;}catch(Exception exp){logger.error(threadName,exp);}}
				}else{logger.info(threadName+"Cannot connect to the MSSQL "+dbpool_mssql+" Database...");}
			}catch(Exception exp){logger.error(threadName,exp);}
			finally{connMgr.freeConnection(dbpool_mssql,conn);}
		}catch(Exception exp){logger.error(threadName,exp);}
		return retStr;
	}

	private String updateRecord(final String threadName,final String circle,final String remoteIp,final String msisdn,final String srvKey,final String content_gid,final String content_id,final String contentDesc,final String contentType,final int days,final String price,final String req_id,final String res_id,final String result,final String res_desc,final String mode,final String sub_info)
	{
		String retStr			= "nok",res="fail";
		String resid			= res_id;
		String[] arr_result		= result.split(",");
		String[] arr_url		= res_desc.split("[|]");
		final String err_code	= arr_url[0].trim();
		try{if(arr_url.length==2)res= arr_url[1];}catch(Exception exp){/*TODO: Handle the exception in future*/}
		Connection conn_offnet	= connMgr.getConnection(dbpool_mysql);
		Connection conn_ins_sdp	= connMgr.getConnection(dbpool_mssql);
		float price_flt			= 0;
		try{price_flt			= Integer.parseInt(price)/100;}catch(Exception exp){logException(threadName,exp);price_flt=0;}
		if(arr_result.length > 0)
		{
			//try{resid = arr_result[1];}catch(Exception exp){}
			try{if(arr_result.length > 1)res = arr_result[2];}catch(Exception exp){}
		}
		if(result.indexOf("success")>=0) res="success";

		logger.info(threadName+"err_code: "+err_code+", Result: "+result+", Length: "+arr_result.length+", resid = "+resid+", res "+res+", price: "+price_flt+"("+price+")");

		try
		{
			String sdpStatus		= "act";
			String sdp_retStr		= "bal-low";
			if(err_code.equalsIgnoreCase("200"))		{sdpStatus = "act";      sdp_retStr="success";}
			else if(err_code.equalsIgnoreCase("903"))	{sdpStatus = "act";      price_flt=0;sdp_retStr="success";}
			else if(err_code.equalsIgnoreCase("999"))	{sdpStatus = "dct";      price_flt=0;sdp_retStr="success";}
			else if(err_code.equalsIgnoreCase("906"))	{sdpStatus = "act";      price_flt=0;sdp_retStr="success";}
			else 										{sdpStatus = "act_retry";price_flt=0;sdp_retStr="bal-low";}
			if(conn_ins_sdp != null && err_code.equalsIgnoreCase("200"))
			{
				Statement stm_ins_sdp	= conn_ins_sdp.createStatement();
				String str_ins			= "insert into sdpCentral_ss.dbo.tbl_callback(callbackInsertDateTime,RefId, retry_num, circle, msisdn, prepost, srvkey, mode, price, sdpStatus, status, action, precharge, startDateTime, endDateTime, originator, cp_user, cp_password, info) values(getdate(),'"+req_id+"',0,'"+circle+"','"+ msisdn+"','U','"+ srvKey+"','"+ mode+"','"+ price_flt+"','"+sdp_retStr+"',0,'"+ sdpStatus+"','N',getDate(),getdate()+"+days+",'"+content_gid+"','spice','spice_internal','"+content_id+"')";
				int counter				= stm_ins_sdp.executeUpdate(str_ins);
				logger.trace(threadName+str_ins+" : "+counter);
			}

			if(conn_offnet != null)
			{
				try
				{
					CallableStatement cstmt		= null;
					try
					{
						//refid,msisdn,content_gid,content_id,content_desc,price,validDays,srvkey,content_type
						cstmt 	= conn_offnet.prepareCall("{call proc_OffnetResponse(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
						cstmt.setString(1,req_id);
						cstmt.setString(2,resid);
						cstmt.setString(3,remoteIp);
						cstmt.setString(4,circle);
						cstmt.setString(5,msisdn);
						cstmt.setString(6,content_gid);
						cstmt.setString(7,content_id);
						cstmt.setString(8,contentDesc);
						cstmt.setString(9,contentType);
						cstmt.setFloat(10,price_flt);
						cstmt.setString(11,srvKey);
						cstmt.setInt(12,days);
						cstmt.setString(13,res);
						cstmt.setString(14,res_desc);
						cstmt.setString(15,mode);
						cstmt.setString(16,sdpStatus);
						cstmt.setString(17,sdp_retStr);
						cstmt.registerOutParameter(18, java.sql.Types.VARCHAR);
						cstmt.execute();
						retStr	= cstmt.getString(18);
						logger.info(threadName+"{call proc_OffnetResponse('"+req_id+"','"+resid+"','"+remoteIp+"','"+circle+"','"+msisdn+"','"+content_gid+"','"+content_id+"','"+contentDesc+"','"+contentType+"','"+price+"','"+srvKey+"','"+days+"','"+result+"','"+res_desc+"','"+mode+"','"+sdpStatus+"','"+sdp_retStr+"',@res)} = "+retStr);
					}catch(Exception exp){logger.info(threadName+"{call proc_OffnetRecord('"+req_id+"','"+resid+"','"+remoteIp+"','"+circle+"','"+msisdn+"','"+content_gid+"','"+content_id+"','"+contentDesc+"','"+contentType+"','"+price+"','"+srvKey+"','"+days+"','"+result+"','"+res_desc+"','"+mode+"','"+sdpStatus+"','"+sdp_retStr+"',@res)} = error");logException(threadName,exp);}
					finally{try{cstmt.close();cstmt = null;}catch(Exception exp){logger.error(threadName,exp);}}
				}catch(Exception exp){logger.error(threadName,exp);}
			}else{logger.error(threadName+"Cannot connect to the Local "+dbpool_mysql+" Database...");}
		}catch(Exception exp){logger.error("OffnetError: ",exp);}
		finally
		{
			if(conn_offnet != null){connMgr.freeConnection(dbpool_mysql,conn_offnet);retStr	= "ok";}
			if(conn_ins_sdp != null){connMgr.freeConnection(dbpool_mssql,conn_ins_sdp);retStr	= "ok";}
		}
		return retStr;
	}

 	private String callHttp(final String threadName,final String url)
 	{
 		String retStr 								= "";
 		String header 								= "";
 		try
 		{
 			URL Url 								= new URL(url);
 			URLConnection conn 						= Url.openConnection();
 			conn.setReadTimeout(1000*60);
 			header 									= String.valueOf(conn.getHeaderFields());
 			if(header.indexOf("200 OK")>0) header = "[HTTP/1.1 200 OK]"; else retStr = header;
 			String str 								= null;
 			BufferedReader in 						= new BufferedReader(new InputStreamReader(conn.getInputStream()));
 			while ((str = in.readLine()) != null) retStr += str;
 			in.close();
 			if(retStr.length()<2) if(header.equalsIgnoreCase("[HTTP/1.1 200 OK]")){retStr="success";}
 		}catch(Exception exp){retStr="error";logger.error(threadName+url);logException(threadName,exp);}
 		return retStr;
 	}

 	private String callHttps(final String url)
 	{
 		String retStr 								= "";
 		String header 								= "";
 		try
 		{
			HostnameVerifier hv 					= new HostnameVerifier(){public boolean verify(String urlHostName, SSLSession session){return true;}};
			HttpsURLConnection.setDefaultHostnameVerifier(hv);

 			URL Url 								= new URL(url);
 			URLConnection conn 						= Url.openConnection();
 			conn.setReadTimeout(1000*20);
 			header 									= String.valueOf(conn.getHeaderFields());
 			//logger.debug("["+threadName+"] ->(sdp   :"+circle+") responseHeader: "+header);
 			if(header.indexOf("200 OK")>0) header 	= "[HTTP/1.1 200 OK]"; else retStr = header;
 			String str 								= null;
 			BufferedReader in 						= new BufferedReader(new InputStreamReader(conn.getInputStream()));
 			while ((str = in.readLine()) != null) retStr += str;
 			in.close();
 			//if(retStr.length()<2) if(header.equalsIgnoreCase("[HTTP/1.1 200 OK]")){retStr="success";logger.debug(retStr);}
 		}catch(Exception exp){retStr="error,error,fail";logger.error("OffnetError: ",exp);}
 		//logger.trace(url+" :"+retStr);
 		return retStr;
 	}
	private String getId()
	{
		String retStr	= "";
		UUID uid		= UUID.randomUUID();
		retStr			= String.valueOf(uid);
		return retStr;
	}

	private void logException(final String threadName,Exception exp)
	{
		logger.error(threadName+getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}
	private static void trustAllHttpsCertificates() throws Exception
	{
		javax.net.ssl.TrustManager[] trustAllCerts	= new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm 				= new miTM();

		trustAllCerts[0] 							= tm;
		javax.net.ssl.SSLContext sc 				= javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	}

	public static class miTM implements javax.net.ssl.TrustManager,javax.net.ssl.X509TrustManager
	{
		public java.security.cert.X509Certificate[] getAcceptedIssuers()			{return null;}
		public boolean isServerTrusted(java.security.cert.X509Certificate[] certs)	{return true;}
		public boolean isClientTrusted(java.security.cert.X509Certificate[] certs)	{return true;}
		public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)	throws java.security.cert.CertificateException{return;}
		public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) throws java.security.cert.CertificateException{return;}
	}
}