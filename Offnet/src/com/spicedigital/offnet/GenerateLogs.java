package com.spicedigital.offnet;

import java.io.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.Date;
import org.apache.log4j.*;

public class GenerateLogs implements Runnable
{
	private static Logger logger 		= Logger.getLogger(GenerateLogs.class.getName());
	private DBConnectionManager connMgr	= null;
	private String dbpool_hsql			= "hsql";
	private String dateLogger			= "";

	private FileWriter fstream			= null;//new FileWriter("vasLogs/"+dateLogger+"/vasRecords_"+dateLogger+".txt",true);
	private BufferedWriter out 			= null;//new BufferedWriter(fstream);

	public GenerateLogs()
	{
		try{PropertyConfigurator.configure("log4j.properties");}catch(Exception exp){}
		try{connMgr 	= DBConnectionManager.getInstance();}catch(Exception exp){logException("",exp);}
		Thread t 		= new Thread(this); t.start();
	}

	public void run()
	{
		final String threadName			= String.format("[%-10s] ->(log :al) ",Thread.currentThread().getName());

		int rowCtr						= 0;
		int prntCtr						= 1;
		int sleepTimer					= 0;
		boolean retbool					= checkFile(threadName);
		while(true)
		{
			if(checkFile(threadName))
			{
				rowCtr	= processRequest(threadName);try{out.flush();}catch(Exception exp){}
				if(rowCtr>0) sleepTimer = 50; else sleepTimer = 5000;
				if(prntCtr%25 == 0)
				{
					logger.debug(threadName+"Total records processed "+rowCtr+". Sleeping for "+sleepTimer+" msecs.");
					prntCtr		= 0;
				}
				try{Thread.sleep(sleepTimer);}catch(Exception exp){}
				prntCtr++;
			}
			else
			{
				logger.error(threadName+"Cannot create new file for cdr writing kindly check the issue.");
				//try{Thread.sleep(1000*60*5);}catch(Exception exp){}
			}
		}
	}

	private boolean checkFile(final String threadName)
	{
		boolean retbool					= false;
		if(dateLogger.equalsIgnoreCase(generateDate()))
		{
			retbool	= true;
		}
		else
		{
			dateLogger					= generateDate();

			try
			{

				if(fstream!=null)	fstream.close();

				boolean success			= (new File("cdr/"+generateMonth())).mkdirs();
				if(success)	{}
				fstream				= new FileWriter("cdr/"+generateMonth()+"/ussd_"+dateLogger+".txt",true);
				out					= new BufferedWriter(fstream);
				retbool				= true;

			}catch(Exception exp){logException(threadName,exp);}
		}
		return retbool;
	}

	private int processRequest(final String threadName)
	{
		int rowCtr 									= 0;

		Connection connSel 							= connMgr.getConnection(dbpool_hsql);
		Connection connUpd 							= connMgr.getConnection(dbpool_hsql);
		try
		{
			if(connSel != null && connUpd != null)
			{
				try
				{
					Statement stmtSel					= connSel.createStatement();
					Statement stmtUpd					= connUpd.createStatement();
					String strSel						= "select dlg_id,circle,short_code,response,out_level,out_menu,continue_flag,data_coding,msisdn,status,to_char(date_time,'YYYYMMDD HH24:MI:SS') date_time from tbl_ussd_mis where log_status = 0 order by date_time asc limit 100";
					ResultSet rs						= stmtSel.executeQuery(strSel);

					while(rs.next())
					{
						String dlg_id		= rs.getString("dlg_id");
						String circle		= rs.getString("circle");
						String short_code	= rs.getString("short_code");
						String response		= rs.getString("response");
						String out_level	= rs.getString("out_level");
						String out_menu		= rs.getString("out_menu");
						String data_coding	= rs.getString("data_coding");
						String msisdn		= rs.getString("msisdn");
						String status		= rs.getString("status");
						String date_time	= rs.getString("date_time");

						String logstr					= String.format("%-5s|%-16s|%-3s|%-12s|%-15s|%-2s|%-4s|%-150s|",dlg_id,date_time,circle,msisdn,short_code,out_level,out_menu,response);
						logger.debug(threadName+"logstr = "+logstr);
						boolean retbool					= writefile(threadName,logstr);
						if(retbool)
						{
							String strdel				= "delete from tbl_ussd_mis where dlg_id = '"+dlg_id+"' and msisdn = '"+msisdn+"'";
							logger.debug(threadName+"del = "+stmtUpd.executeUpdate(strdel));
						}
						try{Thread.sleep(10);}catch(Exception exp){}
					}
				}catch(Exception exp){logException(threadName,exp);}
			}
			else{}
		}catch(Exception exp){logException(threadName,exp);}
		finally
		{
			if(connSel != null){connMgr.freeConnection(dbpool_hsql,connSel);}
			if(connUpd != null){connMgr.freeConnection(dbpool_hsql,connUpd);}
		}
		return rowCtr;
	}

	private boolean writefile(final String threadName,final String logstr)
	{
		boolean retbool		= false;
		try{out.write(logstr);out.newLine();retbool=true;}catch(Exception exp){logException(threadName,exp);}
		return retbool;
	}



	private String generateDate()
	{
		java.util.Date date 				= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("yyyyMMdd");
		return SQL_DATE_FORMAT.format(date.getTime());
	}

	private String generateMonth()
	{
		java.util.Date date 				= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("MMM");
		return SQL_DATE_FORMAT.format(date.getTime());
	}

	private void logException(final String threadName,Exception exp)
	{
		logger.error(threadName+getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}
}