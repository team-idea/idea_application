package com.spicedigital.offnet;

import java.nio.channels.*;
import java.io.*;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.*;
import java.util.*;
import java.util.Date;
import org.apache.log4j.*;

public class Offnet
{
	private static Logger logger 				= Logger.getLogger(Offnet.class.getName());
	private static String instance				= "";
	private String circles						= "xx";
	private static int dbChkTmr					= 60;
	private int threads							= 0;

	public Offnet()
	{
		try{PropertyConfigurator.configure("log4j.properties");}catch(Exception exp){}
		try
		{
			Properties prop 					= new Properties();
			prop.load(new DataInputStream(new FileInputStream("offnet.properties")));
			instance							= prop.getProperty("offnet.application.instance");
			circles								= prop.getProperty("offnet.CallbackSync.circles");
			try{dbChkTmr						= Integer.parseInt(prop.getProperty("offnet.application.dbChkTmr"));}catch(Exception exp){dbChkTmr=60;}
			try{threads							= Integer.parseInt(prop.getProperty("offnet.application.processthreads"));}catch(Exception exp){threads=0;}
			logger.debug("[Thread-main]<-(main  : al) Starting the Spice Offnet Retry Application ["+instance+"]. In case of issue contact central technical team @9218514140.");

			if(accessFile())
			{
				try
				{
					RandomAccessFile file 			= new RandomAccessFile("c:/windows/system/offnet_Spice_"+instance+".tmp", "rw");
					FileChannel fileChannel 		= file.getChannel();
					FileLock fileLock 				= fileChannel.tryLock();
					if (fileLock != null)
					{
						logger.debug("File is locked. Starting the execution of the Application");
						java.util.Timer timer		= new java.util.Timer();
						DBCheckPoint dbChk			= new DBCheckPoint(timer);
						timer.schedule(dbChk,1000,(1000*dbChkTmr));//the time specified in millisecond.
						//GenerateLogs logs			= new GenerateLogs();
						OffNetDeactivate offnet_dct	= new OffNetDeactivate();
						FetchRetryRecords ftch_rec	= new FetchRetryRecords(threads);
						for(int i=0;i<=threads;i++) {OffNetRetry offnet_retry	= new OffNetRetry(i);Thread.sleep(25);}
						//String[] circle				= circles.split(",");
						//for(int i=0;i<circle.length;i++){CallbackSync sync		= new CallbackSync(circle[i]);Thread.sleep(25);}
					}
					else
					{
						logger.error("Cannot create lock. Already another instance of the program is running.");
						logger.error("System shutting down. Please wait");
						try{Thread.sleep(1000*30);}catch(Exception exp){}
						System.exit(0);
					}
				}
				catch (Exception exp) {logException(exp);}
			}
			else
			{
				logger.error("Cannot create lock. Already another instance of the program is running.");
				logger.error("System shutting down. Please wait");
				try{Thread.sleep(1000*30);}catch(Exception exp){}
				System.exit(0);
			}
		}catch (Exception exp) {logException(exp);}
	}


	private boolean accessFile()
	{
		boolean retCode					= false;
		try
		{
			String line 				= "";
			try {BufferedWriter bw		= new BufferedWriter(new FileWriter("c:/windows/system/offnet_Spice_"+instance+".tmp",false));bw.close();}
			catch (Exception exp) {}
			retCode						= true;
		}
		catch (Exception e) {System.out.println(e);}
		logger.debug("accessFile() = "+retCode);
		return retCode;
	}



	public static void main(String argv[]){Offnet loan					= new Offnet();}

	private void logException(Exception exp){logger.error(String.format("[%-10s] ->(log :al) ",Thread.currentThread().getName())+getStackTrace(exp));}
	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}

}