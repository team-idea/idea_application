package com.spicedigital.offnet;

import java.nio.channels.*;
import java.io.*;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.zip.*;
import org.apache.log4j.*;

public class ManageAlerts implements Runnable
{
	private static Logger logger 							= Logger.getLogger( ManageAlerts.class.getName() );
	private static int clients								= 0;
	private static int thread_sleeptime						= 5;
	private static int sleep_timer_noselect					= 5;
	private static int sleep_timer_select					= 5;

	private static int MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_1	= 50;
	private static int MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_2	= 50;
	private static int MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_3	= 50;
	private static int MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_4	= 50;
	private static int CONNECTION_MAX_THRESHOLD_ERROR_LEVEL	= 5;
	private static int EXCEPTION_MAX_THRESHOLD_ERROR_LEVEL	= 5;


	private static String machine_name						= "sdp";
	private static String instance_id						= "db";


	private static ManageAlerts instance					= null;				       		// The single instance

	static private Vector<String> MyDealalertVector			= new Vector<String>();
	static private Vector<String> ConnectionAlertVector		= new Vector<String>();
	static private Vector<String> ExceptionAlertVector		= new Vector<String>();

	private DBConnectionManager connMgr						= null;
	private String dbpool_smsc								= "smsc";
	private String dbpool_mysql								= "mysql";
	private String dbpool_mysql_local						= "mysql_local";
	private String dbpool_hsql								= "hsql";

	static private String alert_msisdn_list_level_1			= "918198831804";
	static private String alert_msisdn_list_level_2			= "918198831804";
	static private String alert_msisdn_list_level_3			= "918198831804";
	static private String alert_msisdn_list_level_4			= "918198831804";
	static private String fromList_ExceptionAlerts			= "";
	static private String toList_ExceptionAlerts			= "";
	static private String ccList_ExceptionAlerts			= "";
	static private String bccList_ExceptionAlerts			= "";
	static private String attachment_ExceptionAlerts		= "";


	static synchronized public ManageAlerts getInstance()
	{
		if (instance == null){instance = new ManageAlerts();}
		clients++;
		return instance;
	}

	public ManageAlerts()
	{
		try{PropertyConfigurator.configure("log4j.properties");}catch(Exception exp){}
		try{connMgr= DBConnectionManager.getInstance();}catch(Exception exp){logger.error("",exp);}
		try{boolean retBool=sendInstaSms("","USSD-App",alert_msisdn_list_level_1,"USSD application have restarted on 172.27.32.204 at "+generateDateTime());}catch(Exception exp){}
		try
		{
			Properties prop=new Properties();
			prop.load(new DataInputStream(new FileInputStream("alert.properties")));
			thread_sleeptime=Integer.parseInt(prop.getProperty("ussd.alert.sleeptime.select"));
			machine_name=prop.getProperty("alert.machine.name");
			instance_id=prop.getProperty("alert.application.instance");

			try{sleep_timer_noselect= Integer.parseInt(prop.getProperty("alert.noselect.log.print"));}catch(Exception exp){sleep_timer_noselect = 5;}
			try{sleep_timer_select= Integer.parseInt(prop.getProperty("alert.select.log.print"));}catch(Exception exp){sleep_timer_select = 5;}

			alert_msisdn_list_level_1=prop.getProperty("alert.msisdn.list.level.1");
			alert_msisdn_list_level_2=prop.getProperty("alert.msisdn.list.level.2");
			alert_msisdn_list_level_3=prop.getProperty("alert.msisdn.list.level.3");
			alert_msisdn_list_level_4=prop.getProperty("alert.msisdn.list.level.4");

			try{MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_1= Integer.parseInt(prop.getProperty("alert.max.myDealError.threshold.level.1"));}catch(Exception exp){MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_1 = 5;}
			try{MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_2= Integer.parseInt(prop.getProperty("alert.max.myDealError.threshold.level.2"));}catch(Exception exp){MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_2 = 25;}
			try{MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_3= Integer.parseInt(prop.getProperty("alert.max.myDealError.threshold.level.3"));}catch(Exception exp){MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_3 = 45;}
			try{MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_4= Integer.parseInt(prop.getProperty("alert.max.myDealError.threshold.level.4"));}catch(Exception exp){MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_4 = 65;}

			try{CONNECTION_MAX_THRESHOLD_ERROR_LEVEL= Integer.parseInt(prop.getProperty("alert.max.connectionError.threshold"));}catch(Exception exp){CONNECTION_MAX_THRESHOLD_ERROR_LEVEL = 5;}
			try{EXCEPTION_MAX_THRESHOLD_ERROR_LEVEL	= Integer.parseInt(prop.getProperty("alert.max.exceptionError.threshold"));}catch(Exception exp){EXCEPTION_MAX_THRESHOLD_ERROR_LEVEL = 5;}

			fromList_ExceptionAlerts=prop.getProperty("alert.mail.from");
			toList_ExceptionAlerts=prop.getProperty("alert.mail.to");
			ccList_ExceptionAlerts=prop.getProperty("alert.mail.cc");
			bccList_ExceptionAlerts=prop.getProperty("alert.mail.bcc");
			Thread t = new Thread(this);	t.start();
		}
		catch(Exception exp){exp.printStackTrace();}
	}

	public void run()
	{
		final String threadName				= String.format("[%-10s] ->(alrt:al) ",Thread.currentThread().getName());
		try
		{
			int rowCtr						= 0;
			int sleepTimer					= 50;
			int prntCtr						= 0;
			while(true)
			{
				try
				{
					if(rowCtr>0){sleepTimer	= sleep_timer_select;}else sleepTimer	= sleep_timer_noselect;
					if(prntCtr%250 == 0)
					{
						logger.debug(threadName+"Total records processed "+rowCtr+". Sleeping for "+sleepTimer+" msecs. prntCtr = "+prntCtr+" prntCtr%250 = "+prntCtr%250);
						prntCtr			= 0;
					}
					try{Thread.sleep(sleepTimer);}catch(Exception exp){}
				}catch(Exception exp){logger.error(threadName,exp);try{Thread.sleep(1000*5);}catch(Exception expTmr){}}
				prntCtr++;
			}
		}catch(Exception exp){logger.error(threadName,exp);}
	}

	private int processMyDealAlert(final String threadName)
	{
		int retCode						= 0;

		logger.debug(threadName+"MyDealalertVector.size() = "+MyDealalertVector.size());
		if(MyDealalertVector.size() > 0)
		{
			try
			{
				Vector<String> MyDealalertVectorLocal 		= (Vector) MyDealalertVector.clone();
				logger.debug(threadName+"MyDealalertVectorLocal.size() = "+MyDealalertVectorLocal.size()+"MyDealalertVector.size() = "+MyDealalertVector.size());
				String error_string = "";
				for(int i=0;i<MyDealalertVectorLocal.size();i++){error_string+=String.valueOf(MyDealalertVectorLocal.get(i))+"";}
				logger.debug(threadName+""+error_string);
				if(MyDealalertVectorLocal.size()>=MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_1)
				{
					String messageSubject			= "My Deal Esception Alerts:\nException count reached at "+MyDealalertVectorLocal.size()+".\nPlease take relevant action.";
					String messageBody				= error_string;
					String attachment				= "";
					boolean retBool					= sendInstaSms(threadName,"MyDeal-Error",alert_msisdn_list_level_1,messageSubject);
					if(MyDealalertVectorLocal.size()>=MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_2)
					{
						retBool						= sendInstaSms(threadName,"MyDeal-Error",alert_msisdn_list_level_2,messageSubject);
						if(MyDealalertVectorLocal.size()>=MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_3)
						{
							retBool					= sendInstaSms(threadName,"MyDeal-Error",alert_msisdn_list_level_3,messageSubject);
							if(MyDealalertVectorLocal.size()>=MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_4)
							{
								retBool				= sendInstaSms(threadName,"MyDeal-Error",alert_msisdn_list_level_4,messageSubject);
								setMyDealError("","clear");
							}
						}
					}
					//boolean mailRetCode				= mail.sendMail(toList_ExceptionAlerts, ccList_ExceptionAlerts, bccList_ExceptionAlerts, fromList_ExceptionAlerts, messageSubject, messageBody, attachment);
					logger.debug(threadName+"Value from MyDealalertVector >= MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_1 ("+MYDEAL_MAX_THRESHOLD_ERROR_LEVEL_1+") mail.sendMail() = ");
				}else logger.debug(threadName+"No Threshold value exceeded for sending alerts. Current value is: "+MyDealalertVectorLocal.size());
				MyDealalertVectorLocal.clear();
				Thread.sleep(5);
				retCode++;
			}catch(Exception exp){logger.error(exp);}

		}else {retCode=0;}
		return retCode;
	}

	public int setMyDealError(final String expception,final String flag)
	{
		int retCode						= 0;
		synchronized(this)
		{
			if(flag.equalsIgnoreCase("add"))	try{MyDealalertVector.add(generateDateTime()+" "+expception+"\n");	}catch(Exception exp){retCode = -1;}
			else								try{MyDealalertVector.clear();}catch(Exception exp){retCode = -1;}
		}
		return retCode;
	}

	private String zipLogs(final String threadName,final String sourceString,final String fileName)
	{
		String retStr					= null;
		ZipOutputStream out 			= null;

		try
		{
			String dateTime				= generateDateTime();
			retStr						= fileName+"_"+dateTime+"";
			logger.debug(threadName+"File Name generated = "+retStr);
			File outFolder				= new File(retStr+".zip");
			out							= new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(outFolder)));
			BufferedInputStream in 		= null;
			byte[] data    				= new byte[1000];

			byte buff[]					= sourceString.getBytes();
			in 							= new BufferedInputStream(new ByteArrayInputStream(buff));
			out.putNextEntry(new ZipEntry(retStr+".txt"));
			int count;
			while((count 				= in.read(data,0,1000)) != -1){out.write(data, 0, count);}
			out.closeEntry();
			in.close();
			out.flush();
			out.close();
		}catch(Exception exp){/*logger.error(exp);*/try{out.flush();out.close();}catch(Exception expI){/*logger.error(expI);*/}}
		return retStr;
	}
	private String generateDateTime()
	{
		java.util.Date date 				= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("yyyyMMdd_HHmmss");
		return SQL_DATE_FORMAT.format(date.getTime());
	}

	private boolean deleteFile(final String threadName,final String fullFileName)
	{
		boolean retBool 		= false;
		try
		{
			File tmp = new File(fullFileName);
			logger.debug(threadName+"tmp = "+tmp);
			logger.debug(threadName+"tmp.delete() = "+tmp.delete());
			retBool				= true;
		}catch(Exception exp){logger.error(threadName,exp);}
		return retBool;
	}
	private boolean sendInstaSms(final String threadName,final String cli,final String msisdn,final String message) throws Exception
	{
		boolean retbool				= false;
		final Connection conn_smsc	= connMgr.getConnection(dbpool_smsc);
		String[] arr_msisdn			= msisdn.split(",");
		for(int i=0;i<arr_msisdn.length;i++)
		{
			try
			{
				CallableStatement cstmt = conn_smsc.prepareCall("{call insert_mt(?,?,?,?,?,?,?)}");
				cstmt.setString(1,"admin");
				cstmt.setString(2,cli);
				cstmt.setString(3,arr_msisdn[i]);
				cstmt.setString(4,message);
				cstmt.setString(5,"text");
				cstmt.setString(6,"a");
				cstmt.registerOutParameter(7, java.sql.Types.VARCHAR);
				cstmt.execute();
				logger.debug(threadName+"{call insert_mt(admin,"+cli+","+msisdn+","+message+",text,a,@res)} = "+cstmt.getString(7));
				retbool = true;
			}catch(Exception exp){throw exp;}
			finally
			{
				if(conn_smsc != null){connMgr.freeConnection(dbpool_smsc,conn_smsc);}
			}
		}
		return retbool;
	}
}