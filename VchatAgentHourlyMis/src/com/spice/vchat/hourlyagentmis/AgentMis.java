package com.spice.vchat.hourlyagentmis;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class AgentMis {
	private static Logger logger = Logger.getLogger(AgentMis.class.getName());
	private static Properties prop = new Properties();
	private static int dateDiff = 0;
	public static int curr_hour = 0;

	static {
		Calendar calHour = Calendar.getInstance();
		curr_hour = calHour.get(Calendar.HOUR_OF_DAY);
		System.out.println("curr hour is [" + curr_hour + "]");
	}

	public AgentMis() {
		try {
			PropertyConfigurator.configure("./log4j.properties");
			prop.load(new DataInputStream(new FileInputStream("./config.properties")));
			dateDiff = Integer.parseInt(prop.getProperty("DateDiff"));
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public static void main(String[] args) {
		try {
			AgentMis agentMis = new AgentMis();
			if (agentMis.accessFile() && agentMis.getLock()) {
				logger.info("program is started..");

				if (curr_hour == 0) {
					curr_hour = 23;
					dateDiff = 1;
				} else {
					curr_hour = curr_hour - 1;
				}
				String mis_date = agentMis.getDate(dateDiff);
				GenerateExcel generateExcel = new GenerateExcel();
				generateExcel.getExcelProcess(mis_date);
				Zipper zipper = new Zipper();
				zipper.getZipper("./misData/!dea_VC_Hourly_Report_" + mis_date + "_" + curr_hour + ".xlsx", mis_date);
				MailHelper mailHelper = new MailHelper();
				mailHelper.sendMailRequest("./misData/!dea_VC_Hourly_Report_" + mis_date + "_" + curr_hour + ".zip");
				logger.debug("Program is END..");
				logger.info("program is end..");
				System.exit(0);
			} else {
				System.exit(1);
			}
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	private boolean getLock() {
		boolean lock = false;
		try {
			RandomAccessFile file = new RandomAccessFile("./AgentMis.tmp", "rw");
			FileChannel fileChannel = file.getChannel();
			FileLock fileLock = fileChannel.tryLock();
			if (fileLock != null) {
				logger.debug("File is locked. Starting the execution of the Application");
				lock = true;
			} else {
				logger.error("Cannot create lock. Already another instance of the program is running.");
				logger.error("System shutting down. Please wait...");
				try {
					Thread.sleep(1000 * 3);
				} catch (Exception exp) {
					exp.printStackTrace();
				}
				System.exit(0);
			}
		} catch (Exception exp) {
			exp.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(exp));
		}
		return lock;
	}

	private boolean accessFile() {
		boolean retCode = false;
		try {
			String line = "";
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter("./AgentMis.tmp", false));
				bw.close();
			} catch (Exception exp) {
				exp.printStackTrace();
			}
			retCode = true;
		} catch (Exception e) {
			System.out.println(e);
			logger.debug(LogStackTrace.getStackTrace(e));
		}
		logger.debug("accessFile() = " + retCode);
		return retCode;
	}

	public String getDate(int datediff) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -datediff);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String strDate = formatter.format(cal.getTime());
		logger.debug("date return[" + strDate.toString() + "]");
		return strDate;
	}

}
