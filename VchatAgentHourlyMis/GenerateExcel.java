package com.spice.vchat.hourlyagentmis;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.formula.functions.Replace;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class GenerateExcel {
	private static Logger logger = Logger.getLogger(GenerateExcel.class.getName());
	private FileOutputStream fos = null;
	private String columnsNames = null;
	// private Connection con = null;
	// private Statement stmt = null;
	// private ResultSet rs = null;
	private String sqlQuery = null;
	private String workBookPath = "./MIS.xlsx";
	private String[] columnsName = null;
	private String sheetName = null;
	private XSSFWorkbook workBook = null;
	private XSSFSheet sheet = null;
	private Properties prop = null;
	private int RowCounter = 0;
	private int ColCounter = 2;
	private String dbUrl = null;
	private String dbUserName = null;
	private String dbPassword = null;
	private XSSFRow SheetRow = null;
	private XSSFCell SheetCell = null;
	private String dbDriverName = "";
	private XSSFCellStyle styleHeader = null;
	private XSSFCellStyle styleColumn = null;
	private String circleNames = null;
	private String[] circleName = null;
	private DBConnectionManager connectionManager = null;

	public GenerateExcel() {
		prop = new Properties();
		PropertyConfigurator.configure("./log4j.properties");
		connectionManager = DBConnectionManager.getInstance();
		try {
			workBook = new XSSFWorkbook();
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			sheetName = prop.getProperty("sheetName");
			columnsNames = prop.getProperty("columnsNames");
			sqlQuery = prop.getProperty("sqlQuery");
			circleNames = prop.getProperty("circleNames");
			setStyle();
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}

	}

	public void setStyle() {
		styleHeader = workBook.createCellStyle();
		styleHeader.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		styleHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleHeader.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		styleHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		styleHeader.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderTop(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderRight(XSSFCellStyle.BORDER_THIN);
		XSSFFont mergeCellFont = workBook.createFont();
		mergeCellFont.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		styleHeader.setFont(mergeCellFont);

		styleColumn = workBook.createCellStyle();
		// styleColumn.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		styleColumn.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleColumn.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		// styleColumn.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		styleColumn.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		styleColumn.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		styleColumn.setBorderTop(XSSFCellStyle.BORDER_THIN);
		styleColumn.setBorderRight(XSSFCellStyle.BORDER_THIN);

	}

	public void getExcelProcess(String misDate) throws SQLException {
		try {
			// Class.forName(dbDriverName);
			circleName = circleNames.split("[,]+");
			sheet = workBook.createSheet(sheetName);
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
		RowCounter = 0;
		ColCounter = 0;
		columnsName = columnsNames.split("[,]+");
		SheetRow = sheet.createRow(RowCounter);
		for (String columnsCode : columnsName) {
			SheetCell = SheetRow.createCell(ColCounter);
			SheetCell.setCellStyle(styleHeader);
			SheetCell.setCellValue(columnsCode);
			ColCounter++;
		}
		RowCounter = 1;
		for (String circleCode : circleName) {
			Connection con = null;
			Statement stmt = null;
			ResultSet rs = null;
			try {				
				logger.debug("\nConnecting--> [" + circleCode + "] circle and db url is ["
						+ prop.getProperty(circleCode + ".url") + "]");
				con = connectionManager.getConnection(circleCode);
				// logger.debug("circle["+circleCode+"]----------------------["+prop.getProperty(circleCode+"_dbUrl")+"]");
				// con =
				// DriverManager.getConnection(prop.getProperty(circleCode+"_dbUrl"),
				// prop.getProperty(circleCode+"_dbUserName"),
				// prop.getProperty(circleCode+"_dbPassword"));
				// logger.debug("connect is [" + con + "]");
				stmt = con.createStatement();
				// sqlQuery=prop.getProperty(circleCode+"_sqlQuery");
				sqlQuery = sqlQuery.replaceAll("YYYYMMDD", misDate);
				logger.debug("Query is [" + sqlQuery + "]");
				//stmt.setQueryTimeout(300);
				rs = stmt.executeQuery(sqlQuery);
				ColCounter = 0;
				while (rs.next()) {
					SheetRow = sheet.createRow(RowCounter);
					for (String columnsCode : columnsName) {
						SheetCell = SheetRow.createCell(ColCounter);
						// System.out.println("columnsCode["+columnsCode+"]");
						SheetCell.setCellStyle(styleColumn);
						if (columnsCode.equalsIgnoreCase("circle") || columnsCode.equalsIgnoreCase("agent_location")
								|| columnsCode.equalsIgnoreCase("login_logout")
								|| columnsCode.equalsIgnoreCase("curr_date")
								|| columnsCode.equalsIgnoreCase("login_logout_time")
								|| columnsCode.equalsIgnoreCase("Last_call_Time")
								|| columnsCode.equalsIgnoreCase("Last_AnsCall_time")
								|| columnsCode.equalsIgnoreCase("vendor")
								|| columnsCode.equalsIgnoreCase("current_status")
								|| columnsCode.equalsIgnoreCase("Category")
								|| columnsCode.equalsIgnoreCase("att")) {
							SheetCell.setCellValue(rs.getString(columnsCode));
						} else if (columnsCode.equalsIgnoreCase("ani") || columnsCode.equalsIgnoreCase("chat_id")) {
							SheetCell.setCellValue(rs.getLong(columnsCode));
						} else {
							SheetCell.setCellValue(rs.getInt(columnsCode));
						}
						ColCounter++;
					}
					RowCounter++;
					ColCounter = 0;
				}

			} catch (Exception ex) {
				logger.debug(LogStackTrace.getStackTrace(ex));
			} finally {
				if (null != con) {
					connectionManager.freeConnection(circleCode, con);
				}
				if (null != stmt) {
					stmt.close();
				}
				if (null != rs) {
					rs.close();
				}
			}
		}
		try {
			saveExcels("./misData/!dea_VC_Hourly_Report_" + misDate + "_" + AgentMis.curr_hour + ".xlsx");
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public void saveExcels(String workBookPath) throws IOException {
		this.workBookPath = workBookPath; // for saving file to the
		try {
			fos = new FileOutputStream(workBookPath); // saves xlsx
			workBook.write(fos);
			fos.close();
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			fos.close();
		}
	}
}
