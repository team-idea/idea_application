package com.spicedigital.dnd;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.servlet.ServletException;
//import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;




//@WebServlet("/CheckDndNumberStatus")
public class CheckDndNumberStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static Logger logger= Logger.getLogger(CheckDndNumberStatus.class.getName()); 	
	private String driverName = "com.mysql.jdbc.Driver";
	private String sqlQuery;
	private String dbUrl = "jdbc:mysql://10.196.222.46:3306/dnd";
	private String dbUserName = "bill";
	private String dbPassword = "passpass";
	private Properties prop=null;
	
       
    public CheckDndNumberStatus() {
    	try {
			prop = new Properties();
			PropertyConfigurator.configure("Dnd_log4j.properties");
			prop.load(new DataInputStream(new FileInputStream("DND_DB.properties")));
			driverName = prop.getProperty("driverName");
			dbUrl = prop.getProperty("dbUrl");
			dbUserName = prop.getProperty("dbUserName");
			dbPassword = prop.getProperty("dbPassword");
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/HTML");
		Connection con = null;
		Statement stmt=null;
		ResultSet rs= null;
		String description=null;
		String circle=null;
		String msisdn=null;
		int status=1;
		PrintWriter out=response.getWriter();
		msisdn=request.getParameter("msisdn");
		if(msisdn.length()>10){
			msisdn=msisdn.substring(2);
		}
		circle=request.getParameter("circle");
		System.out.println("msisdn["+msisdn+"]circle["+circle+"]");
		
		logger.debug("msisdn["+msisdn+"]circle["+circle+"]");
		try{
		Class.forName(driverName);
		sqlQuery = "select count(1) from dnd.tbl_dnd_base where msisdn='"+msisdn+"' and opstype='A';";		
		con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
		//System.out.println("connection::-->"+con);
		//System.out.println("circle["+circle+"]Query::-->"+sqlQuery);
		stmt= con.createStatement();
		rs=stmt.executeQuery(sqlQuery);
		while(rs.next()){
			 status = rs.getInt(1);
			 System.out.println("status is ["+status+"]");
		}
		rs.close();
		stmt.close();
		con.close();
		if(status > 0){
			description="dndActive#"+msisdn+"#"+circle;
		}else{
			description="dndNotActive#"+msisdn+"#"+circle;
		}
		out.println(description);
		logger.debug(description);
		}catch(Exception ex){
			description="dndActive#"+msisdn+"#"+circle+"#ExceptionCase";
			out.println(description);
			logger.debug(description);
			logger.debug(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		}finally{
				try {
					if( rs!=null || !rs.isClosed()){
						rs.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				try {
					if( stmt!=null || !stmt.isClosed()){
						stmt.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
				try {
					if( con!=null || !con.isClosed()){
						con.close();
					}
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
