package com.spice.mis.sports.cricket;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


public class SportsCricketMisMain {
	
	private static Logger logger = Logger.getLogger(SportsCricketMisMain.class.getName()); 
	//private Properties prop = new Properties();
	public static String dateParam = "1";
	
	
	SportsCricketMisMain(){
		try{
		PropertyConfigurator.configure("./log4j.properties");
		}catch(Exception ex){
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}
	
	public static void main(String[] args) {
		logger.debug("program is start..");
		int argsLength = args.length;
		try {
			if (argsLength == 0) {
				dateParam = getDate(1);
				logger.debug("no argument ::" + dateParam);
			} else if (argsLength == 1) {
				dateParam = getDate(Integer.parseInt(args[0]));
				logger.debug("one argument ::" + dateParam);
			} else {
				dateParam = getDate(1);
				logger.debug("multiple argument ::" + dateParam);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
		}

		int dateCounter=Integer.parseInt(dateParam.substring(6, 8));
		System.out.println("date--->"+dateCounter);
		String yearMonth=dateParam.substring(0, 6);
		System.out.println("ym---->"+yearMonth);
		SportsCricketMisMain sportsCricketMisMain = new SportsCricketMisMain();
		//DBConnection dbConnection= new 
		//dbConnection.getExcelProcess(yearMonth,dateCounter);
		GenerateExcel generateExcel= new GenerateExcel();
		generateExcel.getExcelProcess(yearMonth,dateCounter);
		MailHelper mailHelper = new MailHelper();
		mailHelper.sendMailRequest("./misData/SportsCricket_MIS_"+dateParam+".xlsx");
		logger.debug("Program is END..");
	}
	
	public static String getDate(int datediff) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -datediff);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String strDate = formatter.format(cal.getTime());
		logger.debug("date return["+strDate.toString()+"]");
		return strDate;
	}

}
