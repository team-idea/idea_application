package com.spice.mis.sports.cricket;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class GenerateExcel {
	private static Logger logger = Logger.getLogger(GenerateExcel.class.getName());
	private FileOutputStream fos = null;
	private String yearMonth = null;
	private int dateCounter = '1';
	private String mis_Date = null;
	private String sheetNames = null;
	private String circleColumnsNames = null;
	private String circleHeaderNames = null;
	// private DBConnectionManager connectionManager = null;
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	private String sqlQuery = null;
	private String sqlQueryMTD = null;
	private String workBookPath = "./MIS.xlsx";
	private Date MIS_DATE;

	private String[] circleColumnsName = null;
	private String[] sheetName = null;
	private String[] circleHeaderName = null;
	private String[] SummaryRowName = null;
	private String[] SummaryColName = null;
	private String SummaryRowNames = null;
	private String SummaryColNames = null;
	private XSSFWorkbook workBook = null;
	private XSSFRow circleSheetRow = null;
	private XSSFCell circleSheetCell = null;
	private XSSFSheet sheet = null;
	private Properties prop = null;
	private String tableColumns = null;
	private String[] tableColumn = null;
	private Properties propExcel = null;
	private int circleRowCounter = 0;
	private int mtdColCounter = 2;
	private int circleColCounter = 0;
	private String dbUrl = null;
	private String user = null;
	private String password = null;
	private String driverName = "";
	private XSSFCellStyle styleMerge = null;
	private XSSFCellStyle styleHeader = null;
	private XSSFCellStyle styleColumn = null;
	private String consolidateHeader = null;
	private String consolidateCol = null;
	private String consolidateCircle = null;
	private String[] consolidateCircleName = null;
	private String[] consolidateColName = null;
	private String consolidatetableColumns=null;
	private String[] tableColumnsName=null;
	private String[] Summarytablecol=null;
	private String finalDate=null;
	private String SummarytablecolName=null;
	


	public GenerateExcel() {
		prop = new Properties();
		propExcel = new Properties();
		PropertyConfigurator.configure("./log4j.properties");

		try {
			workBook = new XSSFWorkbook();
			// this.connectionManager = DBConnectionManager.getInstance();
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			propExcel.load(new DataInputStream(new FileInputStream("./excelConfig.properties")));
			sheetNames = propExcel.getProperty("sheetNames");
			circleHeaderNames = propExcel.getProperty("circleHeaderNames");
			circleColumnsNames = propExcel.getProperty("circleColumnsNames");
			SummaryRowNames = propExcel.getProperty("SummaryRowNames");
			SummaryColNames = propExcel.getProperty("SummaryColNames");
			SummaryColNames = propExcel.getProperty("SummaryColNames");
			tableColumns = propExcel.getProperty("tableColumns");
			consolidateHeader = propExcel.getProperty("consolidateHeader");
			consolidateCol = propExcel.getProperty("consolidateCol");
			consolidateCircle = propExcel.getProperty("consolidateCircle");
			consolidatetableColumns=propExcel.getProperty("consolidatetableColumns");
			SummarytablecolName=propExcel.getProperty("SummarytablecolName");
			driverName = prop.getProperty("driverName");
			dbUrl = prop.getProperty("dbUrl");
			user = prop.getProperty("user");
			password = prop.getProperty("password");
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}

	}

	public void setStyle() {
		styleMerge = workBook.createCellStyle();
		// styleMerge.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		styleMerge.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleMerge.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		// styleMerge.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		styleMerge.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		styleMerge.setBorderTop(XSSFCellStyle.BORDER_THIN);
		styleMerge.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		styleMerge.setBorderRight(XSSFCellStyle.BORDER_THIN);
		XSSFFont mergeCellFont = workBook.createFont();
		mergeCellFont.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		styleMerge.setFont(mergeCellFont);

		styleHeader = workBook.createCellStyle();
		styleHeader.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		styleHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleHeader.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		styleHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		styleHeader.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderTop(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderRight(XSSFCellStyle.BORDER_THIN);

		styleColumn = workBook.createCellStyle();
		// styleColumn.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		styleColumn.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleColumn.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		// styleColumn.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		styleColumn.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		styleColumn.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		styleColumn.setBorderTop(XSSFCellStyle.BORDER_THIN);
		styleColumn.setBorderRight(XSSFCellStyle.BORDER_THIN);

	}

	public void getExcelProcess(String yearMonth, int dateCounter) {
		sheetName = sheetNames.split("[,]+");
		circleColumnsName = circleColumnsNames.split("[,]+");
		circleHeaderName = circleHeaderNames.split("[,]+");
		tableColumn = tableColumns.split("[,]+");
		finalDate = yearMonth + dateCounter;
		if (dateCounter < 10) {
			finalDate = yearMonth + "0" + dateCounter;
		} else {
			finalDate = yearMonth + dateCounter;
		}
		setStyle();
		this.yearMonth = yearMonth;
		this.dateCounter = dateCounter;
		/*
		 * if (dateCounter < 10) { mis_Date = yearMonth + "0" + dateCounter; }
		 * else { mis_Date = yearMonth + dateCounter; }
		 */// logger.debug("mis_date is[" + mis_Date + "]");
		try {
			Class.forName(driverName);
			con = DriverManager.getConnection(dbUrl, user, password);
			logger.debug("connect is [" + con + "]");
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
		}

		for (String sheetCode : sheetName) {
			sheet = workBook.createSheet(sheetCode);
			if (sheetCode.equalsIgnoreCase("PAN India Summary")) {
				getPanIndiaSummarySheet(finalDate);
				continue;
			}
			if (sheetCode.equalsIgnoreCase("Circle-wise consolidated")) {
				getCircleWiseConsolidatedSheet(finalDate);
				continue;
			}
			for (circleRowCounter = 0; circleRowCounter < circleColumnsName.length; circleRowCounter++) {
				circleSheetRow = sheet.createRow(circleRowCounter);
			}
			for (String circleHeaderCode : circleHeaderName) {
				String[] headerContainer = circleHeaderCode.split("[#]+");
				circleSheetRow = sheet.getRow(Integer.parseInt(headerContainer[1]));
				circleSheetCell = circleSheetRow.createCell(0);
				circleSheetCell.setCellValue(headerContainer[0]);
				sheet.addMergedRegion(CellRangeAddress.valueOf(headerContainer[2]));
				if (headerContainer[2].equalsIgnoreCase("A1:A1")) {
					circleSheetCell.setCellStyle(styleHeader);
				} else {
					circleSheetCell.setCellStyle(styleMerge);
				}
				sheet.setColumnWidth(circleColCounter, 8000);
			}

			circleRowCounter = 0;
			circleColCounter = 1;
			sheet.setColumnWidth(0, 8000);
			sheet.setColumnWidth(circleColCounter, 8000);
			for (String circleColumnscode : circleColumnsName) {
				if (sheet.getRow(circleRowCounter) == null) {
					circleSheetRow = sheet.createRow(circleRowCounter);
					circleSheetCell = circleSheetRow.createCell(circleColCounter);
					circleSheetCell.setCellValue(circleColumnscode);
				} else {
					circleSheetRow = sheet.getRow(circleRowCounter);
					circleSheetCell = circleSheetRow.createCell(circleColCounter);
					circleSheetCell.setCellValue(circleColumnscode);
				}
				if (circleRowCounter == 0) {
					circleSheetCell.setCellStyle(styleHeader);
				} else {
					circleSheetCell.setCellStyle(styleColumn);
				}
				circleRowCounter++;
			}

			try {
				circleColCounter = 3;
				for (int i = 1; i <= dateCounter; i++) {
					String misDate = "";
					if (i < 10) {
						misDate = yearMonth + "0" + i;
					} else {
						misDate = yearMonth + i;
					}
					logger.debug("mis_date is [" + misDate + "]");
					stmt = con.createStatement();
					circleRowCounter = 0;
					if (i == dateCounter) {
						sqlQueryMTD = "select * from TBL_MIS_SPOCRIC_MASTER_MTD where circle='" + sheetCode
								+ "' and convert(varchar,MIS_DATE,112)='" + misDate + "'";
						rs = stmt.executeQuery(sqlQueryMTD);
						logger.debug("sqlquery hase been executed [" + sqlQueryMTD + "]");
						mtdColCounter = 2;
						while (rs.next()) {
							for (String colName : tableColumn) {
								if (colName.equalsIgnoreCase("MIS_DATE")) {
									MIS_DATE = rs.getDate("MIS_DATE");
									circleSheetRow = sheet.getRow(circleRowCounter);
									circleSheetCell = circleSheetRow.createCell(mtdColCounter);
									circleSheetCell.setCellValue("MTD");
									circleSheetCell.setCellStyle(styleHeader);
								} else {
									int value = rs.getInt(colName);
									circleSheetRow = sheet.getRow(circleRowCounter);
									circleSheetCell = circleSheetRow.createCell(mtdColCounter);
									circleSheetCell.setCellStyle(styleColumn);
									circleSheetCell.setCellValue(value);
								}
								circleRowCounter++;
							}
						}
					}

					circleRowCounter = 0;
					sqlQuery = "select * from TBL_MIS_SPOCRIC_MASTER where circle='" + sheetCode
							+ "' and convert(varchar,MIS_DATE,112)='" + misDate + "'";
					rs = stmt.executeQuery(sqlQuery);
					logger.debug("sqlquery hase been executed [" + sqlQuery + "]");
					while (rs.next()) {
						for (String colName : tableColumn) {
							if (colName.equalsIgnoreCase("MIS_DATE")) {
								MIS_DATE = rs.getDate("MIS_DATE");
								circleSheetRow = sheet.getRow(circleRowCounter);
								circleSheetCell = circleSheetRow.createCell(circleColCounter);
								circleSheetCell.setCellValue(MIS_DATE.toString());
								circleSheetCell.setCellStyle(styleHeader);
							} else {
								int value = rs.getInt(colName);
								circleSheetRow = sheet.getRow(circleRowCounter);
								circleSheetCell = circleSheetRow.createCell(circleColCounter);
								circleSheetCell.setCellStyle(styleColumn);
								circleSheetCell.setCellValue(value);
							}
							circleRowCounter++;
						}
					}
					circleColCounter++;
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				logger.debug(LogStackTrace.getStackTrace(ex));
			} finally {
				try {
					if (stmt != null || !stmt.isClosed()) {
						stmt.close();
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					logger.debug(LogStackTrace.getStackTrace(ex));
				}

				try {
					if (rs != null || !rs.isClosed()) {
						rs.close();
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					logger.debug(LogStackTrace.getStackTrace(ex));
				}
			}
		}

		try {
			if (con != null || !con.isClosed()) {
				con.close();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
		try {
			saveExcels("./misData/SportsCricket_MIS_" + SportsCricketMisMain.dateParam + ".xlsx");
		} catch (IOException e) {
			e.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(e));
		}
	}

	public void getPanIndiaSummarySheet(String lastDate) {
		try {
			//stmt = con.createStatement();
			//String strQuery = "select * from TBL_MIS_SPOCRIC_SUMMARY  where convert(varchar,MIS_DATE,112)='"+lastDate+"';";
			//rs = stmt.executeQuery(strQuery);
			//logger.debug("sqlquery hase been executed [" + strQuery + "]");

			SummaryRowName = SummaryRowNames.split("[,]+");
			SummaryColName = SummaryColNames.split("[,]+");
			for (circleRowCounter = 0; circleRowCounter < SummaryRowName.length; circleRowCounter++) {
				circleSheetRow = sheet.createRow(circleRowCounter);
				circleColCounter = 0;
//				/sheet.setColumnWidth(6, 4000);
				sheet.setColumnWidth(0, 7000);
				if (circleRowCounter == 0) {
					circleSheetRow = sheet.getRow(circleRowCounter);
					for (String str : SummaryColName) {
						circleSheetCell = circleSheetRow.createCell(circleColCounter);
						circleSheetCell.setCellValue(str);
						circleSheetCell.setCellStyle(styleHeader);
						circleColCounter++;
					}
				} else {
					circleSheetRow = sheet.getRow(circleRowCounter);
					//for (String str : SummaryRowName) {
						String str=SummaryRowName[circleRowCounter];
						if (str.equalsIgnoreCase("Details")) {
							continue;
						}
						try {
							Summarytablecol=SummarytablecolName.split("[,]+");
							circleColCounter=0;
							circleSheetCell = circleSheetRow.createCell(circleColCounter);
							circleSheetCell.setCellValue(str);
							circleSheetCell.setCellStyle(styleColumn);
							circleColCounter++;
							String consolidateQuery = "select * from TBL_MIS_SPOCRIC_SUMMARY where category='"+str + "' and convert(varchar,mis_date,112)='" + lastDate + "'";
							logger.debug("combineQuery is going to execute[" + consolidateQuery + "]");
							stmt = con.createStatement();
							rs = stmt.executeQuery(consolidateQuery);
							logger.debug("sqlquery hase been executed [" + rs + "]");
							while (rs.next()) {
								for (String tablecol : Summarytablecol) {
									int value = rs.getInt(tablecol);
									circleSheetCell = circleSheetRow.createCell(circleColCounter);
									circleSheetCell.setCellValue(value);
									circleSheetCell.setCellStyle(styleColumn);
									circleColCounter++;
								}
							}
							System.out.println("row_counter]"+circleRowCounter);
							rs.close();
							stmt.close();

				
							//circleSheetCell = circleSheetRow.createCell(circleColCounter);
							//circleSheetCell.setCellValue("null");
							//circleColCounter++;
						} catch (Exception ex) {
							ex.printStackTrace();
							logger.debug(LogStackTrace.getStackTrace(ex));;
						}
					}

				}
			
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));;
			
		}

	}

	public void getCircleWiseConsolidatedSheet(String lastDate) {
		consolidateCircleName=consolidateCircle.split("[,]+");
		consolidateColName=consolidateCol.split("[,]+");
		tableColumnsName=consolidatetableColumns.split("[,]+");
		for (circleRowCounter = 0; circleRowCounter < consolidateCircleName.length+2; circleRowCounter++) {
			circleSheetRow = sheet.createRow(circleRowCounter);
		}
		//circleSheetCell = circleSheetRow.createCell(circleColCounter);
		
			
		circleRowCounter=0;
		circleColCounter = 0;
			circleSheetRow = sheet.getRow(circleRowCounter);
			circleSheetCell = circleSheetRow.createCell(0);
			circleSheetCell = circleSheetRow.getCell(0);
			circleSheetCell.setCellValue(lastDate);
			circleSheetCell.setCellStyle(styleHeader);
			
			
			circleSheetCell = circleSheetRow.createCell(1);
			circleSheetCell = circleSheetRow.getCell(1);
			circleSheetCell.setCellValue("Calls");
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 2));
			circleSheetCell.setCellStyle(styleHeader);

			circleSheetCell = circleSheetRow.createCell(3);
			circleSheetCell = circleSheetRow.getCell(3);
			circleSheetCell.setCellValue("Gross Adds");
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 3, 6));
			circleSheetCell.setCellStyle(styleHeader);
			
			circleSheetCell = circleSheetRow.createCell(7);
			circleSheetCell = circleSheetRow.getCell(7);
			circleSheetCell.setCellValue("Renewals");
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 7, 10));
			circleSheetCell.setCellStyle(styleHeader);

			circleSheetCell = circleSheetRow.createCell(11);
			circleSheetCell = circleSheetRow.getCell(11);
			circleSheetCell.setCellValue("Churn");
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 11, 14));
			circleSheetCell.setCellStyle(styleHeader);

			circleSheetCell = circleSheetRow.createCell(15);
			circleSheetCell = circleSheetRow.getCell(15);
			circleSheetCell.setCellValue("Base");
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 15, 18));
			circleSheetCell.setCellStyle(styleHeader);

			circleSheetCell = circleSheetRow.createCell(19);
			circleSheetCell = circleSheetRow.getCell(19);
			circleSheetCell.setCellValue("Activation Revenue");
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 19, 22));
			circleSheetCell.setCellStyle(styleHeader);

			circleSheetCell = circleSheetRow.createCell(23);
			circleSheetCell = circleSheetRow.getCell(23);
			circleSheetCell.setCellValue("Renewal Revenue");
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 23, 26));
			circleSheetCell.setCellStyle(styleHeader);

			circleSheetCell = circleSheetRow.createCell(27);
			circleSheetCell = circleSheetRow.getCell(27);
			circleSheetCell.setCellValue("Total Revenue");
			sheet.addMergedRegion(new CellRangeAddress(0, 0, 27, 30));
			circleSheetCell.setCellStyle(styleHeader);
			circleRowCounter++;
			circleColCounter=0;
			circleSheetRow = sheet.createRow(circleRowCounter);		
			for (int i = 0; i < consolidateColName.length; i++) {
				circleSheetCell = circleSheetRow.createCell(circleColCounter);
				circleSheetCell = circleSheetRow.getCell(circleColCounter);
				circleSheetCell.setCellValue(consolidateColName[i]);
				circleSheetCell.setCellStyle(styleHeader);
				circleColCounter++;
			}
			sheet.setColumnWidth(0, 3000);
			sheet.setColumnWidth(1, 3000);
			sheet.setColumnWidth(2, 3000);
			
			circleRowCounter++;
			/*try {
				conn = DriverManager.getConnection(dbUrl, user, password);
			} catch (Exception e1) {
				e1.printStackTrace();
			}*/
		try {
			for (String circleCode : consolidateCircleName) {
				String consolidateQuery = "select TOTAL_CALLS,UNI_CALLERS,FTD_GROSS,MTD_GROSS,LMTD_GROSS,GOLM_GROSS,FTD_RENEWAL,MTD_RENEWAL,LMTD_RENEWAL,GOLM_RENEWAL,FTD_CHURN,MTD_CHURN,LMTD_CHURN,GOLM_CHURN,FTD_BASE,MTD_BASE,LMTD_BASE,GOLM_BASE,FTD_ACT_REV,MTD_ACT_REV,LMTD_ACT_REV,GOLM_ACT_REV,FTD_RENEW_REV,MTD_RENEW_REV,LMTD_RENEW_REV,GOLM_RENEW_REV,FTD_TOTAL_REV,MTD_TOTAL_REV,LMTD_TOTAL_REV,GOLM_TOTAL_REV from TBL_MIS_SPOCRIC_CIRCLE_WISE where circle='"
						+ circleCode + "' and convert(varchar,mis_date,112)='" + lastDate + "'";
				logger.debug("combineQuery is going to execute[" + consolidateQuery + "]");
				stmt = con.createStatement();
				rs = stmt.executeQuery(consolidateQuery);
				logger.debug("sqlquery hase been executed [" + rs + "]");
				circleColCounter=0;
				circleSheetRow = sheet.createRow(circleRowCounter);		
				circleSheetCell = circleSheetRow.createCell(circleColCounter);
				circleSheetCell.setCellValue(circleCode);
				circleSheetCell.setCellStyle(styleHeader);
				circleColCounter++;
				
				while (rs.next()) {
					for (String tableColName : tableColumnsName) {
						int value = rs.getInt(tableColName);
						circleSheetCell = circleSheetRow.createCell(circleColCounter);
						logger.debug("circleSheetCell-------->"+circleSheetCell+"-->"+value);
						circleSheetCell.setCellValue(value);
						circleSheetCell.setCellStyle(styleColumn);
						circleColCounter++;
					}
				}
				rs.close();
				stmt.close();
				circleRowCounter++;
			}
		}catch(Exception ex){
				ex.printStackTrace();
				logger.debug(LogStackTrace.getStackTrace(ex));
			} finally {
				try {
					if (rs != null || !rs.isClosed()) {
						rs.close();
					}
				} catch (Exception e) {
					e.printStackTrace();
					logger.debug(LogStackTrace.getStackTrace(e));
				}
				try {
					if (stmt != null || !stmt.isClosed()) {
						stmt.close();
					}
				} catch (Exception e) {
					e.printStackTrace();
					logger.debug(LogStackTrace.getStackTrace(e));
				}
				/*try {
					if (con != null || !con.isClosed()) {
						con.close();
					}
				} catch (Exception e) {
					e.printStackTrace();
					
				}*/
			}
			
			
	
		

		/*circleRowCounter = 0;
		circleColCounter = 1;
		sheet.setColumnWidth(circleColCounter, 8000);
		for (String circleColumnscode : circleColumnsName) {
			if (sheet.getRow(circleRowCounter) == null) {
				circleSheetRow = sheet.createRow(circleRowCounter);
				circleSheetCell = circleSheetRow.createCell(circleColCounter);
				circleSheetCell.setCellValue(circleColumnscode);
			} else {
				circleSheetRow = sheet.getRow(circleRowCounter);
				circleSheetCell = circleSheetRow.createCell(circleColCounter);
				circleSheetCell.setCellValue(circleColumnscode);
			}
			if (circleRowCounter == 0) {
				circleSheetCell.setCellStyle(styleHeader);
			} else {
				circleSheetCell.setCellStyle(styleColumn);
			}
			circleRowCounter++;
		}

*/		
	}

	public void saveExcels(String workBookPath) throws IOException {
		this.workBookPath = workBookPath; // for saving file to the
		try {
			fos = new FileOutputStream(workBookPath); // saves xlsx
			workBook.write(fos);
			fos.close();
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			fos.close();
		}
	}
}
