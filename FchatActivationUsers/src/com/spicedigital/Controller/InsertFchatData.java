package com.spicedigital.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.spicedigital.Model.FchatDataModel;
import com.spicedigital.util.DBConnectionManager;
import com.spicedigital.util.LogStackTrace;


public class InsertFchatData {
	private static final Logger LOGGER = Logger.getLogger(InsertFchatData.class);
	DBConnectionManager connectionManager = null;

	public InsertFchatData() {
		PropertyConfigurator.configure("./log4j.properties");
		try {
			connectionManager = DBConnectionManager.getInstance();
		} catch (Exception e) {
			LOGGER.error(LogStackTrace.getStackTrace(e));
		}
	}

	public void insertData(List<FchatDataModel> collectFchatDataList) throws SQLException {
		Connection connection = null;
		Statement statement = null;
		String sqlQuery = null;
		CollectFchatData collectFreemiumData = new CollectFchatData();
		FchatDataModel freemiumDataModel = null;
		connection = connectionManager.getConnection("mysql");
		statement = connection.createStatement();
		LOGGER.debug("size of the list is[" + collectFchatDataList.size() + "]");
		try {
			statement.executeUpdate("truncate table tbl_fchat_users");
			LOGGER.debug("truncate query has been executed successfully.");
			
			for (FchatDataModel insertDataModel : collectFchatDataList) {
				String ani = insertDataModel.getAni();
				String circle = insertDataModel.getCircle();
				String PRE_POST = insertDataModel.getPrePost();
				String SUB_DATE = insertDataModel.getSubDate();
				String RENEW_DATE = insertDataModel.getRenewDate();
				String BILLING_DATE = insertDataModel.getBillingDate();
				String STATUS = insertDataModel.getStatus();
				String GRACE_DAYS = insertDataModel.getGraceDay();
				String MODE = insertDataModel.getMode();				
				String status = insertDataModel.getStatus();
				int chatId = insertDataModel.getChatId();
				
				LOGGER.debug("dnd status[" + status + "] for msisdn[" + ani + "]");				
				LOGGER.debug(
						"'" + ani + "', '" + circle + "', '" + chatId + "', '" + PRE_POST + "', now(), '" + SUB_DATE + "', '" + RENEW_DATE
								+ "', '" + BILLING_DATE + "','" + STATUS + "', '" + GRACE_DAYS + "', '" + MODE + "'");
				sqlQuery = "INSERT INTO tbl_fchat_users (ani, circle, chat_id, pre_post, insertDateTime, sub_date, renew_date, billing_date, status, grace_day, mode)"
						+ "VALUES ('" + ani + "', '" + circle + "', '" + chatId + "', '" + PRE_POST + "', now(), '" + SUB_DATE + "', '"
						+ RENEW_DATE + "', '" + BILLING_DATE + "','" + status + "', '" + GRACE_DAYS + "', '" + MODE
						+ "');";
				LOGGER.debug(sqlQuery);
				int count = statement.executeUpdate(sqlQuery);
				LOGGER.debug("[" + count + "] row inserted");
			}

		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		} finally {
			if (null != connection) {
				connectionManager.freeConnection("mssql", connection);
			}
			if (null != statement) {
				statement.close();
			}

		}

	}

}
