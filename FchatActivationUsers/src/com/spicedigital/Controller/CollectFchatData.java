package com.spicedigital.Controller;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.spicedigital.Model.FchatDataModel;
import com.spicedigital.util.DBConnectionManager;
import com.spicedigital.util.LogStackTrace;

public class CollectFchatData {
	private static final Logger LOGGER = Logger.getLogger(CollectFchatData.class);
	DBConnectionManager connectionManager = null;
	public List<FchatDataModel> FchatDataModelList = null;
	public CollectFchatData() {
		try {
			connectionManager = DBConnectionManager.getInstance();
		} catch (Exception e) {
			LOGGER.error(LogStackTrace.getStackTrace(e));
		}
	}

	public List processRequest() throws SQLException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String sqlQuery = null;
		FchatDataModel fchatDataModel = null;
		
		try {
			FchatDataModelList = new ArrayList<FchatDataModel>();
			connection = connectionManager.getConnection("mssql");
			statement = connection.createStatement();
			sqlQuery = "Select * from tbl_fchat_users";
			LOGGER.debug("query going to execute["+sqlQuery+"]");
			resultSet = statement.executeQuery(sqlQuery);
			while (resultSet.next()) {
				fchatDataModel = new FchatDataModel();
				fchatDataModel.setAni(resultSet.getString("ani"));
				fchatDataModel.setCircle(resultSet.getString("circle"));
				fchatDataModel.setChatId(Integer.parseInt(resultSet.getString("chat_id")));				
				fchatDataModel.setPrePost(resultSet.getString("PRE_POST"));
				fchatDataModel.setSubDate(resultSet.getString("SUB_DATE"));
				fchatDataModel.setRenewDate(resultSet.getString("RENEW_DATE"));
				fchatDataModel.setBillingDate(resultSet.getString("BILLING_DATE"));
				fchatDataModel.setStatus(resultSet.getString("STATUS"));
				fchatDataModel.setGraceDay(resultSet.getString("GRACE_DAYS"));
				fchatDataModel.setMode(resultSet.getString("MODE"));			
				FchatDataModelList.add(fchatDataModel);
				//LOGGER.debug(freemiumDataModel.getMode());
				LOGGER.debug(fchatDataModel);
			}
		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		} finally {
			if (null != connection) {
				connectionManager.freeConnection("mssql", connection);
			}
			if (null != statement) {
				statement.close();
			}
			if (null != resultSet) {
				resultSet.close();
			}
		}
		return FchatDataModelList;
	}

}
