package com.spicedigital.Controller;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.spicedigital.Model.FchatDataModel;
import com.spicedigital.util.LogStackTrace;

public class FchatMain {

	private static final Logger LOGGER = Logger.getLogger(FchatMain.class);
	
	public FchatMain() {
		PropertyConfigurator.configure("./log4j.properties");
	}

	public static void main(String[] args) {
		FchatMain fchatMain = new FchatMain();
		try {
			if (fchatMain.accessFile() && fchatMain.getLock()) {
				LOGGER.info("program is started..");
				CollectFchatData collectFchatData = new CollectFchatData();
				List<FchatDataModel> collectFreemiumDataList =collectFchatData.processRequest();
				InsertFchatData insertFchatData = new InsertFchatData();
				insertFchatData.insertData(collectFreemiumDataList);				
				LOGGER.info("program is end..");
				System.exit(0);				
			} else {
			}
		} catch (Exception e) {
			LOGGER.error(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		}
	}

	private boolean getLock() {
		boolean lock = false;
		try {
			RandomAccessFile file = new RandomAccessFile("./fchat.tmp", "rw");
			FileChannel fileChannel = file.getChannel();
			FileLock fileLock = fileChannel.tryLock();
			if (fileLock != null) {
				LOGGER.debug("File is locked. Starting the execution of the Application");
				lock = true;
			} else {
				LOGGER.error("Cannot create lock. Already another instance of the program is running.");
				LOGGER.error("System shutting down. Please wait...");
				try {
					Thread.sleep(1000 * 3);
				} catch (Exception exp) {
					exp.printStackTrace();
				}
				System.exit(0);
			}
		} catch (Exception exp) {
			exp.printStackTrace();
			LOGGER.debug(LogStackTrace.getStackTrace(exp));
		}
		return lock;
	}

	private boolean accessFile() {
		boolean retCode = false;
		try {
			String line = "";
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter("./fchat.tmp", false));
				bw.close();
			} catch (Exception exp) {
				exp.printStackTrace();
			}
			retCode = true;
		} catch (Exception e) {
			System.out.println(e);
			LOGGER.debug(LogStackTrace.getStackTrace(e));
		}
		LOGGER.debug("accessFile() = " + retCode);
		return retCode;
	}


}
