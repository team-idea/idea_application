package com.spicedigital.Freemium;

public class FreemiumDataModel {

	private String ani;
	private String circle;
	private String prePost;
	private String subDate;
	private String renewDate;
	private String billingDate;
	private String status;
	private String graceDay;
	private String mode;
	
	public String getAni() {
		return ani;
	}
	public void setAni(String ani) {
		this.ani = ani;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public String getPrePost() {
		return prePost;
	}
	public void setPrePost(String prePost) {
		this.prePost = prePost;
	}
	public String getSubDate() {
		return subDate;
	}
	public void setSubDate(String subDate) {
		this.subDate = subDate;
	}
	public String getRenewDate() {
		return renewDate;
	}
	public void setRenewDate(String renewDate) {
		this.renewDate = renewDate;
	}
	public String getBillingDate() {
		return billingDate;
	}
	public void setBillingDate(String billingDate) {
		this.billingDate = billingDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getGraceDay() {
		return graceDay;
	}
	public void setGraceDay(String graceDay) {
		this.graceDay = graceDay;
	}
	public String getMode() {
		return mode;
	}
	public void setMode(String mode) {
		this.mode = mode;
	}
	
		
}
