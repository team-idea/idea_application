package com.spicedigital.Freemium;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import com.spicedigital.util.DBConnectionManager;
import com.spicedigital.util.HttpURLForDnd;
import com.spicedigital.util.LogStackTrace;

public class FreemiumCli {
	private static final Logger LOGGER = Logger.getLogger(FreemiumCli.class);
	private DBConnectionManager connectionManager = null;
	public HashMap<String,String> cliMap = new HashMap<String,String>();
	
	public FreemiumCli() {
		try {
			connectionManager = DBConnectionManager.getInstance();
		} catch (Exception e) {
			LOGGER.error(LogStackTrace.getStackTrace(e));
		}
	}
	
	public void CollectFreemiumCli() throws SQLException{
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String sqlQuery = null;
		try{
			connection = connectionManager.getConnection("mysql");
			statement = connection.createStatement();
			sqlQuery="select circle,cli from tbl_freemium_cli";
			resultSet= statement.executeQuery(sqlQuery);
			while(resultSet.next()){
				String circle = resultSet.getString("circle").toUpperCase();
				String cli	= resultSet.getString("cli").toUpperCase();
				cliMap.put(circle, cli);
			}
			LOGGER.debug(cliMap);
		}catch(Exception ex){
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		}finally{
			if (null != connection) {
				connectionManager.freeConnection("mssql", connection);
			}
			if (null != statement) {
				statement.close();
			}
			if (null != resultSet) {
				resultSet.close();
			}
		}
		
		String str = cliMap.get("AP");
		LOGGER.debug("key[ap] and value["+str+"]");
	}

}
