package com.spicedigital.Freemium;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.spicedigital.util.DBConnectionManager;
import com.spicedigital.util.LogStackTrace;

public class CollectFreemiumData {
	private static final Logger LOGGER = Logger.getLogger(CollectFreemiumData.class);
	DBConnectionManager connectionManager = null;
	public List<FreemiumDataModel> FreemiumDataModelList = null;
	public CollectFreemiumData() {
		try {
			connectionManager = DBConnectionManager.getInstance();
		} catch (Exception e) {
			LOGGER.error(LogStackTrace.getStackTrace(e));
		}
	}

	public List processRequest() throws SQLException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String sqlQuery = null;
		FreemiumDataModel freemiumDataModel = null;
		
		try {
			FreemiumDataModelList = new ArrayList<FreemiumDataModel>();
			connection = connectionManager.getConnection("mssql");
			statement = connection.createStatement();
			sqlQuery = "Select * from tbl_5432130_users";
			LOGGER.debug("query going to execute["+sqlQuery+"]");
			resultSet = statement.executeQuery(sqlQuery);
			while (resultSet.next()) {
				freemiumDataModel = new FreemiumDataModel();
				freemiumDataModel.setAni(resultSet.getString("ani"));
				freemiumDataModel.setCircle(resultSet.getString("circle"));
				freemiumDataModel.setPrePost(resultSet.getString("PRE_POST"));
				freemiumDataModel.setSubDate(resultSet.getString("SUB_DATE"));
				freemiumDataModel.setRenewDate(resultSet.getString("RENEW_DATE"));
				freemiumDataModel.setBillingDate(resultSet.getString("BILLING_DATE"));
				freemiumDataModel.setStatus(resultSet.getString("STATUS"));
				freemiumDataModel.setGraceDay(resultSet.getString("GRACE_DAYS"));
				freemiumDataModel.setMode(resultSet.getString("MODE"));			
				FreemiumDataModelList.add(freemiumDataModel);
				//LOGGER.debug(freemiumDataModel.getMode());
				LOGGER.debug(freemiumDataModel);
			}
		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		} finally {
			if (null != connection) {
				connectionManager.freeConnection("mssql", connection);
			}
			if (null != statement) {
				statement.close();
			}
			if (null != resultSet) {
				resultSet.close();
			}
		}
		return FreemiumDataModelList;
	}

}
