package com.spicedigital.Freemium;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.spicedigital.util.LogStackTrace;

public class FreemiumMain {
	private static final Logger LOGGER = Logger.getLogger(FreemiumMain.class);
	
	public FreemiumMain() {
		PropertyConfigurator.configure("./log4j.properties");
	}

	public static void main(String[] args) {
		FreemiumMain freemiumMain = new FreemiumMain();
		try {
			if (freemiumMain.accessFile() && freemiumMain.getLock()) {
				LOGGER.info("program is started..");
				/*CollectFreemiumData collectFreemiumData = new CollectFreemiumData();
				List<FreemiumDataModel> collectFreemiumDataList =collectFreemiumData.processRequest();
				InsertFreemiumData insertFreemiumData = new InsertFreemiumData();
				insertFreemiumData.insertData(collectFreemiumDataList);
				*/
				FreemiumCli cli = new FreemiumCli();
				cli.CollectFreemiumCli();
				LOGGER.info("program is end..");
				System.exit(0);				
			} else {
			}
		} catch (Exception e) {
			LOGGER.error(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		}
	}

	private boolean getLock() {
		boolean lock = false;
		try {
			RandomAccessFile file = new RandomAccessFile("./freemium.tmp", "rw");
			FileChannel fileChannel = file.getChannel();
			FileLock fileLock = fileChannel.tryLock();
			if (fileLock != null) {
				LOGGER.debug("File is locked. Starting the execution of the Application");
				lock = true;
			} else {
				LOGGER.error("Cannot create lock. Already another instance of the program is running.");
				LOGGER.error("System shutting down. Please wait...");
				try {
					Thread.sleep(1000 * 3);
				} catch (Exception exp) {
					exp.printStackTrace();
				}
				System.exit(0);
			}
		} catch (Exception exp) {
			exp.printStackTrace();
			LOGGER.debug(LogStackTrace.getStackTrace(exp));
		}
		return lock;
	}

	private boolean accessFile() {
		boolean retCode = false;
		try {
			String line = "";
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter("./freemium.tmp", false));
				bw.close();
			} catch (Exception exp) {
				exp.printStackTrace();
			}
			retCode = true;
		} catch (Exception e) {
			System.out.println(e);
			LOGGER.debug(LogStackTrace.getStackTrace(e));
		}
		LOGGER.debug("accessFile() = " + retCode);
		return retCode;
	}
}
