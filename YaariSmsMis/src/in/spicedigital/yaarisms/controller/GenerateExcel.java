package in.spicedigital.yaarisms.controller;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.formula.functions.Replace;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import in.spicedigital.yaarisms.util.LogStackTrace;

public class GenerateExcel {
	private static final Logger LOGGER = Logger.getLogger(GenerateExcel.class.getName());
	private FileOutputStream fos = null;
	private String columnsNames = null;
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	private String sqlQuery = null;
	private String workBookPath = "./MIS.xlsx";
	private String[] columnsName = null;
	private String[] sheet1_columnsName = null;
	private String[] sheet2_columnsName = null;
	private String sheetNames = null;
	private String[] sheetName = null;
	private XSSFWorkbook workBook = null;
	private XSSFSheet sheet = null;
	private Properties prop = null;
	private int RowCounter = 0;
	private String sheet1_columnsNames = null;
	private String sheet1_sqlQuery = null;
	private String sheet1_sqlmtdQuery = null;
	private String sheet2_columnsNames = null;
	private String sheet2_sqlQuery = null;
	private int ColCounter = 2;
	private String dbUrl = null;
	private String dbUserName = null;
	private String dbPassword = null;
	private String kpiNames = null;
	private String[] kpiName = null;
	private String circles = null;
	private String date = null;
	private String yearMonth = null;
	private String[] circle = null;
	private XSSFRow SheetRow = null;
	private XSSFCell SheetCell = null;
	private String dbDriverName = "";
	private XSSFCellStyle styleHeader = null;
	private XSSFCellStyle styleHeaderHourly = null;
	private XSSFCellStyle styleColumn = null;
	private String misDate = null;
	private String tableColumns = null;
	private String[] tableColumn = null;

	public GenerateExcel() {
		prop = new Properties();
		PropertyConfigurator.configure("./log4j.properties");
		try {
			workBook = new XSSFWorkbook();
			prop.load(new DataInputStream(new FileInputStream("./config.properties")));
			sheetNames = prop.getProperty("sheetNames");
			tableColumns = prop.getProperty("tableColumns");
			// sheet1_columnsNames = prop.getProperty("sheet1_columnsNames");
			sheet1_columnsNames = prop.getProperty("sheet1_columnsNames");
			sheet1_sqlmtdQuery = prop.getProperty("sheet1_sqlmtdQuery");
			sheet1_sqlQuery = prop.getProperty("sheet1_sqlQuery");
			sheet2_columnsNames = prop.getProperty("sheet2_columnsNames");
			System.out.println(sheet2_columnsNames);
			sheet2_sqlQuery = prop.getProperty("sheet2_sqlQuery");
			kpiNames = prop.getProperty("kpiNames");
			circles = prop.getProperty("circles");
			columnsNames = prop.getProperty("columnsNames");
			dbDriverName = prop.getProperty("dbDriverName");
			dbUrl = prop.getProperty("dbUrl");
			dbUserName = prop.getProperty("dbUserName");
			dbPassword = prop.getProperty("dbPassword");
			sqlQuery = prop.getProperty("sqlQuery");
			setStyle();
		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		}

	}

	public void setStyle() {
		styleHeader = workBook.createCellStyle();
		styleHeader.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		styleHeader.setAlignment(XSSFCellStyle.ALIGN_LEFT);
		styleHeader.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		styleHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		styleHeader.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderTop(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderRight(XSSFCellStyle.BORDER_THIN);
		XSSFFont mergeCellFont = workBook.createFont();
		mergeCellFont.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		styleHeader.setFont(mergeCellFont);

		styleHeaderHourly = workBook.createCellStyle();
		styleHeaderHourly.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		styleHeaderHourly.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleHeaderHourly.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		styleHeaderHourly.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		styleHeaderHourly.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		styleHeaderHourly.setBorderTop(XSSFCellStyle.BORDER_THIN);
		styleHeaderHourly.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		styleHeaderHourly.setBorderRight(XSSFCellStyle.BORDER_THIN);
		XSSFFont mergeCellFontHourly = workBook.createFont();
		mergeCellFontHourly.setBoldweight(XSSFFont.BOLDWEIGHT_NORMAL);
		styleHeader.setFont(mergeCellFontHourly);

		styleColumn = workBook.createCellStyle();
		styleColumn.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleColumn.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		styleColumn.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		styleColumn.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		styleColumn.setBorderTop(XSSFCellStyle.BORDER_THIN);
		styleColumn.setBorderRight(XSSFCellStyle.BORDER_THIN);
	}

	public void excelProcess(String misDate) {
		this.misDate = misDate;// yyyymmdd
		this.date = misDate.substring(6, 8);
		this.yearMonth = misDate.substring(0, 6);
		LOGGER.debug("date[" + date + "]yearMonth[" + yearMonth + "]");
		try {
			Class.forName(dbDriverName);
			sheetName = sheetNames.split("[,]+");
			for (String sheetCode : sheetName) {
				sheet = workBook.createSheet(sheetCode);
				System.out.println("sheetCode-->[" + sheetCode + "]");
				if (sheetCode.equalsIgnoreCase("yaariSmsDateWiseMis")) {
					getyaariSmsDateWiseMisExcel();
				} else if (sheetCode.equalsIgnoreCase("yaariSmsHoulryMis")) {
					getyaariSmsHoulryMis();
				} else {
					LOGGER.debug("sheet handling is not there.");
				}
			}
			saveExcel();
		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public void saveExcel() {
		try {
			saveExcels("./misData/!dea_YAARI_SMS_MIS_" + misDate + ".xlsx");
		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public void getyaariSmsHoulryMis() throws SQLException {
		try {
			SheetRow = sheet.createRow(0);
			SheetRow = sheet.createRow(1);

			SheetRow = sheet.getRow(1);
			SheetCell = SheetRow.createCell(0);
			SheetCell.setCellStyle(styleHeaderHourly);
			SheetRow = sheet.getRow(1);
			SheetCell = SheetRow.createCell(1);
			SheetCell.setCellStyle(styleHeaderHourly);
			SheetRow = sheet.getRow(1);
			SheetCell = SheetRow.createCell(2);
			SheetCell.setCellStyle(styleHeaderHourly);
			SheetRow = sheet.getRow(0);
			SheetCell = SheetRow.createCell(25);
			SheetCell.setCellStyle(styleHeaderHourly);
			SheetCell = SheetRow.createCell(0);
			SheetCell.setCellStyle(styleHeaderHourly);
			SheetCell.setCellValue("MIS_DATE");
			SheetCell.setCellStyle(styleHeaderHourly);
			sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 0));
			SheetCell = SheetRow.createCell(1);
			SheetCell.setCellValue("KPIs");
			SheetCell.setCellStyle(styleHeaderHourly);
			sheet.addMergedRegion(new CellRangeAddress(0, 1, 1, 1));
			SheetCell = SheetRow.createCell(2);
			SheetCell.setCellValue("HOURS");
			SheetCell.setCellStyle(styleHeaderHourly);
			sheet.addMergedRegion(CellRangeAddress.valueOf("C1:Z1"));
			SheetRow = sheet.getRow(1);
			int k = 0;
			for (int i = 2; i <= 25; i++) {
				SheetCell = SheetRow.createCell(i);
				SheetCell.setCellValue(k);
				SheetCell.setCellStyle(styleHeaderHourly);
				k++;
			}

			con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
			stmt = con.createStatement();
			LOGGER.debug("connect is [" + con + "]");
			sheet2_columnsName = sheet2_columnsNames.split("[,]+");
			kpiName = kpiNames.split("[,]+");
			RowCounter = 2;
			ColCounter = 0;
			LOGGER.debug(misDate);
			sheet2_sqlQuery = sheet2_sqlQuery.replaceAll("YYYYMMDD", misDate);
			LOGGER.debug("Query is [" + sheet2_sqlQuery + "]");
			for (String kpiCode : kpiName) {
				sqlQuery = sheet2_sqlQuery;
				sqlQuery = sqlQuery.replaceAll("@kpiName", kpiCode);
				LOGGER.debug("Query is [" + sqlQuery + "]");
				rs = stmt.executeQuery(sqlQuery);
				while (rs.next()) {
					SheetRow = sheet.createRow(RowCounter);
					for (String columnsCode : sheet2_columnsName) {
						SheetCell = SheetRow.createCell(ColCounter);
						SheetCell.setCellStyle(styleColumn);
						if (columnsCode.equalsIgnoreCase("Mis_Date")) {
							SheetCell.setCellValue(rs.getString(columnsCode.trim()));
						} else if (columnsCode.equalsIgnoreCase("kpiName")) {
							sheet.setColumnWidth(1, 10000);
							String kpName = rs.getString(columnsCode.trim());
							if (kpName.equalsIgnoreCase("TotalInitiatedSms")) {
								kpName = "Total SMS initiated by users";
							} else if (kpName.equalsIgnoreCase("TotalReceivedSms")) {
								kpName = "Total SMS received by agents";
							} else if (kpName.equalsIgnoreCase("TotalSentSms")) {
								kpName = "Total SMS sent by agents";
							} else if (kpName.equalsIgnoreCase("TotalPendingUU")) {
								kpName = "Total Unique users pending for reply";
							} else if (kpName.equalsIgnoreCase("TotalSentSmsUU")) {
								kpName = "Total Unique users who have sent SMS to agents";
							} else if (kpName.equalsIgnoreCase("TotalReceivedUU")) {
								kpName = "Total Unique users who have received SMS by agents";
							} else if (kpName.equalsIgnoreCase("TotalInappropriateUsersRptByAgent")) {
								kpName = "Total users been reported Inappropriate by agents";
							} else if (kpName.equalsIgnoreCase("TotalInappropriateEnggUser")) {
								kpName = "Total Inappropriate engaged users";
							} else if (kpName.equalsIgnoreCase("TotalRevenue")) {
								kpName = "Total Revenue";
							} else {
								kpName = kpName;
							}
							SheetCell.setCellValue(kpName);
						} else {
							SheetCell.setCellValue(rs.getInt(columnsCode.trim()));
						}
						ColCounter++;
					}
					RowCounter++;
					ColCounter = 0;
				}
				rs.close();
			}
		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		} finally {
			if (rs != null || !rs.isClosed()) {
				rs.close();
			}
			if (stmt != null || !stmt.isClosed()) {
				stmt.close();
			}
			if (con != null || !con.isClosed()) {
				con.close();
			}
		}
	}

	public void getyaariSmsDateWiseMisExcel() throws SQLException {
		try {
			tableColumn = tableColumns.split("[,]+");
			LOGGER.debug("dbUrl["+dbUrl+"] dbUserName["+dbUserName+"]dbPassword["+dbPassword+"]");
			con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
			LOGGER.debug("connect is [" + con + "]");
			sheet1_columnsName = sheet1_columnsNames.split("[,]+");
			RowCounter = 0;
			ColCounter = 0;
			for (String columnsCode : sheet1_columnsName) {
				sheet.setColumnWidth(0, 10000);
				SheetRow = sheet.createRow(RowCounter);
				SheetCell = SheetRow.createCell(ColCounter);
				SheetCell.setCellStyle(styleHeader);
				SheetCell.setCellValue(columnsCode);
				RowCounter++;
			}
			stmt = con.createStatement();
			String Query = sheet1_sqlQuery;
			ColCounter = 2;
			for (int dateOfMis = 1; dateOfMis <= Integer.parseInt(date); dateOfMis++) {
				if (dateOfMis < 10) {
					misDate = yearMonth + "0" + dateOfMis;
				} else {
					misDate = yearMonth + dateOfMis;
				}
				stmt = con.createStatement();

				if (dateOfMis == Integer.parseInt(date)) {
					RowCounter = 0;
					sheet1_sqlmtdQuery = sheet1_sqlmtdQuery.replaceAll("YYYYMMDD", misDate);
					rs = stmt.executeQuery(sheet1_sqlmtdQuery);
					LOGGER.debug("sqlquery hase been executed [" + sheet1_sqlmtdQuery + "]");
					int mtdColCounter = 1;
					while (rs.next()) {
						for (String colName : tableColumn) {
							if (colName.equalsIgnoreCase("MIS_DATE")) {
								SheetRow = sheet.getRow(RowCounter);
								SheetCell = SheetRow.createCell(mtdColCounter);
								SheetCell.setCellValue("MTD");
								SheetCell.setCellStyle(styleHeader);
							} else {
								int value = rs.getInt(colName);
								SheetRow = sheet.getRow(RowCounter);
								SheetCell = SheetRow.createCell(mtdColCounter);
								SheetCell.setCellStyle(styleColumn);
								SheetCell.setCellValue(value);
							}
							RowCounter++;
						}
					}
				}
				LOGGER.debug(misDate);
				sheet1_sqlQuery = Query;
				sheet1_sqlQuery = sheet1_sqlQuery.replaceAll("YYYYMMDD", misDate);
				LOGGER.debug("Query is [" + sheet1_sqlQuery + "]");
				rs = stmt.executeQuery(sheet1_sqlQuery);
				RowCounter = 0;
				while (rs.next()) {
					for (String colName : tableColumn) {
						if (colName.equalsIgnoreCase("MIS_DATE")) {
							String MIS_DATE = rs.getString("MIS_DATE");
							SheetRow = sheet.getRow(RowCounter);
							SheetCell = SheetRow.createCell(ColCounter);
							SheetCell.setCellValue(MIS_DATE.toString());
							SheetCell.setCellStyle(styleHeader);
						} else {
							int value = rs.getInt(colName);
							SheetRow = sheet.getRow(RowCounter);
							SheetCell = SheetRow.createCell(ColCounter);
							SheetCell.setCellStyle(styleColumn);
							SheetCell.setCellValue(value);
						}
						RowCounter++;
					}
					ColCounter++;
				}
				rs.close();
			}

		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		} finally {
			if (rs != null || !rs.isClosed()) {
				rs.close();
			}
			if (stmt != null || !stmt.isClosed()) {
				stmt.close();
			}
			if (con != null || !con.isClosed()) {
				con.close();
			}
		}

	}

	public void saveExcels(String workBookPath) throws IOException {
		this.workBookPath = workBookPath; // for saving file to the
		try {
			fos = new FileOutputStream(workBookPath); // saves xlsx
			workBook.write(fos);
			fos.close();
		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			fos.close();
		}
	}
}
