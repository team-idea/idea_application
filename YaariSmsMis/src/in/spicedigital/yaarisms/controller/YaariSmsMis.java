package in.spicedigital.yaarisms.controller;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


import in.spicedigital.yaarisms.util.LogStackTrace;
import in.spicedigital.yaarisms.util.MailHelper;
import in.spicedigital.yaarisms.util.Zipper;

public class YaariSmsMis {
	private static final Logger LOGGER = Logger.getLogger(YaariSmsMis.class);
	private Properties prop = null;
	private static int dateDiff = 1;
	private static String misDate;
	private static String zipperFlag = "true";

	public YaariSmsMis() {
		PropertyConfigurator.configure("./log4j.properties");
		prop = new Properties();
		try {
			prop.load(new DataInputStream(new FileInputStream("./config.properties")));
			dateDiff = Integer.parseInt(prop.getProperty("dateDiff"));
			zipperFlag=prop.getProperty("zipperFlag");
		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public static void main(String[] args) {
		YaariSmsMis yaariSmsMis = new YaariSmsMis();		
		LOGGER.debug("program is started.");
		misDate=yaariSmsMis.getDate(dateDiff);		
		GenerateExcel generateExcel = new GenerateExcel();
		generateExcel.excelProcess(misDate);
		//./misData/!dea_YAARI_SMS_MIS" + misDate + ".xlsx"
		if(zipperFlag.equalsIgnoreCase("true")){
			Zipper zip = new Zipper();
			zip.getZipper("./misData/!dea_YAARI_SMS_MIS_" + misDate + ".xlsx",misDate);
			MailHelper mailHelper = new MailHelper();
			mailHelper.sendMailRequest("./misData/!dea_YAARI_SMS_MIS_" + misDate + ".zip");					
		}else{
			MailHelper mailHelper = new MailHelper();
			mailHelper.sendMailRequest("./misData/!dea_YAARI_SMS_MIS_" + misDate + ".xlsx");								
		}
			LOGGER.debug("Program is END..");		
	}

	private String getDate(int dateDiff) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -dateDiff);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String strDate = formatter.format(cal.getTime());
		LOGGER.debug("date return[" + strDate.toString() + "]");
		return strDate;
	}

}
