package com.spice.yaariAgent;

import org.apache.log4j.Logger;

public class YaariAgentMain {
	private static Logger logger = Logger.getLogger(YaariAgentMain.class.getName());

	public static void main(String[] args) {
		logger.debug("Program START");
		AgentData agentData = new AgentData();
		String workBookPath = agentData.getDbConnection();
		MailHelper mailHelper = new MailHelper();
		mailHelper.sendMailRequest(workBookPath);
		logger.debug("Program END");
	}
}
