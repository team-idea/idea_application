package com.spice.yaari.wap.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.spice.yaari.wap.util.DBConnectionManager;



/**
 * Servlet implementation class UnsubRequestYaari
 */
@WebServlet("/UnsubRequestYaari")
public class UnsubRequestYaari extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final Logger LOGGER=Logger.getLogger(BlockUnblockYaari.class);
	public static DBConnectionManager connectionManager=null;
	
	
    /**
     * Default constructor. 
     */
    public UnsubRequestYaari() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ani=null;
		String mode=null;
		String circle=null;
		String srvkey=null;		

		if(null!=request.getQueryString()){
			try{
				LOGGER.debug((Object) "Query String got as ["+request.getQueryString()+"]");
				
				ani= request.getParameter("ANI").trim();
				mode=request.getParameter("MODE").trim();
				circle=request.getParameter("CIRCLE").trim();
				srvkey= request.getParameter("SRVKEY").trim();
				if(srvkey.contains("#")){
					srvkey=srvkey.replace("#","");
				}
				
				String dbResponse=null;
				CallableStatement callableStatement=null;
				Connection connMaria=null;
				PrintWriter out=response.getWriter();
				
				//LOGGER.debug("request.getContextPath() "+request.getServletContext().getContextPath());
				
				//if(null!=ani && null!=circle && ani.length()==10){
				if(this.validateQueryParameters(request, response)){
					try{
						connMaria=connectionManager.getConnection("mariadb");
						LOGGER.debug("Connection got as ["+connMaria+"]");
						callableStatement=connMaria.prepareCall("{call yaariUnsubRequest(?,?,?,?,?)}");
						callableStatement.setString(1,ani);
						callableStatement.setString(2,mode);
						callableStatement.setString(3,circle);
						callableStatement.setString(4,srvkey);
						callableStatement.registerOutParameter(5, java.sql.Types.VARCHAR);
						callableStatement.execute();
						dbResponse=callableStatement.getString(5);
						out.print(dbResponse);
						LOGGER.debug("DB Response returned is ["+dbResponse+"]");
						//out.print(callableStatement.getString(5));
					}catch(Exception ex){
						response.getWriter().println("ERROR");
						LOGGER.error((Object) "Error while calling procedure");
						LOGGER.error(getStackTrace(ex));
					}finally{
						if(null!=connMaria){
							connectionManager.freeConnection("mariadb", connMaria);
						}
						if(null!=callableStatement){
							try {
								callableStatement.close();
							} catch (SQLException ex) {
								response.getWriter().println("ERROR");
								LOGGER.error(getStackTrace(ex));
							}
							if(null!=out){
								out.flush();
								out.close();
							}
						}
					}
				}//else{
//					LOGGER.error((Object) "One or more query parameter(s) is null [ani] ["+ani+"] [circle] ["+circle+"]" );
//				}
			}catch(Exception ex){
				response.getWriter().println("ERROR");
				LOGGER.error("Exception while processing Query String");
				LOGGER.error((Object) "One of the In parametes is null [ani] ["+ani+"] [circle] ["+circle+"] [MODE] ["+mode+"] [SRVKEY] ["+srvkey+"]" );
				LOGGER.error(getStackTrace(ex));
			}
		}else{
			LOGGER.error("Query String got as null request.getQueryString() ["+request.getQueryString()+"]");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
	}

	public void init(ServletConfig servletConfig){
		try{
			PropertyConfigurator.configure("log4j.properties");
			LOGGER.debug("Configuration file loaded successfullly");
		}catch(Exception exception){
			LOGGER.error("/log4j.properties file not found");
			LOGGER.error(getStackTrace(exception));
		}
		
		try{
			connectionManager=DBConnectionManager.getInstance();
			LOGGER.debug("Got instance of DBConnectionManager ["+connectionManager+"]");
		}catch(Exception exception){
			exception.printStackTrace();
			LOGGER.error("Exception while getting instance of DBConnectionManager");
			LOGGER.error(getStackTrace(exception));
		}
		
	}

    private synchronized String getStackTrace(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        t.printStackTrace(pw);
        pw.flush();
        sw.flush();
        return sw.toString();
    }

    private boolean validateQueryParameters(ServletRequest servletRequest,ServletResponse servletResponse){
    	boolean isValid=false;
    	try{
    		if(servletRequest.getParameter("ANI").length()!=10){    		
    			servletResponse.getWriter().println("Length of ANI must be equal to 10");
    			LOGGER.error((Object) "Length of ANI must be equal to 10");
    			isValid=false;
    		}else if(servletRequest.getParameter("CIRCLE").length()!=2){
    			servletResponse.getWriter().println("Length of CIRCLE must be equal to 2");
    			LOGGER.error((Object) "Length of CIRCLE must be equal to 2");
    			isValid=false;
    		}else if(servletRequest.getParameter("MODE")==null || servletRequest.getParameter("MODE").equalsIgnoreCase("")){    			
    			servletResponse.getWriter().println("Please provide a valid MODE");
    			LOGGER.error((Object) "Please provide a valid MODE");
    			isValid=false;    			
    		}else if(servletRequest.getParameter("SRVKEY")==null || servletRequest.getParameter("SRVKEY").equalsIgnoreCase("")){    			
    			servletResponse.getWriter().println("Please provide a valid SRVKEY");
    			LOGGER.error((Object) "Please provide a valid SRVKEY");
    			isValid=false;    			
    		}
    		else{
    			isValid=true;
    		}
    	}catch(Exception e){
    		LOGGER.error((Object) "Exception while validationg request parametes");
    		LOGGER.error((Object) getStackTrace(e));
    	}
    	return isValid;
    }
    
}
