package com.spice.yaari.wap.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Enumeration;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.spice.yaari.wap.util.DBConnectionManager;



/**
 * Servlet implementation class SendYaariOTP
 */
@WebServlet("/MessagingApi")
public class MessagingApi extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final Logger LOGGER=Logger.getLogger(MessagingApi.class);
	public static DBConnectionManager connectionManager=null;
	
	
    /**
     * Default constructor. 
     */
    public MessagingApi() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ani=null;
		String circle=null;
		String message=null;
		String priority=null;
		String type=null;
		String cli=null;
		


		if(null!=request.getQueryString()){
			try{
				LOGGER.debug((Object) "Query String got as ["+request.getQueryString()+"]");
				
				Enumeration<String> parameterNames = request.getParameterNames();
				while (parameterNames.hasMoreElements()) {
				    if(parameterNames.nextElement().equalsIgnoreCase("cli")){
				    	 cli= request.getParameter("cli").trim();
				    }				     				    	
				}

				if(cli==null){
					cli="00000";
				}
				ani= request.getParameter("ani").trim();
				circle= request.getParameter("circle").trim();
				message= request.getParameter("message").trim();
				priority= request.getParameter("priority").trim();
				type= request.getParameter("type").trim();
				
				//cli= request.getParameter("cli").trim();
				//if(cli.contains("#")){
				//cli=cli.replace("#","");
				//}
				
				String dbResponse=null;
				CallableStatement callableStatement=null;
				Connection connMaria=null;
				PrintWriter out=response.getWriter();
				
				//LOGGER.debug("request.getContextPath() "+request.getServletContext().getContextPath());
				
				//if(null!=ani && null!=circle && ani.length()==10){
				if(this.validateQueryParameters(request, response)){
					try{
						connMaria=connectionManager.getConnection("mariadb");
						LOGGER.debug("Connection got as ["+connMaria+"]");
						callableStatement=connMaria.prepareCall("{call procYaariMessages(?,?,?,?,?,?,?)}");
						callableStatement.setString(1,ani);
						callableStatement.setString(2,circle);
						callableStatement.setString(3,message);
						callableStatement.setString(4,priority);
						callableStatement.setString(5,type);
						callableStatement.setString(6,cli);						
						callableStatement.registerOutParameter(7, java.sql.Types.VARCHAR);
						callableStatement.execute();
						dbResponse=callableStatement.getString(7);
						out.print(dbResponse);
						LOGGER.debug("DB Response returned is ["+dbResponse+"]");
						//out.print(callableStatement.getString(5));
					}catch(Exception ex){
						response.getWriter().println("ERROR");
						LOGGER.error((Object) "Error while calling procedure");
						LOGGER.error(getStackTrace(ex));
					}finally{
						if(null!=connMaria){
							connectionManager.freeConnection("mariadb", connMaria);
						}
						if(null!=callableStatement){
							try {
								callableStatement.close();
							} catch (SQLException ex) {
								response.getWriter().println("ERROR");
								LOGGER.error(getStackTrace(ex));
							}
							if(null!=out){
								out.flush();
								out.close();
							}
						}
					}
				}//else{
//					LOGGER.error((Object) "One or more query parameter(s) is null [ani] ["+ani+"] [circle] ["+circle+"]" );
//				}
			}catch(Exception ex){
				response.getWriter().println("ERROR");
				LOGGER.error("Exception while processing Query String");
				LOGGER.error((Object) "One of the In parametes is null [ani] ["+ani+"] [circle] ["+circle+"] [MESSAGE] ["+message+"]");
				LOGGER.error(getStackTrace(ex));
			}
		}else{
			LOGGER.error("Query String got as null request.getQueryString() ["+request.getQueryString()+"]");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {		
	}

	public void init(ServletConfig servletConfig){
		try{
			PropertyConfigurator.configure("log4j.properties");
			LOGGER.debug("Configuration file loaded successfullly");
		}catch(Exception exception){
			LOGGER.error("/log4j.properties file not found");
			LOGGER.error(getStackTrace(exception));
		}
		
		try{
			connectionManager=DBConnectionManager.getInstance();
			LOGGER.debug("Got instance of DBConnectionManager ["+connectionManager+"]");
		}catch(Exception exception){
			exception.printStackTrace();
			LOGGER.error("Exception while getting instance of DBConnectionManager");
			LOGGER.error(getStackTrace(exception));
		}
		
	}

    private synchronized String getStackTrace(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        t.printStackTrace(pw);
        pw.flush();
        sw.flush();
        return sw.toString();
    }

    private boolean validateQueryParameters(ServletRequest servletRequest,ServletResponse servletResponse){
    	boolean isValid=false;
    	try{
    		if(servletRequest.getParameter("ani").length()!=10){    		
    			servletResponse.getWriter().println("Length of ANI must be equal to 10");
    			LOGGER.error((Object) "Length of ANI must be equal to 10");
    			isValid=false;
    		}else if(servletRequest.getParameter("circle").length()!=2){
    			servletResponse.getWriter().println("Length of CIRCLE must be equal to 2");
    			LOGGER.error((Object) "Length of CIRCLE must be equal to 2");
    			isValid=false;
    		}/*else if(servletRequest.getParameter("message")==null || servletRequest.getParameter("message").equalsIgnoreCase("")){    			
    			servletResponse.getWriter().println("Please provide MESSAGE parameter");
    			LOGGER.error((Object) "Please provide MESSAGE parameter");
    			isValid=false;    			
    		}*/
    		else{
    			isValid=true;
    		}
    	}catch(Exception e){
    		LOGGER.error((Object) "Exception while validationg request parametes");
    		LOGGER.error((Object) getStackTrace(e));
    	}
    	return isValid;
    }
    
}
