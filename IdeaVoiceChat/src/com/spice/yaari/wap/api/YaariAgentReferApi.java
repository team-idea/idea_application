package com.spice.yaari.wap.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.spice.yaari.wap.util.CallHttpUrl;
import com.spice.yaari.wap.util.DBConnectionManager;

/**
 * Servlet implementation class YaariAgentReferApi
 */
@WebServlet("/YaariAgentReferApi")
public class YaariAgentReferApi extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public static final Logger LOGGER=Logger.getLogger(YaariAgentReferApi.class);
	public static DBConnectionManager connectionManager=null;	
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public YaariAgentReferApi() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String ani=null;
		String bni=null;
		String circle=null;
		String message=null;
		String dtmf=null;
		String transId=null;
		String httpUrl=null;
		String[] dbResponseArray=null;
		String httpResponse=null;
		String[] httpResponseArray=null;
		String updateQuery=null;


		if(null!=request.getQueryString()){
			try{
				LOGGER.debug((Object) "Query String got as ["+request.getQueryString()+"]");
				
				ani= request.getParameter("ani").trim();
				bni= request.getParameter("bni").trim();
				dtmf= request.getParameter("dtmf").trim();
				transId= request.getParameter("transId").trim();
				circle= request.getParameter("circle").trim();
				
				if(circle.contains("#")){
					circle=circle.replace("#","");
				}
				
				String dbResponse=null;
				CallableStatement callableStatement=null;
				Connection connMaria=null;
				Connection updateConnection=null;
				Statement updateStatement=null;
				PrintWriter out=response.getWriter();
				
				//LOGGER.debug("request.getContextPath() "+request.getServletContext().getContextPath());
				
				//if(null!=ani && null!=circle && ani.length()==10){
//				if(this.validateQueryParameters(request, response)){
					try{
						
						connMaria=connectionManager.getConnection("mariadb");
						updateConnection=connectionManager.getConnection("mariadb");
						updateStatement=updateConnection.createStatement();
						LOGGER.debug("Connection got as ["+connMaria+"]");
						callableStatement=connMaria.prepareCall("{call procYaariAgentRefer(?,?,?,?,?,?)}");
						callableStatement.setString(1,ani);
						callableStatement.setString(2,bni);
						callableStatement.setString(3,circle);
						callableStatement.setString(4,dtmf);
						callableStatement.setString(5,transId);
						callableStatement.registerOutParameter(6, java.sql.Types.VARCHAR);
						callableStatement.execute();
						dbResponse=callableStatement.getString(6);
						//out.print(dbResponse);
						LOGGER.debug("DB Response returned is ["+dbResponse+"]");
						dbResponseArray=dbResponse.split("#");//uid#agent_id#agent_circle
						
						httpUrl="http://192.168.9.87:8091/YaarriServlets/YaarriCallBackApi?service=YAARRI&usercircle="+circle.toUpperCase()+"&operator=IDEA&ani="+ani+"&agent_id="+dbResponseArray[1].trim()+"&agentcircle="+dbResponseArray[2].trim().toUpperCase()+"&bni="+bni+"&uid="+dbResponseArray[0].trim();
						LOGGER.debug("httpUrl ["+httpUrl+"]");
						httpResponse=new CallHttpUrl().callSubmitUrl(httpUrl).trim();
						LOGGER.debug("httpResponse ["+httpResponse+"]");
						if(httpResponse.equalsIgnoreCase("FAIL") || null==httpResponse){							
							updateQuery="update tbl_yaari_agent_refer set app_txn_id='Response Failure',status=-10 where refid='"+dbResponseArray[0].trim()+"'";
							LOGGER.debug("updateQuery ["+updateQuery+"]");
							updateStatement.execute(updateQuery);
							out.print("APP API FAILURE");
						}else{
							httpResponseArray=httpResponse.split("#");							
							updateQuery="update tbl_yaari_agent_refer set app_txn_id='"+httpResponseArray[1].trim()+"',status="+Integer.parseInt(httpResponseArray[0].trim())+" where refid='"+httpResponseArray[2].trim()+"'";
							LOGGER.debug("updateQuery ["+updateQuery+"]");
							updateStatement.execute(updateQuery);
							out.print("SUCCESS");
						}
						//out.print("SUCCESS");
					}catch(Exception ex){
						updateQuery="update tbl_yaari_agent_refer set app_txn_id='Response Exception',status=-9 where refid='"+dbResponseArray[0].trim()+"'";
						LOGGER.debug("updateQuery ["+updateQuery+"]");
						updateStatement.execute(updateQuery);						
						response.getWriter().println("ERROR");
						LOGGER.error((Object) "Error while calling procedure");
						LOGGER.error(getStackTrace(ex));
					}finally{
						if(null!=connMaria){
							connectionManager.freeConnection("mariadb", connMaria);
						}
						if(null!=updateConnection){
							connectionManager.freeConnection("mariadb", updateConnection);
						}						
						if(null!=updateStatement){							
							updateStatement.close();
						}
						if(null!=callableStatement){
							try {
								callableStatement.close();
							} catch (SQLException ex) {
								response.getWriter().println("ERROR");
								LOGGER.error(getStackTrace(ex));
							}
							if(null!=out){
								out.flush();
								out.close();
							}
						}
					}
//20161226//				}//else{
//					LOGGER.error((Object) "One or more query parameter(s) is null [ani] ["+ani+"] [circle] ["+circle+"]" );
//				}
			}catch(Exception ex){
				response.getWriter().println("ERROR");
				LOGGER.error("Exception while processing Query String");
				LOGGER.error((Object) "One of the In parametes is null [ani] ["+ani+"] [circle] ["+circle+"] [MESSAGE] ["+message+"]");
				LOGGER.error(getStackTrace(ex));
			}
		}else{
			LOGGER.error("Query String got as null request.getQueryString() ["+request.getQueryString()+"]");
		}
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

    private synchronized String getStackTrace(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        t.printStackTrace(pw);
        pw.flush();
        sw.flush();
        return sw.toString();
    }	

	public void init(ServletConfig servletConfig){
		try{
			PropertyConfigurator.configure("log4j.properties");
			LOGGER.debug("Configuration file loaded successfullly");
		}catch(Exception exception){
			LOGGER.error("/log4j.properties file not found");
			LOGGER.error(getStackTrace(exception));
		}
		
		try{
			connectionManager=DBConnectionManager.getInstance();
			LOGGER.debug("Got instance of DBConnectionManager ["+connectionManager+"]");
		}catch(Exception exception){
			exception.printStackTrace();
			LOGGER.error("Exception while getting instance of DBConnectionManager");
			LOGGER.error(getStackTrace(exception));
		}
		
	}    
    
}
