package com.spice.yaari.wap.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


public class CallHttpUrl {

	private static final Logger LOGGER=Logger.getLogger(CallHttpUrl.class);
	
	
	public CallHttpUrl(){
		try{
			PropertyConfigurator.configure("log4j.properties");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	
	public String callSubmitUrl(String strUrl) throws IOException {
		String retStr = "";
		String str = null;
		BufferedReader in =null;
		URLConnection urlConn =null;
		int readTimeOut = 2000;
		try {
			//strUrl = strUrl.replaceAll("%", "%25");
			//strUrl = strUrl.replaceAll(" ", "%20");
			//strUrl = strUrl.replaceAll("#", "%23");
			//strUrl = strUrl.replaceAll("%%", "%");
			URL urlObject = new URL(strUrl);
			urlConn = urlObject.openConnection();
			urlConn.setReadTimeout(readTimeOut);
			HttpURLConnection httpURLConnection=(HttpURLConnection)urlConn;
			//LOGGER.debug((Object)strUrl+"|"+httpURLConnection.getResponseCode()+"|"+httpURLConnection.getResponseMessage()+str);
			//LOGGER.debug("Response Code ["+httpURLConnection.getResponseCode()+"] Response Message ["+httpURLConnection.getResponseMessage()+"]");
//			String header = String.valueOf(urlConn.getHeaderFields());
//			if (header.indexOf("200 OK") > 0)
//				header = "[HTTP/1.1 200 OK]";
//			else{
//				header = "error";
//			}
			in = new BufferedReader(new InputStreamReader(urlConn.getInputStream()));
			while ((str = in.readLine()) != null){
				//retStr += str;
				retStr=retStr+str;
				//retStr=retStr.concat(str);			
			}
			LOGGER.debug((Object)strUrl+"|"+httpURLConnection.getResponseCode()+"|"+httpURLConnection.getResponseMessage()+"|"+retStr);
		} catch (Exception exp) {
			LOGGER.error(LogStackTrace.getStackTrace(exp));
			//exp.printStackTrace();
			retStr = "error";
		}finally{			
			if(null!=in){				
				in.close();
			}
			if(null!=urlConn){
			}
		}
		retStr = retStr.replace("$", "\r\n");
		return retStr;
	}
	
public static String encodeMessage(String message){
	String encodedMessage=null;
	try{
		encodedMessage = URLEncoder.encode(message, "UTF-8");
	}catch(Exception e){
	}
	return encodedMessage;
}
	
	
}
