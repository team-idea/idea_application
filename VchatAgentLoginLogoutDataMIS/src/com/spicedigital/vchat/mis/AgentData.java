package com.spicedigital.vchat.mis;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.formula.functions.Replace;

public class AgentData {
	private static Logger logger = Logger.getLogger(AgentData.class.getName());
	private Properties prop = null;
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	private String dbUrl = null;
	private String dbUserName = null;
	private String dbPassword = null;
	private String driverName = null;
	private String sqlQuery = null;
	private String mis_Date =null;

	private Timestamp misDate = null;
	private String circle = null;
	private String ani = null;
	private String chatID = null;
	private String loginLogout = null;
	private String loginAttempts = null;
	private String logoutAttempts = null;
	private String circles = "";
	private String[] circleName;
	private int loginTimeInSeconds = 0;
	private int loginTimeInMins = 0;
	private int loginTimeInHours = 0;
	private int logoutTimeInSeconds = 0;
	private int logoutTimeInMins = 0;
	private int logoutTimeInHours = 0;
	private int TotalMou = 0;
	private CreateExcel createExcel = null;
	private String dateDiff = "1";
	public String MailerMISDate = "201701";

	public AgentData() {
		PropertyConfigurator.configure("./log4j.properties");
		prop = new Properties();
		try {
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			driverName = prop.getProperty("driverName");
			circles = prop.getProperty("circles");
			sqlQuery = prop.getProperty("sqlQuery");
			dateDiff = prop.getProperty("dateDiff");
			System.out.println(driverName + "," + dateDiff);
			// logger.debug(sqlQuery);
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public String getDate(int datediff) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -datediff);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String strDate = formatter.format(cal.getTime());
		return strDate;
	}

	public String getDbConnection() throws SQLException {
		String workBookPath = "./misdata/dummySheet.xlsx";
		try {
			createExcel = new CreateExcel();
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
		try {
			circleName = circles.split("[,]+");
			Class.forName(driverName);
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
		int rowIndex = 1;
		for (String circleCode : circleName) {
			try {
				logger.debug("circle[" + circleCode + "]----------------------["+ prop.getProperty(circleCode + "_dbUrl") + "]");
				con = DriverManager.getConnection(prop.getProperty(circleCode + "_dbUrl"),prop.getProperty(circleCode + "_dbUserName"), prop.getProperty(circleCode + "_dbPassword"));
				stmt = con.createStatement();
				sqlQuery = sqlQuery.replaceAll("dateDiff", dateDiff);
				logger.debug(sqlQuery);
				rs = stmt.executeQuery(sqlQuery);
				
				while (rs.next()) {
					mis_Date = rs.getString("Mis_Date");
					circle = rs.getString("circle");
					ani = rs.getString("ani");
					chatID = rs.getString("chat_id");
					loginLogout = rs.getString("login_logout");
					loginAttempts = rs.getString("login_attempts");
					logoutAttempts = rs.getString("logout_attempts");
					loginTimeInSeconds = rs.getInt("login_time_in_seconds");
					loginTimeInMins = rs.getInt("login_time_in_mins");
					loginTimeInHours = rs.getInt("login_time_in_hours");
					logoutTimeInSeconds = rs.getInt("logout_time_in_seconds");
					logoutTimeInMins = rs.getInt("logout_time_in_mins");
					logoutTimeInHours = rs.getInt("logout_time_in_hours");
					TotalMou = rs.getInt("total_mou");
					String excelRow = mis_Date + "#" + circle + "#" + ani + "#" + chatID + "#" + loginLogout + "#"
							+ loginAttempts + "#" + logoutAttempts + "#" + loginTimeInSeconds + "#" + loginTimeInMins
							+ "#" + loginTimeInHours + "#" + logoutTimeInSeconds + "#" + logoutTimeInMins + "#"
							+ logoutTimeInHours + "#" + TotalMou;
					createExcel.getRow(rowIndex, excelRow);
					rowIndex++;
				}
				rs.close();
				stmt.close();
				con.close();
			} catch (Exception ex) {
				if (rs != null || rs.isClosed())
					rs.close();
				if (stmt != null || stmt.isClosed())
					stmt.close();
				if (con != null || con.isClosed())
					con.close();
				logger.debug(LogStackTrace.getStackTrace(ex));
			} 
		}

		try {
			MailerMISDate = getDate(Integer.parseInt(dateDiff));
			workBookPath = createExcel.saveExcels(MailerMISDate);
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
		return workBookPath;
	}
}
