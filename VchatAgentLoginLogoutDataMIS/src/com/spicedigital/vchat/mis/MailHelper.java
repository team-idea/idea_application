package com.spicedigital.vchat.mis;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import java.util.PropertyPermission;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class MailHelper {

	private static final Logger LOGGER = Logger.getLogger(MailHelper.class.getName());
	private Properties prop = new Properties();
	private String kmpid = "";
	private String pwd = "";
	private String to = "";
	private String cc = "";
	private String bcc = "";
	private String from = "";
	private String messageSubject = "";
	private String messageBody = "";
	private String fileName = "";
	private String smtpServ = "74.125.130.108";
	private int dateDiff = 1;
	private Properties propDB = new Properties();
	private String workBookPath = "./misData/dummySheet.xlsx";

	MailHelper() {
		try {
			PropertyConfigurator.configure("./log4j.properties");
			prop.load(new DataInputStream(new FileInputStream("./mail.properties")));
			propDB.load(new DataInputStream(new FileInputStream("./db.properties")));
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.error(LogStackTrace.getStackTrace(ex));
		}
		smtpServ = prop.getProperty("smtpServ");
		kmpid = prop.getProperty("mail_kmpid");
		pwd = prop.getProperty("mail_pwd");
		to = prop.getProperty("mail_to");
		cc = prop.getProperty("mail_cc");
		bcc = prop.getProperty("mail_bcc");
		from = prop.getProperty("mail_from");
		dateDiff = Integer.parseInt(propDB.getProperty("dateDiff"));
		// fileName=prop.getProperty("fileName");
		messageSubject = prop.getProperty("mail_messageSubject");
		messageBody = prop.getProperty("messageBody");
	}

	public void sendMailRequest(String workBookPath) {
		AgentData agentData = new AgentData();

		this.workBookPath = workBookPath;
		// String fileDate=getDate(dateDiff);
		fileName = workBookPath;
		messageSubject = messageSubject.replaceAll("YYMMDD", agentData.getDate(dateDiff));
		System.out.println("------>" + fileName);
		System.out.println("kmpis[" + kmpid + "]pwd[" + pwd + "]to[" + to + "]cc[" + cc + "]bcc[" + bcc + "]from["
				+ from + "]messageSubject[" + messageSubject + "]messageBody[" + messageBody + "]zipFileName["
				+ fileName);
		boolean result = sendMail(kmpid, pwd, to, cc, bcc, from, messageSubject, messageBody, fileName);
		if (result == true) {
			System.out.println("mail send..");
		} else {
			System.out.println("mail not send..");
		}
	}

	public boolean sendMail(String kmpid, String pwd, String to, String cc, String bcc, String from,
			String messageSubject, String messageBody, String attachment) {
		try {
			Properties props = System.getProperties();

			// String smtpServ = "smtp.gmail.com";
			// smtpServ = "74.125.68.109";
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", smtpServ);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port", "465");
			props.put("mail.smtp.socketFactory.port", "465");
			props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");

			Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {

				protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
					return new javax.mail.PasswordAuthentication("sdpse.idea@spicedigital.in", "p@ss@4321");
				}
			});

			Message msg = new MimeMessage(session);
			msg.setFrom(new InternetAddress(from));
			String toList[] = to.split(",");
			InternetAddress[] addressTo = new InternetAddress[toList.length];
			for (int i = 0; i < toList.length; i++) {
				addressTo[i] = new InternetAddress(toList[i]);
			}
			msg.setRecipients(Message.RecipientType.TO, addressTo);
			String ccList[] = cc.split(",");
			InternetAddress[] addressCC = new InternetAddress[ccList.length];
			for (int i = 0; i < ccList.length; i++) {
				addressCC[i] = new InternetAddress(ccList[i]);
			}
			msg.setRecipients(Message.RecipientType.CC, addressCC);

			msg.setRecipients(Message.RecipientType.TO, addressTo);
			String bccList[] = bcc.split(",");
			InternetAddress[] addressBCC = new InternetAddress[bccList.length];
			for (int i = 0; i < bccList.length; i++) {
				addressBCC[i] = new InternetAddress(bccList[i]);
			}
			msg.setRecipients(Message.RecipientType.BCC, addressBCC);
			msg.setSubject(messageSubject);

			BodyPart messageBodyPart = new MimeBodyPart();

			Multipart multipart = new MimeMultipart();

			/////////////////
			messageBodyPart.setText(messageBody);
			multipart.addBodyPart(messageBodyPart);
			messageBodyPart = new MimeBodyPart();

			System.out.println("atatchement " + attachment);

			DataSource source = new FileDataSource(attachment);
			messageBodyPart.setDataHandler(new DataHandler(source));
			messageBodyPart.setFileName(attachment.substring(attachment.lastIndexOf("/") + 1, attachment.length()));
			multipart.addBodyPart(messageBodyPart);
			// String[] attachmentName=attachment.split("[,]+");
			/*
			 * for (int i = 0; i < attachmentName.length; i++) {
			 * System.out.println(attachmentName[i]); MimeBodyPart
			 * messageBodyPart2 = new MimeBodyPart(); DataSource source = new
			 * FileDataSource(attachmentName[i]);
			 * messageBodyPart2.setDataHandler(new DataHandler(source));
			 * messageBodyPart2.setFileName(attachmentName[i]);
			 * multipart.addBodyPart(messageBodyPart2); }
			 */

			msg.setContent(multipart);

			Transport.send(msg);

			for (int i = 0; i < toList.length; i++) {
				LOGGER.debug("Message sent to " + toList[i] + " OK.");
			}
			for (int i = 0; i < ccList.length; i++) {
				LOGGER.debug("Message sent to[CC] " + ccList[i] + " OK.");
			}
			for (int i = 0; i < bccList.length; i++) {
				LOGGER.debug("Message sent to[BCC] " + bccList[i] + " OK.");
			}

		} catch (Exception ex) {
			LOGGER.error(LogStackTrace.getStackTrace(ex));
			// System.out.println("Exception " + ex);

		}
		LOGGER.debug("mail send");
		return true;
	}

	public String getDate(int datediff) {
		// System.out.println("value of i = "+ i);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -datediff);
		SimpleDateFormat formatter = new SimpleDateFormat("yyMMdd");
		String strDate = formatter.format(cal.getTime());
		LOGGER.debug("filedate[" + strDate + "]");
		return strDate;

	}

}
