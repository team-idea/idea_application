package com.spicedigital.vchat.mis;

import java.sql.SQLException;

import org.apache.log4j.Logger;

public class VchatAgentMain {
	private static Logger logger = Logger.getLogger(VchatAgentMain.class.getName());

	public static void main(String[] args) throws SQLException {
		logger.debug("Program START");
		AgentData agentData = new AgentData();
		String workBookPath = agentData.getDbConnection();
		MailHelper mailHelper = new MailHelper();
		mailHelper.sendMailRequest(workBookPath);
		logger.debug("Program END");
	}
}
