package com.spice.mis.controller;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.Date;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.spice.mis.util.MailHelper;
import com.spice.mis.helper.ExcelGeneratorHelper;
import com.spice.mis.util.LoadPropertiesFile;
import com.spice.mis.util.LogStackTrace;

public class HourlyRevenueMisController {

	private static final Logger LOGGER=Logger.getLogger(HourlyRevenueMisController.class);
	private LoadPropertiesFile loadPropertiesFile=null;
	
	
	public HourlyRevenueMisController(){
		try{
			PropertyConfigurator.configure("log4j.properties");
			loadPropertiesFile= new LoadPropertiesFile();
			loadPropertiesFile.loadPropertiesFile("HourlyMisReport.properties");
		}catch(Exception e){
			LOGGER.error(LogStackTrace.getStackTrace(e));
		}
	}

	public static void main(String[] args) {
		HourlyRevenueMisController hourlyRevenueMisController= new HourlyRevenueMisController();
		try{
			if(hourlyRevenueMisController.accessFile() && hourlyRevenueMisController.getLock()){
				new ExcelGeneratorHelper().generateExcel(1);
				hourlyRevenueMisController.sendMail("Hi\n\nPlease find the attcahed MIS.");
				System.exit(1);
			}
		}catch(Exception e){
			LOGGER.error(LogStackTrace.getStackTrace(e));
		}
	}

    private boolean getLock() {
    	boolean lock=false;
        try {
            RandomAccessFile file = new RandomAccessFile("c:/windows/system/hourlyMisKK.tmp", "rw");
            FileChannel fileChannel = file.getChannel();
            FileLock fileLock = fileChannel.tryLock();
            if (fileLock != null) {
                LOGGER.debug("File is locked. Starting the execution of the Application");
                lock = true;
              } else {
            	  LOGGER.error("Cannot create lock. Already another instance of the program is running.");
            	  LOGGER.error("System shutting down. Please wait");
                try {
                    Thread.sleep(1000 * 3);
                } catch (Exception exp) {
                }
                System.exit(0);
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return lock;
    }

    private boolean accessFile() {
        boolean retCode = false;
        try {
            String line = "";
            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter("c:/windows/system/hourlyMisKK.tmp", false));
                bw.close();
            } catch (Exception exp) {
            }
            retCode = true;
        } catch (Exception e) {
            System.out.println(e);
        }
        LOGGER.debug("accessFile() = " + retCode);
        return retCode;
    }

    private void sendMail(String messageBody){
    	try{
			String kmpid=loadPropertiesFile.getPropertyValue("mail_kmpid");
			String pwd=loadPropertiesFile.getPropertyValue("mail_pwd");
			String to=loadPropertiesFile.getPropertyValue("mail_to");
			String cc=loadPropertiesFile.getPropertyValue("mail_cc");
			String bcc=loadPropertiesFile.getPropertyValue("mail_bcc");
			String from=loadPropertiesFile.getPropertyValue("mail_from");
			String messageSubject=loadPropertiesFile.getPropertyValue("mail_messageSubject")+" "+new Date().toString().substring(4,10).replace(" ", "_");
			String attachment="./DATA/KK_HOUR WISE Voice Chat CALL REV_"+new Date().toString().substring(4,10).replace(" ", "_")+".xls";
			//String attachment="H:/workspace/HourlyRevenueMis/BIHAR_HOUR WISE CALL REV_Dec_16.xls";
			MailHelper.sendMail(kmpid, pwd, to, cc, bcc, from, messageSubject, messageBody, attachment);
    	}catch(Exception e){
    		LOGGER.error(LogStackTrace.getStackTrace(e));
    	}
    }
    
	
}
