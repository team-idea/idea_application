package com.spice.mis.helper;

import java.io.File;
import java.rmi.server.LogStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import jxl.Workbook;
import jxl.WorkbookSettings;
import jxl.write.Label;
import jxl.write.NumberFormats;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;

import org.apache.log4j.Logger;

import com.spice.mis.util.DBConnectionManager;
import com.spice.mis.util.LoadPropertiesFile;
import com.spice.mis.util.LogStackTrace;

public class ExcelGeneratorHelper {
	
	private static final Logger LOGGER=Logger.getLogger(ExcelGeneratorHelper.class);
	private LoadPropertiesFile loadPropertiesFile=null;
	private DBConnectionManager connectionManager=null;
	private Connection connection=null;
	private Statement statement=null;
	private ResultSet resultSet=null;
	private String flag="FLASE";
	
	
	public ExcelGeneratorHelper(){
		this.loadPropertiesFile=new LoadPropertiesFile();
		this.loadPropertiesFile.loadPropertiesFile("HourlyMisReport.properties");
		this.connectionManager=DBConnectionManager.getInstance();
	}
	
	public boolean generateExcel(int dateDiff){
		try{
		    //String misCol[]		= this.loadPropertiesFile.getPropertyValue("misColDetail").trim().split("#");
		    //String misColName[]		= this.loadPropertiesFile.getPropertyValue("misDbColDetail").trim().split("#");
		    //String circleNameColl[]	= this.loadPropertiesFile.getPropertyValue("circleDetail").trim().split("#");		
		    String sheetNames[] = this.loadPropertiesFile.getPropertyValue("sheetNames").trim().split("#");
		    String hours[] = this.loadPropertiesFile.getPropertyValue("hours").trim().split("#");
		    
			WritableCellFormat timesBoldUnderline=null;
			WritableCellFormat dateFont,colFont1,colFont2,colFont_red=null;
			WritableCellFormat timesBoldHeading=null;
			WritableCellFormat times=null;
			
			WorkbookSettings ws = new WorkbookSettings();
			ws.setLocale(new Locale("en", "EN"));
			WritableWorkbook workbook = Workbook.createWorkbook(new File("./DATA/TN_HOUR WISE CALL REV_"+new Date().toString().substring(4,10).replace(" ", "_")+".xls"), ws);
		    
			//this.ppuMisModelList=(ArrayList<PPUMisModel>)new PPUMisService().processRequest(dateDiff);
			//for(PPUMisModel ppuMisModelobject:this.ppuMisModelList){
			//System.out.println("circleNameColl.length ["+circleNameColl.length+"]");

			for(int index=0;index<sheetNames.length;index++){

				WritableSheet sExcel = workbook.createSheet(sheetNames[index], index);
				sExcel.setColumnView(0,35);

				WritableFont wf = new WritableFont(WritableFont.ARIAL, 8);
				WritableCellFormat cf = new WritableCellFormat(wf);

				//Adding Number Format To support Number in Excel Sheet
				WritableCellFormat cfnum=new WritableCellFormat(NumberFormats.INTEGER);
				jxl.write.Number numobj=new jxl.write.Number(0,1,4, cfnum);            // created an object of Number class

				WritableFont times10pt = new WritableFont(WritableFont.TAHOMA, 10);
				// Define the cell format
				times = new WritableCellFormat(times10pt);
				// Lets automatically wrap the cells
				times.setWrap(true);
				times.setBackground(jxl.format.Colour.YELLOW);
				times.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
				
				// Create create a bold font with unterlines
				WritableFont times10ptBoldUnderline = new WritableFont(WritableFont.COURIER, 8,WritableFont.BOLD);
				timesBoldUnderline = new WritableCellFormat(times10ptBoldUnderline);
				//timesBoldUnderline.setWrap(true);
				timesBoldUnderline.setBackground(jxl.format.Colour.VERY_LIGHT_YELLOW);
				timesBoldUnderline.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);

				// Create create a bold font with unterlines
				WritableFont dateFont10pt = new WritableFont(WritableFont.COURIER, 8,WritableFont.BOLD);
				dateFont = new WritableCellFormat(dateFont10pt);
				dateFont.setBackground(jxl.format.Colour.ROSE);
				dateFont.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);


				// Create create a bold font with unterlines
				WritableFont colFont110pt = new WritableFont(WritableFont.COURIER, 8,WritableFont.BOLD);
				colFont1 = new WritableCellFormat(colFont110pt);
				colFont1.setBackground(jxl.format.Colour.IVORY);
				colFont1.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);

				colFont_red = new WritableCellFormat(colFont110pt);
				colFont_red.setBackground(jxl.format.Colour.RED);
				colFont_red.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);


				// Create create a bold font with unterlines
				WritableFont colFont210pt = new WritableFont(WritableFont.COURIER, 8,WritableFont.BOLD);
				colFont2 = new WritableCellFormat(colFont210pt);
				colFont2.setBackground(jxl.format.Colour.WHITE);
				colFont2.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
				
				WritableFont times10ptBoldHeading = new WritableFont(WritableFont.COURIER, 12, WritableFont.BOLD, false);
				timesBoldHeading = new WritableCellFormat(times10ptBoldHeading);
				//timesBoldHeading.setWrap(true);
				timesBoldHeading.setBackground(jxl.format.Colour.LIGHT_GREEN);
				timesBoldHeading.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);


				//FONT DECLARATION  ENDS 


				// EXCEL CREATION STRATS  

						Label head = new Label(0,0,sheetNames[index],timesBoldHeading);
						sExcel.addCell(head);

	 					head = new Label(0,1,"HOURS",timesBoldHeading);
						sExcel.addCell(head);

						this.connection=this.connectionManager.getConnection("HourlyMisDB");
						
						
						int columns	=0; 
						int rows	=2;

						for(int i=2;i<hours.length;i++){
							if(hours[i].equalsIgnoreCase("TOTAL")){
								head = new Label(columns,rows,hours[i],timesBoldHeading);
							}else{
								head = new Label(columns,rows,hours[i],timesBoldUnderline);
							}
							sExcel.addCell(head);
							rows++;
						}

						//CREATING DATE IN EXCEL SHEETS 
						//String sql= "select convert(varchar,mis_date,105) from tbl_hourly_mis where convert(varchar,mis_date,112) between CONVERT(VARCHAR,DATEADD(dd,-(DAY(getdate())-"+dateDiff+"),getdate()),112) and convert(varchar,getdate()-"+dateDiff+",112) and circle='"+circleNameColl[index]+"' order by mis_date asc";
						//String sql= "select distinct date(mis_date) from tbl_hourly_mis where date(mis_date) between '2015-12-01' and date(DATE_ADD(now(),INTERVAL -1 DAY)) order by mis_date asc";
						String sql="select distinct(convert(varchar,DATE_TIME,105)) mis_date from TBL_Vchat_HOURLY_MIS_AM where convert(varchar,DATE_TIME,112) between CONVERT(VARCHAR,DATEADD(dd,-(DAY(getdate())-"+dateDiff+"),getdate()),112) and convert(varchar,getdate()-"+dateDiff+",112) order by mis_date asc";
						System.out.println("query in excel for alaram##### "+sql);
						columns		=1; 
						rows		=1;						
						
						this.statement = this.connection.createStatement();
						this.resultSet = this.statement.executeQuery(sql);
						while(this.resultSet.next())
						{
							sExcel.setColumnView(columns,11);
							//Date d=this.resultSet.getDate(1);
							String d=this.resultSet.getString(1);
							head = new Label(columns,rows,d.toString(),dateFont);
							sExcel.addCell(head);
							columns++;						
						}

						//--String last_col="MTD";
						//--head = new Label(columns,rows,last_col,dateFont);
						//--sExcel.addCell(head);
						//columns++;
						this.resultSet.close();
						this.statement.close();

						//CREATING COLUMN DATA  IN EXCEL SHEETS 
						
						columns		=1; 
						rows		=2;
						
						
						for(int i=2;i<hours.length;i++)
						{					

							if(index==0){
								//sql= "select total_calls from tbl_hourly_mis where date(mis_date) between '2015-12-01' and date(DATE_ADD(now(),INTERVAL -1 DAY)) and hour='"+hours[i]+"' order by mis_date asc";
								sql= "select total_calls from TBL_Vchat_HOURLY_MIS_AM where convert(varchar,DATE_TIME,112) between CONVERT(VARCHAR,DATEADD(dd,-(DAY(getdate())-"+dateDiff+"),getdate()),112) and convert(varchar,getdate()-"+dateDiff+",112) and hour='"+hours[i]+"' order by DATE_TIME asc";
							}else if(index==1){
								sql= "select callsgt10 from TBL_Vchat_HOURLY_MIS_AM where convert(varchar,DATE_TIME,112) between CONVERT(VARCHAR,DATEADD(dd,-(DAY(getdate())-"+dateDiff+"),getdate()),112) and convert(varchar,getdate()-"+dateDiff+",112) and hour='"+hours[i]+"' order by DATE_TIME asc";
							}else if(index==2){
								sql= "select revenue from TBL_Vchat_HOURLY_MIS_AM where convert(varchar,DATE_TIME,112) between CONVERT(VARCHAR,DATEADD(dd,-(DAY(getdate())-"+dateDiff+"),getdate()),112) and convert(varchar,getdate()-"+dateDiff+",112) and hour='"+hours[i]+"' order by DATE_TIME asc";
							}
							
							//System.out.println("query in excel for alaram##### "+sql);
							this.statement = this.connection.createStatement();
							this.resultSet = this.statement.executeQuery(sql);
							NumberFormats mtdcol;
							int mtdval=0;
							while(this.resultSet.next())
							{

							jxl.write.Number number;
							if(i%2==0)
							{
							   
							   	if(hours[i].equalsIgnoreCase("TOTAL")){
							   		number = new jxl.write.Number(columns,rows,this.resultSet.getInt(1), timesBoldHeading);
							   	}else{
							   		number = new jxl.write.Number(columns,rows,this.resultSet.getInt(1), colFont1);
							   	}
							   if(hours[i].equals("TOTAL_BASE")||hours[i].equals("TOTAL_ACTIVE_BASE")||hours[i].equals("TOTAL_GRACE_USERS")||hours[i].equals("TOTAL_GRACE_FAILED")||hours[i].equals("TOTAL_RETRY_USERS")||hours[i].equals("TOTAL_RETRY_FAILED")||hours[i].contains("MTD"))
	                                                   {
	                                                     mtdval=this.resultSet.getInt(1);
	                                                   }
	                                                   else if(hours[i].equals("CATEGORY_NAME")||hours[i].equals("SUBCAT")||hours[i].equals("MAINCAT"))
	                                                   {
	                                                   	//out.println("Chek 1 ");
	                                                     mtdval=0;
	                                                   }                                                   
							   else
							   {
							     mtdval=mtdval+this.resultSet.getInt(1);
							   }
							   sExcel.addCell(number); 						
							}
							else
							{
								if(hours[i].equalsIgnoreCase("TOTAL")){
							   		number = new jxl.write.Number(columns,rows,this.resultSet.getInt(1), timesBoldHeading);
							   	}else{
							   		number = new jxl.write.Number(columns,rows,this.resultSet.getInt(1), colFont1);
							   	}
								if(hours[i].equals("TOTAL_BASE")||hours[i].equals("TOTAL_ACTIVE_BASE")||hours[i].equals("TOTAL_GRACE_USERS")||hours[i].equals("TOTAL_GRACE_FAILED")||hours[i].equals("TOTAL_RETRY_USERS")||hours[i].equals("TOTAL_RETRY_FAILED")||hours[i].contains("MTD"))
							  {
							    mtdval=this.resultSet.getInt(1);
							  }
	                                                  else if(hours[i].equals("CATEGORY_NAME")||hours[i].equals("SUBCAT")||hours[i].equals("MAINCAT"))
	                                                   {
	                                                     //out.println("Chek 2 ");
	                                                     mtdval=0;
	                                                   }                                                   
							  else
							  {
							     mtdval=mtdval+this.resultSet.getInt(1);
	                                                  }
							  sExcel.addCell(number); 						
							}						
							columns++;

							}
							//mtdcol=new jxl.write.Number(columns,rows,mtdval, colFont1);
							//sExcel.addCell(mtdcol);
							//columns++;
							rows++;
							columns=1; 
							
							this.resultSet.close();
							this.statement.close();
						
							
						}						


						this.connection.close();
						
						
			}
						workbook.write();
						workbook.close();
		}catch(Exception e){
			LOGGER.error(LogStackTrace.getStackTrace(e));
		}
		return false;
	}
	
			
}
