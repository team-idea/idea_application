package com.spice.mis.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;



public class LoadPropertiesFile {
	private static final Logger LOGGER=Logger.getLogger(LoadPropertiesFile.class);
	private Properties properties=null;
	private FileInputStream in=null;
	private boolean isPropLoad=false;
	private String propValue=null;
	
	
	public LoadPropertiesFile(){
		try{
			PropertyConfigurator.configure("log4j.properties");
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}
	
	
	public Properties loadPropertiesFile(String propertiesFile){
		if(null!=propertiesFile){
			try{
				properties= new Properties();
				in = new FileInputStream(propertiesFile);
				properties.load(in);
				//isPropLoad=true;
				LOGGER.debug("["+propertiesFile+"] properties file loaded successfully");
			}catch(Exception ex){
				LOGGER.error((Object) "Exception while loading properties file");
				LOGGER.error(getStackTrace(ex));
			}finally{
				if(null!=in){
					try {
						in.close();
					} catch (IOException ex) {
						LOGGER.error(getStackTrace(ex));
					}
				}
			}
		}
		return properties;
	}

	public String getPropertyValue(String propName){
		try{
			propValue=properties.getProperty(propName);
		}catch(Exception ex){			
			LOGGER.error(getStackTrace(ex));
		}
		return propValue;
	}
	
    private synchronized String getStackTrace(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        t.printStackTrace(pw);
        pw.flush();
        sw.flush();
        return sw.toString();
    }
	
	

}
