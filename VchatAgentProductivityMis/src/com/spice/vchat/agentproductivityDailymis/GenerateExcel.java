package com.spice.vchat.agentproductivityDailymis;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.formula.functions.Replace;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class GenerateExcel {
	private static Logger logger = Logger.getLogger(GenerateExcel.class.getName());
	private FileOutputStream fos = null;
	private String columnsNames = null;
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	private String sqlQuery = null;
	private String workBookPath = "./MIS.xlsx";
	private String[] columnsName = null;
	private String[] sheet1_columnsName = null;
	private String[] sheet2_columnsName = null;
	private String sheetNames = null;
	private String[] sheetName = null;
	private XSSFWorkbook workBook = null;
	private XSSFSheet sheet = null;
	private Properties prop = null;
	private int RowCounter = 0;
	private String sheet1_columnsNames = null;
	private String sheet1_sqlQuery = null;
	private String sheet2_columnsNames = null;
	private String sheet2_sqlQuery = null;
	private int ColCounter = 2;
	private String dbUrl = null;
	private String dbUserName = null;
	private String dbPassword = null;
	private String types = null;
	private String[] type = null;
	private String circles = null;
	private String date = null;
	private String yearMonth = null;
	private String[] circle = null;
	private XSSFRow SheetRow = null;
	private XSSFCell SheetCell = null;
	private String dbDriverName = "";
	private XSSFCellStyle styleHeader = null;
	private XSSFCellStyle styleColumn = null;
	private String misDate = null;

	public GenerateExcel() {
		prop = new Properties();
		PropertyConfigurator.configure("./log4j.properties");
		try {
			workBook = new XSSFWorkbook();
			prop.load(new DataInputStream(new FileInputStream("./config.properties")));
			sheetNames = prop.getProperty("sheetNames");
			sheet1_columnsNames = prop.getProperty("sheet1_columnsNames");
			sheet1_sqlQuery = prop.getProperty("sheet1_sqlQuery");
			sheet2_columnsNames = prop.getProperty("sheet2_columnsNames");
			System.out.println(sheet2_columnsNames);
			sheet2_sqlQuery = prop.getProperty("sheet2_sqlQuery");
			types = prop.getProperty("types");
			circles = prop.getProperty("circles");
			columnsNames = prop.getProperty("columnsNames");
			dbDriverName = prop.getProperty("dbDriverName");
			dbUrl = prop.getProperty("dbUrl");
			dbUserName = prop.getProperty("dbUserName");
			dbPassword = prop.getProperty("dbPassword");
			sqlQuery = prop.getProperty("sqlQuery");
			setStyle();
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}

	}

	public void setStyle() {
		styleHeader = workBook.createCellStyle();
		styleHeader.setFillForegroundColor(IndexedColors.GREY_25_PERCENT.getIndex());
		styleHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleHeader.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		styleHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		styleHeader.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderTop(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderRight(XSSFCellStyle.BORDER_THIN);
		XSSFFont mergeCellFont = workBook.createFont();
		mergeCellFont.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		styleHeader.setFont(mergeCellFont);

		styleColumn = workBook.createCellStyle();
		styleColumn.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleColumn.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		styleColumn.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		styleColumn.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		styleColumn.setBorderTop(XSSFCellStyle.BORDER_THIN);
		styleColumn.setBorderRight(XSSFCellStyle.BORDER_THIN);
	}

	public void excelProcess(String misDate) {
		this.misDate = misDate;// yyyymmdd
		this.date = misDate.substring(6, 8);
		this.yearMonth = misDate.substring(0, 6);
		logger.debug("date[" + date + "]yearMonth[" + yearMonth + "]");
		try {
			Class.forName(dbDriverName);
			sheetName = sheetNames.split("[,]+");
			for (String sheetCode : sheetName) {
				sheet = workBook.createSheet(sheetCode);
				System.out.println("sheetCode-->[" + sheetCode + "]");
				;
				if (sheetCode.equalsIgnoreCase("Agent_Alert")) {
					getAgentAlertExcel();
				} else if (sheetCode.equalsIgnoreCase("Agent_HourlyWise_Report")) {
					getAgentHourlyWiseReport();
				} else {
					logger.debug("sheet handling is not there.");
				}
			}
			saveExcel();
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public void saveExcel() {
		try {
			saveExcels("./misData/!dea_VC_AgentProductivity_Daily_Report_" + misDate + ".xlsx");
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public void getAgentHourlyWiseReport() throws SQLException {
		
			SheetRow = sheet.createRow(0);
			SheetRow = sheet.createRow(1);

			SheetRow = sheet.getRow(1);
			SheetCell = SheetRow.createCell(0);
			SheetCell.setCellStyle(styleHeader);
			SheetRow = sheet.getRow(1);
			SheetCell = SheetRow.createCell(1);
			SheetCell.setCellStyle(styleHeader);
			SheetRow = sheet.getRow(1);
			SheetCell = SheetRow.createCell(2);
			SheetCell.setCellStyle(styleHeader);
			SheetRow = sheet.getRow(0);
			SheetCell = SheetRow.createCell(27);
			SheetCell.setCellStyle(styleHeader);
			SheetCell = SheetRow.createCell(0);
			SheetCell.setCellStyle(styleHeader);
			SheetCell.setCellValue("DATE");
			SheetCell.setCellStyle(styleHeader);
			sheet.addMergedRegion(new CellRangeAddress(0, 1, 0, 0));
			SheetCell = SheetRow.createCell(1);
			SheetCell.setCellValue("CIRCLE");
			SheetCell.setCellStyle(styleHeader);
			sheet.addMergedRegion(new CellRangeAddress(0, 1, 1, 1));
			SheetCell = SheetRow.createCell(2);
			SheetCell.setCellValue("TYPE");
			SheetCell.setCellStyle(styleHeader);
			sheet.addMergedRegion(new CellRangeAddress(0, 1, 2, 2));

			SheetCell = SheetRow.createCell(3);
			SheetCell.setCellValue("HOURS");
			SheetCell.setCellStyle(styleHeader);
			sheet.addMergedRegion(CellRangeAddress.valueOf("D1:AB1"));

			SheetRow = sheet.getRow(1);
			int k = 0;
			for (int i = 3; i <= 27; i++) {
				SheetCell = SheetRow.createCell(i);
				SheetCell.setCellValue(k);
				SheetCell.setCellStyle(styleHeader);
				k++;
				if (i == 27) {
					SheetCell = SheetRow.getCell(27);
					SheetCell.setCellValue("TOTAL");
					SheetCell.setCellStyle(styleHeader);
				}
			}
			sheet2_columnsName = sheet2_columnsNames.split("[,]+");			
			type = types.split("[,]+");
			RowCounter = 2;
			
			for(String circleCode : circle){
			try {
				logger.debug("");
				logger.debug("circle["+circleCode+"]----------------------["+prop.getProperty(circleCode+"_dbUrl")+"]");
				con = DriverManager.getConnection(prop.getProperty(circleCode+"_dbUrl"), prop.getProperty(circleCode+"_dbUserName"), prop.getProperty(circleCode+"_dbPassword"));			
				logger.debug("connect is [" + con + "]");
				stmt = con.createStatement();
				//circle = circles.split("[,]+");
				ColCounter = 0;
				//String Query = sheet2_sqlQuery;
				for (int dateOfMis = 1; dateOfMis <= Integer.parseInt(date); dateOfMis++) {
					if (dateOfMis < 10) {
						misDate = yearMonth + "0" + dateOfMis;
					} else {
						misDate = yearMonth + dateOfMis;
					}
					logger.debug(misDate+"["+circleCode+"]");
					//sheet2_sqlQuery = Query;
					String Query = sheet2_sqlQuery;
					Query = Query.replaceAll("YYYYMMDD", misDate);
					//String sqlQuery_circle = sheet2_sqlQuery;
					//for (String circleCode : circle) {
						//sheet2_sqlQuery = sqlQuery_circle;
						Query = Query.replaceAll("@circle", circleCode);
						logger.debug("Query is [" + Query + "]");
						for (String typeCode : type) {
							sqlQuery = Query;
							sqlQuery = sqlQuery.replaceAll("@type", typeCode);
							logger.debug("Query is [" + sqlQuery + "]");
							rs = stmt.executeQuery(sqlQuery);
							while (rs.next()) {
								SheetRow = sheet.createRow(RowCounter);
								for (String columnsCode : sheet2_columnsName) {
									SheetCell = SheetRow.createCell(ColCounter);
									SheetCell.setCellStyle(styleColumn);
									if (columnsCode.equalsIgnoreCase("circle") || columnsCode.equalsIgnoreCase("MisDate")
											|| columnsCode.equalsIgnoreCase("type")) {
										SheetCell.setCellValue(rs.getString(columnsCode.trim()));
									}else if(columnsCode.equalsIgnoreCase("total")){
										SheetCell.setCellValue(rs.getInt(columnsCode.trim()));
									} 
									else {
										SheetCell.setCellValue(rs.getInt("hour_"+columnsCode.trim()));
									}
									ColCounter++;
								}
								RowCounter++;
								ColCounter = 0;
							}
							rs.close();
						}
	
					//}
				}

		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		} finally {
			if (rs != null || !rs.isClosed()) {
				rs.close();
			}
			if (stmt != null || !stmt.isClosed()) {
				stmt.close();
			}
			if (con != null || !con.isClosed()) {
				con.close();
			}
		}
	  }
	}

	public void getAgentAlertExcel() throws SQLException {
		circle = circles.split("[,]+");
		sheet1_columnsName = sheet1_columnsNames.split("[,]+");
		RowCounter = 0;
		ColCounter = 0;
		SheetRow = sheet.createRow(RowCounter);				
		for (String columnsCode : sheet1_columnsName) {
			SheetCell = SheetRow.createCell(ColCounter);
			SheetCell.setCellStyle(styleHeader);
			SheetCell.setCellValue(columnsCode);
			ColCounter++;
		}
		RowCounter = 1;			
		for (String circleCode : circle) {
			try {
				logger.debug("");
				logger.debug("circle[" + circleCode + "]----------------------["
						+ prop.getProperty(circleCode + "_dbUrl") + "]");
				con = DriverManager.getConnection(prop.getProperty(circleCode + "_dbUrl"),
						prop.getProperty(circleCode + "_dbUserName"), prop.getProperty(circleCode + "_dbPassword"));
				logger.debug("connect is [" + con + "]");
				
				stmt = con.createStatement();
				// String Query = sheet1_sqlQuery;
				for (int dateOfMis = 1; dateOfMis <= Integer.parseInt(date); dateOfMis++) {
					if (dateOfMis < 10) {
						misDate = yearMonth + "0" + dateOfMis;
					} else {
						misDate = yearMonth + dateOfMis;
					}
					logger.debug(misDate+"["+circleCode+"]");
					String Query = sheet1_sqlQuery;
					Query = Query.replaceAll("YYYYMMDD", misDate);
					logger.debug("Query is [" + Query + "]");
					rs = stmt.executeQuery(Query);
					ColCounter = 0;
					while (rs.next()) {
						SheetRow = sheet.createRow(RowCounter);
						for (String columnsCode : sheet1_columnsName) {
							SheetCell = SheetRow.createCell(ColCounter);
							SheetCell.setCellStyle(styleColumn);
							if (columnsCode.equalsIgnoreCase("circle") || columnsCode.equalsIgnoreCase("Mis_Date")) {
								SheetCell.setCellValue(rs.getString(columnsCode.trim()));
							} else {
								SheetCell.setCellValue(rs.getInt(columnsCode.trim()));
							}
							ColCounter++;
						}
						RowCounter++;
						ColCounter = 0;
					}
					rs.close();
				}
			} catch (Exception ex) {
				logger.debug(LogStackTrace.getStackTrace(ex));
			} finally {
				if (rs != null || !rs.isClosed()) {
					rs.close();
				}
				if (stmt != null || !stmt.isClosed()) {
					stmt.close();
				}
				if (con != null || !con.isClosed()) {
					con.close();
				}
			}

		}

	}

	public void saveExcels(String workBookPath) throws IOException {
		this.workBookPath = workBookPath; // for saving file to the
		try {
			fos = new FileOutputStream(workBookPath); // saves xlsx
			workBook.write(fos);
			fos.close();
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			fos.close();
		}
	}
}
