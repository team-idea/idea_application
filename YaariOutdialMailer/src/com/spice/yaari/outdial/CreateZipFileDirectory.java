package com.spice.yaari.outdial;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

public class CreateZipFileDirectory {
	private String sourceDirectory = null;
	private String zipFile=null;

	
	public String zipperActionPerform(String sourceDirectory) {

		this.sourceDirectory = sourceDirectory;
		System.out.println("sourceDirectory[" + sourceDirectory + "]");
		// String zipDirctoryName =
		// sourceDirectory.substring(sourceDirectory.lastIndexOf("/") + 1);
		/// System.out.println("zipDirctoryName[" + zipDirctoryName + "]");
		// String zipDirctoryPath = sourceDirectory.substring(0,
		// sourceDirectory.lastIndexOf("/"));
		// System.out.println("zipDirctoryPath[" + zipDirctoryPath + "]");

		try {
			 zipFile = sourceDirectory + ".zip";

			byte[] buffer = new byte[1024];
			FileOutputStream fout = new FileOutputStream(zipFile);
			ZipOutputStream zout = new ZipOutputStream(fout);
			File dir = new File(sourceDirectory);
			if (!dir.isDirectory()) {
				System.out.println(sourceDirectory + " is not a directory");
			} else {
				File[] files = dir.listFiles();

				for (int i = 0; i < files.length; i++) {
					System.out.println("Adding " + files[i].getName());
					FileInputStream fin = new FileInputStream(files[i]);
					zout.putNextEntry(new ZipEntry(files[i].getName()));
					int length;
					while ((length = fin.read(buffer)) > 0) {
						zout.write(buffer, 0, length);
					}
					zout.closeEntry();
					fin.close();
				}
			}

			zout.close();
			System.out.println("Zip file has been created!");

		} catch (IOException ioe) {
			System.out.println("IOException :" + ioe);
		}
		System.out.println("zip file path and name ["+zipFile+"]");
		return zipFile;
	}
}
