package com.spice.yaari.outdial;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class YaariOutdialMain {

	private static Logger logger = Logger.getLogger(YaariOutdialMain.class.getName());
///	private  Properties prop = new Properties();
	/*private  String kmpid = prop.getProperty("kmpid");
	private  String pwd = prop.getProperty("pwd");
	private  String to = prop.getProperty("to");
	private  String cc = prop.getProperty("cc");
	private  String bcc = prop.getProperty("bcc");
	private  String from = prop.getProperty("from");
	private  String messageSubject = prop.getProperty("messageSubject");
	private  String messageBody = prop.getProperty("cc");
*/
	YaariOutdialMain() {
		try {
			PropertyConfigurator.configure("./log4j.properties");
		//	prop.load(new DataInputStream(new FileInputStream("./config.properties")));
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(LogStackTrace.getStackTrace(ex));
		}
		/*kmpid = prop.getProperty("kmpid");
		pwd = prop.getProperty("pwd");
		//to = prop.getProperty("to");
		cc = prop.getProperty("cc");
		bcc = prop.getProperty("bcc");
		from = prop.getProperty("from");
		messageSubject = prop.getProperty("messageSubject");
		messageBody = prop.getProperty("cc");
		*/

	}

	public static void main(String[] args) {
		//YaariOutdialMain yaariOutdialMain=new YaariOutdialMain();
		YaariOutdialMain yaariOutdialMain = new YaariOutdialMain();
		CreateZipFileDirectory createZipFileDirectory = new CreateZipFileDirectory();
		MailHelper mailHelper = new MailHelper();

		try {
			if (yaariOutdialMain.accessFile() && yaariOutdialMain.getLock()) {
				logger.info("program is started..");
				DownloadFileFromFTP downloadFileFromFTP = new DownloadFileFromFTP();
				String fileDownloadedPath = downloadFileFromFTP.FTPDownloadFiles();
				String zipFileName = createZipFileDirectory.zipperActionPerform(fileDownloadedPath);
				mailHelper.sendMailRequest(zipFileName);
	//			System.out.println("kmpis["+kmpid+"]pwd["+pwd+"]to["+to+"]cc["+cc+"]bcc["+bcc+"]from["+from+"]messageSubject["+messageSubject+"]messageBody["+messageBody+"]zipFileName["+zipFileName);
				
		/*		boolean result = mailHelper.sendMail(kmpid, pwd, to, cc, bcc, from, messageSubject, messageBody,zipFileName);
				if (result == true) {
					System.out.println("mail send..");
				} else {
					System.out.println("mail not send..");
				}*/

			} else {

			}
		} catch (Exception e) {
			logger.error(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		}
	}

	private boolean getLock() {
		boolean lock = false;
		try {
			RandomAccessFile file = new RandomAccessFile("./yaariOutdial.tmp", "rw");
			FileChannel fileChannel = file.getChannel();
			FileLock fileLock = fileChannel.tryLock();
			if (fileLock != null) {
				logger.debug("File is locked. Starting the execution of the Application");
				lock = true;
			} else {
				logger.error("Cannot create lock. Already another instance of the program is running.");
				logger.error("System shutting down. Please wait");
				try {
					Thread.sleep(1000 * 3);
				} catch (Exception exp) {
					exp.printStackTrace();
				}
				System.exit(0);
			}
		} catch (Exception exp) {
			exp.printStackTrace();
		}
		return lock;
	}

	private boolean accessFile() {
		boolean retCode = false;
		try {
			String line = "";
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter("./yaariOutdial.tmp", false));
				bw.close();
			} catch (Exception exp) {
				exp.printStackTrace();
			}
			retCode = true;
		} catch (Exception e) {
			System.out.println(e);
		}
		logger.debug("accessFile() = " + retCode);
		return retCode;
	}

}
