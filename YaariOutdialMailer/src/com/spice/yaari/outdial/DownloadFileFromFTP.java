package com.spice.yaari.outdial;

import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.apache.commons.net.ftp.FTP;
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
import org.apache.log4j.DailyRollingFileAppender;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class DownloadFileFromFTP {

	private static Logger logger = Logger.getLogger(DownloadFileFromFTP.class.getName());
	private Properties prop = new Properties();
	private String ftpServerIP = "172.27.32.70";
	private int ftpPort = 21;
	private String ftpUser = "centralftp";
	private String ftpPassword = "central";
	private String downloadFilePath = "d:/";
	private String ftpFilePath = "";
	private int datediff = 1;

	DownloadFileFromFTP() {
		try {
			PropertyConfigurator.configure("./log4j.properties");
			prop.load(new DataInputStream(new FileInputStream("./config.properties")));
			ftpServerIP = prop.getProperty("ftpServerIP");
			ftpPort = Integer.parseInt(prop.getProperty("ftpPort"));
			ftpUser = prop.getProperty("ftpUser");
			ftpPassword = prop.getProperty("ftpPassword");
			downloadFilePath = prop.getProperty("downloadFilePath");
			// ftpFilePath=prop.getProperty("ftpFilePath");
			datediff = Integer.parseInt(prop.getProperty("datediff"));
			System.out.println(ftpServerIP + ftpPort + ftpPort + ftpUser + ftpPassword + downloadFilePath + ftpFilePath
					+ datediff + datediff);
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(LogStackTrace.getStackTrace(ex));
		}
	}

	private String getdate(int datediff) {
		// System.out.println("value of i = "+ i);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -datediff);
		SimpleDateFormat formatter = new SimpleDateFormat("YYYYMMdd");
		String strDate = formatter.format(cal.getTime());
		// System.out.println("Yesterday's date = "+ strDate);
		return strDate;

	}

	public String FTPDownloadFiles() {

		FTPClient ftpClient = new FTPClient();

		try {

			ftpClient.connect(ftpServerIP, ftpPort);
			ftpClient.login(ftpUser, ftpPassword);
			ftpClient.enterLocalPassiveMode();
			ftpClient.setFileType(FTP.BINARY_FILE_TYPE);

			// APPROACH #1: using retrieveFile(String, OutputStream)
			downloadFilePath = downloadFilePath + "/" + getdate(datediff);
			File file = new File(downloadFilePath);
			boolean b = false;
			if (!file.exists()) {
				try {
					b = file.mkdirs();
				} catch (Exception ex) {
					ex.printStackTrace();
				}
				if (b) {
					System.out.println("Directory Created");
				} else {
					System.out.println("Directory not Created");
				}
			}

			ftpFilePath = ftpFilePath + "/" + getdate(datediff) + "/";

			// String remoteFile1 = "/shamsher/hello.txt";
			FTPFile[] files = ftpClient.listFiles(ftpFilePath);
			System.out.println("file in " + getdate(datediff) + " folder: " + files.length);

			for (FTPFile file1 : files) {
				String ftpFileName = file1.getName();
				System.out.println("file name is-->" + ftpFilePath + ftpFileName);

				File downloadFile1 = new File(file + "/" + ftpFileName);
				System.out.println("download file path is"); // )["+downloadFile1+"]");
				OutputStream outputStream1 = new BufferedOutputStream(new FileOutputStream(downloadFile1));
				boolean success = ftpClient.retrieveFile(ftpFilePath + ftpFileName, outputStream1);
				outputStream1.close();

				if (success) {
					System.out.println("File #1 has been downloaded successfully.");
				} else {
					System.out.println("file downloadning problem.");
				}
			}

		} catch (IOException ex) {
			System.out.println("Error: " + ex.getMessage());
			ex.printStackTrace();
		} finally {
			try {
				if (ftpClient.isConnected()) {
					ftpClient.logout();
					ftpClient.disconnect();
				}
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		return downloadFilePath;
	}

}
