package in.spicedigital.freemium.ftp;

import java.nio.channels.*;
import java.io.*;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import org.apache.log4j.*;


import in.spicedigital.freemium.util.DBConnectionManager;
import in.spicedigital.freemium.util.misInfo;
import in.spicedigital.freemium.util.zipper;

public class configureThreads extends Thread
{
	final static int MAX_THREAD_POOL			= 1000;
	private static Logger logger 				= Logger.getLogger( configureThreads.class.getName() );
	private static String CONFIG_FILE			= "log4j.properties";
	private static String INSTANCE				= "";

	private static pushResponse[] sdpPush		= new pushResponse[MAX_THREAD_POOL/2];


	private static String[][] threadPool_orig	= new String[MAX_THREAD_POOL][4];
	private static String[] circles				= null;
	private static String[][] circlesPushThreads= null;
	private DBConnectionManager connMgr			= null;
	private static configureAlerts alertValue	= null;
	private static misInfo mis					= null;
	private String conPool_local				= "local";
	private String conPool_remote				= "remote";

	public configureThreads()
	{
		try{PropertyConfigurator.configure("log4j.properties");}catch(Exception exp){}
		try
		{
			alertValue							= configureAlerts.getInstance();
			Properties prop 					= new Properties();
			prop.load(new DataInputStream(new FileInputStream("sdp.properties")));
			String[] Circles					= prop.getProperty("sdp.circles").split(",");
			String[] pushThreadsConf			= prop.getProperty("sdp.circle.pushthreads").split(",");
			INSTANCE							= prop.getProperty("sdp.application.instance");

			logger.info("[Thread-main]<-(main  : al) Starting the Spice SDP send / receive central application ["+INSTANCE+"]. In case of issue contact central technical team @9218514140.");

			circles 							= new String[Circles.length];
			circles								= Circles;

			if(accessFile())
			{
				try
				{
					RandomAccessFile file 		= new RandomAccessFile("c:/windows/system/billingApp_spice_"+INSTANCE+".tmp", "rw");
					FileChannel fileChannel 	= file.getChannel();
					FileLock fileLock 			= fileChannel.tryLock();
					if (fileLock != null){logger.debug("File is locked. Starting the execution of the Application");}
					else
					{
						logger.error("Cannot create lock. Already another instance of the program is running.");
						alertValue.setAlertString("mail:Tried to run a second instance of the program.");
						try{Thread.sleep(1000*30);}catch(Exception exp){}
						logger.error("System shutting down. Please wait");
						System.exit(0);
					}
				}
				catch (Exception exp) {logException(exp);}
			}
			else
			{
				logger.error("Cannot create lock. Already another instance of the program is running.");
				alertValue.setAlertString("mail:Tried to run a second instance of the program.");
				try{Thread.sleep(1000*30);}catch(Exception exp){}
				logger.error("System shutting down. Please wait");
				System.exit(0);
			}
			alertValue							= configureAlerts.getInstance();
			mis									= misInfo.getInstance();

			logger.debug("pushThreadsConf.length = "+pushThreadsConf.length);
			int k							= 0;
			for(int i=0;i<pushThreadsConf.length;i++)
			{
				String[] circlePushThreads	= pushThreadsConf[i].split("-");
				logger.debug("circlePushThreads.length = "+circlePushThreads.length);
				for(int j=0;j<Integer.parseInt(circlePushThreads[1]);j++)
				{
					sdpPush[k]	= new pushResponse(circlePushThreads[0].toLowerCase(),j);try{Thread.sleep(150);}catch(Exception exp){logException(exp);}
					logger.debug("sdpPush["+k+"] = "+sdpPush[k].getName());	k++;
				}
			}
		}catch(Exception exp){logException(exp);System.exit(2);}
	}
	private boolean accessFile()
	{
		boolean retCode					= false;
		try
		{
			String line 				= "";
			try {BufferedWriter bw		= new BufferedWriter(new FileWriter("c:/windows/system/billingApp_spice_"+INSTANCE+".tmp",false));bw.close();}
			catch (Exception exp) {}
			retCode						= true;
		}
		catch (Exception e) {System.out.println(e);}
		logger.debug("accessFile() = "+retCode);
		return retCode;
	}



	public static void main(String argv[])
	{
		configureThreads cft			= new configureThreads();
		zipper		     zip			= new zipper();
		ftpClientSdp ftp				= new ftpClientSdp();

		sendRequestSDP[] sdp			= new sendRequestSDP[circles.length];
		pullRecords[] sdpPull			= new pullRecords[circles.length];
		//CallbackSync[] sync_callback	= new CallbackSync[circles.length];
		for(int i=0;i<circles.length;i++)
		{
			sdp[i]						= new sendRequestSDP(circles[i].toLowerCase());	try{Thread.sleep(100*3);}catch(Exception exp){} logger.debug("sdp["+i+"] = "+sdp[i].getName());
			sdpPull[i]					= new pullRecords(circles[i].toLowerCase());	try{Thread.sleep(100*3);}catch(Exception exp){}	logger.debug("sdpPull["+i+"] = "+sdpPull[i].getName());
			//sync_callback[i]			= new CallbackSync(circles[i].toLowerCase());	try{Thread.sleep(100*3);}catch(Exception exp){} logger.debug("sync_callback["+i+"] = "+sync_callback[i].getName());
		}
		manageThreads(sdp,sdpPush,sdpPull);
	}

	private static void manageThreads(sendRequestSDP[] sdp,pushResponse[] sdpPush,pullRecords[] sdpPull)
	{
		String[] threadPool_default	= new String[MAX_THREAD_POOL];
		while(true)
		{
			for(int i=0;i<sdp.length;i++)
			{
				//logger.info("[Thread-000] sdp["+i+"] = "+sdp[i].getName()+" : "+sdp[i].isAlive());
				if(sdp[i].isAlive() == false)
				{
					alertValue.setAlertString(new java.util.Date()+" mail-1: sdpPull["+i+"] "+sdp[i].getName()+" is not responding / exited abruptly. Please Check.");
				}
			}
			for(int i=0;i<sdpPull.length;i++)
			{
				//logger.info("[Thread-000] sdpPull["+i+"] = "+sdpPull[i].getName()+" = "+sdpPull[i].isAlive());
				if(sdpPull[i].isAlive() == false)
				{
					alertValue.setAlertString(new java.util.Date()+" mail-1: sdpPull["+i+"] "+sdpPull[i].getName()+" is not responding / exited abruptly. Please Check.");
				}
			}
			for(int i=0;i<sdpPush.length;i++)
			{
				if(sdpPush[i]==null) break;
				//try{logger.info("[Thread-000] sdpPush["+i+"] = "+sdpPush[i].getName()+" = "+sdpPush[i].isAlive());}catch(Exception exp){exp.printStackTrace();}
				if(sdpPush[i].isAlive() == false)
				{
					alertValue.setAlertString(new java.util.Date()+" mail-1: sdpPush["+i+"] "+sdpPush[i].getName()+" is not responding / exited abruptly. Please Check.");
				}
			}
			try{Thread.sleep(1000*3);}catch(Exception exp){}
		}
	}

	public void run(){while(true){try{Thread.sleep(1000*60);}catch(Exception exp){logException(exp);}}}


	private void logException(Exception exp)
	{
		logger.error(getStackTrace(exp));
		alertValue.setExceptionAlert("[Thread-main]<-(main  : al) "+getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}
}