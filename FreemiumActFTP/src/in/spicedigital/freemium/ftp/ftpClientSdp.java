package in.spicedigital.freemium.ftp;

import java.util.*;
import java.io.*;
import java.text.*;
import java.sql.*;

import org.apache.log4j.*;

import in.spicedigital.freemium.util.DBConnectionManager;
import in.spicedigital.freemium.util.HttpURLForDnd;

public class ftpClientSdp extends Thread {
	private static Logger logger = Logger.getLogger(ftpClientSdp.class.getName());
	private DBConnectionManager connMgr = null;
	private static configureAlerts alertValue = null;

	private static String sdpFtpDownloadDetails = "";
	private static String sdpFtpUploadDetails = "";

	public static void main(String argv[]) {
		ftpClientSdp ftpClient = new ftpClientSdp();
	}

	public ftpClientSdp() {
		try {
			PropertyConfigurator.configure("log4j.properties");
		} catch (Exception exp) {
		}
		init();
	}

	private void init() {
		try {
			Properties prop = new Properties();
			prop.load(new DataInputStream(new FileInputStream("sdp.properties")));
			sdpFtpDownloadDetails = prop.getProperty("sdp.ftp.download.remote.host.details");
			sdpFtpUploadDetails = prop.getProperty("sdp.ftp.upload.remote.host.details");
			connMgr = DBConnectionManager.getInstance();
			alertValue = configureAlerts.getInstance();
			this.start();
		} catch (Exception exp) {
		}
	}

	public void run() {
		final String sdpFtpDownloadDetail = this.sdpFtpDownloadDetails;
		final String sdpFtpUploadDetail = this.sdpFtpUploadDetails;
		final String appender = "[" + String.format("%-10s", Thread.currentThread().getName()) + "] ->(ftp   :al) ";
		final String downloadfileName = "Freemium_";
		final String pbMciFileName = "Free_Missed_Call_";

		String currDate = generateDate(); // yyyyMMdd format is used here as per// SDP-SE naming conventions.
		String[] ftpDownload = sdpFtpDownloadDetail.split(","); // User	// credentials// to connect to	// SDP-SE FTP// server.
		ftpDownload[4] = ftpDownload[4].replace("<yyyymmdd>", currDate); // replacing the date String with actual date.
		boolean success = (new File(ftpDownload[4])).mkdirs(); // Making a new folder for the FTP download of the Failed CDR Batch downloaded from the SDP-SE.
		if (success)
			logger.debug(appender + "Directory Created for Daily Records.");

		String renamingOver = "None";
		while (renamingOver != null) {
			renamingOver = renameFile(appender, ftpDownload[0], ftpDownload[1], ftpDownload[2], ftpDownload[3],	pbMciFileName, downloadfileName);
		}
		while (true) {
			logger.info(appender + "Trying to download file " + downloadfileName + " from SDP FTP location {"+ ftpDownload[0] + "} .");
			final String retFileName = downloadFile(appender, ftpDownload[0], ftpDownload[1], ftpDownload[2],ftpDownload[3], ftpDownload[4], downloadfileName);

			if (retFileName != null && !(retFileName.equalsIgnoreCase(""))) {
				Vector<String> processedRecords = processFile(appender, ftpDownload[4] + "/" + retFileName);
				if (processedRecords != null) {
					String retStr = writeProcessedFile(appender, ftpDownload[4], retFileName, processedRecords);
					// boolean retBool =uploadFile(appender,ftpDownload[0],ftpDownload[1],ftpDownload[2],ftpDownload[3],ftpDownload[4],retStr);
				}

				try {
					Thread.sleep(1000 * 5);
				} catch (Exception exp) {
				}
			} else {
				logger.info(appender + "No Freemium file found on FTP to download. Existing Thread for today....");
				try {
					Thread.sleep(1000 * 10);
					System.exit(0);
				} catch (Exception exp) {
				}
			}
		}
	}

	public synchronized String renameFile(final String appender, final String host, final String user, final String pwd,
			final String dir, final String oldFilePrefix, final String newFilePrefix) {
		String retStr = null;
		try {
			logger.debug(appender + host + " - " + user + " - " + pwd + " - " + dir + " - " + oldFilePrefix + " - "
					+ newFilePrefix);
			FtpWrapper ftp = new FtpWrapper();
			boolean login = ftp.connectAndLogin(host, user, pwd);
			if (login) {
				logger.debug(appender + "Connected to the FTP remote server: " + host);
				ftp.setPassiveMode(true);
				ftp.binary();
				try {
					logger.debug(appender + "Welcome message: " + ftp.getReplyString());
					logger.debug(appender + "Current Directory: " + ftp.printWorkingDirectory());
					if (!(dir.equalsIgnoreCase("none"))) {
						logger.debug(appender + "Changing this directory to " + dir + ": " + ftp.changeDirectory(dir));
						logger.debug(appender + "Current Directory after change: " + ftp.printWorkingDirectory());
					} else {
						logger.debug(appender + "Fetching the content in current directory: Root");
					}

					logger.debug(appender + "fileExists(" + oldFilePrefix + ")  = " + ftp.fileExists(oldFilePrefix));

					if (ftp.fileExists(oldFilePrefix)) {
						String oldfile = ftp.getFileName(oldFilePrefix);
						String newfile = oldfile.replace(oldFilePrefix, newFilePrefix);
						logger.debug(appender + "File " + oldfile + " to be renamed as : " + newfile + "/");
						boolean retFlag = ftp.ren(oldfile, newfile);
						if (retFlag) {
							logger.debug(appender + "File " + oldfile + " renamed successfully as: " + newfile + ".");
							retStr = newfile;
						} else {
							logger.debug(appender + "File " + oldfile + " cannot be renamed as: " + newfile + ".");
							retStr = null;
						}
					} else {
						retStr = null;
					}
				} catch (Exception exp) {
				} finally {
					try {
						ftp.logout();
						ftp.disconnect();
					} catch (Exception expt) {
					}
				}
			} else {
				logger.debug(appender + "Unable to connect to: " + host);
			}
		} catch (Exception expM) {
		}
		return retStr;
	}

	public synchronized String downloadFile(final String appender, final String host, final String user,
			final String pwd, final String dir, final String localfilePath, final String Serverfile) {
		String retStr = null;
		try {
			logger.debug(appender + host + " - " + user + " - " + pwd + " - " + dir + " - " + localfilePath + " - "
					+ Serverfile);
			FtpWrapper ftp = new FtpWrapper();
			boolean login = ftp.connectAndLogin(host, user, pwd);
			if (login) {
				logger.debug(appender + "Connected to the FTP remote server: " + host);
				ftp.setPassiveMode(true);
				ftp.binary();
				try {
					logger.debug(appender + "Welcome message: " + ftp.getReplyString());
					logger.debug(appender + "Current Directory: " + ftp.printWorkingDirectory());
					if (!(dir.equalsIgnoreCase("none"))) {
						logger.debug(appender + "Changing this directory to " + dir + ": " + ftp.changeDirectory(dir));
						logger.debug(appender + "Current Directory after change: " + ftp.printWorkingDirectory());
					} else {
						logger.debug(appender + "Fetching the content in current directory: Root");
					}

					logger.debug(appender + "fileExists(" + Serverfile + ")  = " + ftp.fileExists(Serverfile));

					if (ftp.fileExists(Serverfile)) {
						final String serverfile = ftp.getFileName(Serverfile);
						logger.debug(appender + "File " + serverfile + " to be downloaded to local directory : "+ localfilePath + "/");
						boolean retFlag = ftp.downloadFile(serverfile, localfilePath + "/" + serverfile + "_dw");
						if (retFlag) {
							logger.debug(appender + "File " + serverfile + " downloaded to destination directory " + localfilePath + ".");
							retStr = serverfile;
							boolean retbool = ftp.del(serverfile);
							logger.debug(appender + "File " + serverfile + " deleted from successfully from the destination directory.");
							try {
								File oldFile = new File(localfilePath + "/" + serverfile + "_dw");
								File newFile = new File(localfilePath + "/" + serverfile);
								logger.debug(appender + "tmp().renameTo() = " + oldFile.renameTo(newFile));
							} catch (Exception expI) {
								logException(expI, appender);
							}
						} else {
							logger.debug(appender + "File " + serverfile
									+ " cannot be downladed to destination directory " + localfilePath + ".");
							retStr = null;
						}
					} else {
						retStr = null;
					}
				} catch (Exception exp) {
				} finally {
					try {
						ftp.logout();
						ftp.disconnect();
					} catch (Exception expt) {
					}
				}
			} else {
				logger.debug(appender + "Unable to connect to: " + host);
			}
		} catch (Exception expM) {
		}
		return retStr;
	}

	private Vector<String> processFile(final String appender, final String fileName) {
		Vector<String> retVectStr = new Vector<String>();
		final File file = new File(fileName);
		try {
			Scanner scanner = new Scanner(file);
			while (scanner.hasNextLine()) {
				final String callBack = scanner.nextLine();
				boolean retBool = false;
				logger.debug(appender + callBack);
				if (callBack.length() > 3) {
					retBool = insertCallback(appender, callBack);
					String callBack_processed = null;
					logger.info(appender + "insertCallback(appender,callBack) = " + retBool + " retVectStr.size() = "+ retVectStr.size());

					if (retBool) {
						callBack_processed = callBack + "||Accepted";
					} /*
						 * write the success record into vector for SDP-SE
						 * response file
						 */
					else {
						callBack_processed = callBack + "||Rejected";
					} /*
						 * write the failed record into vector for SDP-SE
						 * response file
						 */
					if (callBack_processed != null)
						retVectStr.add(callBack_processed);
				}
			}
			scanner.close();
		} catch (Exception exp) {
			logException(exp, appender);
		}
		return retVectStr;
	}

	private boolean insertCallback(final String appender, final String callBack) {
		final String conPool = "Server";
		boolean retBool = false;
		String content = callBack;
		logger.debug(appender + content);
		// logValue.addLog(content);

		try {
			String[] record = content.split("[|][|]");
			logger.debug(appender + record[0] + " - " + record[1] + " - " + record[2] + " - " + record[3]);

			String RefId = "";
			int retry_num = 0;
			String circle = record[2].toLowerCase();
			String msisdn = record[1];
			String prepost = "P";
			String srvkey = "spfmm";
			String mode = "ftp_" + record[3];
			String price = "0.00";
			String sdpStatus = "success";
			String action = "act";
			String precharge = "n";
			String startDate = "";
			String startTime = "";
			String endDate = "";
			String endTime = "";
			String originator = "172.30.196.249";
			String cp_user = "SESPICE1";
			String cp_password = "13april2015";
			String info = content;
			String startDateTime = generateDate();
			String endDateTime = generateDate();
			String cdrDateTime = record[0];

//			if (msisdn.length() == 10) {
//				msisdn = "91" + msisdn;
//			}
			if (msisdn.length() > 10) {
				msisdn = msisdn.substring(2);
			}
			if (circle.equalsIgnoreCase("BH") || circle.equalsIgnoreCase("MB") || circle.equalsIgnoreCase("KA")
					|| circle.equalsIgnoreCase("WB") || circle.equalsIgnoreCase("UE") || circle.equalsIgnoreCase("AS")
					|| circle.equalsIgnoreCase("NS") || circle.equalsIgnoreCase("GJ") || circle.equalsIgnoreCase("HR")
					|| circle.equalsIgnoreCase("RJ") || circle.equalsIgnoreCase("AP") || circle.equalsIgnoreCase("UW")
					|| circle.equalsIgnoreCase("PB") || circle.equalsIgnoreCase("HP") || circle.equalsIgnoreCase("JK")) {
				if (circle.equalsIgnoreCase("MB")) {
					circle = "mu";
				}
				if (circle.equalsIgnoreCase("KA")) {
					circle = "kl";
				}
				if (circle.equalsIgnoreCase("NS")) {
					circle = "ne";
				}

				if (circle.equalsIgnoreCase("BH")) {
					if (!record[3].equals("13")) {
						logger.debug(appender + "Price Point (" + record[3] + ") not Configured for Circle: " + circle);
						return retBool;
					}
				}
				
				if (circle.equalsIgnoreCase("HP")) {
					if (!(record[3].equals("9")||record[3].equals("19")||record[3].equals("75"))) {
						logger.debug(appender + "Price Point (" + record[3] + ") not Configured for Circle: " + circle);
						return retBool;
					}
				}
				
				if (circle.equalsIgnoreCase("JK")) {
					if (!(record[3].equals("26")||record[3].equals("36")||record[3].equals("28"))) {
						logger.debug(appender + "Price Point (" + record[3] + ") not Configured for Circle: " + circle);
						return retBool;
					}
				}
				if (circle.equalsIgnoreCase("PB")) {
					if (!record[3].equals("28")) {
						logger.debug(appender + "Price Point (" + record[3] + ") not Configured for Circle: " + circle);
						return retBool;
					}
				}
				if (circle.equalsIgnoreCase("AP")) {
					if (!record[3].equals("43")) {
						logger.debug(appender + "Price Point (" + record[3] + ") not Configured for Circle: " + circle);
						return retBool;
					}
				}
				if (circle.equalsIgnoreCase("GJ")) {
					if (!(record[3].equals("33") || record[3].equals("69") || record[3].equals("97")
							|| record[3].equals("98") || record[3].equals("139"))) {
						logger.debug(appender + "Price Point (" + record[3] + ") not Configured for Circle: " + circle);
						return retBool;
					}
				}
				if (circle.equalsIgnoreCase("MU")) {
					if (!(record[3].equals("5") || record[3].equals("16") || record[3].equals("34")
							|| record[3].equals("37") || record[3].equals("57"))) {
						logger.debug(appender + "Price Point (" + record[3] + ") not Configured for Circle: " + circle);
						return retBool;
					}
				}
				if (circle.equalsIgnoreCase("UE")) {
					if (!(record[3].equals("9") || record[3].equals("23") || record[3].equals("16")
							|| record[3].equals("19") || record[3].equals("22") || record[3].equals("28"))) {
						logger.debug(appender + "Price Point (" + record[3] + ") not Configured for Circle: " + circle);
						return retBool;
					}
				}
				if (circle.equalsIgnoreCase("KL") || circle.equalsIgnoreCase("WB")) {
					if (!(record[3].equals("13") || record[3].equals("16") || record[3].equals("18")
							|| record[3].equals("27") || record[3].equals("29") || record[3].equals("31")
							|| record[3].equals("33") || record[3].equals("34") || record[3].equals("39"))) {
						logger.debug(appender + "Price Point (" + record[3] + ") not Configured for Circle: " + circle);
						return retBool;
					}
				}
				if (circle.equalsIgnoreCase("HR")) {
					if (!(record[3].equals("26") || record[3].equals("38") || record[3].equals("99")
							|| record[3].equals("108") || record[3].equals("109"))) {
						logger.debug(appender + "Price Point (" + record[3] + ") not Configured for Circle: " + circle);
						return retBool;
					}
				}
				if (circle.equalsIgnoreCase("RJ")) {
					if (!(record[3].equals("12") || record[3].equals("16") || record[3].equals("58"))) {
						logger.debug(appender + "Price Point (" + record[3] + ") not Configured for Circle: " + circle);
						return retBool;
					}
				}
				if (circle.equalsIgnoreCase("UW")) {
					if (!(record[3].equals("17") || record[3].equals("25") || record[3].equals("77"))) {
						logger.debug(appender + "Price Point (" + record[3] + ") not Configured for Circle: " + circle);
						return retBool;
					}
				}
				if (circle.equalsIgnoreCase("NE") || circle.equalsIgnoreCase("AS")) {
					if (!record[3].equals("97")) {
						logger.debug(appender + "Price Point (" + record[3] + ") not Configured for Circle: " + circle);
						return retBool;
					}
				}
			} else {
				logger.debug(appender + "Invalid Circle or Circle not Configured...");
				return retBool;
			}

			SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyyHHmmss");
			java.util.Date cdrTimeStamp = sdf.parse(cdrDateTime);
			java.util.Date old24hrsTimeStamp = getYesterdayDate();

			if (cdrTimeStamp.compareTo(old24hrsTimeStamp) < 0) {
				logger.debug(appender + "Record is more than 24hrs OLD. So rejecting it.");
				return retBool;
			}

			// DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd
			// HH:mm:ss");
			// logger.info(appender+"Date1 is:
			// "+dateFormat.format(date1).toString());

			HttpURLForDnd httpURLForDnd = new HttpURLForDnd();
			int status = httpURLForDnd.sendGet(msisdn, circle);
			logger.debug("dnd status[" + status + "] for msisdn[" + msisdn + "]");
			try {
				Connection conn_80 = null;
				int connCntr = 0;
				logger.debug("dnd status[" + status + "] for msisdn[" + msisdn + "]");				
				while (connCntr < 3) {
					conn_80 = connMgr.getConnection("conPool");
					if (conn_80 != null)
						break;
					else
						try {
							Thread.sleep(50);
						} catch (Exception exp) {
							logException(exp, appender);
						}
					connCntr++;
				}
				logger.debug(appender + "Connection conn = " + conn_80);
				if (conn_80 != null) {
					try {
						Statement stmt_80 = conn_80.createStatement();
							String insQry = "insert into sdpCentral_"+circle+".dbo.tbl_callback"
									+ "(RefId, retry_num, circle, msisdn, prepost, srvkey, mode, price, sdpStatus, status, action, precharge, startDateTime, endDateTime, originator, cp_user, cp_password, info) values(newid(),"
									+ retry_num + ",'" + circle + "','" + msisdn + "','" + prepost + "','" + srvkey
									+ "','" + mode + "','" + price + "','" + sdpStatus + "',0,'" + action
									+ "','" + precharge + "',getdate(), getdate()+30,'" + originator
									+ "','" + cp_user + "','" + cp_password + "','" + info + "')";
							logger.debug(appender + "Circle database pool = " + conn_80 + " ins stmt = " + insQry);
							logger.debug(appender + insQry + " = " + stmt_80.executeUpdate(insQry));
							retBool = true;						
					} catch (Exception exp) {
						logException(exp, appender);
					} finally {
						if (conn_80 != null) {
							connMgr.freeConnection(conPool, conn_80);
						}
					}
				} else
					logger.error(appender + "No free connections available");
			} catch (Exception expConn) {
				logException(expConn, appender);
				retBool = false;
			}
			
			try {
				Connection conn = null;
				int connCntr = 0;
				while (connCntr < 3) {
					conn = connMgr.getConnection("Server");
					if (conn != null)
						break;
					else
						try {
							Thread.sleep(50);
						} catch (Exception exp) {
							logException(exp, appender);
						}
					connCntr++;
				}
				logger.debug(appender + "Connection conn = " + conn);
				if (conn != null) {
					try {
						Statement stmt = conn.createStatement();
						ResultSet rs = stmt.executeQuery("select count(*) as count from national_freemium.tbl_callback_"
								+ circle + " where msisdn=" + msisdn);
						rs.next();
						int count = rs.getInt("count");
						rs.close();
						logger.debug("count of the msisdn is [" + count + "]");
						// stmt= conn.createStatement();
						if (count <= 0) {
							String insQry = "insert into national_freemium.tbl_callback_" + circle
									+ "(RefId, retry_num, circle, msisdn, prepost, srvkey, mode, price, sdpStatus, status, action, precharge, startDateTime, endDateTime, originator, cp_user, cp_password, info) values(uuid(),"
									+ retry_num + ",'" + circle + "','" + msisdn + "','" + prepost + "','" + srvkey
									+ "','" + mode + "','" + price + "','" + sdpStatus + "','" + status + "','" + action
									+ "','" + precharge + "',now(), ADDDATE(now(),INTERVAL 30 DAY),'" + originator
									+ "','" + cp_user + "','" + cp_password + "','" + info + "')";
							logger.debug(appender + "Circle database pool = " + conPool + " ins stmt = " + insQry);
							logger.debug(appender + insQry + " = " + stmt.executeUpdate(insQry));
							retBool = true;
						} else {
							String uptQry = "update national_freemium.tbl_callback_" + circle + " set mode='" + mode
									+ "', status='" + status
									+ "', startDateTime=now(), endDateTime=ADDDATE(now(),INTERVAL 30 DAY) where msisdn='"
									+ msisdn + "';";
							logger.debug(appender + "Circle database pool = " + conPool + " ins stmt = " + uptQry);
							logger.debug(appender + uptQry + " = " + stmt.executeUpdate(uptQry));
							retBool = true;
						}
					} catch (Exception exp) {
						logException(exp, appender);
					} finally {
						if (conn != null) {
							connMgr.freeConnection(conPool, conn);
						}
					}
				} else
					logger.error(appender + "No free connections available");
			} catch (Exception expConn) {
				logException(expConn, appender);
				retBool = false;
			}
		} catch (Exception exp) {
			logException(exp, appender);
			retBool = false;
		}
		return retBool;
	}

	private String writeProcessedFile(final String appender, final String path, final String fileName,final Vector<String> processedRecords) {
		String retStr = null;
		try {
			retStr = fileName.replace(".csv", "_Processed.csv");
			String file_name = retStr;
			File file = new File(path + "/" + file_name);
			FileWriter txt = new FileWriter(file);
			PrintWriter out = new PrintWriter(txt);

			// preparing the roll-in header
			String header = "80|" + getRollDateTime();
			String count = String.format("%1$" + 5 + "s", processedRecords.size()).replace(" ", "0");
			String footer = "90|" + count + "|" + getRollDateTime(); // 03092012164400

			out.println(header);
			int i = 0;
			while (i < processedRecords.size()) {
				out.println(processedRecords.get(i));
				i++;
			}
			out.println(footer);
			out.close();
		} catch (Exception exp) {
			logException(exp, appender);
			retStr = null;
		}
		return retStr;
	}

	private synchronized String getDateTime(final String appender, final String date, final String time) {
		String retDateTime = null;
		try {
			DateFormat sdp = new SimpleDateFormat("yyyyMMdd HHmmSS");
			DateFormat db = new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			java.util.Date dt_sdp = sdp.parse(date + " " + time);
			retDateTime = db.format(dt_sdp);
		} catch (Exception exp) {
			retDateTime = null;
			logException(exp, appender);
		}
		logger.debug(appender + retDateTime);
		return retDateTime;
	}

	private java.util.Date getYesterdayDate() {
		return new java.util.Date(System.currentTimeMillis() - 24 * 60 * 60 * 1000);
	}

	private String generateDate() {
		java.util.Date date = new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");
		String logDate = SQL_DATE_FORMAT.format(date.getTime());
		return logDate;
	}

	private String getRollDateTime() {
		java.util.Date date = new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT = new SimpleDateFormat("MMddyyyyHHmmss");
		String logDate = SQL_DATE_FORMAT.format(date.getTime());
		return logDate;
	}

	private void logException(Exception exp, final String appender) {
		logger.error(appender + getStackTrace(exp));
		alertValue.setExceptionAlert(appender + getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t) {
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		t.printStackTrace(pw);
		pw.flush();
		sw.flush();
		return sw.toString();
	}

	private boolean uploadFile(final String appender, final String host, final String user, final String pwd,
			final String dir, final String localfilePath, final String fileName) {
		boolean retBool = false;
		boolean uploadFlag = true;
		try {
			logger.info(appender + host + " - " + user + " - " + pwd + " - " + dir + " - " + localfilePath + " - "
					+ fileName);
			FtpWrapper ftp = new FtpWrapper();
			boolean login = ftp.connectAndLogin(host, user, pwd);
			if (login) {
				logger.debug(appender + "Connected to the FTP remote server: " + host);
				ftp.setPassiveMode(true);
				ftp.binary();
				try {
					logger.debug(appender + "Welcome message: " + ftp.getReplyString());
					logger.debug(appender + "Current Directory: " + ftp.printWorkingDirectory());
					if (!(dir.equalsIgnoreCase("none"))) {
						boolean retBoolDir = ftp.changeDirectory(dir);
						logger.debug(appender + "Changing this directory: " + retBoolDir);
						if (retBoolDir) {
							logger.debug(appender + "Current Directory after change: " + ftp.printWorkingDirectory());
						} else {
							logger.debug(appender
									+ "change Directory Failed. Trying to create the directory and than change.");
							boolean retBoolNewDir = ftp.makedirectory(dir);
							if (!retBoolNewDir) {
								logger.debug(appender
										+ "{cirtical}: creating new Directory Failed. FTP upload starting to root directory.");
								uploadFlag = true;
							}
						}
					} else {
						logger.debug(appender + "Uploading the content in current directory: Root");
					}

					if (uploadFlag) {
						boolean retFlag = ftp.uploadFile(localfilePath + "/" + fileName, fileName + "_up");
						logger.debug(
								appender + "Uploading file [" + fileName + "] to destination directory: " + retFlag);
						if (retFlag) {
							try {
								retFlag = ftp.ren(fileName + "_up", fileName);
							} catch (Exception rx) {
								retBool = false;
							}
							if (retFlag) {
								logger.debug(appender + "Renaming files in this directory: " + retFlag);
							} else {
								retFlag = ftp.deleteFile(fileName);
								if (retFlag) {
									try {
										retFlag = ftp.ren(fileName + "_up", fileName);
									} catch (Exception rx) {
										retBool = false;
									}
									if (retFlag) {
										logger.debug(appender + "Renaming files in this directory: " + retFlag);
									}
									logger.debug(appender + "File uploading to the destination directory [" + dir
											+ "] done host: " + host);
								}
							}

						}
						retBool = true;
					} else
						retBool = false;
				} catch (Exception exp) {
					logger.error(appender + "" + localfilePath + "/" + fileName + " = " + exp.toString());
					retBool = false;
				} finally {
					try {
						ftp.logout();
						ftp.disconnect();
					} catch (Exception expt) {
						retBool = false;
					}
				}
			} else {
				logger.debug(appender + "Unable to connect to: " + host);
			}
		} catch (Exception expM) {
			retBool = false;
		}
		return retBool;
	}
}