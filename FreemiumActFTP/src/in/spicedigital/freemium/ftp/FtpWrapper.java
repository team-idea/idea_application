package in.spicedigital.freemium.ftp;

import org.apache.commons.net.ftp.*;
import java.util.Vector;
import java.util.*;
import java.text.*;
import java.io.*;
import java.net.UnknownHostException;

import org.apache.log4j.*;

public class FtpWrapper extends FTPClient
{

	private static Logger logger 				= Logger.getLogger( FtpWrapper.class.getName() );

	public FtpWrapper(){try{PropertyConfigurator.configure("log4j.properties");}catch(Exception exp){ };}

	/** A convenience method for connecting and logging in */
	public boolean connectAndLogin (String host, String userName, String password) throws  IOException, UnknownHostException, FTPConnectionClosedException
	{
		boolean success 		= false;
		connect(host);
		int reply = getReplyCode();
		if (FTPReply.isPositiveCompletion(reply))
		{
			success = login(userName, password);
			if (!success)	disconnect();
		}
		return success;
	}

	public void setPassiveMode(boolean setPassive)
	{
		if (setPassive)		enterLocalPassiveMode();
		else				enterLocalActiveMode();
	}

	public boolean hasXCRCSupport() throws IOException
	{
		sendCommand("feat");
		String response = getReplyString();
		Scanner scanner = new Scanner(response);
		while(scanner.hasNextLine())
		{
			String line = scanner.nextLine();
			if(line.contains("XCRC")){return true;}
		}
		return false;
	}

	public boolean makedirectory(String mkDir)  throws IOException
	{
		boolean retBool							= false;
		boolean dirExists 						= true;
		String[] directories 					= mkDir.split("/");
		for (String dir : directories )
		{
			if (!dir.isEmpty() )
			{
				if (dirExists){dirExists 		= changeWorkingDirectory(dir);}

				if (!dirExists)
				{
					boolean retBoolMkDir		= false;
					try{retBoolMkDir			= makeDirectory(dir);}catch(Exception exp){exp.printStackTrace();}
					if (!retBoolMkDir)
					{
						throw new IOException("Unable to create remote directory '" + dir + "'.  error='" + getReplyString()+"'");
					}else {}
					if (!changeWorkingDirectory(dir))
					{
						throw new IOException("Unable to change into newly created remote directory '" + dir + "'.  error='" + getReplyString()+"'");
					}else retBool 				= true;
				}
			}
		}
		return retBool;
	}

	public boolean changeDirectory(String chDir)  		throws IOException	{return  changeWorkingDirectory(chDir);}
	public boolean ren(String from,String to) 			throws IOException	{return rename(from,to);}
	public boolean del(String pathname) 				throws Exception	{return deleteFile(pathname);}
	public boolean ascii () 							throws IOException 	{return setFileType(FTP.ASCII_FILE_TYPE);}			/** Use ASCII mode for file transfers */
	public boolean binary () 							throws IOException 	{return setFileType(FTP.BINARY_FILE_TYPE);}			/** Use Binary mode for file transfers */

	public String listFileNamesString () 				throws Exception	{return vectorToString(listFileNames(), "\n");}		/** Get the list of files in the current directory as a single Strings, delimited by \n (char '10') (excludes subdirectories) */
	public String listSubdirNamesString ()				throws Exception	{return vectorToString(listSubdirNames(), "\n");}	/** Get the list of subdirectories in the current directory as a single Strings delimited by \n (char '10') (excludes files) */
	public String returnLastFile () 					throws Exception	{return String.valueOf(getMaxLastModified(listFiles()).getName());}
	public FTPFile getMaxLastModified(FTPFile[] ftpFiles)					{return Collections.max(Arrays.asList(ftpFiles), new LastModifiedComparator());}


	public boolean downloadFile (String serverFile, String localFile) throws Exception											/** Download a file from the server, and save it to the specified local file */
	{
		FileOutputStream out = new FileOutputStream(localFile);
		boolean result = retrieveFile(serverFile, out);
		out.close();
		return result;
	}

	public boolean uploadFile (String localFile, String serverFile)	throws IOException, FTPConnectionClosedException			/** Upload a file to the server */
	{
		FileInputStream in = new FileInputStream(localFile);
		boolean result = storeFile(serverFile, in);
		in.close();
		return result;
	}

	public Vector listFileNames ()	throws IOException, FTPConnectionClosedException 											/** Get the list of files in the current directory as a Vector of Strings (excludes subdirectories) */
	{
		FTPFile[] files = listFiles();
		Vector v = new Vector();
		for (int i = 0; i < files.length; i++)
		{
			if (!files[i].isDirectory())
			{
				java.util.Calendar cal 				= files[i].getTimestamp();
				java.util.Date 	   date 			= cal.getTime();
				SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("yyyyMMddhhmmss");
				v.addElement(files[i].getName()+","+SQL_DATE_FORMAT.format(date.getTime()));
			}
		}
		return v;
	}

	public boolean fileExists (String fileName)	throws IOException, FTPConnectionClosedException
	{
		final String appender						= "["+String.format("%-10s",Thread.currentThread().getName())+"] ->(ftpWrp:al) ";
		boolean retBool = false;
		FTPFile[] files = listFiles();
		for (int i = 0; i < files.length; i++)
		{
			if (!files[i].isDirectory())
			{
				java.util.Calendar cal 				= files[i].getTimestamp();
				java.util.Date 	   date 			= cal.getTime();
				SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("yyyyMMddhhmmss");
				//if(files[i].getName().equalsIgnoreCase(fileName)){retBool = true; break;}
				logger.debug(appender+"File Name is: "+files[i].getName().toLowerCase());
				if(files[i].getName().toLowerCase().startsWith(fileName.toLowerCase())&&files[i].getName().toLowerCase().endsWith(".csv")&&(!files[i].getName().toLowerCase().endsWith("_processed.csv"))){retBool = true; break;}
			}
		}
		return retBool;
	}

	public String getFileName (String fileName) throws IOException, FTPConnectionClosedException
	{
		String retStr = null;
		FTPFile[] files = listFiles();
		for (int i = 0; i < files.length; i++)
		{
			if (!files[i].isDirectory())
			{
				String dateTime						= generateDate();
				final String FileName				= fileName+"_"+dateTime;
				if(files[i].getName().toLowerCase().startsWith(fileName.toLowerCase())&&files[i].getName().toLowerCase().endsWith(".csv")&&(!files[i].getName().toLowerCase().endsWith("_processed.csv"))){retStr = files[i].getName();break;}
			}
		}
		return retStr;
	}

	private String generateDate()
	{
		java.util.Date date 						= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT			= new SimpleDateFormat("yyyyMMdd");
		String logDate								= SQL_DATE_FORMAT.format(date.getTime());
		return logDate;
	}


	/** Get the list of subdirectories in the current directory as a Vector of Strings (excludes files) */
	public Vector listSubdirNames () throws IOException, FTPConnectionClosedException
	{
		FTPFile[] files = listFiles();
		Vector v = new Vector();
		for (int i = 0; i < files.length; i++){if (files[i].isDirectory())	v.addElement(files[i].getName());}
		return v;
	}

	/** Convert a Vector to a delimited String */
	private String vectorToString (Vector v, String delim)
	{
		StringBuffer sb = new StringBuffer();
		String s = "";
		for (int i = 0; i < v.size(); i++)
		{
			sb.append(s).append((String)v.elementAt(i));
			s = delim;
		}
		return sb.toString();
	}
}

class LastModifiedComparator implements Comparator<FTPFile>
{
	public int compare(FTPFile file_1,FTPFile file_2)
	{
		return file_1.getTimestamp().compareTo(file_2.getTimestamp());
	}
}