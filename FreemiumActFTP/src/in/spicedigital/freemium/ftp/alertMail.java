package in.spicedigital.freemium.ftp;


import java.util.*;
import java.io.*;
import java.sql.*;
import java.text.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import org.apache.log4j.*;


public class alertMail extends Thread
{
	private static int clients						= 0;
	private static alertMail instance				= null;				       		// The single instance

	private boolean html							= true;
	private static Logger logger 					= Logger.getLogger(alertMail.class.getName());

	private static String _to						= "sandeep.rajan@spicedigital.in";
	private static String _cc						= "sandeep.rajan@spicedigital.in";
	private static String _bcc						= "sandeep.rajan@spicedigital.in";
	private static String from						= "sdpse.idea@spicedigital.in";
	private static String smtpServer				= "webmail.cellebrum.com";
	private static String accountUserName			= "sdpse.idea";
	private static String accountUserPwd			= "p@ss@4321";
	private static String attachment				= "output.txt";



	static synchronized public alertMail getInstance()
	{
		if (instance == null)
		{
			instance = new alertMail();
			Thread mail								= new Thread(instance);	//mail.start();
		}
		clients++;
		return instance;
	}

    public alertMail()	{init();}


	public void init()
	{
		try
		{
			try{PropertyConfigurator.configure("log4j.properties");}catch(Exception e ){e.printStackTrace();}

			Properties prop = new Properties();
			prop.load(new DataInputStream(new FileInputStream("sdp.properties")));

			_to						= prop.getProperty("alert.mail.to");
			_cc						= prop.getProperty("alert.mail.cc");
			_bcc					= prop.getProperty("alert.mail.bcc");
			from					= prop.getProperty("alert.mail.from");
			smtpServer				= prop.getProperty("alert.mail.smtpServer");
			accountUserName			= prop.getProperty("alert.mail.accountUserName");
			accountUserPwd			= prop.getProperty("alert.mail.accountUserPwd");
			attachment				= prop.getProperty("alert.mail.attachment");
			try{html				= Boolean.parseBoolean(prop.getProperty("alert.mail.html"));}catch(Exception exp){html = false;}
		}catch (Exception e){logException(e);}
	}

	public boolean sendMail(final String messageSubject,final String message)
	{
		return sendMail( _to, _cc, _bcc, from, smtpServer,"25", accountUserPwd, messageSubject, message, attachment, accountUserName, accountUserPwd);
	}

	public boolean sendMail(final String listTo,final String listCc,final String listBcc,String From,final String messageSubject,final String message,final String Attachment)
	{
		return sendMail( listTo, listCc, listBcc, From, smtpServer,"25",accountUserPwd, messageSubject, message, Attachment, accountUserName, accountUserPwd);
	}

    /*private boolean sendMail(final String to,final String cc,final String bcc,final String from,final String host,final String pwd,final String messageSubject,final String message,final String attachment,final String username,final String password)
    {

		Properties props 		= new Properties();														// Create properties for the Session

		props.put("mail.debug", "true");																// To see what is going on behind the scene
		props.put("mail.smtp.host", host);																// If using static Transport.send(),need to specify the mail server here
		//props.put("mail.smtp.socketFactory.port", "25");
		//props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "false");
		props.put("mail.smtp.port", "25");


		//Session session			= Session.getInstance(props);											// Get a session

		Session session			= Session.getDefaultInstance(props,new javax.mail.Authenticator()
								  {
									protected PasswordAuthentication getPasswordAuthentication()
									{
										return new PasswordAuthentication(username,password);
									}
								   });


        try
        {

            Transport bus		= session.getTransport("smtp");											// Get a Transport object to send e-mail
            bus.connect(host,from,pwd);																// Connect only once here Transport.send() disconnects after each send Usually, no username and password is required for SMTP
            Message msg 		= new MimeMessage(session);												// Instantiate a message


            msg.setFrom(new InternetAddress(from,"Idea SDP SE"));														// Set message attributes

            String addressListTo[]		= to.split(",");
            InternetAddress[] addressTo	= new InternetAddress[addressListTo.length];
			for(int i=0;i<addressListTo.length;i++){addressTo[i]				= new InternetAddress(addressListTo[i]);}


            String addressListCc[]		= cc.split(",");
            InternetAddress[] addressCc	= new InternetAddress[addressListCc.length];
			for(int i=0;i<addressListCc.length;i++){addressCc[i]				= new InternetAddress(addressListCc[i]);}

            String addressListBcc[]		= bcc.split(",");
            InternetAddress[] addressBcc	= new InternetAddress[addressListBcc.length];
			for(int i=0;i<addressListBcc.length;i++){addressBcc[i]				= new InternetAddress(addressListBcc[i]);}

            msg.setRecipients(Message.RecipientType.TO, addressTo);										// Parse a comma-separated list of email addresses. Be strict.
			msg.setRecipients(Message.RecipientType.CC, addressCc);
            msg.setRecipients(Message.RecipientType.BCC,addressBcc);									// Parse comma/space-separated list. Cut some slack.
            msg.setSubject(messageSubject);
            msg.setSentDate(new java.util.Date());
			//setHTMLContent(msg,message);


			BodyPart messageBodyPart = new MimeBodyPart();

			Multipart multipart = new MimeMultipart();

			if(html){messageBodyPart.setContent(message, "text/html");}
			else	{messageBodyPart.setText(message);}

			multipart.addBodyPart(messageBodyPart);
			String[] attachementFinal = attachment.split(",");
			for(int i=0;i<attachementFinal.length;i++)
			{
				try
				{
					attachementFinal[i] 		= attachementFinal[i].replace("yyyymmdd",generateDate());
					File file					= new File(attachementFinal[i] );
					boolean exists 				= file.exists();
					if (exists)
					{
						String fileNameActual = new java.io.File(attachementFinal[i]).getName();
						logger.debug("attachementFinal["+i+"] = "+attachementFinal[i]+" fileNameActual = "+fileNameActual);
						// Part two is attachment
						messageBodyPart = new MimeBodyPart();
						DataSource source = new FileDataSource(attachementFinal[i]);
						messageBodyPart.setDataHandler(new DataHandler(source));
						messageBodyPart.setFileName(fileNameActual);
						multipart.addBodyPart(messageBodyPart);
					}else {logger.debug("File "+attachementFinal[i]+" not present");}
				}catch(Exception exp){logException(exp);}
			}

			// Put parts in message
			msg.setContent(multipart);


            msg.saveChanges();
            bus.send(msg);
            //bus.sendMessage(msg,address);
            bus.close();
			return true;
        }
        catch (MessagingException mex)
        {

            mex.printStackTrace();																		// Prints all nested (chained) exceptions as well

            while (mex.getNextException() != null)														// How to access nested exceptions
            {
                Exception ex = mex.getNextException();													// Get next exception in chain
                ex.printStackTrace();
                if (!(ex instanceof MessagingException)) break;
                else mex = (MessagingException)ex;
            }
            return false;
        }
        catch(Exception exp){logException(exp);return false;}
    }

	private String generateDate()
	{
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		cal.add(Calendar.DATE, -1);
		return String.valueOf(dateFormat.format(cal.getTime()));

	}

	private void logException(Exception exp)
	{
		logger.error(getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
		StringWriter sw 	= new StringWriter();
		PrintWriter pw 		= new PrintWriter(sw, true);
		t.printStackTrace(pw);
		pw.flush();
		sw.flush();
		return sw.toString();
	}*/
	private boolean sendMail(final String to,final String cc,final String bcc,final String from,final String host,final String port,final String pwd,final String messageSubject,final String message,final String attachement,final String username,final String password)
	{
		try
		{


			String addressListTo[]				= to.split(",");
			InternetAddress[] addressTo			= new InternetAddress[addressListTo.length];
			for(int i=0;i<addressListTo.length;i++){addressTo[i]= new InternetAddress(addressListTo[i]);}

			String addressListCc[]				= cc.split(",");
			InternetAddress[] addressCc			= new InternetAddress[addressListCc.length];
			for(int i=0;i<addressListCc.length;i++){addressCc[i]= new InternetAddress(addressListCc[i]);}

			String addressListBcc[]				= bcc.split(",");
			InternetAddress[] addressBcc		= new InternetAddress[addressListBcc.length];
			for(int i=0;i<addressListBcc.length;i++){addressBcc[i]= new InternetAddress(addressListBcc[i]);}

			Session session						= this.getSession("true","false",host,port,username,password);
			Message msg 						= new MimeMessage(session);																// Instantiate a message
			msg.setFrom(new InternetAddress(from,"SdpSe Idea"));																		// Set message attributes
			msg.setRecipients(Message.RecipientType.TO, addressTo);																		// Parse a comma-separated list of email addresses. Be strict.
			msg.setRecipients(Message.RecipientType.CC, addressCc);																		// Parse comma/space-separated list. Cut some slack.
			msg.setRecipients(Message.RecipientType.BCC,addressBcc);																	// Parse comma/space-separated list. Cut some slack.
			msg.setSubject(messageSubject);
			msg.setSentDate(new java.util.Date());

			BodyPart messageBodyPart			= new MimeBodyPart();
			Multipart multipart 				= new MimeMultipart();

			if(html){messageBodyPart.setContent(message, "text/html");}
			else	{messageBodyPart.setText(message);}

			multipart.addBodyPart(messageBodyPart);
			String[] attachementFinal 			= attachement.split(",");
			for(int i=0;i<attachementFinal.length;i++)
			{
				try
				{
					attachementFinal[i] 		= attachementFinal[i].replace("yyyymmdd",generateDate());
					File file					= new File(attachementFinal[i] );
					boolean exists 				= file.exists();
					if (exists)
					{
						String fileNameActual 	= new java.io.File(attachementFinal[i]).getName();
						logger.debug("attachementFinal["+i+"] = "+attachementFinal[i]+" fileNameActual = "+fileNameActual);
						// Part two is attachment
						messageBodyPart 		= new MimeBodyPart();
						DataSource source 		= new FileDataSource(attachementFinal[i]);
						messageBodyPart.setDataHandler(new DataHandler(source));
						messageBodyPart.setFileName(fileNameActual);
						multipart.addBodyPart(messageBodyPart);
					}else {logger.debug("File "+attachementFinal[i]+" not present");}
				}catch(Exception exp){logException(exp);}
			}

			// Put parts in message
			msg.setContent(multipart);
			msg.saveChanges();


			Transport bus			= session.getTransport("smtp");															// Get a Transport object to send e-mail
			bus.connect(host,from,pwd);																					// Connect only once here Transport.send() disconnects after each send Usually, no username and password is required for SMTP
			bus.send(msg);
			bus.close();
			return true;
		}
		catch (MessagingException mex)
		{

			mex.printStackTrace();																		// Prints all nested (chained) exceptions as well

			while (mex.getNextException() != null)														// How to access nested exceptions
			{
				Exception ex = mex.getNextException();													// Get next exception in chain
				ex.printStackTrace();
				if (!(ex instanceof MessagingException)) break;
				else mex = (MessagingException)ex;
			}
			return false;
		}
		catch(Exception exp){logException(exp);return false;}
	}

	private String generateDate()
	{
		Calendar cal = Calendar.getInstance();
		DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
		cal.add(Calendar.DATE, -1);
		return String.valueOf(dateFormat.format(cal.getTime()));
	}

	private void logException(Exception exp)
	{
		logger.error(getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
		StringWriter sw 	= new StringWriter();
		PrintWriter pw 		= new PrintWriter(sw, true);
		t.printStackTrace(pw);
		pw.flush();
		sw.flush();
		return sw.toString();
	}

	private Session getSession(String debugStatus,String authorization,String host,String port,String username,String password)
	{
		Authenticator authenticator		= new Authenticator(username, password);
		Properties properties 			= new Properties();
		properties.setProperty("mail.debug",debugStatus);
		properties.setProperty("mail.transport.protocol", "smtp");
		properties.setProperty("mail.smtp.submitter", authenticator.getPasswordAuthentication().getUserName());
		properties.setProperty("mail.smtp.auth", authorization);
		properties.setProperty("mail.smtp.host", host);
		properties.setProperty("mail.smtp.port", port);

		//properties.setProperty("mail.smtp.socketFactory.port", port);
		//properties.setProperty("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");

		return Session.getInstance(properties, authenticator);
	}

	private class Authenticator extends javax.mail.Authenticator
	{
		private PasswordAuthentication authentication;
		public Authenticator(String username,String password)			{authentication = new PasswordAuthentication(username, password);}
		protected PasswordAuthentication getPasswordAuthentication() 	{return authentication;}
	}
}