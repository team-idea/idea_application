package in.spicedigital.freemium.ftp;
import java.io.*;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import org.apache.log4j.*;

import in.spicedigital.freemium.util.DBConnectionManager;
import in.spicedigital.freemium.util.sdpLogger;

public class pullRecords extends Thread
{



	private static Logger logger 				= Logger.getLogger( pullRecords.class.getName() );
	private static String CONFIG_FILE			= "log4j.properties";
	private DBConnectionManager connMgr			= null;
	private String conPool_site					= "Site";
	private String conPool_server				= "Server";
	private String circle_name					= "";
	private static String service_key_select	= "";

	private static int select_top_rows			= 10;
	private static int thread_sleeptime			= 10;
	private static int idle_log_print			= 5;

	private static sdpLogger logValue			= null;

	private static configureAlerts alertValue	= null;

	public pullRecords(final String circle)
	{
		try
		{
			PropertyConfigurator.configure("log4j.properties");
			alertValue							= configureAlerts.getInstance();
			connMgr 							= DBConnectionManager.getInstance();
			logValue							= sdpLogger.getInstance();
			Properties prop = new Properties();
			prop.load(new DataInputStream(new FileInputStream("sdp.properties")));
			select_top_rows	= Integer.parseInt(prop.getProperty("sdp.Request.top.select"));
			thread_sleeptime= Integer.parseInt(prop.getProperty("sdp.Request.sleeptime.select"));
			service_key_select=prop.getProperty("sdp.circle."+circle+".service");
			service_key_select="'"+service_key_select+"'";
			service_key_select=service_key_select.replace(",","','");
			idle_log_print= Integer.parseInt(prop.getProperty("sdp.idle.log.print"));if(idle_log_print<=0) idle_log_print = 5;
		}
		catch(Exception exp){exp.printStackTrace();}
		this.conPool_site						= circle+conPool_site;
		this.conPool_server						= conPool_server;
		this.circle_name						= circle;
		logger.info("[Thread-X  ] ->(pull0 :"+circle_name+") Starting pull thread for "+circle_name+".");
		this.start();
	}

	public void run()
	{
		final String threadName			= String.format("%-10s",Thread.currentThread().getName());
		final String poolSite			= conPool_site;
		final String poolServer			= conPool_server;
		final String circleName			= circle_name;
		final String service_key		= service_key_select;
		int prntCtr						= 0;
		while(true)
		{
			int processedRows 			= processCircleReq(threadName,poolSite,poolServer,circleName,service_key);
			if(processedRows<=0)
			{
				prntCtr++;
				if(prntCtr%idle_log_print == 0)
				{
					logger.info("["+threadName+"] ->(pull0 :"+circleName+") No Records found to be processed. Sleeping for "+String.format("%3d",thread_sleeptime)+" secs. ");
					prntCtr = 0;
				}
				try{Thread.sleep(thread_sleeptime*1000);}catch(Exception expTmr){}
			}
			else{logger.info("["+threadName+"] ->(pull0 :"+circleName+") Total Records processed = "+processedRows+". Sleeping for 500 msecs.");try{Thread.sleep(500);}catch(Exception expTmr){}}
		}
	}

	private int processCircleReq(final String threadName,final String poolSite,final String poolServer,final String circleName,final String service_key)
	{
		int retCode = 0;
		try
		{
			Connection connSelect			= connMgr.getConnection(poolSite);
			Connection connUpdate			= connMgr.getConnection(poolSite);
			Connection connInsert			= connMgr.getConnection(poolServer);
			try
			{
				if(connSelect!=null&&connUpdate!=null&&connInsert!=null)
				{
					logger.debug("["+threadName+"] ->(pull0 :"+circleName+") Into processCircleReq() function. poolServer= "+poolServer+" poolSite= "+poolSite);
					Statement stmtSelect	= connSelect.createStatement();
					Statement stmtUpdate	= connUpdate.createStatement();
					Statement stmtInsert	= connInsert.createStatement();
					String stmSelect		= "select top "+select_top_rows+" RefId, circle, msisdn, prepost, srvKey, eventKey, reqType, precharge, reqdateTime, mode, status, originator from sdpSite.dbo.tbl_sdpRequest with(nolock) where circle = '"+circleName+"' and status =0 and srvKey in ("+service_key+") and mode in ('SMS','WEB','WAP','IVR','OBD','CRM','RetailVTopUp','STK','USSD','CC','SANCHYA','SIVR','CBC','ivr_offnet')"; //
					logger.debug("["+threadName+"] ->(pull0 :"+circleName+") "+stmSelect);
					ResultSet rs			= stmtSelect.executeQuery(stmSelect);
					logger.debug("["+threadName+"] ->(pull0 :"+circleName+") Into select ResultSet iteration");
					while(rs.next())
					{
						String RefId				= rs.getString("RefId");
						String circle				= rs.getString("circle");
						String msisdn				= rs.getString("msisdn");
						String prepost				= rs.getString("prepost");
						String srvKey				= rs.getString("srvKey");
						String eventKey				= rs.getString("eventKey");
						String reqType				= rs.getString("reqType");
						String precharge			= rs.getString("precharge");
						String reqDateTime			= rs.getString("reqdateTime");
						String mode					= rs.getString("mode");
						String status				= rs.getString("status");
						String originator			= rs.getString("originator");

						if(circle.length()>2){circle = circleName;}

						if(msisdn.length() == 10 || msisdn.length() == 12)
						{
							String insert_stmt			= "insert into sdpCentral_"+circleName+".dbo.tbl_sdpRequest(RefId, circle, msisdn, prepost, srvKey, eventKey, reqType, precharge, reqdateTime, mode, status, originator,num_retry) values ('"+RefId+"','"+circle+"','"+msisdn+"','"+prepost+"','"+srvKey+"','"+eventKey+"','"+reqType+"','"+precharge+"','"+reqDateTime+"','"+mode+"','"+status+"','"+originator+"',0)";
							String update_stmt			= "";
							int rowCtr					= 0;
							try
							{
								rowCtr					= stmtInsert.executeUpdate(insert_stmt);
								update_stmt				= "update sdpSite.dbo.tbl_sdpRequest set status = 1 where RefId='"+RefId+"' and srvKey = '"+srvKey+"' and msisdn='"+msisdn+"'";
							}catch(Exception exp){logger.error("["+threadName+"] ->pull0 :"+circleName+") "+insert_stmt+": error");logException(exp,circle,threadName);rowCtr=0;}

							logger.debug("["+threadName+"] ->(pull0 :"+circleName+") "+insert_stmt+":"+rowCtr);
							logValue.addLog("pull0 :"+circle+"|"+RefId+"|"+msisdn+"|"+srvKey+"|"+eventKey+"|"+reqType+"|"+mode+"|"+reqDateTime+"|"+rowCtr+"|");
							if(rowCtr != 1)	{logger.error("["+threadName+"] ->(pull0 :"+circleName+") Not Able to submit the message correctly to central Database rowCtr = "+rowCtr);}
							else 			{stmtUpdate.executeUpdate(update_stmt); }
							try{Thread.sleep(10*2);}catch(Exception expT){}
							retCode++;
						}
						else
						{
							logger.debug("["+threadName+"] ->(pull0 :"+circleName+") Invalid msisdn ["+msisdn+"] received. Updating the entry as invalid entry.");
							try{Thread.sleep(10*5);}catch(Exception expT){}
							String update_stmt			= "update sdpSite.dbo.tbl_sdpRequest set status = -1 where RefId='"+RefId+"' and srvKey = '"+srvKey+"' and msisdn='"+msisdn+"'";
							stmtUpdate.executeUpdate(update_stmt);
						}
					}
				}
				else
				{
					logger.info("["+threadName+"] ->(pull0 :"+circleName+") [Site: "+poolSite+"<--->"+poolServer+" :Server] Connection Cannot be established connSelect="+connSelect+" connUpdate="+connUpdate+" connInsert"+connInsert);
					try{Thread.sleep(1000*3);}catch(Exception expTmr){}
					//retCode = 0;
					alertValue.setDatabaseConnAlert("["+threadName+"] ->(pull0 :"+circleName+") [Site: "+poolSite+"<--->"+poolServer+" :Server] Connection Cannot be established connSelect="+connSelect+" connUpdate="+connUpdate+" connInsert"+connInsert);
					try{Thread.sleep(1000*3);}catch(Exception expTmr){}
				}
			}catch(Exception expCon){logException(expCon,circleName,threadName);}
			finally
			{
				if(connSelect!=null) connMgr.freeConnection(poolSite,connSelect);	else connMgr.dropConnection(poolSite,connSelect);
				if(connUpdate!=null) connMgr.freeConnection(poolSite,connUpdate);	else connMgr.dropConnection(poolSite,connUpdate);
				if(connInsert!=null) connMgr.freeConnection(poolServer,connInsert);	else connMgr.dropConnection(poolServer,connInsert);
			}
		}
		catch(Exception exp){logException(exp,circleName,threadName);}
		return retCode;
	}


	private String generateUUID()
	{
		UUID idOne = UUID.randomUUID();
		return ""+idOne;
	}

	private String generateDateTime()
	{
		java.util.Date date 				= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
		return SQL_DATE_FORMAT.format(date.getTime());
	}

	private void logException(Exception exp,final String circleName,final String threadName)
	{
		logger.error("["+threadName+"] ->(pull0 :"+circleName+") "+getStackTrace(exp));
		alertValue.setExceptionAlert("["+threadName+"] ->(pull0 :"+circleName+") "+getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}

}


