package in.spicedigital.freemium.util;

import java.text.*;
import java.io.*;
import java.util.zip.*;
import java.util.Properties;

import org.apache.log4j.*;

import in.spicedigital.freemium.ftp.configureAlerts;

public class zipper extends Thread
{
	private static Logger logger 				= Logger.getLogger( zipper.class.getName() );

	private static int thread_sleeptime	= 20;
	private static boolean keepAlive	= true;
	private static String sourceFolder	= "";
	private static configureAlerts alertValue	= null;

	public zipper()
	{
		try
		{
			PropertyConfigurator.configure("log4j.properties");
			alertValue					= configureAlerts.getInstance();
			Properties prop 			= new Properties();
			prop.load(new DataInputStream(new FileInputStream("sdp.properties")));
			thread_sleeptime			= Integer.parseInt(prop.getProperty("sdp.zipper.sleeptime.value"));
			sourceFolder				= prop.getProperty("sdp.zipper.sourceFolder.value");
			this.start();
		}
		catch(Exception exp){exp.printStackTrace();}
	}

	public void run()
	{
		final String[] sourceFolders	= sourceFolder.split(",");
		while(keepAlive)
		{
			logger.debug("Starting the file Zipping process...");
			if(sourceFolders.length>0)
			{
				for(int i=0;i<sourceFolders.length;i++)
				{
					try
					{
						String[] folderInfo		= sourceFolders[i].split("-");
						String dateTime			= generateDateTime();
						String folderName		= folderInfo[0];folderName = folderName.replace("YYYYMMDD", dateTime.substring(0,dateTime.indexOf("_")));
						String excludeExtn		= folderInfo[1];
						boolean retBool			= zip(folderName,excludeExtn);
						logger.debug("zip("+folderName+","+excludeExtn+") = "+retBool);
					}catch(Exception exp){logException(exp);}
				}
			}
			try{Thread.sleep(thread_sleeptime*1000);}catch(Exception exp){logException(exp);}
		}
	}

	private boolean zip(final String sourceFolder,final String excludeExtn)
	{
		boolean retBool					= false;
		ZipOutputStream out 			= null;

		try
		{
			String dateTime				= generateDateTime();
			File inFolder				= new File(sourceFolder);
			File outFolder				= new File(sourceFolder+"/"+dateTime+".zip");
			out							= new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(outFolder)));
			BufferedInputStream in 		= null;
			byte[] data    				= new byte[1000];
			FilenameFilter filter 		= new FilenameFilter() {public boolean accept(File dir, String name) {return !(name.endsWith("."+excludeExtn)||name.endsWith(".zip"));}};
			File[]  files 				= new File(sourceFolder).listFiles(filter);


			for (int i=0; i<files.length; i++)
			{
				if (files[i].isFile())
				{
					in 					= new BufferedInputStream(new FileInputStream(inFolder.getPath() + "/" + files[i].getName()), 1000);
					out.putNextEntry(new ZipEntry(files[i].getName()));
					int count;
					while((count 		= in.read(data,0,1000)) != -1)
					{
						out.write(data, 0, count);
					}
					out.closeEntry();
					in.close();

					File tmp 		= new File(inFolder.getAbsolutePath() + "/" + files[i].getName());
					logger.debug("tmp = "+tmp);
					logger.debug("tmp.delete() = "+tmp.delete());
					retBool			= true;
				}
			}
			out.flush();
			out.close();
		}catch(Exception exp){/*logException(exp);*/try{out.flush();out.close();}catch(Exception expI){/*logException(expI);*/}}
		return retBool;
	}


 	private boolean isZipValid(final String fileName)
 	{
		boolean retBool		= false;
		ZipFile zipfile 	= null;
		try
		{
			final File file = new File(fileName);
			zipfile 		= new ZipFile(file);
			retBool 		= true;
		}catch(Exception expZ){logException(expZ);}
		finally{try{if (zipfile != null){zipfile.close();zipfile = null;}}catch(IOException exp){logException(exp);}}
		return retBool;
	}

	private String generateDateTime()
	{
		java.util.Date date 				= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("yyyyMMdd_HHmm");
		return SQL_DATE_FORMAT.format(date.getTime());
	}

	private void logException(Exception exp)
	{
		logger.error(getStackTrace(exp));
		alertValue.setExceptionAlert(getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}
}
