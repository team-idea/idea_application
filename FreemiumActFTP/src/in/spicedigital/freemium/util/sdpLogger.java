package in.spicedigital.freemium.util;

import java.text.*;
import java.util.*;
import java.io.*;
import java.net.*;
import org.apache.log4j.*;

import in.spicedigital.freemium.ftp.configureAlerts;

public class sdpLogger implements Runnable
{
	private static Logger logger 					= Logger.getLogger(sdpLogger.class.getName());
	static private boolean keepAlive				= true;
	static private boolean waitFlag					= false;
	static private String dateLogger				= null;
	static private sdpLogger instance				= null;				       		// The single instance
	static private int clients						= 0;
	static private Vector<String> logVector			= new Vector<String>();
	static private FileWriter fstream 				= null;
	static private int loggerValueThreshHold		= 1500;
	static private int thread_sleeptime				= 5;
	static private String fileName					= "";
	private static configureAlerts alertValue		= null;


	static synchronized public sdpLogger getInstance()
	{
		if (instance == null)
		{
			instance = new sdpLogger();
			alertValue=configureAlerts.getInstance();
			Thread sdpLog							= new Thread(new sdpLogger());	sdpLog.start();
		}
		clients++;
		return instance;
	}

	public sdpLogger()
	{
		try
		{
			try
			{
				PropertyConfigurator.configure("log4j.properties");
				Properties prop = new Properties();
				prop.load(new DataInputStream(new FileInputStream("sdp.properties")));
				loggerValueThreshHold	= Integer.parseInt(prop.getProperty("sdp.logger.threshold.value"));
				thread_sleeptime= Integer.parseInt(prop.getProperty("sdp.logger.sleeptime.value"));
			}
			catch(Exception exp){exp.printStackTrace();}
			dateLogger								= generateDate();
			boolean success							= (new File("sdpLogs/"+dateLogger.substring(0,dateLogger.indexOf("_")))).mkdirs();
			if(success)	logger.debug("Directory Created for hourly Records.");
			fstream									= new FileWriter("sdpLogs/"+dateLogger.substring(0,dateLogger.indexOf("_"))+"/sdpRecords_"+dateLogger+".txt",true);
			logger.debug("["+String.format("%-10s",Thread.currentThread().getName())+"] <>(log : xx) loggerValueThreshHold set as "+loggerValueThreshHold);
		}catch(Exception exp){exp.printStackTrace();}
	}

	public synchronized int addLog(String reqStr)
	{
		int retCode	= 0;
		reqStr		=String.format("|%-10s %s",generateTime(),reqStr);
		do
		{
			if(!waitFlag)	try{logVector.add(reqStr); break; }catch(Exception exp){retCode = -1;}
			else			try{Thread.sleep(20);continue;}catch(Exception exp){} ;
		}while(waitFlag);
		return retCode;
	}

	public void run()
	{
		BufferedWriter out 							= null;
		final String threadName						= String.format("%-10s",Thread.currentThread().getName());
		int prntCtr									= 0;
		while(keepAlive)
		{
			if(dateLogger.equalsIgnoreCase(generateDate()))
			{
				logger.debug("["+threadName+"] <>(log : xx) logVector.size() = "+logVector.size()+" loggerValueThreshHold = "+loggerValueThreshHold);
				if(logVector.size() >= loggerValueThreshHold)
				{
					try
					{
						waitFlag	= true;
						Vector<String> logVectorLocal 		= (Vector) logVector.clone(); logVector.clear();
						waitFlag	= false;
						out 		= new BufferedWriter(fstream);
						for(int i=0;i<logVectorLocal.size();i++)
						{
							String logStr	= logVectorLocal.get(i)+"\n";
							out.write(logStr);
						}
						out.flush();
						logVectorLocal.clear();
						Thread.sleep(5);
					}catch(Exception exp){logException(exp);}

				}else {try{Thread.sleep(1000*thread_sleeptime);continue;}catch(Exception exp){}}
			}
			else
			{
				try
				{
					dateLogger	= generateDate();
					try
					{
						waitFlag	= true;
						Vector<String> logVectorLocal 		= (Vector) logVector.clone(); logVector.clear();
						waitFlag	= false;
						out 		= new BufferedWriter(fstream);
						for(int i=0;i<logVectorLocal.size();i++)
						{
							String logStr	= logVectorLocal.get(i)+"\n";
							out.write(logStr);
						}
						out.flush();
						logVectorLocal.clear();
						Thread.sleep(5);
					}catch(Exception exp){logException(exp);}
					fstream.close();

					logger.debug("["+threadName+"] <>(log : xx) fileName = "+fileName);
					try
					{
						File oldFile = new File(fileName); File newFile = new File(fileName+".cmp");
						logger.debug("["+threadName+"] <>(log : xx) tmp().renameTo() = "+oldFile.renameTo(newFile));
					}catch(Exception expI){logException(expI);}

					fileName		= "sdpLogs/"+dateLogger.substring(0,dateLogger.indexOf("_"))+"/sdpRecords_"+dateLogger+".txt";
					boolean success	= (new File("sdpLogs/"+dateLogger.substring(0,dateLogger.indexOf("_")))).mkdirs();
					if(success)	logger.debug("["+threadName+"] <>(log : xx) Directory Created for hourly Records.");
					fstream		= new FileWriter("sdpLogs/"+dateLogger.substring(0,dateLogger.indexOf("_"))+"/sdpRecords_"+dateLogger+".txt",true);
				}catch(Exception exp){logException(exp);}
			}
		}
	}

	private String generateDate()
	{
		java.util.Date date 				= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("yyyyMMdd_HH");
		return SQL_DATE_FORMAT.format(date.getTime());
	}

	private String generateTime()
	{
		java.util.Date date 				= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("HH:mm:ss");
		return SQL_DATE_FORMAT.format(date.getTime());
	}
	private void logException(Exception exp)
	{
		logger.error(getStackTrace(exp));
		alertValue.setExceptionAlert(getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}
}