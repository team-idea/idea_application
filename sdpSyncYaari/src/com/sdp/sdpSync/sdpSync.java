package com.sdp.sdpSync;

import java.io.*;
import java.nio.channels.*;
import java.text.*;
import org.apache.log4j.*;

public class sdpSync extends Thread
{

	private static Logger logger 				= Logger.getLogger( sdpSync.class.getName() );
	private static int INSTANCE_ID				= 0;
	public sdpSync()
	{
		try
		{
			PropertyConfigurator.configure("log4j.properties");
			if(accessFile())
			{
				try
				{
					try{INSTANCE_ID				= Integer.parseInt(ConfigProperties.getProperty("log4j.AppInstanceId"));}catch(Exception exp){ INSTANCE_ID= 0;}
					RandomAccessFile file 		= new RandomAccessFile(System.getProperty("java.io.tmpdir")+"/"+INSTANCE_ID+".tmp", "rw");
					FileChannel fileChannel 	= file.getChannel();
					FileLock fileLock 			= fileChannel.tryLock();
					if (fileLock != null)
					{
						logger.debug("File is locked. Starting the execution of the Application");
						sdpSyncFromSDP_208 from_208 	= new sdpSyncFromSDP_208();		from_208.start();
						//sdpSyncFromSDP_201 from_201 	= new sdpSyncFromSDP_201();		from_201.start();
						//sdpSyncFromSDP_202 from_202 	= new sdpSyncFromSDP_202();		from_202.start();
						//sdpSyncFromSDP_212 from_212 	= new sdpSyncFromSDP_212();		from_212.start();

						sdpSyncToSDP_208   to_208		= new sdpSyncToSDP_208();		to_208.start();
						//sdpSyncToSDP_201   to_201		= new sdpSyncToSDP_201();		to_201.start();
						//sdpSyncToSDP_202   to_202		= new sdpSyncToSDP_202();		to_202.start();
						//sdpSyncToSDP_212   to_212		= new sdpSyncToSDP_212();		to_212.start();

					}
					else
					{
						logger.error("Cannot create lock. Already another instance of the program is running.");
						try{Thread.sleep(1000*30);}catch(Exception exp){}
						logger.error("System shutting down. Please wait");
						System.exit(0);
					}
				}
				catch (Exception exp) {logException(exp);}
			}
			else
			{
				logger.error("Cannot create lock. Already another instance of the program is running.");
				try{Thread.sleep(1000*30);}catch(Exception exp){}
				logger.error("System shutting down. Please wait");
				System.exit(0);
			}
		}
		catch(Exception exp){exp.printStackTrace();}
	}

	private boolean accessFile()
	{
		boolean retCode					= false;
		try
		{
			String line 				= "";
			try {BufferedWriter bw		= new BufferedWriter(new FileWriter(System.getProperty("java.io.tmpdir")+"/"+INSTANCE_ID+".tmp",false));bw.close();}
			catch (Exception exp) {}
			retCode						= true;
		}
		catch (Exception e) {System.out.println(e);}
		logger.debug("accessFile() = "+retCode);
		return retCode;
	}

	public static void main(String argv[])
	{
		sdpSync			sdp	= new sdpSync();
	}
	private void logException(Exception exp)
	{
		logger.error("[main] "+getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}
}