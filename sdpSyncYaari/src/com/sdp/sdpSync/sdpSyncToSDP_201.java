package com.sdp.sdpSync;

import java.io.*;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import org.apache.log4j.*;

public class sdpSyncToSDP_201 extends Thread
{

	private static Logger logger 				= Logger.getLogger( sdpSyncToSDP_201.class.getName() );
	private static String CONFIG_FILE			= "log4j.properties";
	private DBConnectionManager connMgr			= null;
	private String conPool_local				= "local";
	private String conPool_remote				= "remote_201";

	public sdpSyncToSDP_201()
	{
		try
		{
			PropertyConfigurator.configure("log4j.properties");
			connMgr 							= DBConnectionManager.getInstance();
		}
		catch(Exception exp){exp.printStackTrace();}
	}

	public static void main(String argv[])
	{
		sdpSyncToSDP_201 http = new sdpSyncToSDP_201();
		http.start();
	}

	public void run()
	{
		while(true)
		{
			final String thread_id		= "0";
			int processedRows 			= processSyncReq(thread_id);
			if(processedRows<=0) {logger.info("No Records found to be processed. Sleeping for 10 secs.");try{Thread.sleep(1000*10);}catch(Exception expTmr){}}
			else{logger.info("Total Records processed = "+processedRows+". Sleeping for 500 msecs.");try{Thread.sleep(500);}catch(Exception expTmr){}}
		}
	}

	private int processSyncReq(final String thread_id)
	{
		int retCode = 0;
		try
		{
			logger.debug("Into processSyncReq() function.");
			Connection connSelect			= connMgr.getConnection(conPool_remote);
			Connection connUpdate			= connMgr.getConnection(conPool_remote);
			Connection connInsert			= connMgr.getConnection(conPool_local);
			try
			{
				if(connSelect!=null&&connUpdate!=null&&connInsert!=null)
				{
					Statement stmtSelect	= connSelect.createStatement();
					Statement stmtUpdate	= connUpdate.createStatement();
					Statement stmtInsert	= connInsert.createStatement();
					String stmSelect		= "select RefId, circle, msisdn, prepost, srvKey, eventKey, reqType, precharge, to_char(reqdateTime,'YYYY-MM-DD HH24:MI:SS') reqdateTime, sub_mode, status, originator from tbl_sdpRequest where status =0";
					logger.debug(stmSelect);
					ResultSet rs			= stmtSelect.executeQuery(stmSelect);
					logger.debug("Into select ResultSet iteration");
					while(rs.next())
					{
						String RefId			= rs.getString("RefId");
						String circle			= rs.getString("circle");
						String msisdn			= rs.getString("msisdn");
						String prepost			= rs.getString("prepost");
						String srvKey			= rs.getString("srvKey");
						String eventKey			= rs.getString("eventKey");
						String reqType			= rs.getString("reqType");
						String precharge		= rs.getString("precharge");
						String reqdateTime		= rs.getString("reqdateTime");
						String mode				= rs.getString("sub_mode");
						String status			= rs.getString("status");
						String originator		= rs.getString("originator");

						if(eventKey == null){eventKey = "";}

						String insert_stmt			= "insert into tbl_sdpRequest(RefId,circle,msisdn,prepost,srvKey,eventKey,reqType,precharge,reqdateTime,mode,status,originator) values ('"+RefId+"','"+circle+"','"+msisdn+"','"+prepost+"','"+srvKey+"','"+eventKey+"','"+reqType+"','"+precharge+"','"+reqdateTime+"','"+mode+"','"+status+"','"+originator+"')";
						logger.debug(insert_stmt);
						int rowCtr					= stmtInsert.executeUpdate(insert_stmt);

						if(rowCtr != 1)	{logger.error("Not Able to submit the message correctly to central Database rowCtr = "+rowCtr);}
						else 			{stmtUpdate.executeUpdate("update tbl_sdpRequest set status = 1 where RefId='"+RefId+"' and msisdn = '"+msisdn+"' and srvKey='"+srvKey+"'");}
						Thread.sleep(15);
						retCode++;
					}
				}
				else
				{
					logger.info("Connection Cannot be established connSelect="+connSelect+" connUpdate="+connUpdate+" connInsert"+connInsert);
					retCode = 0;
				}
			}catch(Exception expCon){logException(expCon,thread_id);}
			finally
			{
				connMgr.freeConnection(conPool_remote,connSelect);
				connMgr.freeConnection(conPool_remote,connUpdate);
				connMgr.freeConnection(conPool_local,connInsert);
			}
		}
		catch(Exception exp){}
		return retCode;
	}


	private String generateUUID()
	{
		UUID idOne = UUID.randomUUID();
		return ""+idOne;
	}

	private String generateDateTime()
	{
		java.util.Date date 				= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
		return SQL_DATE_FORMAT.format(date.getTime());
	}

	private void logException(Exception exp,final String thread_id)
	{
		logger.error("["+thread_id+"] "+getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}

}
