package com.sdp.sdpSync;

import java.io.*;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import org.apache.log4j.*;

public class sdpSyncFromSDP_212 extends Thread
{

	private static Logger logger 				= Logger.getLogger( sdpSyncFromSDP_212.class.getName() );
	private static String CONFIG_FILE			= "log4j.properties";
	private DBConnectionManager connMgr			= null;
	private String conPool_local				= "local";
	private String conPool_remote				= "remote_212";

	public sdpSyncFromSDP_212()
	{
		try
		{
			PropertyConfigurator.configure("log4j.properties");
			connMgr 							= DBConnectionManager.getInstance();
		}
		catch(Exception exp){exp.printStackTrace();}
	}

	public static void main(String argv[])
	{
		sdpSyncFromSDP_212 http = new sdpSyncFromSDP_212();
		http.start();
	}

	public void run()
	{
		while(true)
		{
			final String thread_id		= "0";
			int processedRows 			= processSyncReq(thread_id);
			if(processedRows<=0) {logger.info("No Records found to be processed. Sleeping for 10 secs.");try{Thread.sleep(1000*10);}catch(Exception expTmr){}}
			else{logger.info("Total Records processed = "+processedRows+". Sleeping for 500 msecs.");try{Thread.sleep(500);}catch(Exception expTmr){}}
		}
	}

	private int processSyncReq(final String thread_id)
	{
		int retCode = 0;
		try
		{
			logger.debug("Into processSyncReq() function.");
			Connection connSelect			= connMgr.getConnection(conPool_local);
			Connection connUpdate			= connMgr.getConnection(conPool_local);
			Connection connInsert			= connMgr.getConnection(conPool_remote);
			try
			{
				if(connSelect!=null&&connUpdate!=null&&connInsert!=null)
				{
					Statement stmtSelect	= connSelect.createStatement();
					Statement stmtUpdate	= connUpdate.createStatement();
					Statement stmtInsert	= connInsert.createStatement();

					String stmSelect		= "select top 50 RefId, circle, msisdn, prepost, srvKey, eventKey, action, precharge, price, convert(varchar,startDateTime,120) startDateTime, convert(varchar,endDateTime,120) endDateTime, mode, status, convert(varchar,responseTime,120) responseTime from tbl_sdpResponse with(nolock) where status =0 and srvKey in ('bundle3','dpaj15','dpaol15','dpbf15','dpbg15','dpbib15','dpbn15','dpcri15','dpct15','dpdt15','dpdyk30','dpgs15','dphft15','dphor15','dpht15','dpih15','dpin15','dpjok15','dpkm15','dpkt15','dplq15','dpls15','dplsb30','dplsf30','dplsw30','dpmt15','dpmuk15','dpnn15','dpos15','dpsht30','dpsht30','dpsmt15','dpsn15','dpsu15','dpsuv15','dptd30','dptit15','dpwh15','dpwp15','gita30','mfbf30','mfdm30','mffb30','mfha30','mfhf30','mfhmt30','mfih30','mfjks30','mfkm30','mfkq30','mfkt30','mfmt30','mfow30','mfpn30','mfrs30','mfssc30','mfstk30','mfts30','mftw30','mfupsc30','mfut30','muk30','pbcontest3','pbs4000','pbsa4000','pbsb4000','pbsc4000','sbeaj30','sbeaol30','sbeast30','sbeaut30','sbebct30','sbebib30','sbebn30','sbebul30','sbecos30','sbecp30','sbedur30','sbedyk30','sbefor30','sbefs30','sbegad30','sbegan30','sbehan30','sbehg30','sbeht30','sbejoke30','sbekp30','sbelove30','sbelsb30','sbelsf30','sbelsw30','sbemot30','sbenews30','sbeos30','sbeos30','sbesht30','sbesk30','sbesm30','sbesn30','sbesu30','sbesuv30','sbetd30','sbetec30','sbevt30','sbewp30','sbg30','sgl30','sint30','smpmp30','smpwp10','ssm30')";
					logger.debug(stmSelect);
					ResultSet rs			= stmtSelect.executeQuery(stmSelect);
					logger.debug("Into select ResultSet iteration");
					while(rs.next())
					{
						String RefId			= rs.getString("RefId");
						String circle			= rs.getString("circle");
						String msisdn			= rs.getString("msisdn");
						String prepost			= rs.getString("prepost");
						String srvKey			= rs.getString("srvKey");
						String eventKey			= rs.getString("eventKey");
						String action			= rs.getString("action");
						String precharge		= rs.getString("precharge");
						String price			= rs.getString("price");
						String startDateTime	= rs.getString("startDateTime");
						String endDateTime		= rs.getString("endDateTime");
						String mode				= rs.getString("mode");
						String status			= rs.getString("status");
						String responseTime		= rs.getString("responseTime");

						String insert_stmt			= "insert into tbl_sdpResponse(RefId, circle, msisdn, prepost, srvKey, eventKey, action, precharge, price, startDateTime, endDateTime, mode, status, responseTime) values ('"+RefId+"','"+circle+"','"+msisdn+"','"+prepost+"','"+srvKey+"','"+eventKey+"','"+action+"','"+precharge+"','"+price+"','"+startDateTime+"','"+endDateTime+"','"+mode+"','"+status+"',getdate())";
						logger.debug(insert_stmt);
						int rowCtr					= stmtInsert.executeUpdate(insert_stmt);

						if(rowCtr != 1)	{logger.error("Not Able to submit the insert correctly to oracle Database rowCtr = "+rowCtr);}
						else 			{stmtUpdate.executeUpdate("update tbl_sdpResponse set status = 1 where RefId='"+RefId+"' and msisdn='"+msisdn+"' and srvKey='"+srvKey+"' and convert(varchar,startDateTime,120) = '"+startDateTime+"'");}
						Thread.sleep(15);
						retCode++;
					}
				}
				else
				{
					logger.info("Connection Cannot be established connSelect="+connSelect+" connUpdate="+connUpdate+" connInsert"+connInsert);
					retCode = 0;
				}
			}catch(Exception expCon){logException(expCon,thread_id);}
			finally
			{
				connMgr.freeConnection(conPool_local,connSelect);
				connMgr.freeConnection(conPool_local,connUpdate);
				connMgr.freeConnection(conPool_remote,connInsert);
			}
		}
		catch(Exception exp){}
		return retCode;
	}


	private String generateUUID()
	{
		UUID idOne = UUID.randomUUID();
		return ""+idOne;
	}

	private String generateDateTime()
	{
		java.util.Date date 				= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
		return SQL_DATE_FORMAT.format(date.getTime());
	}

	private void logException(Exception exp,final String thread_id)
	{
		logger.error("["+thread_id+"] "+getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}

}
