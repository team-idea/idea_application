import java.io.*;
import java.sql.*;
import java.net.*;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;

import org.apache.log4j.*;

public class rail139 extends HttpServlet
{
	private static final long serialVersionUID		= 6626962295622776147L;
	private static Logger logger 					= Logger.getLogger(rail139.class.getName());

	private static long requestCounter				= 0L;
	private static long requestCounter_succ			= 0L;

	private DBConnectionManager connMgr				= null;
	private String conPool							= "sdpCentral";
	private static sdpLogger logValue				= null;



	public void init(ServletConfig config) throws ServletException
	{
		try
		{
			PropertyConfigurator.configure( "log4j.properties" );
			logValue					= sdpLogger.getInstance();
			connMgr 					= DBConnectionManager.getInstance();
		}catch(Exception exp){}
	}

	public void doGet(HttpServletRequest request,HttpServletResponse response)	throws ServletException, IOException
	{
		try{doPost(request,response);}catch(Exception exp){logException(exp);}
	}

	synchronized private void updateCounter(boolean result)
	{
		requestCounter++;
		if(result){requestCounter_succ++;}
		if(requestCounter%50 == 0)
		{
			System.out.println(new java.util.Date()+" |Total Requests = "+requestCounter+" Success Requests = "+requestCounter_succ+" Failed Requests = "+(requestCounter-requestCounter_succ)+"|");
		}
	}

	public void doPost(HttpServletRequest request,HttpServletResponse response)	throws ServletException, IOException
	{
		response.setContentType("text/html");
		boolean callback_result			= false;
		PrintWriter out 				= response.getWriter();
		Enumeration parameters 			= request.getHeaderNames();
		Enumeration parameterNames		= request.getParameterNames();

		String content 				= "";
		Map param_map 				= request.getParameterMap();
		if (param_map == null)	throw new ServletException("getParameterMap returned null in: " + getClass().getName());
		Iterator iterator			= param_map.entrySet().iterator();

		while(iterator.hasNext())
		{
			Map.Entry me = (Map.Entry)iterator.next();
			//System.out.println(me.getKey() +""+ me.getValue());
			String[] arr = (String[]) me.getValue();
			//System.out.println("arr.length = "+arr.length);
			for(int i=0;i<arr.length;i++)
			{
				content+= me.getKey()+"="+arr[i]+",";
				if (i > 0 && i != arr.length-1)	content+=",";
			}
		}
		try{content = content.toLowerCase();}catch(Exception exp){logException(exp);}
		logger.debug(content);
		logValue.addLog(content);
		final String CallBack	= content.replace(",","&");
		try{ content	= content.replace("{",""); content= content.replace("}","");content= content.replace(",","\n"); /*System.out.println(content);*/}catch(Exception exp){logException(exp);}

		response.setHeader("Host","spice");
		response.setHeader("Accept","text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2");
		response.setHeader("Content-Type","text/html");
		response.setHeader("Connection","keep-alive");
		response.setHeader("User-Agent","spiceDigital Server");
		response.setHeader("Content-Length","8");
		out.println("Accepted");//* /
		out.flush();
	}

	private synchronized String getDateTime(final String date,final String time)
	{
		String retDateTime = null;
		if(date.equalsIgnoreCase("x") || time.equalsIgnoreCase("x"))
		{
			retDateTime = "2000-01-01 00:00:00";
			logger.debug("wrong type of date and time encountered["+date+" "+time+"]. Changing to "+retDateTime);
		}
		else
		{
			try
			{
				DateFormat sdp	= new SimpleDateFormat("yyyyMMdd HHmmSS"); DateFormat db	= new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
				java.util.Date dt_sdp 	= sdp.parse(date+" "+time);
				retDateTime		= db.format(dt_sdp);
			}catch(Exception exp){retDateTime = null;logException(exp);}
			logger.debug(retDateTime);
		}
		return retDateTime;
	}

	private String callHttpPostUrl(final String str)
	{
		String resp = "";
		URL url = null;
		URLConnection urlConn = null;
		DataInputStream input = null;
		final String urlString 		= "http://172.27.32.73/SmsRequestHandlerSubId/req.hndlr.sub.ss";
		try
		{
			url = new URL(urlString);
			urlConn = url.openConnection();
			HttpURLConnection httpConn = (HttpURLConnection) urlConn;

			// Set the appropriate HTTP parameters.
			httpConn.setRequestMethod("POST");
			httpConn.setReadTimeout(30000);


			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);

			// Everything's set up; send the XML.
			OutputStream out = httpConn.getOutputStream();
			OutputStreamWriter wout = new OutputStreamWriter(out, "UTF-8");

			wout.write(str);
			wout.flush();
			out.close();
			String retstr = "";


			input = new DataInputStream(urlConn.getInputStream());
			while (null != ((retstr = input.readLine())))
			{
				if (retstr.length() > 0)
				{
					retstr = retstr.trim();
					if (!retstr.equals("")){resp += retstr;}
				}
			}
			input.close();
			//logger.fatal("SMS 56300  Read response as - " + resp);
		}
		catch (Exception exp) {exp.printStackTrace();}
		return resp;
	}


	private void logException(Exception exp)
	{
		logger.error(getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}

	public void destroy()
	{
		requestCounter			= 0L;
		requestCounter_succ		= 0L;
		logger.debug("destroy() function called for chrgInfo servlet...");
		connMgr.initiateStop();
		logValue.stopSdpLogger();
		try{Thread.sleep(500);}catch(Exception exp){}
		super.destroy();
	}
}


