import java.io.*;
import java.sql.*;
import java.net.*;
import java.util.*;
import java.text.*;
import javax.servlet.*;
import javax.servlet.http.*;
import java.security.*;
import javax.net.ssl.*;
import org.apache.log4j.*;

public class Callback extends HttpServlet
{
	private static final long serialVersionUID		= 7526962295622776147L;
	private static long requestCounter				= 0L;
	private static long requestCounter_succ			= 0L;

	private DBConnectionManager connMgr				= null;
	private String conPool							= "sdpCentral";
	private static sdpLogger logValue				= null;

	private static final String srvKeyList			= "bundle3,pbcontest3,dpaj15,dpaol15,dpbf15,dpbg15,dpbib15,dpbn15,dpcri15,dpct15,dpdt15,dpdyk30,dpgs15,dphft15,dphor15,dpht15,dpih15,dpin15,dpjok15,dpkm15,dpkt15,dplq15,dpls15,dplsb30,dplsf30,dplsw30,dpmt15,dpmuk15,dpnn15,dpos15,dpsht30,dpsht30,dpsmt15,dpsn15,dpsu15,dpsuv15,dptd30,dptit15,dpwh15,dpwp15,gita30,mfbf30,mfdm30,mffb30,mfha30,mfhf30,mfhmt30,mfih30,mfjks30,mfkm30,mfkq30,mfkt30,mfmt30,mfow30,mfpn30,mfrs30,mfssc30,mfstk30,mfts30,mftw30,mfupsc30,mfut30,muk30,pbs4000,pbsa4000,pbsb4000,pbsc4000,sbeaj30,sbeaol30,sbeast30,sbeaut30,sbebct30,sbebib30,sbebn30,sbebul30,sbecos30,sbecp30,sbedur30,sbedyk30,sbefor30,sbefs30,sbegad30,sbegan30,sbehan30,sbehg30,sbeht30,sbejoke30,sbekp30,sbelove30,sbelsb30,sbelsf30,sbelsw30,sbemot30,sbenews30,sbeos30,sbeos30,sbesht30,sbesk30,sbesm30,sbesn30,sbesu30,sbesuv30,sbetd30,sbetec30,sbevt30,sbewp30,sbg30,sgl30,sint30,smpmp30,smpwp10,ssm30,regjaol,regnews,regbala,regcric,regkitchen,reglove,regmehfil,regspo,regsensex,regtech,regvastu,regword,regyoga,regjob,regjoke,dpnb15,pbcri30,cric1,cric30,cric36,cric7,spicecricket10,spicecricket25,spicecricket5,spicecricket5_spcrick5,spicecricket50,vercric30,vercric5,news0010,spicejoke30nb,spicejoke30dst,sports5,sports5_spspevnt,SPSPTEVNT,spsptevnt_sspt5evnt,SSPMP180,SSPMP180_SPMP6EVNT";
	private static final String srvKeyList_wap		= "vchat30,vchat30,dynamicvchat,vchat30,vchat30,vchat30,vchat30,vchat30,vchat15,vchat30,vchat30,vchat30,vchat30,kkvchat,vchat30,vchat30";

	private static Logger logger 					= Logger.getLogger(Callback.class.getName());

	public void init(ServletConfig config) throws ServletException
	{
		try
		{
			try{trustAllHttpsCertificates();}catch(Exception tmr){}
			PropertyConfigurator.configure( "log4j.properties" );
			logValue					= sdpLogger.getInstance();
			connMgr 					= DBConnectionManager.getInstance();
		}catch(Exception exp){}
	}

	public void doGet(HttpServletRequest request,HttpServletResponse response)	throws ServletException, IOException
	{
		try{doPost(request,response);}catch(Exception exp){logException(exp);}
	}

	synchronized private void updateCounter(boolean result)
	{
		requestCounter++;
		if(result){requestCounter_succ++;}
		if(requestCounter%50 == 0)
		{
			System.out.println(new java.util.Date()+" |Total Requests = "+requestCounter+" Success Requests = "+requestCounter_succ+" Failed Requests = "+(requestCounter-requestCounter_succ)+"|");
		}
	}

	public void doPost(HttpServletRequest request,HttpServletResponse response)	throws ServletException, IOException
	{
		response.setContentType("text/html");
		boolean callback_result			= false;
		PrintWriter out 				= response.getWriter();
		Enumeration parameters 			= request.getHeaderNames();
		Enumeration parameterNames		= request.getParameterNames();





		String content 				= "";
		Map param_map 				= request.getParameterMap();
		if (param_map == null)	throw new ServletException("getParameterMap returned null in: " + getClass().getName());
		Iterator iterator			= param_map.entrySet().iterator();

		while(iterator.hasNext())
		{
			Map.Entry me = (Map.Entry)iterator.next();
			//System.out.println(me.getKey() +""+ me.getValue());
			String[] arr = (String[]) me.getValue();
			//System.out.println("arr.length = "+arr.length);
			for(int i=0;i<arr.length;i++)
			{
				content+= me.getKey()+"="+arr[i]+",";
				if (i > 0 && i != arr.length-1)	content+=",";
			}
		}
		try{content = content.toLowerCase();}catch(Exception exp){logException(exp);}
		logger.debug(content);
		logValue.addLog(content);
		final String CallBack	= content.replace(",","&");
		try{content	= content.replace("{",""); content= content.replace("}","");content= content.replace(",","\n"); /*System.out.println(content);*/}catch(Exception exp){logException(exp);}


		Properties prop = new Properties();
		prop.load(new DataInputStream(new ByteArrayInputStream(content.getBytes("UTF-8"))));
		//prop.list(System.out);

		String RefId=prop.getProperty("refid");
		int retry_num=Integer.parseInt(prop.getProperty("retry_num"));
		String circle=prop.getProperty("circle").toLowerCase();
		String msisdn=prop.getProperty("msisdn");
		String prepost=prop.getProperty("type");
		String srvkey=prop.getProperty("srvkey");
		String mode=prop.getProperty("mode").toLowerCase();
		String price=prop.getProperty("price");
		String sdpStatus=prop.getProperty("status");
		String action=prop.getProperty("action");
		String precharge=prop.getProperty("precharge");
		String startDate=prop.getProperty("startdate");
		String startTime=prop.getProperty("starttime");
		String endDate=prop.getProperty("enddate");
		String endTime=prop.getProperty("endtime");
		String originator=prop.getProperty("originator").toLowerCase();
		String cp_user=prop.getProperty("user");
		String cp_password=prop.getProperty("pass");
		String info=prop.getProperty("info");
		String startDateTime=getDateTime(startDate.trim(),startTime.trim());
		String endDateTime=getDateTime(endDate.trim(),endTime.trim());
		String eventkey ="";


		if(srvkey.equalsIgnoreCase("music2")||srvkey.equalsIgnoreCase("music30")||srvkey.equalsIgnoreCase("music50")||srvkey.equalsIgnoreCase("freemusic")||srvkey.equalsIgnoreCase("spicejoke30")||srvkey.equalsIgnoreCase("spicejoke36")||srvkey.equalsIgnoreCase("spicelove30")||srvkey.equalsIgnoreCase("spicelove36")||srvkey.equalsIgnoreCase("cric1")||srvkey.equalsIgnoreCase("cric30")||srvkey.equalsIgnoreCase("cric36")||srvkey.equalsIgnoreCase("cric7")||srvkey.equalsIgnoreCase("spicecricket10")||srvkey.equalsIgnoreCase("spicecricket25")||srvkey.equalsIgnoreCase("spicecricket5")||srvkey.equalsIgnoreCase("spicecricket5_spcrick5")||srvkey.equalsIgnoreCase("spicecricket50")||srvkey.equalsIgnoreCase("vercric30")||srvkey.equalsIgnoreCase("vercric5")||srvkey.equalsIgnoreCase("news0010")||srvkey.equalsIgnoreCase("spicejoke30")||srvkey.equalsIgnoreCase("spicejoke36")||srvkey.equalsIgnoreCase("spicelove30")||srvkey.equalsIgnoreCase("spicelove36")||srvkey.equalsIgnoreCase("cric1")||srvkey.equalsIgnoreCase("cric30")||srvkey.equalsIgnoreCase("cric36")||srvkey.equalsIgnoreCase("cric7")||srvkey.equalsIgnoreCase("spicecricket10")||srvkey.equalsIgnoreCase("spicecricket25")||srvkey.equalsIgnoreCase("spicecricket5")||srvkey.equalsIgnoreCase("spicecricket5_spcrick5")||srvkey.equalsIgnoreCase("spicecricket50")||srvkey.equalsIgnoreCase("vercric30")||srvkey.equalsIgnoreCase("vercric5")||srvkey.equalsIgnoreCase("regcric")||srvkey.equalsIgnoreCase("spicejoke30nb") ||srvkey.equalsIgnoreCase("sports5") ||srvkey.equalsIgnoreCase("sports5_spspevnt") ||srvkey.equalsIgnoreCase("SPSPTEVNT") ||srvkey.equalsIgnoreCase("spsptevnt_sspt5evnt")||srvkey.equalsIgnoreCase("SSPMP180") ||srvkey.equalsIgnoreCase("SSPMP180_SPMP6EVNT"))
		{
			try{callHttpPostUrl(CallBack);}catch(Exception exp){}
		}
		if(srvkey.equalsIgnoreCase("spmgn001")||srvkey.equalsIgnoreCase("spmgn007")||srvkey.equalsIgnoreCase("spmgn015"))
		{
			try{callHttpPostUrl_Wap(CallBack);}catch(Exception exp){}
		}
		try
		{
			if(action.equalsIgnoreCase("ACT")||action.equalsIgnoreCase("DCT")||action.equalsIgnoreCase("SDCT")||action.equalsIgnoreCase("NETCHURN"))
			{
				mode+="_"+originator;
			}
			else {mode+=""; originator = "sdpRsp";}

			final String actual_circle = circle;

			if(srvKeyList.contains(srvkey))
			{
				if(circle.equalsIgnoreCase("pb") ||circle.equalsIgnoreCase("kk")) {if(!srvkey.equalsIgnoreCase("pbcontest")) circle = "ss";}
				else if(circle.equalsIgnoreCase("kr")&&!(srvkey.equalsIgnoreCase("spicejoke30")|| srvkey.equalsIgnoreCase("spicecricket5") || srvkey.equalsIgnoreCase("spicecricket5_spcrick5") || srvkey.equalsIgnoreCase("sports5")||srvkey.equalsIgnoreCase("sports5_spspevnt") || srvkey.equalsIgnoreCase("SPSPTEVNT") ||srvkey.equalsIgnoreCase("spsptevnt_sspt5evnt") ||srvkey.equalsIgnoreCase("SSPMP180") ||srvkey.equalsIgnoreCase("SSPMP180_SPMP6EVNT"))){try{callHttpPostUrl(CallBack);}catch(Exception exp){}}
			}

			/* Added to handle Event Call back only
			if(action.equalsIgnoreCase("DCT")||action.equalsIgnoreCase("SDCT")||action.equalsIgnoreCase("NETCHURN"))
			{
				try {eventkey	= prop.getProperty("eventkey");	if(eventkey!="" || eventkey!=null){srvkey+ = "_"+eventkey;}}
				catch(Exception exp){eventkey = "";  logger.info("No event key found");}
			}
			*/
			String insQry="insert into sdpCentral_"+circle+".dbo.tbl_callback(callbackInsertDateTime,RefId, retry_num, circle, msisdn, prepost, srvkey, mode, price, sdpStatus, status, action, precharge, startDateTime, endDateTime, originator, cp_user, cp_password, info) values(getdate(),'"+RefId+"',"+retry_num+",'"+actual_circle+"','"+ msisdn+"','"+ prepost+"','"+ srvkey+"','"+ mode+"','"+ price+"','"+ sdpStatus+"',0,'"+ action+"','"+ precharge+"','"+ startDateTime+"','"+ endDateTime+"','"+ originator+"','"+ cp_user+"','"+ cp_password+"','"+ info+"')";

			Connection conn= null;
			int connCntr= 0;

			while(connCntr<3)
			{
				conn= connMgr.getConnection(conPool);
				if(conn!=null) break; else try{Thread.sleep(50);}catch(Exception exp){logException(exp);}
				connCntr++;
			}
			//logger.debug("Connection conn = "+conn);


			if(conn != null)
			{
				int retCode				= -1;
				try
				{
					Statement stmt		= conn.createStatement();
					retCode				= stmt.executeUpdate(insQry);
					callback_result		= true;
				}catch(Exception exp){logException(exp);parkCallback(content);}
				finally{if(conn != null){connMgr.freeConnection(conPool,conn);}logger.debug("Circle database pool = "+conPool+" ins stmt = "+insQry+" = "+retCode);}
			}
			else logger.info("No free connections available");

			if(callback_result)
			{
				response.setHeader("Host","spice");
				response.setHeader("Accept","text/html, image/gif, image/jpeg, *; q=.2, */*; q=.2");
				response.setHeader("Content-Type","text/html");
				response.setHeader("Connection","keep-alive");
				response.setHeader("User-Agent","spiceDigital Server");
				response.setHeader("Content-Length","8");
				out.println("Accepted");//* /
				out.flush();
			}
		}catch(Exception exp){logException(exp);}
		updateCounter(callback_result);
	}

	private synchronized boolean parkCallback(String parkedCallback)
	{
		boolean retBool	= false;
		logger.info("parkedCallback: "+parkedCallback);
		return retBool;
	}

	private synchronized String getDateTime(final String date,final String time)
	{
		String retDateTime = null;
		if(date.equalsIgnoreCase("x") || time.equalsIgnoreCase("x"))
		{
			retDateTime = "2000-01-01 00:00:00";
			logger.debug("wrong type of date and time encountered["+date+" "+time+"]. Changing to "+retDateTime);
		}
		else
		{
			try
			{
				DateFormat sdp	= new SimpleDateFormat("yyyyMMdd HHmmSS"); DateFormat db	= new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
				java.util.Date dt_sdp 	= sdp.parse(date+" "+time);
				retDateTime		= db.format(dt_sdp);
			}catch(Exception exp){retDateTime = null;logException(exp);}
			//logger.debug(retDateTime);
		}
		return retDateTime;
	}

	private String callHttpPostUrl(final String str)
	{
		String resp = "";
		URL url = null;
		URLConnection urlConn = null;
		DataInputStream input = null;
		final String urlString 		= "http://172.27.32.73/SmsRequestHandlerSubId/req.hndlr.sub.ss";
		try
		{
			url = new URL(urlString);
			urlConn = url.openConnection();
			HttpURLConnection httpConn = (HttpURLConnection) urlConn;

			// Set the appropriate HTTP parameters.
			httpConn.setRequestMethod("POST");
			httpConn.setReadTimeout(30000);
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);

			// Everything's set up; send the XML.
			OutputStream out = httpConn.getOutputStream();
			OutputStreamWriter wout = new OutputStreamWriter(out, "UTF-8");
			wout.write(str);
			wout.flush();
			out.close();
			String retstr = "";

			input = new DataInputStream(urlConn.getInputStream());
			while (null != ((retstr = input.readLine())))
			{
				if (retstr.length() > 0)
				{
					retstr = retstr.trim();
					if (!retstr.equals("")){resp += retstr;}
				}
			}
			input.close();
			//logger.fatal("SMS 56300  Read response as - " + resp);
		}
		catch (Exception exp) {logger.error("Error: ",exp);try{Thread.sleep(100);}catch(Exception exp_tmr){}}
		return resp;
	}

	private String callHttpPostUrl_Wap(final String str)
	{
		String resp					= "";
		URL url						= null;
		URLConnection urlConn		= null;
		DataInputStream input		= null;
		final String urlString 		= "http://wap.smartgang.in/ideanotification/sub/notification";
		try
		{
			url = new URL(urlString);
			urlConn = url.openConnection();
			HttpURLConnection httpConn = (HttpURLConnection) urlConn;

			// Set the appropriate HTTP parameters.
			httpConn.setRequestMethod("POST");
			httpConn.setReadTimeout(30000);
			httpConn.setDoOutput(true);
			httpConn.setDoInput(true);

			// Everything's set up; send the XML.
			OutputStream out = httpConn.getOutputStream();
			OutputStreamWriter wout = new OutputStreamWriter(out, "UTF-8");
			wout.write(str);
			wout.flush();
			out.close();
			String retstr = "";

			input = new DataInputStream(urlConn.getInputStream());
			while (null != ((retstr = input.readLine())))
			{
				if (retstr.length() > 0)
				{
					retstr = retstr.trim();
					if (!retstr.equals("")){resp += retstr;}
				}
			}
			input.close();
			//logger.fatal("SMS 56300  Read response as - " + resp);
		}
		catch (Exception exp) {logger.error("Error: ",exp);try{Thread.sleep(100);}catch(Exception exp_tmr){}}
		return resp;
	}

	private void logException(Exception exp)
	{
		logger.error(getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}

	private static void trustAllHttpsCertificates() throws Exception
	{
		javax.net.ssl.TrustManager[] trustAllCerts  = new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm 				= new miTM();

		trustAllCerts[0] 							= tm;
		javax.net.ssl.SSLContext sc 				= javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(
		sc.getSocketFactory());
	}

	public static class miTM implements javax.net.ssl.TrustManager,javax.net.ssl.X509TrustManager
	{
		public java.security.cert.X509Certificate[] getAcceptedIssuers()			{return null;}
		public boolean isServerTrusted(java.security.cert.X509Certificate[] certs)	{return true;}
		public boolean isClientTrusted(java.security.cert.X509Certificate[] certs)	{return true;}
		public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)	throws java.security.cert.CertificateException{return;}
		public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) throws java.security.cert.CertificateException{return;}
	}

	public void destroy()
	{
		requestCounter			= 0L;
		requestCounter_succ		= 0L;
		logger.debug("destroy() function called for chrgInfo servlet...");
		connMgr.initiateStop();
		logValue.stopSdpLogger();
		try{Thread.sleep(500);}catch(Exception exp){}
		super.destroy();
	}
}


