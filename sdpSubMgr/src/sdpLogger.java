import java.text.*;
import java.util.*;
import java.io.*;
import java.net.*;
import org.apache.log4j.*;

public class sdpLogger implements Runnable
{
	private static Logger logger 					= Logger.getLogger(sdpLogger.class.getName());
	static private boolean keepAlive				= true;
	static private boolean waitFlag					= false;
	static private String dateLogger				= null;
	static private sdpLogger instance				= null;				       		// The single instance
	static private int clients						= 0;
	static private Vector<String> logVector			= new Vector<String>();
	static private FileWriter fstream 				= null;
	static private int loggerValueThreshHold		= 1500;
	static private int thread_sleeptime				= 5;



	static synchronized public sdpLogger getInstance()
	{
		if (instance == null)
		{
			instance = new sdpLogger();
			Thread sdpLog							= new Thread(new sdpLogger());	sdpLog.start();
		}
		clients++;
		return instance;
	}

	public sdpLogger()
	{
		try
		{
			try
			{
				PropertyConfigurator.configure("log4j.properties");
				loggerValueThreshHold	= 1;
				thread_sleeptime= 25;
			}
			catch(Exception exp){exp.printStackTrace();}
			dateLogger								= generateDate();
			boolean success							= (new File("../logs/sdpLogs/"+dateLogger.substring(0,dateLogger.indexOf("_")))).mkdirs();
			if(success)	logger.debug("Directory Created for hourly Records.");
			fstream									= new FileWriter("../logs/sdpLogs/"+dateLogger.substring(0,dateLogger.indexOf("_"))+"/sdpRecords_"+dateLogger+".txt",true);
			logger.debug("["+String.format("%-10s",Thread.currentThread().getName())+"] <>(log : xx) loggerValueThreshHold set as "+loggerValueThreshHold);
		}catch(Exception exp){exp.printStackTrace();}
	}


	public synchronized int stopSdpLogger()
	{
		keepAlive	= false;
		try{logRecordToFile(false);Thread.sleep(10);return 0;}catch(Exception exp){return -1;}
	}

	public synchronized int addLog(String reqStr)
	{
		int retCode	= 0;
		reqStr		=generateTime()+"|"+reqStr;
		do
		{
			if(!waitFlag)	try{logVector.add(reqStr); break; }catch(Exception exp){retCode = -1;}
			else			try{Thread.sleep(20);continue;}catch(Exception exp){} ;
		}while(waitFlag);
		return retCode;
	}

	public void run()
	{
		while(keepAlive)
		{
			logRecordToFile(keepAlive);try{Thread.sleep(1000*thread_sleeptime);}catch(Exception exp){}
		}
	}

	private synchronized void logRecordToFile(boolean keepAlive)
	{
		BufferedWriter out 							= null;
		final String threadName						= String.format("%-10s",Thread.currentThread().getName());
		if(dateLogger.equalsIgnoreCase(generateDate()))
		{
			if(logVector.size() >= loggerValueThreshHold||keepAlive == false)
			{
				try
				{
					waitFlag	= true;
					Vector<String> logVectorLocal 		= (Vector) logVector.clone(); logVector.clear();
					waitFlag	= false;
					out 		= new BufferedWriter(fstream);
					for(int i=0;i<logVectorLocal.size();i++)
					{
						String logStr	= logVectorLocal.get(i)+"\n";
						out.write(logStr);
					}
					out.flush();
					logVectorLocal.clear();
					Thread.sleep(5);
				}catch(Exception exp){}

			}
		}
		else
		{
			try
			{
				dateLogger	= generateDate();
				try
				{
					waitFlag	= true;
					Vector<String> logVectorLocal 		= (Vector) logVector.clone(); logVector.clear();
					waitFlag	= false;
					out 		= new BufferedWriter(fstream);
					for(int i=0;i<logVectorLocal.size();i++)
					{
						String logStr	= logVectorLocal.get(i)+"\n";
						out.write(logStr);
					}
					out.flush();
					logVectorLocal.clear();
					Thread.sleep(5);
				}catch(Exception exp){}
				fstream.close();
				boolean success	= (new File("../logs/sdpLogs/"+dateLogger.substring(0,dateLogger.indexOf("_")))).mkdirs();
				if(success)	logger.debug("["+threadName+"] <>(log : xx) Directory Created for hourly Records.");
				fstream		= new FileWriter("../logs/sdpLogs/"+dateLogger.substring(0,dateLogger.indexOf("_"))+"/sdpRecords_"+dateLogger+".txt",true);
			}catch(Exception exp){}
		}
	}

	private String generateDate()
	{
		java.util.Date date 				= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("yyyyMMdd_HH");
		return SQL_DATE_FORMAT.format(date.getTime());
	}

	private String generateTime()
	{
		java.util.Date date 				= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("HH:mm:ss");
		return SQL_DATE_FORMAT.format(date.getTime());
	}
}