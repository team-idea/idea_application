package in.spicedigital.auditMailer.model;

public class AuditDataModel {

	private String auditMisDate;
	private String operator;
	private String circle;
	private String shortCode;
	private int durationMTD;
	
	public String getAuditMisDate() {
		return auditMisDate;
	}
	public void setAuditMisDate(String auditMisDate) {
		this.auditMisDate = auditMisDate;
	}
	public String getOperator() {
		return operator;
	}
	public void setOperator(String operator) {
		this.operator = operator;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public String getShortCode() {
		return shortCode;
	}
	public void setShortCode(String shortCode) {
		this.shortCode = shortCode;
	}
	public int getDurationMTD() {
		return durationMTD;
	}
	public void setDurationMTD(int durationMTD) {
		this.durationMTD = durationMTD;
	}
	
	

}
