package in.spicedigital.auditMailer.controller;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import in.spicedigital.auditMailer.model.AuditDataModel;
import in.spicedigital.auditMailer.util.DBConnectionManager;
import in.spicedigital.auditMailer.util.LogStackTrace;


public class CollectAuditData {
	private static final Logger LOGGER = Logger.getLogger(CollectAuditData.class);
	private DBConnectionManager connectionManager = null;
	private ArrayList<AuditDataModel> auditDataModels = null;
	private Properties prop = null;
	private String sqlQuery = null;
	//private String[] circles = null;

	public CollectAuditData() {
		PropertyConfigurator.configure("./log4j.properties");
		connectionManager = DBConnectionManager.getInstance();
		try {
			prop = new Properties();
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			sqlQuery = prop.getProperty("sqlQuery");
			//circles = circle.split("[,]+");
		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public ArrayList arrangeData() throws SQLException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		//String sqlQuery = null;
		AuditDataModel auditDataModel = null;
		auditDataModels = new ArrayList<AuditDataModel>();
		
			try {
				LOGGER.debug("Connecting [yaari] database");
				connection = connectionManager.getConnection("mysql");
				statement = connection.createStatement();
				//sqlQuery = "select date(mis_date) as mis_date,operator,circle,short_code,duration_mtd from tbl_auidt_data where date(mis_date)='20170724'";
				sqlQuery =sqlQuery.replaceAll("@misDate", AuditMis.misDate);
				LOGGER.debug("query going to execute circle sqlQuery["+sqlQuery+"]");
				resultSet = statement.executeQuery(sqlQuery);
				while (resultSet.next()) {
					auditDataModel = new AuditDataModel();
					auditDataModel.setAuditMisDate(resultSet.getString("mis_date"));
					auditDataModel.setCircle(resultSet.getString("circle"));
					auditDataModel.setDurationMTD(resultSet.getInt("duration_mtd"));
					auditDataModel.setOperator(resultSet.getString("operator"));
					auditDataModel.setShortCode(resultSet.getString("short_code"));
					auditDataModels.add(auditDataModel);
					//LOGGER.debug(yaariAuditDataModel);
				}
				LOGGER.debug("size of the list is[" + auditDataModels.size() + "]\n");
				
			} catch (Exception ex) {
				LOGGER.debug(LogStackTrace.getStackTrace(ex));
			}finally {
				if (null != connection) {
					connectionManager.freeConnection("mysql", connection);
				}
				if (null != statement) {
					statement.close();
				}
				if (null != resultSet) {					
					resultSet.close();
				}
			}
		return auditDataModels;

	}
}
