package com.spicedigital.vchatfchat;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class CollectFchatData {
	private static final Logger LOGGER = Logger.getLogger(CollectFchatData.class);
	DBConnectionManager connectionManager = null;
	public List<VchatIdDataModel> FchatDataModelList = null;
	private Properties prop = null;
	private String circle = null;
	private String[] circles = null;

	public CollectFchatData() {
		PropertyConfigurator.configure("./log4j.properties");
		connectionManager = DBConnectionManager.getInstance();
		try {
			prop = new Properties();
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			circle = prop.getProperty("circle");
			circles = circle.split("[,]+");
		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public List processRequest() throws SQLException {
		Connection connection = null;
		Statement statement = null;
		ResultSet resultSet = null;
		String sqlQuery = null;
		VchatIdDataModel fchatDataModel = null;
		FchatDataModelList = new ArrayList<VchatIdDataModel>(1000);
		for (String circleName : circles) {
			try {
				LOGGER.debug("Connecting ["+circleName+"] circle and db url is ["+prop.getProperty(circleName+".url")+"]");
				connection = connectionManager.getConnection(circleName);
				statement = connection.createStatement();
				sqlQuery = prop.getProperty(circleName+".query");
				LOGGER.debug("query going to execute circle["+circleName+"] sqlQuery["+sqlQuery+"]");
				resultSet = statement.executeQuery(sqlQuery);
				while (resultSet.next()) {
					fchatDataModel = new VchatIdDataModel();
					fchatDataModel.setChatId(resultSet.getString("chatId"));
					fchatDataModel.setCircle(circleName);
					//fchatDataModel.setCircle(resultSet.getString("circle"));
					FchatDataModelList.add(fchatDataModel);
					LOGGER.debug(fchatDataModel);
				}
			} catch (Exception ex) {
				LOGGER.debug(LogStackTrace.getStackTrace(ex));
			} finally {
				if (null != connection) {
					connectionManager.freeConnection("mssql", connection);
				}
				if (null != statement) {
					statement.close();
				}
				if (null != resultSet) {
					resultSet.close();
				}
			}
		}
		return FchatDataModelList;
	}

}
