package com.spicedigital.vchatfchat;

public class VchatIdDataModel {

	private String chatId;
	private String circle;
	
	public String getChatId() {
		return chatId;
	}
	public void setChatId(String chatId) {
		this.chatId = chatId;
	}
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}	
}
