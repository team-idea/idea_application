package com.spicedigital.vchatfchat;

import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


public class InsertChatidData {
	private static final Logger LOGGER = Logger.getLogger(InsertChatidData.class);
	private DBConnectionManager connectionManager = null;
	private static int insertRow = 0;

	public InsertChatidData() {
		PropertyConfigurator.configure("./log4j.Properties");
		try {
			connectionManager = DBConnectionManager.getInstance();
		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public void insertData(List<VchatIdDataModel> collectDataList) throws SQLException {
		Connection connection = null;
		Statement statement = null;
		String sqlQuery = null;
		String delQuery = "truncate table TBL_vchatid";
		LOGGER.debug("Connecting yaari database ");		
		connection = connectionManager.getConnection("mysql");
		statement = connection.createStatement();
		int delCount = statement.executeUpdate(delQuery);
		LOGGER.debug("truncate query executed [" + delQuery + "]");
		LOGGER.debug("[" + delCount + "] rows has been deleted.\n");
		
		LOGGER.debug("insertion is in process..........." );
		
		for (VchatIdDataModel insertData : collectDataList) {
			try {
				String chatId = insertData.getChatId();
				String circle = insertData.getCircle();				
				sqlQuery = "INSERT INTO TBL_vchatid (chat_id,circle,insertDatetime) VALUES ('" + chatId + "', '" + circle + "', now());";
				int count = statement.executeUpdate(sqlQuery);
				insertRow = insertRow + count;
			} catch (Exception ex) {
				LOGGER.debug("Exception query ["+sqlQuery+"]");
				LOGGER.debug(LogStackTrace.getStackTrace(ex));
			}
		}
		LOGGER.debug("[" + insertRow + "] row inserted");
		LOGGER.debug("sample query-->[" + sqlQuery + "]");

	}
}
