package com.spice.automailer;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class ObdMisMailerMain {
	private static Logger logger = Logger.getLogger(ObdMisMailerMain.class.getName());
	public static String dateParam = "1";
	private static String circle = null;
	private static Properties prop = null;

	public ObdMisMailerMain() {
		PropertyConfigurator.configure("./log4j.properties");
		prop = new Properties();
		try {
			prop.load(new DataInputStream(new FileInputStream("config.properties")));
			circle = prop.getProperty("circle");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void main(String[] args) {

		logger.debug("program is start..");
		ObdMisMailerMain obdMisMailerMain = new ObdMisMailerMain();
		String[] circleName = circle.split("[,]+");
		
		for (String circleCode : circleName) {
			logger.debug("circle[" + circle + "]");
			MailHelper mailHelper = new MailHelper(circleCode);
			mailHelper.sendMailRequest();
		}
		
		logger.debug("Program is END..");
	}
}
