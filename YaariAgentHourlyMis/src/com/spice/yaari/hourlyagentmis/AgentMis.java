package com.spice.yaari.hourlyagentmis;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class AgentMis {
	private static Logger logger = Logger.getLogger(AgentMis.class.getName());
	private static Properties prop = new Properties();
	private static int dateDiff = 0;
	public static int curr_hour = 0;

	static {
		Calendar calHour = Calendar.getInstance();
		curr_hour = calHour.get(Calendar.HOUR_OF_DAY);
		System.out.println("curr hour is [" + curr_hour + "]");
	}

	public AgentMis() {
		try {
			PropertyConfigurator.configure("./log4j.properties");
			prop.load(new DataInputStream(new FileInputStream("./config.properties")));
			dateDiff = Integer.parseInt(prop.getProperty("DateDiff"));
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public static void main(String[] args) {
		try {
			AgentMis agentMis = new AgentMis();
			if (curr_hour == 0) {
				curr_hour = 23;
				dateDiff = 1;
			} else {
				curr_hour = curr_hour - 1;
			}
			String mis_date = agentMis.getDate(dateDiff);
			GenerateExcel generateExcel = new GenerateExcel();
			generateExcel.getExcelProcess(mis_date);
			Zipper zipper = new Zipper();
			zipper.getZipper("./misData/!dea_YAARI_Hourly_Report_" + mis_date + "_" + curr_hour + ".xlsx", mis_date);
			MailHelper mailHelper = new MailHelper();
			mailHelper.sendMailRequest("./misData/!dea_YAARI_Hourly_Report_" + mis_date + "_" + curr_hour + ".zip");
			logger.debug("Program is END..");
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public String getDate(int datediff) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -datediff);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String strDate = formatter.format(cal.getTime());
		logger.debug("date return[" + strDate.toString() + "]");
		return strDate;
	}
}
