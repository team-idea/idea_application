package com.spice.yaari.hourlyagentmis;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.apache.log4j.Logger;


public class Zipper {	
	private static Logger logger = Logger.getLogger(Zipper.class.getName());
	    
		public void getZipper(String sourceFile,String mis_date)
	    {
	    	byte[] buffer = new byte[1024];
	    	try{
	    		FileOutputStream fos = new FileOutputStream("./misData/!dea_YAARI_Hourly_Report_" + mis_date + "_" + AgentMis.curr_hour + ".zip");
	    		ZipOutputStream zos = new ZipOutputStream(fos);
	    		ZipEntry ze= new ZipEntry("!dea_YAARI_Hourly_Report_" + mis_date + "_" + AgentMis.curr_hour + ".xlsx");
	    		zos.putNextEntry(ze);
	    		FileInputStream in = new FileInputStream("./misData/!dea_YAARI_Hourly_Report_" + mis_date + "_" + AgentMis.curr_hour + ".xlsx");
	    		int len;
	    		while ((len = in.read(buffer)) > 0) {
	    			zos.write(buffer, 0, len);
	    		}
	    		in.close();
	    		zos.closeEntry();
	    		zos.close();
	    		File file = new File(sourceFile);
				if (file.delete()) {
					logger.debug(file.getName() + " is deleted!");
				} else {
					logger.debug(file.getName() + " is not deleted!");
				}
	    		logger.debug("file has been zipped");
	    	}catch(IOException ex){
	    	   ex.printStackTrace();
	    	}
	    }
	}
	
	

