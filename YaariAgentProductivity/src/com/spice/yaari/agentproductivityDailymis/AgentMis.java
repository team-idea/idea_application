package com.spice.yaari.agentproductivityDailymis;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class AgentMis {
	private static Logger logger = Logger.getLogger(AgentMis.class.getName());
	private static Properties prop = new Properties();
	private static int dateDiff = 0;
	public static int curr_hour = 0;

	public AgentMis() {
		try {
			PropertyConfigurator.configure("./log4j.properties");
			prop.load(new DataInputStream(new FileInputStream("./config.properties")));
			dateDiff = Integer.parseInt(prop.getProperty("DateDiff"));
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public static void main(String[] args) {
		try {
			AgentMis agentMis = new AgentMis();			
			String mis_date = agentMis.getDate(dateDiff);
			GenerateExcel generateExcel = new GenerateExcel();
			generateExcel.excelProcess(mis_date);
			Zipper zip = new Zipper();
			zip.getZipper("./misData/!dea_YAARI_AgentProductivity_Daily_Report_" + mis_date + ".xlsx",mis_date);
			MailHelper mailHelper = new MailHelper();
			mailHelper.sendMailRequest("./misData/!dea_YAARI_AgentProductivity_Daily_Report_" + mis_date + ".zip");
			logger.debug("Program is END..");
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public String getDate(int datediff) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -datediff);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String strDate = formatter.format(cal.getTime());
		logger.debug("date return[" + strDate.toString() + "]");
		return strDate;
	}
}
