package com.sdp.billing;

import java.io.*;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;

import java.security.*;
import javax.net.ssl.*;


import org.apache.log4j.*;


public class misInfo extends Thread
{
	private static Logger logger 							= Logger.getLogger( sendRequestSDP.class.getName() );
	private static int clients								= 0;
	private static misInfo instance							= null;		// The single instance
	private static configureAlerts alertValue				= null;
	private static alertMail mail							= null;


	private static String machine_name						= "sdp";
	private static String instance_id						= "db";
	private DBConnectionManager connMgr						= null;
	private String conPool_server							= "Server";

	private static String from								= "";
	private static String hourly_to_list					= "";
	private static String hourly_cc_list					= "";
	private static String hourly_bcc_list					= "";

	private static String daily_to_list						= "";
	private static String daily_cc_list						= "";
	private static String daily_bcc_list					= "";

	private static final String[] circleList				= {"ap","as","bh","dl","gj","hp","hr","jk","kk","kl","kr","mh","mp","mu","ne","or","pb","rj","tn","ue","uw","wb","ss"};


	static synchronized public misInfo getInstance()
	{
		if (instance == null){instance = new misInfo();mail=alertMail.getInstance();Thread sdpMIS = new Thread(new misInfo());	sdpMIS.start();}
		clients++;
		return instance;
	}
	public misInfo()
	{
		try
		{
			PropertyConfigurator.configure("log4j.properties");
			Properties prop = new Properties();
			prop.load(new DataInputStream(new FileInputStream("sdp.properties")));

			from			= prop.getProperty("alert.mail.from");

			hourly_to_list	= prop.getProperty("mis.hourly.mail.to");
			hourly_cc_list	= prop.getProperty("mis.hourly.mail.cc");
			hourly_bcc_list	= prop.getProperty("mis.hourly.mail.bcc");

			daily_to_list	= prop.getProperty("mis.daily.mail.to");
			daily_cc_list	= prop.getProperty("mis.daily.mail.cc");
			daily_bcc_list	= prop.getProperty("mis.daily.mail.bcc");

			machine_name	= prop.getProperty("sdp.machine.name");
			instance_id		= prop.getProperty("sdp.application.instance");

			alertValue		= configureAlerts.getInstance();
			connMgr			= DBConnectionManager.getInstance();

		}catch(Exception exp){exp.printStackTrace();}
	}

	public void run()
	{
		final String threadName			= String.format("%-10s",Thread.currentThread().getName());
		String[] circle_mis_info		= new String[23];
		while(true)
		{
			if((Integer.parseInt(getHour()) % 3 == 0) && (Integer.parseInt(getMinutes()) >=56 && Integer.parseInt(getMinutes()) < 58))
			{
				logger.debug("["+threadName+"] ->(mis  : al) appropriate time found for MIS sending. Sending the latest MIS to registered users.");
				try
				{
					int TotalCallBacks=0,ActSuccess=0,ActFail=0,ActRev=0,RenSuccess=0,RenFail=0,RenRev=0,TotalRev=0,TotalDeactivations=0;

					for(int i=0;i<circleList.length;i++)
					{
						//@Circle-@TotalCallBacks-@ActSuccess-@ActFail-@ActRev-@RenSuccess-@RenFail-@RenRev-@TotalRev-@TotalDeactivations
						circle_mis_info[i] = generateHourlyCircleMIS(threadName,circleList[i]);
					}

					String mail_str		= "<table border=\"1\"><tr><th>Circle</th><th>TotalCallBacks</th><th>ActSuccess</th><th>ActFail</th><th>ActRev</th><th>RenSuccess</th><th>RenFail</th><th>RenRev</th><th>TotalRev</th><th>TotalDeactivations</th></tr>";
					for(int i=0;i<circle_mis_info.length;i++)
					{
						String[] totalInfo= circle_mis_info[i].split("-");
						circle_mis_info[i]= "<tr><td>"+circle_mis_info[i]+"</td></tr>";
						circle_mis_info[i]= circle_mis_info[i].replace("-","</td><td>");
						mail_str		 += circle_mis_info[i];

						TotalCallBacks+= 		Integer.parseInt(totalInfo[1]);
						ActSuccess+= 			Integer.parseInt(totalInfo[2]);
						ActFail+= 				Integer.parseInt(totalInfo[3]);
						ActRev+= 				Integer.parseInt(totalInfo[4]);
						RenSuccess+= 			Integer.parseInt(totalInfo[5]);
						RenFail+= 				Integer.parseInt(totalInfo[6]);
						RenRev+= 				Integer.parseInt(totalInfo[7]);
						TotalRev= 				ActRev+RenRev;
						TotalDeactivations+= 	Integer.parseInt(totalInfo[9]);
					}
					mail_str += "<tr><th>All</th><th>"+TotalCallBacks+"</th><th>"+ActSuccess+"</th><th>"+ActFail+"</th><th>"+ActRev+"</th><th>"+RenSuccess+"</th><th>"+RenFail+"</th><th>"+RenRev+"</th><th>"+TotalRev+"</th><th>"+TotalDeactivations+"</th></tr>";
					mail_str += "</table>";

					String messageSubject			= "SDP MIS Alert: [machine_name = "+machine_name+". instance_id = "+instance_id+"] ALL circles MIS for  "+generateDateTime()+".";
					String messageBody				= mail_str;
					String attachment				= "";

					boolean mailRetCode				= mail.sendMail(hourly_to_list, hourly_cc_list, hourly_bcc_list, from, messageSubject, messageBody, attachment);
					Thread.sleep(1000*60*4);

				}catch(Exception exp){}
			}else{try{Thread.sleep(1000*60*1);}catch(Exception exp){}}
		}
	}

	public String generateHourlyTotalMIS(){String retStr = ""; return retStr;}

	public String generateHourlyCircleMIS(final String threadName,final String circle)
	{
		String retStr = "";
		Connection connSelect = connMgr.getConnection(conPool_server);
		try
		{
			if(connSelect!=null)
			{
				String prePareCall = "{call  sdpCentral_"+circle+".dbo.centralMisCircleWise(?,?)}";
				boolean procFlag=false;
				CallableStatement statement = connSelect.prepareCall(prePareCall);
				statement.setString(1,circle);
				statement.registerOutParameter(2, java.sql.Types.VARCHAR);
				procFlag	= statement.execute();
				if(!procFlag)retStr 		= statement.getString(2);
				else retStr			= "error";
				logger.debug("["+threadName+"] ->(mis  :"+circle+") "+prePareCall+" ="+procFlag);
			}
			else
			{
				logger.info("["+threadName+"] ->(mis  :"+circle+") Connection Cannot be established connSelect="+connSelect);
				alertValue.setDatabaseConnAlert("["+threadName+"] ->(mis   :"+circle+") Connection Cannot be established connSelect="+connSelect);
				try{Thread.sleep(1000*3);}catch(Exception expTmr){}
				//retCode = 0;
			}
		}catch(Exception exp){logException(exp);}
		finally{if(connSelect!=null) connMgr.freeConnection(conPool_server,connSelect);}
		return retStr;
	}

	private String generateDateTime()
	{
		java.util.Date date 				= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
		return SQL_DATE_FORMAT.format(date.getTime());
	}

	private String getMinutes()
	{
		java.util.Date date 				= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("mm");
		return SQL_DATE_FORMAT.format(date.getTime());
	}

	private String getHour()
	{
		java.util.Date date 				= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("HH");
		return SQL_DATE_FORMAT.format(date.getTime());
	}


	private void logException(Exception exp)
	{
		logger.error(getStackTrace(exp));
		alertValue.setExceptionAlert("[Thread-mis] <-(mis   : al) "+getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}

}