package com.sdp.billing;


import java.util.*;
import java.io.*;
import java.text.*;
import java.sql.*;

import org.apache.log4j.*;

public class ftpClientSdp extends Thread
{
	private static Logger logger 				= Logger.getLogger( ftpClientSdp.class.getName() );
	private DBConnectionManager connMgr			= null;
	private static configureAlerts alertValue	= null;

	private static String sdpFtpDownloadDetails	= "";
	private static String sdpFtpUploadDetails	= "";


	public static void main(String argv[])
	{
		ftpClientSdp ftpClient = new ftpClientSdp();
	}

	public ftpClientSdp()
	{
		try{PropertyConfigurator.configure("log4j.properties");}catch(Exception exp){ };
		init();
	}

	private void init()
	{
		try
		{
			Properties prop 						= new Properties();
			prop.load(new DataInputStream(new FileInputStream("sdp.properties")));
			sdpFtpDownloadDetails					= prop.getProperty("sdp.ftp.download.remote.host.details");
			sdpFtpUploadDetails						= prop.getProperty("sdp.ftp.upload.remote.host.details");
			connMgr 								= DBConnectionManager.getInstance();
			alertValue								= configureAlerts.getInstance();
			this.start();
		}catch(Exception exp){}
	}

	public void run()
	{
		final String sdpFtpDownloadDetail			= this.sdpFtpDownloadDetails;
		final String sdpFtpUploadDetail				= this.sdpFtpUploadDetails;
		final String appender						= "["+String.format("%-10s",Thread.currentThread().getName())+"] ->(ftp   :al) ";
		final String downloadfileName				= "SE_spice";

		while(true)
		{
			String currDate							= generateDate();									// yyyyMMdd format is used here as per SDP-SE naming conventions.
			String[] ftpDownload					= sdpFtpDownloadDetail.split(",");					// User credentials to connect to SDP-SE FTP server.
			ftpDownload[4]							= ftpDownload[4].replace("<yyyymmdd>",currDate);	// replacing the date String with actual date.

			boolean success							= (new File(ftpDownload[4])).mkdirs();				// Making a new folder for the FTP download of the Failed CDR Batch downloaded from the SDP-SE.
			if(success)	logger.debug(appender+"Directory Created for Daily Records.");

			logger.info(appender+"Trying to download file "+downloadfileName+" from SDP FTP location {"+ftpDownload[0]+"} .");
			final String retFileName				= downloadFile(appender,ftpDownload[0],ftpDownload[1],ftpDownload[2],ftpDownload[3],ftpDownload[4],downloadfileName);

			if(retFileName != null && !(retFileName.equalsIgnoreCase("")))
			{
				Vector<String> processedRecords		= processFile(appender,ftpDownload[4]+"/"+retFileName);
				if(processedRecords!=null)
				{
					String retStr					= writeProcessedFile(appender,ftpDownload[4],retFileName,processedRecords);
					boolean retBool					= uploadFile(appender,ftpDownload[0],ftpDownload[1],ftpDownload[2],ftpDownload[3],ftpDownload[4],retStr);
				}
				try{Thread.sleep(1000*5);}catch(Exception exp){}
			}
			else
			{
				logger.info(appender+"No Callback Failed file found on FTP to download. Sleeping for 10 minutes.");
				try{Thread.sleep(1000*60*10);}catch(Exception exp){}
			}
		}
	}

	public synchronized String downloadFile(final String appender,final String host,final String user,final String pwd,final String dir,final String localfilePath,final String Serverfile)
	{
		String retStr = null;
		try
		{
			logger.debug(appender+host+" - "+user+" - "+pwd+" - "+dir+" - "+localfilePath+" - "+Serverfile);
			FtpWrapper ftp	= new FtpWrapper();
			boolean login	= ftp.connectAndLogin(host,user,pwd);
			if(login)
			{
				logger.debug(appender+"Connected to the FTP remote server: "+host);
				ftp.setPassiveMode(true); ftp.binary();
				try
				{
					logger.debug(appender+"Welcome message: "+ftp.getReplyString());
					logger.debug(appender+"Current Directory: "+ftp.printWorkingDirectory());
					if(!(dir.equalsIgnoreCase("none")))
					{
						logger.debug(appender+"Changing this directory to "+dir+": "+ftp.changeDirectory(dir));
						logger.debug(appender+"Current Directory after change: "+ftp.printWorkingDirectory());
					}else{logger.debug(appender+"Fetching the content in current directory: Root");}

					logger.debug(appender+"fileExists("+Serverfile+")  = "+ftp.fileExists(Serverfile));

					if(ftp.fileExists(Serverfile))
					{
						final String serverfile	= ftp.getFileName(Serverfile);
						logger.debug(appender+"File "+serverfile+" to be downloaded to local directory : " + localfilePath+"/");
						boolean retFlag = ftp.downloadFile(serverfile, localfilePath+"/"+serverfile+"_dw");
						if(retFlag)
						{
							logger.debug(appender+"File "+serverfile+" downloaded to destination directory "+localfilePath+".");
							retStr	= serverfile;
							boolean retbool		= ftp.del(serverfile);
							logger.debug(appender+"File "+serverfile+" deleted from successfully from the destination directory.");
							try
							{
								File oldFile = new File(localfilePath+"/"+serverfile+"_dw"); File newFile = new File(localfilePath+"/"+serverfile);
								logger.debug(appender+"tmp().renameTo() = "+oldFile.renameTo(newFile));
							}catch(Exception expI){logException(expI,appender);}
						}
						else
						{
							logger.debug(appender+"File "+serverfile+" cannot be downladed to destination directory "+localfilePath+".");
							retStr=null;
						}
					}else{retStr=null;}
				}catch(Exception exp){ }
				finally
				{
					try
					{
						ftp.logout(); ftp.disconnect();
					}catch(Exception expt){ }
				}
			}else{logger.debug(appender+"Unable to connect to: "+host);}
		}catch(Exception expM){ }
		return retStr;
	}


	private Vector<String> processFile(final String appender,final String fileName)
	{
		Vector<String> retVectStr					= new Vector<String>();
		final File file 							= new File(fileName);
		try
		{
			Scanner scanner 						= new Scanner(file);
			while (scanner.hasNextLine())
			{
				final String callBack				= scanner.nextLine();
				boolean retBool						= false;
				logger.debug(appender+callBack);
				if(callBack.startsWith("{"))
				{
					retBool							= insertCallback(appender,callBack);
				}
				String callBack_processed			= null;
				logger.info(appender+"insertCallback(appender,callBack) = "+retBool+" retVectStr.size() = "+retVectStr.size());

				callBack_processed					= callBack.replace(",}","}");
				if(retBool)	{callBack_processed=callBack.replace("}",",cp_process_status=ack}");}	/*write the success record into vector for SDP-SE response file*/
				else		{callBack_processed=callBack.replace("}",",cp_process_status=nack}");}	/*write the failed record into vector for SDP-SE response file */
				if(callBack_processed!=null&&callBack.startsWith("{"))	retVectStr.add(callBack_processed);
			}
			scanner.close();
		}
		catch (Exception exp){logException(exp,appender);}
		return retVectStr;
	}
	private boolean insertCallback(final String appender,final String callBack)
	{
		final String conPool	= "Server";
		boolean retBool			= false;
		String content			= callBack;
		logger.debug(appender+content);
		//logValue.addLog(content);

		try
		{
			content				= content.replace("{",""); content= content.replace("}","");content= content.replace(",","\n");
			content				= content.toLowerCase();
			Properties prop 	= new Properties();
			prop.load(new DataInputStream(new ByteArrayInputStream(content.getBytes("UTF-8"))));
			//prop.list(System.out);

			String RefId=prop.getProperty("refid");
			int retry_num=Integer.parseInt(prop.getProperty("retry_num"));
			String circle=prop.getProperty("circle").toLowerCase();
			String msisdn=prop.getProperty("msisdn");
			String prepost=prop.getProperty("type");
			String srvkey=prop.getProperty("srvkey");
			String mode=prop.getProperty("mode").toLowerCase();
			String price=prop.getProperty("price");
			String sdpStatus=prop.getProperty("status");
			String action=prop.getProperty("action");
			String precharge=prop.getProperty("precharge");
			String startDate=prop.getProperty("startdate");
			String startTime=prop.getProperty("starttime");
			String endDate=prop.getProperty("enddate");
			String endTime=prop.getProperty("endtime");
			String originator=prop.getProperty("originator").toLowerCase();
			String cp_user=prop.getProperty("user");
			String cp_password=prop.getProperty("pass");
			String info=prop.getProperty("info");
			String startDateTime=getDateTime(appender,startDate,startTime);
			String endDateTime=getDateTime(appender,endDate,endTime);


			if(action.equalsIgnoreCase("ACT")||action.equalsIgnoreCase("DCT")||action.equalsIgnoreCase("SDCT")||action.equalsIgnoreCase("NETCHURN"))
			{
				mode+="_"+originator;
			}
			else {mode+=""; originator = "sdpRsp";}

			String insQry="insert into sdpCentral_"+circle+".dbo.tbl_callback(RefId, retry_num, circle, msisdn, prepost, srvkey, mode, price, sdpStatus, status, action, precharge, startDateTime, endDateTime, originator, cp_user, cp_password, info) values('"+RefId+"',"+retry_num+",'"+circle+"','"+ msisdn+"','"+ prepost+"','"+ srvkey+"','"+ mode+"','"+ price+"','"+ sdpStatus+"',0,'"+ action+"','"+ precharge+"','"+ startDateTime+"','"+ endDateTime+"','"+ originator+"','"+ cp_user+"','"+ cp_password+"','"+ info+"')";
			logger.debug(appender+"Circle database pool = "+conPool+" ins stmt = "+insQry);
			try
			{
				Connection conn	= null;
				int connCntr= 0;
				while(connCntr<3)
				{
					conn= connMgr.getConnection(conPool);
					if(conn!=null) break; else try{Thread.sleep(50);}catch(Exception exp){logException(exp,appender);}
					connCntr++;
				}
				logger.debug(appender+"Connection conn = "+conn);
				if(conn != null)
				{
					try
					{
						Statement stmt= conn.createStatement();
						logger.debug(appender+insQry+" = "+stmt.executeUpdate(insQry));
						retBool = true;
					}catch(Exception exp){logException(exp,appender);}
					finally{if(conn != null){connMgr.freeConnection(conPool,conn);}}
				}
				else logger.error(appender+"No free connections available");
			}catch(Exception expConn){logException(expConn,appender);retBool = false;}
		}catch(Exception exp){logException(exp,appender);retBool = false;}
		return retBool;
	}

	private String writeProcessedFile(final String appender,final String path,final String fileName,final Vector<String> processedRecords)
	{
		String retStr			= null;
		try
		{
			//SE_SPICE_CALLBACK_FAILED_BATCH_20120309164400_00001.dat
			//SE_SPICE_CALLBACK_FAILED_BATCH_20120309164400_00001_Processed.dat
			retStr				= fileName.replace(".dat","_Processed.dat");
			String file_name	= retStr;
			File file			= new File(path+"/"+file_name);
			FileWriter txt 		= new FileWriter(file);
			PrintWriter out 	= new PrintWriter(txt);

			//preparing the roll-in header
			String header		= "80|"+getRollDateTime();
			String count		= String.format("%1$" + 5 + "s", processedRecords.size()).replace(" ","0");
			String footer		= "90|"+count+"|"+getRollDateTime();					//03092012164400

			out.println(header);
			int i				= 0;
			while(i<processedRecords.size()){out.println(processedRecords.get(i));i++;}
			out.println(footer);
			out.close();
		}catch(Exception exp){logException(exp,appender);retStr = null;}
		return retStr;
	}

	private synchronized String getDateTime(final String appender,final String date,final String time)
	{
		String retDateTime = null;
		try
		{
			DateFormat sdp	= new SimpleDateFormat("yyyyMMdd HHmmSS"); DateFormat db	= new SimpleDateFormat("yyyy-MM-dd HH:mm:SS");
			java.util.Date dt_sdp 	= sdp.parse(date+" "+time);
			retDateTime		= db.format(dt_sdp);
		}catch(Exception exp){retDateTime = null;logException(exp,appender);}
		logger.debug(appender+retDateTime);
		return retDateTime;
	}

	private String generateDate()
	{
		java.util.Date date 						= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT			= new SimpleDateFormat("yyyyMMdd");
		String logDate								= SQL_DATE_FORMAT.format(date.getTime());
		return logDate;
	}

	private String getRollDateTime()
	{
		java.util.Date date 						= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT			= new SimpleDateFormat("MMddyyyyHHmmss");
		String logDate								= SQL_DATE_FORMAT.format(date.getTime());
		return logDate;
	}

	private void logException(Exception exp,final String appender)
	{
		logger.error(appender+getStackTrace(exp));
		alertValue.setExceptionAlert(appender+getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 						= new StringWriter();
			PrintWriter pw 							= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}

	private  boolean uploadFile(final String appender,final String host,final String user,final String pwd,final String dir,final String localfilePath,final String fileName)
	{
		boolean retBool			= false;
		boolean uploadFlag		= true;
		try
		{
			logger.info(appender+host+" - "+user+" - "+pwd+" - "+dir+" - "+localfilePath+" - "+fileName);
			FtpWrapper ftp		= new FtpWrapper();
			boolean login		= ftp.connectAndLogin(host,user,pwd);
			if(login)
			{
				logger.debug(appender+"Connected to the FTP remote server: "+host);
				ftp.setPassiveMode(true); ftp.binary();
				try
				{
					logger.debug(appender+"Welcome message: "+ftp.getReplyString());
					logger.debug(appender+"Current Directory: "+ftp.printWorkingDirectory());
					if(!(dir.equalsIgnoreCase("none")))
					{
						boolean	retBoolDir		= ftp.changeDirectory(dir);
						logger.debug(appender+"Changing this directory: "+retBoolDir);
						if(retBoolDir)
						{
							logger.debug(appender+"Current Directory after change: "+ftp.printWorkingDirectory());
						}
						else
						{
							logger.debug(appender+"change Directory Failed. Trying to create the directory and than change.");
							boolean retBoolNewDir	= ftp.makedirectory(dir);
							if(!retBoolNewDir)
							{
								logger.debug(appender+"{cirtical}: creating new Directory Failed. FTP upload starting to root directory.");
								uploadFlag = true;
							}
						}
					}else{logger.debug(appender+"Uploading the content in current directory: Root");}

					if(uploadFlag)
					{
						boolean retFlag = ftp.uploadFile(localfilePath+"/"+fileName, fileName+"_up");
						logger.debug(appender+"Uploading file ["+fileName+"] to destination directory: " + retFlag);
						if(retFlag)
						{
							try{retFlag		= ftp.ren(fileName+"_up",fileName);}catch(Exception rx){retBool = false;}
							if(retFlag){logger.debug(appender+"Renaming files in this directory: " + retFlag);}
							else
							{
								retFlag	= ftp.deleteFile(fileName);
								if(retFlag)
								{
									try{retFlag		= ftp.ren(fileName+"_up",fileName);}catch(Exception rx){retBool = false;}
									if(retFlag){logger.debug(appender+"Renaming files in this directory: " + retFlag);}
									logger.debug(appender+"File uploading to the destination directory ["+dir+"] done host: "+host);
								}
							}

						}
						retBool = true;
					}else retBool = false;
				}catch(Exception exp){logger.error(appender+""+localfilePath+"/"+fileName+" = "+exp.toString());retBool = false;}
				finally
				{
					try
					{
						ftp.logout(); ftp.disconnect();
					}catch(Exception expt){retBool = false; }
				}
			}else{logger.debug(appender+"Unable to connect to: "+host);}
		}catch(Exception expM){retBool = false; }
		return retBool;
	}
}