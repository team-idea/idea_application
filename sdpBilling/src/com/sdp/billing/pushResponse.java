package com.sdp.billing;

import java.io.*;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import org.apache.log4j.*;

public class pushResponse extends Thread
{

	private static boolean isOk					= true;
	private static Logger logger 				= Logger.getLogger( pushResponse.class.getName() );
	private static String CONFIG_FILE			= "log4j.properties";
	private DBConnectionManager connMgr			= null;
	private String conPool_site					= "Site";
	private String conPool_server				= "Server";
	private String poolCentralDctLog			= "dctLog";
	private String circle_name					= "";

	private static int select_top_rows			= 10;
	private static int thread_sleeptime			= 10;
	private static int idle_log_print			= 5;

	private static sdpLogger logValue			= null;
	private static int threadID					= 0;

	private static configureAlerts alertValue	= null;

	public pushResponse(final String circle,final int threadId)
	{
		try
		{
			PropertyConfigurator.configure("log4j.properties");
			alertValue							= configureAlerts.getInstance();
			connMgr 							= DBConnectionManager.getInstance();
			logValue							= sdpLogger.getInstance();
			Properties prop = new Properties();
			prop.load(new DataInputStream(new FileInputStream("sdp.properties")));
			select_top_rows	= Integer.parseInt(prop.getProperty("sdp.Response.top.select"));
			thread_sleeptime= Integer.parseInt(prop.getProperty("sdp.Response.sleeptime.select"));
			idle_log_print= Integer.parseInt(prop.getProperty("sdp.idle.log.print"));if(idle_log_print<=0) idle_log_print = 5;
			this.threadID=threadId;
		}
		catch(Exception exp){exp.printStackTrace();}
		this.conPool_site						= circle+conPool_site;
		this.conPool_server						= conPool_server;
		this.circle_name						= circle;
		logger.info("[Thread-X  ] <-(push"+threadId+" :"+circle_name+") Starting push thread for "+circle_name);
		if(isOk) this.start();
	}

	public void run()
	{
		final String threadName			= String.format("%-10s",Thread.currentThread().getName());
		final String poolSite			= conPool_site;
		final String poolServer			= conPool_server;
		final String circleName			= circle_name;
		final int threadId				= this.threadID;
		int prntCtr						= 0;
		while(true)
		{
			int processedRows 			= processSmsReq(threadName,threadId,poolSite,poolServer,circleName);
			if(processedRows<=0)
			{
				prntCtr++;
				if(prntCtr%idle_log_print == 0)
				{
					logger.info("["+threadName+"] <-(push"+threadId+" :"+String.format("%s",circleName)+") No Records found to be processed. Sleeping for "+String.format("%3d",thread_sleeptime)+" secs. ");
					prntCtr = 0;
				}
				try{Thread.sleep(thread_sleeptime*1000);}catch(Exception expTmr){}
			}
			else{logger.info("["+threadName+"] <-(push"+threadId+" :"+String.format("%s",circleName)+") Total Records processed = "+processedRows+". Sleeping for 500 msecs.");try{Thread.sleep(500);}catch(Exception expTmr){}}
		}
	}

	private int processSmsReq(final String threadName,final int threadId,final String poolSite,final String poolServer,final String circleName)
	{
		int retCode = 0;
		try
		{
			Connection connSelect			= connMgr.getConnection(poolServer);
			Connection connUpdate			= connMgr.getConnection(poolServer);
			Connection connInsert			= connMgr.getConnection(poolSite);
			Connection connInsert_dct		= connMgr.getConnection(poolCentralDctLog);
			try
			{
				if(connSelect!=null&&connUpdate!=null&&connInsert!=null&&connInsert_dct!=null)
				{
					logger.debug("["+threadName+"] <-(push"+threadId+" :"+String.format("%s",circleName)+") Into processSmsReq() function. poolServer= "+poolServer+" poolSite= "+poolSite);
					Statement stmtSelect	= connSelect.createStatement();
					Statement stmtUpdate	= connUpdate.createStatement();
					Statement stmtInsert	= connInsert.createStatement();
					Statement stmtInsert_dct= connInsert_dct.createStatement();

					String stmSel			= "";
					if(threadId >-1)stmSel	= "select top "+select_top_rows+" RefId, circle, msisdn, prepost, srvKey, eventKey, action, precharge, price, startDateTime, endDateTime, mode, status from sdpCentral_"+circleName+".dbo.tbl_sdpResponse with(nolock) where status in (-1,0) and threadId = "+threadId;
					else		    stmSel	= "select top "+select_top_rows+" RefId, circle, msisdn, prepost, srvKey, eventKey, action, precharge, price, startDateTime, endDateTime, mode, status from sdpCentral_"+circleName+".dbo.tbl_sdpResponse with(nolock) where status in (-1,0)";

					logger.debug("["+threadName+"] <-(push"+threadId+" :"+String.format("%s",circleName)+") "+stmSel);
					ResultSet rs			= stmtSelect.executeQuery(stmSel);
					logger.debug("["+threadName+"] <-(push"+threadId+" :"+String.format("%s",circleName)+") Into select ResultSet iteration");
					while(rs.next())
					{
						String RefId				= rs.getString("RefId");
						String circle				= rs.getString("circle");
						String msisdn				= rs.getString("msisdn");
						String prepost				= rs.getString("prepost");
						String srvKey				= rs.getString("srvKey");
						String eventKey				= rs.getString("eventKey");
						String action				= rs.getString("action");
						String precharge			= rs.getString("precharge");
						String price				= rs.getString("price");
						String startDateTime		= rs.getString("startDateTime");
						String endDateTime			= rs.getString("endDateTime");
						String mode					= rs.getString("mode");
						String status				= rs.getString("status");

						String insert_stmt			= "insert into sdpSite.dbo.tbl_sdpResponse(RefId, circle, msisdn, prepost, srvKey, eventKey, action, precharge, price, startDateTime, endDateTime, mode, status) values ('"+RefId+"','"+circle+"','"+ msisdn+"','"+ prepost+"','"+ srvKey+"','"+ eventKey+"','"+ action+"','"+ precharge+"','"+ price+"','"+ startDateTime+"','"+ endDateTime+"','"+ mode+"','"+ status+"')";
						int rowCtr					= 0;
						try
						{
							rowCtr					= stmtInsert.executeUpdate(insert_stmt);
						}catch(Exception exp){connMgr.dropConnection(poolSite,connInsert); connInsert = null;}

						logger.debug("["+threadName+"] <-(push"+threadId+" :"+String.format("%s",circleName)+") "+insert_stmt+":"+rowCtr);
						int addLog = logValue.addLog("push"+threadId+" :"+circle+"|"+RefId+"|"+msisdn+"|"+srvKey+"|"+eventKey+"|"+action+"|"+price+"|"+startDateTime+"|"+endDateTime+"|"+mode+"|"+rowCtr+"|");
						logger.debug("["+threadName+"] <-(push"+threadId+" :"+String.format("%s",circleName)+") addLog= "+addLog);

						if(rowCtr != 1)	{logger.error("["+threadName+"] <-(push"+threadId+" :"+String.format("%s",circleName)+") Not Able to submit the message correctly to central Database rowCtr = "+rowCtr);}
						else
						{
							stmtUpdate.executeUpdate("insert into sdpCentral_"+circleName+".dbo.tbl_sdpResponse_log select * from sdpCentral_"+circleName+".dbo.tbl_sdpResponse with(nolock) where RefId='"+RefId+"' and srvKey='"+srvKey+"' and msisdn = '"+msisdn+"'"); Thread.sleep(15);
							stmtUpdate.executeUpdate("delete from sdpCentral_"+circleName+".dbo.tbl_sdpResponse where RefId='"+RefId+"' and srvKey='"+srvKey+"' and msisdn = '"+msisdn+"'"); Thread.sleep(15);
							if(action.equalsIgnoreCase("dct")||action.equalsIgnoreCase("sdct")||action.equalsIgnoreCase("netchrn"))
							{
								String insert_stmt_central	= "insert into sdpSite.dbo.tbl_sdpResponse(RefId, circle, msisdn, prepost, srvKey, eventKey, action, precharge, price, startDateTime, endDateTime, mode, status) values ('"+RefId+"','"+circle+"','"+ msisdn+"','"+ prepost+"','"+ srvKey+"','"+ eventKey+"','"+ action+"','"+ precharge+"','"+ price+"','"+ startDateTime+"','"+ endDateTime+"','"+ mode+"','"+ status+"')";
								int rowCtr_dct				= stmtInsert_dct.executeUpdate(insert_stmt_central);
							}
						}
						Thread.sleep(10*5);
						retCode++;
					}
				}
				else
				{
					logger.info("["+threadName+"] <-(push"+threadId+" :"+circleName+") [Site: "+poolSite+"<--->"+poolServer+" :Server] Connection Cannot be established connSelect="+connSelect+" connUpdate="+connUpdate+" connInsert="+connInsert+" connInsert_dct="+connInsert_dct);
					alertValue.setDatabaseConnAlert("["+threadName+"] <-(push"+threadId+" :"+circleName+") [Site: "+poolSite+"<--->"+poolServer+" :Server] Connection Cannot be established connSelect="+connSelect+" connUpdate="+connUpdate+" connInsert="+connInsert+" connInsert_dct="+connInsert_dct);
					try{Thread.sleep(1000*3);}catch(Exception expTmr){}
					//retCode = 0;
				}
			}catch(Exception expCon){logException(expCon,threadId,circleName,threadName);}
			finally
			{
				if(connSelect!=null) 		connMgr.freeConnection(poolServer,connSelect);else connMgr.dropConnection(poolSite,connSelect);
				if(connUpdate!=null) 		connMgr.freeConnection(poolServer,connUpdate);else connMgr.dropConnection(poolSite,connUpdate);
				if(connInsert!=null) 		connMgr.freeConnection(poolSite,connInsert);  else connMgr.dropConnection(poolServer,connInsert);
				if(connInsert_dct!=null)	connMgr.freeConnection(poolCentralDctLog,connInsert_dct);else connMgr.dropConnection(poolCentralDctLog,connInsert_dct);
			}
		}
		catch(Exception exp){logException(exp,threadId,circleName,threadName);}
		return retCode;
	}


	private String generateUUID()
	{
		UUID idOne = UUID.randomUUID();
		return ""+idOne;
	}

	private String generateDateTime()
	{
		java.util.Date date 				= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
		return SQL_DATE_FORMAT.format(date.getTime());
	}

	private void logException(Exception exp,final int threadId,final String circleName,final String threadName)
	{
		logger.error("["+threadName+"] <-(push"+threadId+" :"+String.format("%s",circleName)+") "+getStackTrace(exp));
		alertValue.setExceptionAlert("["+threadName+"] <-(push"+threadId+" :"+String.format("%s",circleName)+") "+getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}

}

