package com.sdp.billing;

import java.io.*;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;

import java.security.*;
import javax.net.ssl.*;


import org.apache.log4j.*;


public class sendRequestSDP extends Thread
{
	private static Logger logger 					= Logger.getLogger( sendRequestSDP.class.getName() );
	private DBConnectionManager connMgr				= null;
	private String conPool_server					= "Server";
	private String circle_name						= "";

	private static int select_top_rows				= 10;
	private static int thread_sleeptime				= 10;
	private static int idle_log_print				= 5;

	private static sdpLogger logValue				= null;

	private static configureAlerts alertValue		= null;

	public sendRequestSDP(final String circle)
	{
		try{trustAllHttpsCertificates();}catch(Exception tmr){}

		try
		{
			PropertyConfigurator.configure("log4j.properties");
			alertValue								= configureAlerts.getInstance();
			connMgr 								= DBConnectionManager.getInstance();
			try{logValue							= sdpLogger.getInstance();}catch(Exception expCon){logException(expCon,circle,String.format("%-10s",Thread.currentThread().getName()));}
			Properties prop 						= new Properties();
			prop.load(new DataInputStream(new FileInputStream("sdp.properties")));
			select_top_rows							= Integer.parseInt(prop.getProperty("sdp.sdp.top.select"));
			thread_sleeptime						= Integer.parseInt(prop.getProperty("sdp.sdp.sleeptime.select"));
			idle_log_print							= Integer.parseInt(prop.getProperty("sdp.idle.log.print"));if(idle_log_print<=0) idle_log_print = 5;
		}
		catch(Exception exp){exp.printStackTrace();}
		this.conPool_server							= conPool_server;
		this.circle_name							= circle;
		logger.info("[Thread-X  ] ->(sdp   :"+circle_name+") Starting sdp  thread for "+circle_name);
		this.start();
	}

	public void run()
	{
		final String threadName						= String.format("%-10s",Thread.currentThread().getName());
		final String circle							= circle_name;
		final String poolServer						= conPool_server;
		int prntCtr									= 0;
		while(true)
		{
			int processedRows 						= processRecords(threadName,circle,poolServer);
			if(processedRows<=0)
			{
				prntCtr++;
				if(prntCtr%idle_log_print == 0)
				{
					logger.info("["+threadName+"] ->(sdp   :"+circle+") No Records found to be processed. Sleeping for "+String.format("%3d",thread_sleeptime)+" secs. ");
					prntCtr = 0;
				}
				try{Thread.sleep(thread_sleeptime*1000);}catch(Exception expTmr){}
			}
			else{logger.info("["+threadName+"] ->(sdp   :"+circle+") Total Records processed = "+processedRows+". Sleeping for 500 msecs.");try{Thread.sleep(500);}catch(Exception expTmr){}}
		}
	}

	private int processRecords(final String threadName,final String circle,final String poolServer)
	{
		int retCode 								= -1;
		try
		{
			Connection connSelect					= connMgr.getConnection(poolServer);
			Connection connUpdate					= connMgr.getConnection(poolServer);
			Connection connInsert					= connMgr.getConnection(poolServer);
			try
			{
				if(connSelect!=null&&connUpdate!=null&&connInsert!=null)
				{
					logger.debug("["+threadName+"] ->(sdp   :"+circle+") Into processSmsReq() function. poolServer= "+poolServer+"");
					Statement stmtSelect			= connSelect.createStatement();
					Statement stmtUpdate			= connUpdate.createStatement();
					Statement stmtInsert			= connInsert.createStatement();
					String stmSelect				= "select top "+select_top_rows+" RefId, circle, msisdn, prepost, srvKey, eventKey, reqType, precharge, reqdateTime, mode, status, originator,num_retry from sdpCentral_"+circle+".dbo.tbl_sdpRequest with(nolock) where status =0 and mode in ('SMS','WEB','WAP','IVR','OBD','CRM','RetailVTopUp','STK','USSD','CC','SANCHYA','SIVR','CBC','ivr_offnet','ussd_offnet') order by reqdateTime asc";
					logger.debug("["+threadName+"] ->(sdp   :"+circle+") "+stmSelect);
					ResultSet rs					= stmtSelect.executeQuery(stmSelect);
					logger.debug("["+threadName+"] ->(sdp   :"+circle+") Into select ResultSet iteration");
					while(rs.next())
					{
						String RefId				= rs.getString("RefId").trim();
						String Circle				= rs.getString("circle").trim();
						String msisdn				= rs.getString("msisdn").trim();
						String prepost				= rs.getString("prepost").trim();
						String srvKey				= rs.getString("srvKey").trim();
						String eventKey				= rs.getString("eventKey").trim();
						String reqType				= rs.getString("reqType").trim();
						String precharge			= rs.getString("precharge").trim();
						String reqDateTime			= rs.getString("reqdateTime").trim();
						String mode					= rs.getString("mode").trim();
						String status				= rs.getString("status").trim();
						String originator			= rs.getString("originator").trim();
						int num_retry				= rs.getInt("num_retry");

						String respUrl				= "1059|ERROR|IP ORIGINATOR VALIDATION FAILS";
						String upd_stmt				= null;
						String ins_stmt				= null;
						String action				= null;

						if(eventKey == null || eventKey.equalsIgnoreCase(""))
						{
							String url 				= "";
							if(reqType.equalsIgnoreCase("sub"))
							{
								action 				= "ACT";
								if(mode.equalsIgnoreCase("ussd"))		url = "https://10.64.131.24:6006/subscription/ActivateSubscription?msisdn="+msisdn+"&srvkey="+srvKey+"&user=Spice&pass=Hidvm58n&Refid="+RefId+"&mode="+mode+"&reqType="+reqType+"&precharge=N&reqdate="+generateDate()+"&reqtime="+generateTime()+"&originator=spice";
								else if(mode.equalsIgnoreCase("ivr_offnet"))url = "http://10.0.252.103/Offnet/Offnet?circle="+Circle+"&msisdn="+msisdn+"&contentid="+srvKey+"";
								//else if(mode.equalsIgnoreCase("ussd_offnet") && circle.equalsIgnoreCase("TN"))	url = "http://10.0.252.103/Offnet/Offnet?circle="+Circle+"&msisdn="+msisdn+"&contentid="+srvKey+"";
								//else									url = "https://172.30.80.30:7005/subscription/ActivateSubscription?msisdn="+msisdn+"&srvkey="+srvKey+"&user=Spice&pass=Hidvm58n&Refid="+RefId+"&mode="+mode+"&reqType="+reqType+"&precharge=N&reqdate="+generateDate()+"&reqtime="+generateTime()+"&originator=spice";
								//20161227//else						url = "https://172.30.80.30:8005/subscription/ActivateSubscription?msisdn="+msisdn+"&srvkey="+srvKey+"&user=Spice&pass=Hidvm58n&Refid="+RefId+"&mode="+mode+"&reqType="+reqType+"&precharge=N&reqdate="+generateDate()+"&reqtime="+generateTime()+"&originator=spice";
								else									url = "https://172.30.80.30:7005/subscription/ActivateSubscription?msisdn="+msisdn+"&srvkey="+srvKey+"&user=Spice&pass=Hidvm58n&Refid="+RefId+"&mode="+mode+"&reqType="+reqType+"&precharge=N&reqdate="+generateDate()+"&reqtime="+generateTime()+"&originator=spice";
								respUrl				= callHttp(threadName,url,circle).trim();
								logger.info("["+threadName+"] ->(sdp   :"+circle+") "+url+": "+respUrl);
							}
							else if(reqType.equalsIgnoreCase("unsub"))
							{
								action				= "DCT";
								if(mode.equalsIgnoreCase("ussd_"))	url = "https://10.64.131.24:6006/subscription/DeactivateSubscription?msisdn="+msisdn+"&srvkey="+srvKey+"&user=Spice&pass=Hidvm58n&Refid="+RefId+"&mode="+mode+"&reqdate="+generateDate()+"&reqtime="+generateTime()+"&originator=spice";
								else if(mode.equalsIgnoreCase("ivr_offnet"))url = "http://10.0.252.103/Offnet/DeactivateSubscription?circle="+Circle+"&msisdn="+msisdn+"&srvKey="+srvKey+"&mode="+mode;
								//else 								url = "https://172.30.80.30:7005/subscription/DeactivateSubscription?msisdn="+msisdn+"&srvkey="+srvKey+"&user=Spice&pass=Hidvm58n&Refid="+RefId+"&mode="+mode+"&reqdate="+generateDate()+"&reqtime="+generateTime()+"&originator=spice";
								//20161227else 						url = "https://172.30.80.30:8005/subscription/DeactivateSubscription?msisdn="+msisdn+"&srvkey="+srvKey+"&user=Spice&pass=Hidvm58n&Refid="+RefId+"&mode="+mode+"&reqdate="+generateDate()+"&reqtime="+generateTime()+"&originator=spice";
								else 								url = "https://172.30.80.30:7005/subscription/DeactivateSubscription?msisdn="+msisdn+"&srvkey="+srvKey+"&user=Spice&pass=Hidvm58n&Refid="+RefId+"&mode="+mode+"&reqdate="+generateDate()+"&reqtime="+generateTime()+"&originator=spice";
								respUrl					= callHttp(threadName,url,circle).trim();
								logger.info("["+threadName+"] ->(sdp   :"+circle+") "+url+": "+respUrl);
							}

							if(respUrl.equalsIgnoreCase("ok|success")||respUrl.equalsIgnoreCase("200|ok|success")||respUrl.equalsIgnoreCase("200|success")||respUrl.equalsIgnoreCase("1006|ERROR|SUBREQUESTALREADYINQUEUE"))
							{
								upd_stmt			= "update sdpCentral_"+circle+".dbo.tbl_sdpRequest set status=1,urlResponse='"+respUrl+"' where RefId='"+RefId+"' and msisdn='"+msisdn+"' and srvKey='"+srvKey+"'";
								ins_stmt			= "insert into sdpCentral_"+circle+".dbo.tbl_callbackPending(msisdn, prepost, srvkey, mode, action, status,reqDateTime,responseCode) values('"+msisdn+"','"+ prepost+"','"+ srvKey+"','"+ mode+"','"+action+"',0,getdate(),'"+respUrl+"')";
								int rowCtr			= stmtInsert.executeUpdate(ins_stmt);
								logger.debug("["+threadName+"] ->(sdp   :"+circle+") "+ins_stmt+":"+rowCtr);
								try{Thread.sleep(15);}catch(Exception exp){}
							}
							else if(respUrl.equalsIgnoreCase("error"))
							{
								upd_stmt 			= null;
								logger.error("["+threadName+"] ->(sdp   :"+circle+") Error received while calling URL. Will retry the request with RefId="+RefId);
								try{Thread.sleep(1000*5);}catch(Exception exp){}
							}
							else if(url.startsWith("http://10.0.252.103/"))
							{
								upd_stmt			= "update sdpCentral_"+circle+".dbo.tbl_sdpRequest set status=1,urlResponse='"+respUrl+"' where RefId='"+RefId+"' and msisdn='"+msisdn+"' and srvKey='"+srvKey+"'";
								try{Thread.sleep(15);}catch(Exception exp){}
							}
							else
							{
								if(respUrl.equalsIgnoreCase("702|ERROR|NO SUBSCRIPTIONS FOUND")||respUrl.equalsIgnoreCase("1017|ERROR|PORTED OUT USER")||respUrl.equalsIgnoreCase("722|ERROR|SUBSCIRPTION DEACTIVATION PENDING")||respUrl.equalsIgnoreCase("1012|ERROR|INVALIDSUBSCRIBERSTATUS")||respUrl.equalsIgnoreCase("1015|ERROR|SERVICE NOT AVAILABLE")||respUrl.equalsIgnoreCase("1008|ERROR|NOTAVAILFORCIRCLE")||respUrl.equalsIgnoreCase("2007|ERROR|SERVICENOTACTIVE")||respUrl.equalsIgnoreCase("1016|ERROR|BLACKLISTED USER")||respUrl.equalsIgnoreCase("703|ERROR|SERVICE NOT FOUND")||respUrl.equalsIgnoreCase("707|ERROR| srvkey is missing or invalid")||respUrl.equalsIgnoreCase("1011|ERROR|SERVICENOTACTIVE")|| respUrl.equalsIgnoreCase("1059|ERROR|IP ORIGINATOR VALIDATION FAILS"))
								{
									action			= "DCT";
									logger.error("["+threadName+"] ->(sdp   :"+circle+") Error ["+respUrl+"] received while calling URL. Will not retry the request with RefId="+RefId+".");
									upd_stmt		= "update sdpCentral_"+circle+".dbo.tbl_sdpRequest set status=1,urlResponse='"+respUrl+"' where RefId='"+RefId+"' and msisdn='"+msisdn+"' and srvKey='"+srvKey+"'";
									ins_stmt		= "insert into sdpCentral_"+circle+".dbo.tbl_sdpResponse (RefId, circle, msisdn, prepost, srvKey, eventKey, action, precharge, price, startDateTime, endDateTime, mode, status,threadId) values ('"+RefId+"','"+circle+"','"+msisdn+"','"+prepost+"','"+srvKey+"','"+eventKey+"','"+action+"','Y','0.0','"+reqDateTime+"','"+reqDateTime+"','"+mode+"_"+originator+"',0,0)";
									try
									{
										int rowctr	= stmtInsert.executeUpdate(ins_stmt);
										logger.debug("["+threadName+"] ->(sdp   :"+circle+") "+ins_stmt+":"+rowctr);
										try{Thread.sleep(20);}catch(Exception exp){}
									}catch(Exception exp){logger.error("["+threadName+"] ->(sdp   :"+circle+") "+ins_stmt+": error");logException(exp,circle,threadName);}
									alertValue.setSdpErrorCodeAlert("["+threadName+"] ->(sdp   :"+circle+") url: "+url+" Error: "+respUrl);
								}
								else if(respUrl.equalsIgnoreCase("705|ERROR|SUBSCRIPTION ALREADY EXISTS|"+srvKey+"|A"))
								{
									action			= "ACT";
									logger.error("["+threadName+"] ->(sdp   :"+circle+") Error ["+respUrl+"] received while calling URL. Will not retry the request with RefId="+RefId+", without sending mail.");
									upd_stmt		= "update sdpCentral_"+circle+".dbo.tbl_sdpRequest set status=1,urlResponse='"+respUrl+"'  where RefId='"+RefId+"' and msisdn='"+msisdn+"' and srvKey='"+srvKey+"'";
									ins_stmt		= "insert into sdpCentral_"+circle+".dbo.tbl_sdpResponse (RefId, circle, msisdn, prepost, srvKey, eventKey, action, precharge, price, startDateTime, endDateTime, mode, status,threadId) values ('"+RefId+"','"+circle+"','"+msisdn+"','"+prepost+"','"+srvKey+"','"+eventKey+"','"+action+"','N','0.0','"+reqDateTime+"','"+reqDateTime+"','"+mode+"',0,0)";
									try
									{
										int rowctr	= stmtInsert.executeUpdate(ins_stmt);
										logger.debug("["+threadName+"] ->(sdp   :"+circle+") "+ins_stmt+":"+rowctr);
										try{Thread.sleep(20);}catch(Exception exp){}
									}catch(Exception exp){logger.error("["+threadName+"] ->(sdp   :"+circle+") "+ins_stmt+": error");logException(exp,circle,threadName);}
									//alertValue.setSdpErrorCodeAlert("["+threadName+"] ->(sdp   :"+circle+") url: "+url+" Error: "+respUrl);
								}
								else if(respUrl.startsWith("1027|ERROR|PREVIOUS SERVICE IN QUEUE"))
								{
									action			= "siq";
									logger.error("["+threadName+"] ->(sdp   :"+circle+") Error ["+respUrl+"] received while calling URL. Will not retry the request with RefId="+RefId+", without sending mail.");
									upd_stmt		= "update sdpCentral_"+circle+".dbo.tbl_sdpRequest set status=1,urlResponse='"+respUrl+"'  where RefId='"+RefId+"' and msisdn='"+msisdn+"' and srvKey='"+srvKey+"'";
									ins_stmt		= "insert into sdpCentral_"+circle+".dbo.tbl_sdpResponse (RefId, circle, msisdn, prepost, srvKey, eventKey, action, precharge, price, startDateTime, endDateTime, mode, status,threadId) values ('"+RefId+"','"+circle+"','"+msisdn+"','"+prepost+"','"+srvKey+"','"+eventKey+"','"+action+"','N','0.0','"+reqDateTime+"','"+reqDateTime+"','"+mode+"',0,0)";
									try
									{
										int rowctr	= stmtInsert.executeUpdate(ins_stmt);
										logger.debug("["+threadName+"] ->(sdp   :"+circle+") "+ins_stmt+":"+rowctr);
										try{Thread.sleep(20);}catch(Exception exp){}
									}catch(Exception exp){logger.error("["+threadName+"] ->(sdp   :"+circle+") "+ins_stmt+": error");logException(exp,circle,threadName);}
									//alertValue.setSdpErrorCodeAlert("["+threadName+"] ->(sdp   :"+circle+") url: "+url+" Error: "+respUrl);

								}
								else
								{
									upd_stmt		= null;//"update sdpCentral_"+circle+".dbo.tbl_sdpRequest set status=0,urlResponse='"+respUrl+"' where RefId='"+RefId+"' and msisdn='"+msisdn+"' and srvKey='"+srvKey+"'";
									logger.error("["+threadName+"] ->(sdp   :"+circle+") Error ["+respUrl+"] received {N/H} while calling URL. Will retry the request with RefId="+RefId);
									alertValue.setSdpErrorCodeAlert("["+threadName+"] ->(sdp   :"+circle+") url: "+url+" Error: "+respUrl);
									try{Thread.sleep(1000*3);}catch(Exception exp){}
								}
							}
							if(upd_stmt != null)
							{
								try
								{
									int rowCtr		= stmtInsert.executeUpdate(upd_stmt);
									logger.debug("["+threadName+"] ->(sdp   :"+circle+") "+upd_stmt+":"+rowCtr);
									logValue.addLog("sdp : "+circle+"|"+RefId+"|"+msisdn+"|"+srvKey+"|"+eventKey+"|"+reqType+"|"+mode+"|"+reqDateTime+"|"+respUrl+"|"+rowCtr+"|");
									/*
									if(rowCtr != 1)	{logger.error("["+threadName+"] ->(sdp   :"+circle+") Not Able to submit the message correctly to central Database rowCtr = "+rowCtr);}
									else 			{stmtUpdate.executeUpdate("update sdpCentral_"+circle+".dbo.tbl_sdpRequest set status = 1,urlResponse='"+respUrl+"' where RefId='"+RefId+"'"); }
									*/
								}catch(Exception exp){logger.error("["+threadName+"] ->(sdp   :"+circle+") "+upd_stmt+": error");logException(exp,circle,threadName);}
							}
						}
						else if(eventKey.length()>2)
						{
							upd_stmt				= null;
							String price_hypo		= "0.0";
							/*
								This block has been introduced on July 20, 2012 to overcome the issue during reconcilation of the top-up values where we
								need to send the success price in the event response if the request is successfull.
							*/
							String[] eventKey_local	= eventKey.split("[|]");
							action					= eventKey_local[0];

							try{if(eventKey_local.length>1){price_hypo = String.valueOf(Float.parseFloat(eventKey_local[1]));}else{price_hypo = "0.0";}}catch(Exception exp){price_hypo = "0.0";}

							logger.debug("["+threadName+"] ->(sdp   :"+circle+") Event Detected direct hit initiated...");
							String url		= "https://172.30.80.30:7005/subscription/EventCharging?msisdn="+msisdn+"&srvkey="+srvKey+"&eventkey="+action+"&user=Spice&pass=Hidvm58n&Refid="+RefId+"&mode="+mode+"&reqdate="+generateDate()+"&reqtime="+generateTime()+"&originator=spice";
							if(mode.equalsIgnoreCase("ivr_offnet"))	url = "http://10.0.252.103/Offnet/OffnetSubscription?circle="+Circle+"&msisdn="+msisdn+"&contentid="+action+"&mode="+mode;
							else if(mode.equalsIgnoreCase("ussd_offnet") && (circle.equalsIgnoreCase("TN")||circle.equalsIgnoreCase("AP")||circle.equalsIgnoreCase("KK")||circle.equalsIgnoreCase("KR")))	url = "http://10.0.252.103/Offnet/OffnetSubscription?circle="+Circle+"&msisdn="+msisdn+"&contentid="+action+"&mode="+mode;							
							//else									url	= "https://172.30.80.30:7005/subscription/EventCharging?msisdn="+msisdn+"&srvkey="+srvKey+"&eventkey="+action+"&user=Spice&pass=Hidvm58n&Refid="+RefId+"&mode="+mode+"&reqdate="+generateDate()+"&reqtime="+generateTime()+"&originator=spice";
							//20161227else							url	= "https://172.30.80.30:8005/subscription/EventCharging?msisdn="+msisdn+"&srvkey="+srvKey+"&eventkey="+action+"&user=Spice&pass=Hidvm58n&Refid="+RefId+"&mode="+mode+"&reqdate="+generateDate()+"&reqtime="+generateTime()+"&originator=spice";
							else									url	= "https://172.30.80.30:7005/subscription/EventCharging?msisdn="+msisdn+"&srvkey="+srvKey+"&eventkey="+action+"&user=Spice&pass=Hidvm58n&Refid="+RefId+"&mode="+mode+"&reqdate="+generateDate()+"&reqtime="+generateTime()+"&originator=spice";
							respUrl					= callHttp(threadName,url,circle);
							logger.info("["+threadName+"] ->(sdp   :"+circle+") "+url+": "+respUrl);
							mode					= mode+"_"+respUrl;	if(mode.length()>=50){mode = mode.substring(0,49);}
							if(respUrl.equalsIgnoreCase("ok|success")||respUrl.equalsIgnoreCase("200|ok|success")||respUrl.equalsIgnoreCase("200|success")||respUrl.equalsIgnoreCase("1006|ERROR|SUBREQUESTALREADYINQUEUE"))
							{
								action				+="_success";
								ins_stmt			= "insert into sdpCentral_"+circle+".dbo.tbl_sdpResponse (RefId, circle, msisdn, prepost, srvKey, eventKey, action, precharge, price, startDateTime, endDateTime, mode, status,threadId) values ('"+RefId+"','"+circle+"','"+msisdn+"','"+prepost+"','"+srvKey+"','"+eventKey+"','"+action+"','"+precharge+"','"+price_hypo+"','"+reqDateTime+"','"+reqDateTime+"','"+mode+"',0,0)";
								upd_stmt			= "update sdpCentral_"+circle+".dbo.tbl_sdpRequest set status=1,urlResponse='"+respUrl+"' where RefId='"+RefId+"' and msisdn='"+msisdn+"' and srvKey='"+srvKey+"' and eventKey='"+eventKey+"'";
							}
							else if(respUrl.equalsIgnoreCase("602|ERROR|INTERNAL ERROR"))
							{
								logger.error("["+threadName+"] ->(sdp   :"+circle+") Error ["+respUrl+"] received while calling URL. Will retry the request with RefId="+RefId+".");
								upd_stmt			= null;
								try{Thread.sleep(1000);}catch(Exception exp){}
							}
							else if(url.startsWith("http://10.0.252.103/"))
							{
								upd_stmt			= "update sdpCentral_"+circle+".dbo.tbl_sdpRequest set status=1,urlResponse='"+respUrl+"' where RefId='"+RefId+"' and msisdn='"+msisdn+"' and srvKey='"+srvKey+"'";
								try{Thread.sleep(15);}catch(Exception exp){}
							}
							else
							{
								action				+="_fail";
								ins_stmt			= "insert into sdpCentral_"+circle+".dbo.tbl_sdpResponse (RefId, circle, msisdn, prepost, srvKey, eventKey, action, precharge, price, startDateTime, endDateTime, mode, status,threadId) values ('"+RefId+"','"+circle+"','"+msisdn+"','"+prepost+"','"+srvKey+"','"+eventKey+"','"+action+"','"+precharge+"','0.0','"+reqDateTime+"','"+reqDateTime+"','"+mode+"',0,0)";
								upd_stmt			= "update sdpCentral_"+circle+".dbo.tbl_sdpRequest set status=-1,urlResponse='"+respUrl+"' where RefId='"+RefId+"' and msisdn='"+msisdn+"' and srvKey='"+srvKey+"' and eventKey='"+eventKey+"'";
								alertValue.setSdpErrorCodeAlert("["+threadName+"] ->(sdp   :"+circle+") url: "+url+" Error: "+respUrl);
							}

							if(ins_stmt != null)
							{
								try
								{

									int rowCtr			= stmtInsert.executeUpdate(ins_stmt);
									logger.debug("["+threadName+"] ->(sdp   :"+circle+") "+ins_stmt+":"+rowCtr);
								}catch(Exception exp){logger.error("["+threadName+"] ->(sdp   :"+circle+") "+ins_stmt+": error");logException(exp,circle,threadName);}
								try{Thread.sleep(15);}catch(Exception exp){}
							}

							if(upd_stmt != null)
							{
								try
								{
									int rowCtr		= stmtInsert.executeUpdate(upd_stmt);
									logger.debug("["+threadName+"] ->(sdp   :"+circle+") "+upd_stmt+":"+rowCtr);
									logValue.addLog("sdp : "+circle+"|"+RefId+"|"+msisdn+"|"+srvKey+"|"+eventKey+"|"+reqType+"|"+mode+"|"+reqDateTime+"|"+respUrl+"|"+rowCtr+"|");
									if(rowCtr != 1)	{logger.error("["+threadName+"] ->(sdp   :"+circle+") Not Able to submit the message correctly to central Database rowCtr = "+rowCtr);}
									else 			{stmtUpdate.executeUpdate("update sdpCentral_"+circle+".dbo.tbl_sdpRequest set status = 1,urlResponse='"+respUrl+"' where RefId='"+RefId+"'"); }
								}catch(Exception exp){logger.error("["+threadName+"] ->(sdp   :"+circle+") "+upd_stmt+": error");logException(exp,circle,threadName);}
							}
						}

						try{Thread.sleep(10*6);}catch(Exception exp){}
						retCode++;
					}
				}
				else
				{
					logger.info("["+threadName+"] ->(sdp   :"+circle+") Connection Cannot be established connSelect="+connSelect+" connUpdate="+connUpdate+" connInsert"+connInsert);
					alertValue.setDatabaseConnAlert("["+threadName+"] ->(sdp   :"+circle+") Connection Cannot be established connSelect="+connSelect+" connUpdate="+connUpdate+" connInsert"+connInsert);
					try{Thread.sleep(1000*3);}catch(Exception expTmr){}
					//retCode = 0;
				}
			}catch(Exception expCon){logException(expCon,circle,threadName);}
			finally
			{
				if(connSelect!=null) connMgr.freeConnection(poolServer,connSelect);
				if(connUpdate!=null) connMgr.freeConnection(poolServer,connUpdate);
				if(connInsert!=null) connMgr.freeConnection(poolServer,connInsert);
			}
		}
		catch(Exception exp){logException(exp,circle,threadName);}
		return retCode;

	}

 	private String callHttp(final String threadName,final String url,final String circle)
 	{
 		String retStr 								= "";
 		String header 								= "";
 		try
 		{
			HostnameVerifier hv 					= new HostnameVerifier(){public boolean verify(String urlHostName, SSLSession session){return true;}};
			HttpsURLConnection.setDefaultHostnameVerifier(hv);

			URL Url 								= new URL(url);
			URLConnection conn 						= Url.openConnection();
			conn.setReadTimeout(1000*20);
			header 									= String.valueOf(conn.getHeaderFields());
			logger.debug("["+threadName+"] ->(sdp   :"+circle+") responseHeader: "+header);
			if(header.indexOf("200 OK")>0) header = "[HTTP/1.1 200 OK]"; else retStr = header;
			String str 								= null;
			BufferedReader in 						= new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while ((str = in.readLine()) != null) retStr += str;
			in.close();
			if(retStr.length()<2) if(header.equalsIgnoreCase("[HTTP/1.1 200 OK]")){retStr="success";}
 		}catch(Exception exp){retStr="error";logger.error("["+threadName+"] ->(sdp   :"+circle+") url: "+url);logException(exp,circle,threadName);}
 		return retStr;
 	}

	private String generateDate()
	{
		java.util.Date date 						= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT			= new SimpleDateFormat("yyyyMMdd");
		String logDate								= SQL_DATE_FORMAT.format(date.getTime());
		return logDate;
	}

	private String generateTime()
	{
		java.util.Date date 						= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT			= new SimpleDateFormat("HHmmss");
		String logDate								= SQL_DATE_FORMAT.format(date.getTime());
		return logDate;
	}

	private void logException(Exception exp,final String circleName,final String threadName)
	{
		logger.error("["+threadName+"] ->(sdp   :"+circleName+") "+getStackTrace(exp));
		alertValue.setExceptionAlert("["+threadName+"] ->(sdp   :"+circleName+") "+getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 						= new StringWriter();
			PrintWriter pw 							= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}
	private static void trustAllHttpsCertificates() throws Exception
	{
		javax.net.ssl.TrustManager[] trustAllCerts	= new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm 				= new miTM();

		trustAllCerts[0] 							= tm;
		javax.net.ssl.SSLContext sc 				= javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
	}

	public static class miTM implements javax.net.ssl.TrustManager,javax.net.ssl.X509TrustManager
	{
		public java.security.cert.X509Certificate[] getAcceptedIssuers()			{return null;}
		public boolean isServerTrusted(java.security.cert.X509Certificate[] certs)	{return true;}
		public boolean isClientTrusted(java.security.cert.X509Certificate[] certs)	{return true;}
		public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)	throws java.security.cert.CertificateException{return;}
		public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) throws java.security.cert.CertificateException{return;}
	}

}