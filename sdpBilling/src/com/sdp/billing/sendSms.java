package com.sdp.billing;

import java.io.*;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import org.apache.log4j.*;

public class sendSms extends Thread
{
	private static Logger logger 				= Logger.getLogger(sendSms.class.getName());
	private static sendSms instance				= null;
	private static int clients					= 0;
	private static configureAlerts alertValue	= null;

	private static String[] level1,level2,level3=null;

	static synchronized public sendSms getInstance()
	{
		if (instance == null)
		{
			alertValue=configureAlerts.getInstance();
			instance = new sendSms();
			Thread sendsms						= new Thread(new sendSms());
		}
		clients++;
		return instance;
	}

	public sendSms()
	{
		try
		{
			try{PropertyConfigurator.configure("log4j.properties");}catch(Exception e ){e.printStackTrace();}

			Properties prop = new Properties();
			prop.load(new DataInputStream(new FileInputStream("sdp.properties")));

			level1					= prop.getProperty("alert.sms.level1").split(",");
			level2					= prop.getProperty("alert.sms.level2").split(",");
			level3					= prop.getProperty("alert.sms.level3").split(",");
		}catch (Exception exp){logException(exp);}
	}

	private void logException(Exception exp)
	{
		logger.error(getStackTrace(exp));
		alertValue.setExceptionAlert(getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}
}