package com.sdp.billing;

import java.nio.channels.*;
import java.io.*;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.*;
import java.sql.*;
import java.text.*;
import java.util.*;
import java.util.zip.*;
import org.apache.log4j.*;

public class configureAlerts extends Thread
{
	private static Logger logger 							= Logger.getLogger( configureAlerts.class.getName() );
	private static int clients								= 0;
	private static int thread_sleeptime						= 5;
	private static int idle_log_print						= 5;

	private static int CALLBACK_MAX_THRESHOLD_ERROR_LEVEL	= 50;
	private static int CONNECTION_MAX_THRESHOLD_ERROR_LEVEL	= 5;
	private static int EXCEPTION_MAX_THRESHOLD_ERROR_LEVEL	= 5;
	private static int EXCEPTION_MAX_THRESHOLD_SDP_ERROR_LEVEL	= 5;

	private static String machine_name						= "sdp";
	private static String instance_id						= "db";


	private static configureAlerts instance					= null;				       		// The single instance
	static private boolean waitFlag							= false;
	static private boolean waitFlag_conn					= false;
	static private boolean waitFlag_exp						= false;
	static private boolean waitFlag_sdpExp					= false;

	static private Vector<String> alertVector				= new Vector<String>();
	static private Vector<String> connAlertVector			= new Vector<String>();
	static private Vector<String> exceptionAlertVector		= new Vector<String>();
	static private Vector<String> sdpErrorCodeAlertVector	= new Vector<String>();
	static private Vector<String> callBackAlertVector		= new Vector<String>();

	private DBConnectionManager connMgr						= null;
	private String conPool_alert							= "alert";
	private String conPool_server							= "Server";
	private static alertMail mail							= null;

	static private String fromList_ExceptionAlerts			= "";
	static private String toList_ExceptionAlerts			= "";
	static private String ccList_ExceptionAlerts			= "";
	static private String bccList_ExceptionAlerts			= "";
	static private String attachment_ExceptionAlerts		= "";


	static synchronized public configureAlerts getInstance()
	{
		if (instance == null){instance = new configureAlerts();}
		clients++;
		return instance;
	}

	public configureAlerts()
	{
		try
		{
			PropertyConfigurator.configure("log4j.properties");
			connMgr 										= DBConnectionManager.getInstance();
			mail											= alertMail.getInstance();

			Properties prop = new Properties();
			prop.load(new DataInputStream(new FileInputStream("sdp.properties")));
			thread_sleeptime= Integer.parseInt(prop.getProperty("sdp.alert.sleeptime.select"));
			machine_name= prop.getProperty("sdp.machine.name");
			instance_id= prop.getProperty("sdp.application.instance");
			idle_log_print= Integer.parseInt(prop.getProperty("sdp.idle.log.print"));if(idle_log_print<=0) idle_log_print = 5;
			CONNECTION_MAX_THRESHOLD_ERROR_LEVEL= Integer.parseInt(prop.getProperty("sdp.max.connectionError.threshold"));if(CONNECTION_MAX_THRESHOLD_ERROR_LEVEL<=0) CONNECTION_MAX_THRESHOLD_ERROR_LEVEL = 5;
			EXCEPTION_MAX_THRESHOLD_ERROR_LEVEL= Integer.parseInt(prop.getProperty("sdp.max.exceptionError.threshold"));if(EXCEPTION_MAX_THRESHOLD_ERROR_LEVEL<=0) EXCEPTION_MAX_THRESHOLD_ERROR_LEVEL = 5;
			EXCEPTION_MAX_THRESHOLD_SDP_ERROR_LEVEL= Integer.parseInt(prop.getProperty("sdp.max.exceptionSdpError.threshold"));if(EXCEPTION_MAX_THRESHOLD_SDP_ERROR_LEVEL<=0) EXCEPTION_MAX_THRESHOLD_SDP_ERROR_LEVEL = 5;

			fromList_ExceptionAlerts						= prop.getProperty("alert.mail.from");
			toList_ExceptionAlerts							= prop.getProperty("alert.mail.to");
			ccList_ExceptionAlerts							= prop.getProperty("alert.mail.cc");
			bccList_ExceptionAlerts							= prop.getProperty("alert.mail.bcc");

			logger.info("[Thread-X  ] ->(alert :al) Starting alert thread for all Circles. "+Thread.currentThread().getName());
			this.start();
		}
		catch(Exception exp){exp.printStackTrace();}
	}

	public void run()
	{
		final String threadName			= String.format("%-10s",Thread.currentThread().getName());
		int prntCtr						= 0;
		while(true)
		{
			int processedRows 			= 0;
			processedRows				= processMailAlertReq(threadName);
			processedRows				+=processDatabaseAlertReq(threadName);
			processedRows				+=processExceptionAlertReq(threadName);
			processedRows				+=processSdpExceptionAlertReq(threadName);
			processedRows				+=processCallBackCountAlert(threadName);
			if(processedRows<=0)
			{
				prntCtr++;
				if(prntCtr%idle_log_print==0)
				{
					logger.info("["+threadName+"] <-(alert :al) [AV: "+alertVector.size()+", CV: "+connAlertVector.size()+", EV: "+exceptionAlertVector.size()+", SV: "+sdpErrorCodeAlertVector.size()+"] No Records found to be processed. Sleeping for "+String.format("%3d",thread_sleeptime)+" secs.");
					prntCtr = 0;
				}try{Thread.sleep(thread_sleeptime*1000);}catch(Exception expTmr){}
			}
			else{logger.info("["+threadName+"] <-(alert :al) Total Records processed = "+processedRows+". Sleeping for 500 msecs.");try{Thread.sleep(500);}catch(Exception expTmr){}}
		}
	}



	private int processExceptionAlertReq(final String threadName)
	{
		int retCode						= 0;
		String attachementFileName		= "Exceptions";

		logger.debug("["+threadName+"] <-(alert :al) alertVector.size() = "+alertVector.size());
		if(exceptionAlertVector.size() > EXCEPTION_MAX_THRESHOLD_ERROR_LEVEL)
		{
			try
			{
				waitFlag_sdpExp	= true;
				Vector<String> exceptionAlertVectorLocal 		= (Vector) exceptionAlertVector.clone(); exceptionAlertVector.clear();
				waitFlag_sdpExp	= false;
				logger.debug("["+threadName+"] <-(alert :al) exceptionAlertVectorLocal.size() = "+exceptionAlertVectorLocal.size()+"exceptionAlertVector.size() = "+exceptionAlertVector.size());
				String error_string = "";
				for(int i=0;i<exceptionAlertVectorLocal.size();i++){error_string+=String.valueOf(exceptionAlertVectorLocal.get(i))+"";}
				try{error_string	= error_string.replace("<br>","\n");}catch(Exception exp){}
				logger.debug("["+threadName+"] <-(alert :al) "+error_string);
				if(exceptionAlertVectorLocal.size()>=EXCEPTION_MAX_THRESHOLD_ERROR_LEVEL)
				{
					//String messageSubject			= "SDP Exp Alert: [machine_name = "+machine_name+". instance_id = "+instance_id+"] Exception Error reached the Threshold of "+EXCEPTION_MAX_THRESHOLD_ERROR_LEVEL+". Please take relevant action.";
					//String messageBody				=	error_string;
					//String attachment				= "";
					//boolean mailRetCode				= mail.sendMail(toList_ExceptionAlerts, ccList_ExceptionAlerts, bccList_ExceptionAlerts, fromList_ExceptionAlerts, messageSubject, messageBody, attachment);
					//logger.debug("["+threadName+"] <-(alert :al) Value from exceptionAlertVector >= EXCEPTION_MAX_THRESHOLD_ERROR_LEVEL ("+EXCEPTION_MAX_THRESHOLD_ERROR_LEVEL+") mail.sendMail() = "+mailRetCode);
					String messageSubject			= "SDP Exp Alert: [machine_name = "+machine_name+". instance_id = "+instance_id+"] Exception Error reached the Threshold of "+EXCEPTION_MAX_THRESHOLD_ERROR_LEVEL+". Please take relevant action.";
					String retFileName				= zipLogs(threadName,error_string,attachementFileName);
					if(retFileName!=null)
					{
						String attachment			= retFileName+".zip";
						String messageBody			= "<p>Dear All,</p><p>Please find attached {"+attachment+"} the <b><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";color:black'>Exceptions</span></b> coming. Please take a relevant action against the same.</p><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";color:black'><b>Regards,</b></span><br>Team Idea SDP";
						logger.info("["+threadName+"] <-(alert :al) attachment = "+attachment+" messageBody= "+messageBody);
						boolean mailRetCode			= mail.sendMail(toList_ExceptionAlerts, ccList_ExceptionAlerts, bccList_ExceptionAlerts, fromList_ExceptionAlerts, messageSubject, messageBody, attachment);
						logger.debug("["+threadName+"] <-(alert :al) Value from sdpErrorCodeAlertVector >= EXCEPTION_MAX_THRESHOLD_ERROR_LEVEL ("+EXCEPTION_MAX_THRESHOLD_ERROR_LEVEL+") mail.sendMail() = "+mailRetCode);
						try{Thread.sleep(1000);}catch(Exception expTmr){}
						deleteFile(threadName,attachment);
					}
				}
				exceptionAlertVectorLocal.clear();
				Thread.sleep(5);
				retCode++;
			}catch(Exception exp){logger.error(exp);}

		}else {retCode=0;}
		return retCode;
	}

	private int processDatabaseAlertReq(final String threadName)
	{
		int retCode						= 0;
		String attachementFileName		= "db_Exceptions";
		logger.debug("["+threadName+"] <-(alert :al) alertVector.size() = "+alertVector.size());
		if(connAlertVector.size() > CONNECTION_MAX_THRESHOLD_ERROR_LEVEL)
		{
			try
			{
				waitFlag_conn	= true;
				Vector<String> connAlertVectorLocal 		= (Vector) connAlertVector.clone(); connAlertVector.clear();
				waitFlag_conn	= false;
				logger.debug("["+threadName+"] <-(alert :al) connAlertVectorLocal.size() = "+connAlertVectorLocal.size()+"connAlertVector.size() = "+connAlertVector.size());
				String error_string = "";
				for(int i=0;i<connAlertVectorLocal.size();i++){error_string+=String.valueOf(connAlertVectorLocal.get(i))+"";}
				try{error_string	= error_string.replace("<br>","\n");}catch(Exception exp){}
				logger.debug("["+threadName+"] <-(alert :al) "+error_string);
				if(connAlertVectorLocal.size()>= CONNECTION_MAX_THRESHOLD_ERROR_LEVEL)
				{
					//String messageSubject			= "SDP DB  Alert: [machine_name = "+machine_name+". instance_id = "+instance_id+"] Connection Error reached the Threshold of "+CONNECTION_MAX_THRESHOLD_ERROR_LEVEL+". Please take relevant action.";
					//String messageBody				= error_string;
					//String attachment				= "";
					//boolean mailRetCode				= mail.sendMail(toList_ExceptionAlerts, ccList_ExceptionAlerts, bccList_ExceptionAlerts, fromList_ExceptionAlerts, messageSubject, messageBody, attachment);
					//logger.debug("["+threadName+"] <-(alert :al) Value from connAlertVector >= CONNECTION_MAX_THRESHOLD_ERROR_LEVEL ("+CONNECTION_MAX_THRESHOLD_ERROR_LEVEL+") mail.sendMail() = "+mailRetCode);
					String messageSubject			= "SDP DB  Alert: [machine_name = "+machine_name+". instance_id = "+instance_id+"] Connection Error reached the Threshold of "+CONNECTION_MAX_THRESHOLD_ERROR_LEVEL+". Please take relevant action.";
					String retFileName				= zipLogs(threadName,error_string,attachementFileName);
					if(retFileName!=null)
					{
						String attachment			= retFileName+".zip";
						String messageBody			= "<p>Dear All,</p><p>Please find attached {"+attachment+"} the <b><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";color:black'>Database exceptions</span></b> coming during the database operations. Please take a relevant action against the same.</p><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";color:black'><b>Regards,</b></span><br>Team Idea SDP";
						logger.info("["+threadName+"] <-(alert :al) attachment = "+attachment+" messageBody= "+messageBody);
						boolean mailRetCode			= mail.sendMail(toList_ExceptionAlerts, ccList_ExceptionAlerts, bccList_ExceptionAlerts, fromList_ExceptionAlerts, messageSubject, messageBody, attachment);
						logger.debug("["+threadName+"] <-(alert :al) Value from sdpErrorCodeAlertVector >= EXCEPTION_MAX_THRESHOLD_ERROR_LEVEL ("+EXCEPTION_MAX_THRESHOLD_ERROR_LEVEL+") mail.sendMail() = "+mailRetCode);
						try{Thread.sleep(1000);}catch(Exception expTmr){}
						deleteFile(threadName,attachment);
					}
				}
				connAlertVectorLocal.clear();
				Thread.sleep(5);
				retCode++;
			}catch(Exception exp){logger.error(exp);}

		}else {retCode=0;}
		return retCode;
	}

	private int processSdpExceptionAlertReq(final String threadName)
	{
		int retCode						= 0;
		String attachementFileName		= "sdp_Exceptions";

		logger.debug("["+threadName+"] <-(alert :al) alertVector.size() = "+alertVector.size());
		if(sdpErrorCodeAlertVector.size() > EXCEPTION_MAX_THRESHOLD_SDP_ERROR_LEVEL)
		{
			try
			{
				waitFlag_conn	= true;
				Vector<String> sdpErrorCodeAlertVectorLocal 		= (Vector) sdpErrorCodeAlertVector.clone(); sdpErrorCodeAlertVector.clear();
				waitFlag_conn	= false;
				logger.debug("["+threadName+"] <-(alert :al) sdpErrorCodeAlertVectorLocal.size() = "+sdpErrorCodeAlertVectorLocal.size()+" sdpErrorCodeAlertVector.size() = "+sdpErrorCodeAlertVector.size());
				String error_string = "";
				for(int i=0;i<sdpErrorCodeAlertVectorLocal.size();i++){error_string+=String.valueOf(sdpErrorCodeAlertVectorLocal.get(i))+"\n";}
				try{error_string	= error_string.replace("<br>","\n");}catch(Exception exp){}
				logger.debug("["+threadName+"] <-(alert :al) "+error_string);
				if(sdpErrorCodeAlertVectorLocal.size()>= EXCEPTION_MAX_THRESHOLD_SDP_ERROR_LEVEL)
				{
					String messageSubject			= "SDP Submit  Alert: [machine_name = "+machine_name+". instance_id = "+instance_id+"] SDP Submit Error reached the Threshold of "+EXCEPTION_MAX_THRESHOLD_SDP_ERROR_LEVEL+". Please take relevant action.";
					String retFileName				= zipLogs(threadName,error_string,attachementFileName);
					if(retFileName!=null)
					{
						String attachment				= retFileName+".zip";
						String messageBody				= "<p>Dear All,</p><p>Please find attached {"+attachment+"} the <b><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";color:black'>SDP exceptions</span></b> coming during the request submission to SDP. Please take a relevant action against the same.</p><span style='font-size:10.0pt;font-family:\"Verdana\",\"sans-serif\";color:black'><b>Regards,</b></span><br>Team Idea SDP";
						logger.info("["+threadName+"] <-(alert :al) attachment = "+attachment+" messageBody= "+messageBody);
						boolean mailRetCode				= mail.sendMail(toList_ExceptionAlerts, ccList_ExceptionAlerts, bccList_ExceptionAlerts, fromList_ExceptionAlerts, messageSubject, messageBody, attachment);
						logger.debug("["+threadName+"] <-(alert :al) Value from sdpErrorCodeAlertVector >= EXCEPTION_MAX_THRESHOLD_SDP_ERROR_LEVEL ("+EXCEPTION_MAX_THRESHOLD_SDP_ERROR_LEVEL+") mail.sendMail() = "+mailRetCode);
						try{Thread.sleep(1000);}catch(Exception expTmr){}
						deleteFile(threadName,attachment);
					}
				}
				sdpErrorCodeAlertVectorLocal.clear();
				Thread.sleep(5);
				retCode++;
			}catch(Exception exp){logger.error(exp);}

		}else {retCode=0;}
		return retCode;
	}

	private int processMailAlertReq(final String threadName)
	{
		int retCode						= 0;

		logger.debug("["+threadName+"] <-(alert :al) alertVector.size() = "+alertVector.size());
		if(alertVector.size() > 0)
		{
			try
			{
				waitFlag	= true;
				Vector<String> alertVectorLocal 		= (Vector) alertVector.clone(); alertVector.clear();
				waitFlag	= false;
				logger.debug("["+threadName+"] <-(alert :al) alertVectorLocal.size() = "+alertVectorLocal.size());
				for(int i=0;i<alertVectorLocal.size();i++)
				{
					String messageSubject			= "SDP App Alert: [machine_name = "+machine_name+". instance_id = "+instance_id+"] ";
					String messageBody				= String.valueOf(alertVectorLocal.get(i));
					String attachment				= "";
					boolean mailRetCode				= mail.sendMail(toList_ExceptionAlerts, ccList_ExceptionAlerts, bccList_ExceptionAlerts, fromList_ExceptionAlerts, messageSubject, messageBody, attachment);

					logger.debug("["+threadName+"] <-(alert :al) Value from alertVector = "+String.valueOf(alertVectorLocal.get(i))+" mail.sendMail() = "+mailRetCode);
				}
				alertVectorLocal.clear();
				Thread.sleep(5);
				retCode++;
			}catch(Exception exp){}

		}else {retCode=0;}
		return retCode;
	}

	private int processCallBackCountAlert(final String threadName)
	{
		int retCode						= 0;

		logger.debug("["+threadName+"] <-(alert :al) callBackAlertVector.size() = "+callBackAlertVector.size());
		if(callBackAlertVector.size() > 0)
		{
			try
			{
				waitFlag_sdpExp	= true;
				Vector<String> callBackAlertVectorLocal 		= (Vector) callBackAlertVector.clone(); callBackAlertVector.clear();
				waitFlag_sdpExp	= false;
				logger.debug("["+threadName+"] <-(alert :al) callBackAlertVectorLocal.size() = "+callBackAlertVectorLocal.size()+"callBackAlertVector.size() = "+callBackAlertVector.size());
				String error_string = "";
				for(int i=0;i<callBackAlertVectorLocal.size();i++){error_string+=String.valueOf(callBackAlertVectorLocal.get(i))+"";}
				logger.debug("["+threadName+"] <-(alert :al) "+error_string);
				if(callBackAlertVectorLocal.size()>=CALLBACK_MAX_THRESHOLD_ERROR_LEVEL)
				{
					String messageSubject			= "SDP Callback Pendency Alert: [machine_name = "+machine_name+". instance_id = "+instance_id+"] Callback pendency reached the Threshold of "+CALLBACK_MAX_THRESHOLD_ERROR_LEVEL+". Please take relevant action.";
					String messageBody				= error_string;
					String attachment				= "";
					boolean mailRetCode				= mail.sendMail(toList_ExceptionAlerts, ccList_ExceptionAlerts, bccList_ExceptionAlerts, fromList_ExceptionAlerts, messageSubject, messageBody, attachment);
					logger.debug("["+threadName+"] <-(alert :al) Value from callBackAlertVector >= CALLBACK_MAX_THRESHOLD_ERROR_LEVEL ("+CALLBACK_MAX_THRESHOLD_ERROR_LEVEL+") mail.sendMail() = "+mailRetCode);
				}
				callBackAlertVectorLocal.clear();
				Thread.sleep(5);
				retCode++;
			}catch(Exception exp){logger.error(exp);}

		}else {retCode=0;}
		return retCode;
	}


	private int checkCallBackQue(final String threadName)
	{
		final String[] circleName					= {"AP","AS","BH","DL","GJ","HP","HR","JK","KK","KL","KR","MH","MP","MU","NE","OR","RJ","PB","TN","UE","UW","WB"};
		int count									= 0;

		for(int i=0;i<circleName.length;i++)
		{
			Connection conn							= null;
			try
			{
				conn								= connMgr.getConnection(conPool_server);
				if(conn!=null)
				{
					Statement stmt					= conn.createStatement();
					String checkCount				= "select count(1) cnt from sdpCentral_"+circleName[i]+".dbo.tbl_callback where status = 0";
					ResultSet rs					= stmt.executeQuery(checkCount);
					if(rs.next())
					{
						count						= rs.getInt("cnt");
						rs.close();
					}
					logger.debug("["+threadName+"] <-(alert :al) Current Count of sdpCentral_"+circleName[i]+".dbo.tbl_callback = "+count+".");
					if(count>CALLBACK_MAX_THRESHOLD_ERROR_LEVEL)
					{
						callBackAlertVector.add(new java.util.Date()+" Unprocessed records in Callback Table for "+circleName[i]+" is above the acceptable limit. Current Count = "+count+" .Please check."+"<br>");
					}
				}
			}catch(Exception exp){}
			finally
			{
				if(conn!=null) connMgr.freeConnection(conPool_server,conn);
			}
		}
		return count;
	}

	public synchronized int setAlertString(String alertString)
	{
		int retCode						= 0;
		do
		{
			if(!waitFlag)	try{alertVector.add(new java.util.Date()+" "+alertString+"<br>"); break; }catch(Exception exp){retCode = -1;}
			else			try{Thread.sleep(20);continue;}catch(Exception exp){} ;
		}while(waitFlag);
		return retCode;
	}

	public synchronized int setDatabaseConnAlert(String connectionDBAlert)
	{
		int retCode						= 0;
		do
		{
			if(!waitFlag_conn)	try{connAlertVector.add(new java.util.Date()+" "+connectionDBAlert+"<br>"); break; }catch(Exception exp){retCode = -1;}
			else				try{Thread.sleep(20);continue;}catch(Exception exp){} ;
		}while(waitFlag_conn);
		return retCode;
	}

	public synchronized int setExceptionAlert(String exceptionAlert)
	{
		int retCode						= 0;
		do
		{
			if(!waitFlag_exp)	try{exceptionAlertVector.add(new java.util.Date()+" "+exceptionAlert+"\n"); break; }catch(Exception exp){retCode = -1;}
			else				try{Thread.sleep(20);continue;}catch(Exception exp){} ;
		}while(waitFlag_exp);
		return retCode;
	}

	public synchronized int setSdpErrorCodeAlert(String sdpErrorCodeAlert)
	{
		int retCode						= 0;
		do
		{
			if(!waitFlag_sdpExp)	try{sdpErrorCodeAlertVector.add(new java.util.Date()+" "+sdpErrorCodeAlert+""); break; }catch(Exception exp){retCode = -1;}
			else				try{Thread.sleep(20);continue;}catch(Exception exp){} ;
		}while(waitFlag_sdpExp);
		return retCode;
	}

	private String zipLogs(final String threadName,final String sourceString,final String fileName)
	{
		String retStr					= null;
		ZipOutputStream out 			= null;

		try
		{
			String dateTime				= generateDateTime();
			retStr						= fileName+"_"+dateTime+"";
			logger.debug("["+threadName+"] <-(alert :al) File Name generated = "+retStr);
			File outFolder				= new File(retStr+".zip");
			out							= new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(outFolder)));
			BufferedInputStream in 		= null;
			byte[] data    				= new byte[1000];

			byte buff[]					= sourceString.getBytes();
			in 							= new BufferedInputStream(new ByteArrayInputStream(buff));
			out.putNextEntry(new ZipEntry(retStr+".txt"));
			int count;
			while((count 				= in.read(data,0,1000)) != -1)
			{
				out.write(data, 0, count);
			}
			out.closeEntry();
			in.close();
			out.flush();
			out.close();
		}catch(Exception exp){/*logException(exp);*/try{out.flush();out.close();}catch(Exception expI){/*logException(expI);*/}}
		return retStr;
	}
	private String generateDateTime()
	{
		java.util.Date date 				= new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT	= new SimpleDateFormat("yyyyMMdd_HHmmss");
		return SQL_DATE_FORMAT.format(date.getTime());
	}

	private boolean deleteFile(final String threadName,final String fullFileName)
	{
		boolean retBool 		= false;
		try
		{
			File tmp = new File(fullFileName);
			logger.debug("["+threadName+"] <-(alert :al) tmp = "+tmp);
			logger.debug("["+threadName+"] <-(alert :al) tmp.delete() = "+tmp.delete());
			retBool				= true;
		}catch(Exception exp){logger.error("["+threadName+"] <-(alert :al) "+exp);}
		return retBool;
	}
}