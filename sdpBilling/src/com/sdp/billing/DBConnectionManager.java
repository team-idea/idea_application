package com.sdp.billing;


import java.io.*;
import java.sql.*;
import java.util.*;
import java.util.Date;
import org.apache.log4j.*;


/**
* This class is a Singleton that provides access to one or many
* connection pools defined in a Property file. A client gets
* access to the single instance through the static getInstance()
* method and can then check-out and check-in connections from a pool.
* When the client shuts down it should call the release() method
* to close all open connections and do other clean up.
*/
public class DBConnectionManager
{
	static private DBConnectionManager instance;    // The single instance
	static private int clients;

	private static Logger logger 				= Logger.getLogger(DBConnectionManager.class.getName());
	private static String CONFIG_FILE			= "log4j.properties";
	private static configureAlerts alertValue	= null;


	private Vector drivers 						= new Vector();
	private PrintWriter log						= null;
	private Hashtable pools 					= new Hashtable();

	/**
	* Returns the single instance, creating one if it's the
	* first time this method is called.
	*
	* @return DBConnectionManager The single instance.
	*/
	static synchronized public DBConnectionManager getInstance()
	{
		if (instance == null){instance = new DBConnectionManager();alertValue=configureAlerts.getInstance();}
		clients++;
		return instance;
	}

	/**
	* A private constructor since this is a Singleton
	*/
	private DBConnectionManager() {init();}

	/**
	* Returns a connection to the named pool.
	*
	* @param name The pool name as defined in the properties file
	* @param con The Connection
	*/
	public void freeConnection(String name, Connection con)
	{
		DBConnectionPool pool = (DBConnectionPool) pools.get(name);
		if (pool != null)
		{
			pool.freeConnection(con);
			//logger.debug("Connection Free: "+con+" For Pool: "+name);
		}
	}

	public void dropConnection(String name, Connection con)
	{
		DBConnectionPool pool = (DBConnectionPool) pools.get(name);
		if (pool != null)
		{
			pool.dropConnection(con);
			//logger.debug("Connection Free: "+con+" For Pool: "+name);
		}
	}


	/**
	* Returns an open connection. If no one is available, and the max
	* number of connections has not been reached, a new connection is
	* created.
	*
	* @param name The pool name as defined in the properties file
	* @return Connection The connection or null
	*/
	public Connection getConnection(String name)
	{
		DBConnectionPool pool = (DBConnectionPool) pools.get(name);
		if (pool != null)
		{
			Connection conn = pool.getConnection();
			if(conn!=null)
			{
				boolean isClosed = false;
				try
				{
					isClosed	= conn.isClosed();
					if(conn.isClosed())
					{
						freeConnection(name,conn);
					}
				}
				catch(Exception exp)
				{
					logger.fatal("["+String.format("%-10s",Thread.currentThread().getName())+"] <-(conn xxx :xx) Cannot provide a connection to the client. returning NULL connection value. ConnPool = "+name+" Conn = "+conn+" conn.isClosed() = "+isClosed);
					if(conn!=null){freeConnection(name,conn);}//logException(exp);
				}
				//logger.debug("["+String.format("%-10s",Thread.currentThread().getName())+"] <-(conn xxx :xx) Connection "+conn+" Used: For Pool: "+name);
				return conn;
			}
		}
		return null;
	}

	/**
	* Returns an open connection. If no one is available, and the max
	* number of connections has not been reached, a new connection is
	* created. If the max number has been reached, waits until one
	* is available or the specified time has elapsed.
	*
	* @param name The pool name as defined in the properties file
	* @param time The number of milliseconds to wait
	* @return Connection The connection or null
	*/
	public Connection getConnection(String name, long time)
	{
		DBConnectionPool pool = (DBConnectionPool) pools.get(name);
		if (pool != null)
		{
			return pool.getConnection(time);
		}
		return null;
	}

	/**
	* Closes all open connections and deregisters all drivers.
	*/
	public synchronized void release()
	{
	// Wait until called by the last client
		if (--clients != 0) {logger.debug("Connections are still active with database...");return;}

		Enumeration allPools = pools.elements();
		while (allPools.hasMoreElements())
		{
			DBConnectionPool pool = (DBConnectionPool) allPools.nextElement();
			pool.release();
		}
		Enumeration allDrivers = drivers.elements();
		while (allDrivers.hasMoreElements())
		{
			Driver driver = (Driver) allDrivers.nextElement();
			try
			{
				DriverManager.deregisterDriver(driver);
				//logger.debug("Deregistered JDBC driver " + driver.getClass().getName());
			}
			catch (SQLException e)
			{
				//logger.error("Can't deregister JDBC driver: " + driver.getClass().getName());
			}
		}
	}

	/**
	* Creates instances of DBConnectionPool based on the properties.
	* A DBConnectionPool can be defined with the following properties:
	* <PRE>
	* &lt;poolname&gt;.url     The JDBC URL for the database
	* &lt;poolname&gt;.user    A database user (optional)
	* &lt;poolname&gt;.password  A database user password (if user specified)
	* &lt;poolname&gt;.maxconn   The maximal number of connections (optional)
	* </PRE>
	*
	* @param props The connection pool properties
	*/
	private void createPools(Properties props)
	{
		Enumeration propNames = props.propertyNames();
		while (propNames.hasMoreElements())
		{
			String name = (String) propNames.nextElement();
			if (name.endsWith(".url"))
			{
				String poolName = name.substring(0, name.lastIndexOf("."));
				String url = props.getProperty(poolName + ".url");
				if (url == null)
				{
					//logger.debug("No URL specified for " + poolName);
					continue;
				}
				String user = props.getProperty(poolName + ".user");
				String password = props.getProperty(poolName + ".password");
				String maxconn = props.getProperty(poolName + ".maxconn", "0");
				int max;
				try
				{
					max = Integer.valueOf(maxconn).intValue();
				}
				catch (NumberFormatException e)
				{
					//logger.error("Invalid maxconn value " + maxconn + " for " + poolName);
					max = 0;
				}
				DBConnectionPool pool =	new DBConnectionPool(poolName, url, user, password, max);pool.start();
				pools.put(poolName, pool);
				//logger.debug("Initialized pool " + poolName);
			}
		}
	}

	/**
	* Loads properties and initializes the instance with its values.
	*/
	private void init()
	{
		try{PropertyConfigurator.configure(CONFIG_FILE);}catch(Exception e ){e.printStackTrace();}

		Properties dbProps = null;
		if ( null == dbProps )
		{
			try
			{
				dbProps = new Properties();
				FileInputStream in = new FileInputStream("db.properties");
				dbProps.load(in);
				in.close();
			}
			catch(Exception exp)
			{
				logException(exp);
			}
		}
		loadDrivers(dbProps);
		createPools(dbProps);
	}

	/**
	* Loads and registers all JDBC drivers. This is done by the
	* DBConnectionManager, as opposed to the DBConnectionPool,
	* since many pools may share the same driver.
	*
	* @param props The connection pool properties
	*/
	private void loadDrivers(Properties props)
	{
		String driverClasses = props.getProperty("drivers");
		StringTokenizer st = new StringTokenizer(driverClasses);
		while (st.hasMoreElements())
		{
			String driverClassName = st.nextToken().trim();
			try
			{
				Driver driver = (Driver)
				Class.forName(driverClassName).newInstance();
				DriverManager.registerDriver(driver);
				drivers.addElement(driver);
				//logger.debug("Registered JDBC driver " + driverClassName);
			}
			catch (Exception exp)
			{
				logException(exp);
			}
		}
	}

	private void logException(Exception exp)
	{
		logger.error(getStackTrace(exp));
		alertValue.setExceptionAlert(getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}

	/**
	* This inner class represents a connection pool. It creates new
	* connections on demand, up to a max number if specified.
	* It also makes sure a connection is still open before it is
	* returned to a client.
	*/
	class DBConnectionPool extends Thread
	{
		private int checkedOut;
		private Vector freeConnections = new Vector();
		private int maxConn;
		private String name;
		private String password;
		private String URL;
		private String user;

		/**
		* Creates new connection pool.
		*
		* @param name The pool name
		* @param URL The JDBC URL for the database
		* @param user The database user, or null
		* @param password The database user password, or null
		* @param maxConn The maximal number of connections, or 0
		*  for no limit
		*/
		public DBConnectionPool(String name, String URL, String user, String password,int maxConn)
		{
			this.name = name;
			this.URL = URL;
			this.user = user;
			this.password = password;
			this.maxConn = maxConn;
		}

		public void run()
		{
			final String poolName = name;
			while(true)
			{
				try
				{
					Thread.sleep(1000*60*5);
					//logger.debug("Releasing the pool "+poolName+" of the connections...");
					release();
				}catch(Exception exp){logException(exp);}
			}
		}

		/**
		* Checks in a connection to the pool. Notify other Threads that
		* may be waiting for a connection.
		*
		* @param con The connection to check in
		*/
		public synchronized void freeConnection(Connection con)
		{
			// Put the connection at the end of the Vector
			freeConnections.addElement(con);
			checkedOut--;
			////logger.debug("Total Shared Connections in the pool(After releasing): "+checkedOut+" Total Pool Size: "+freeConnections.size());
			notifyAll();
		}

		public synchronized void dropConnection(Connection con)
		{
			checkedOut--;
			try{con.close();/*logger.debug("Closed connection for pool " + name);*/}
			catch (NullPointerException null_exp){logger.error(String.format("%-10s",Thread.currentThread().getName())+" Null Pointer Exception for connection " + con);}
			catch (Exception exp){}
			notifyAll();
		}
		/**
		* Checks out a connection from the pool. If no free connection
		* is available, a new connection is created unless the max
		* number of connections has been reached. If a free connection
		* has been closed by the database, it's removed from the pool
		* and this method is called again recursively.
		*/
		public synchronized Connection getConnection()
		{
			Connection con = null;
			if (freeConnections.size() > 0)
			{
				// Pick the first Connection in the Vector
				// to get round-robin usage
				con = (Connection) freeConnections.firstElement();
				freeConnections.removeElementAt(0);
				try
				{
					if (con.isClosed())
					{
						//logger.debug("Removed bad connection from " + name);
						// Try again recursively
						con = getConnection();
					}
				}
				catch (SQLException e)
				{
					//logger.error("Removed bad connection from " + name);
					// Try again recursively
					con = getConnection();
				}
			}
			else if (maxConn == 0 || checkedOut < maxConn)
			{
				con = newConnection();
			}
			if (con != null){checkedOut++;}
			////logger.debug("Total Shared Connections in the pool(After Sharing): "+checkedOut+" Total Pool Size: "+freeConnections.size());
			return con;
		}

		/**
		* Checks out a connection from the pool. If no free connection
		* is available, a new connection is created unless the max
		* number of connections has been reached. If a free connection
		* has been closed by the database, it's removed from the pool
		* and this method is called again recursively.
		* <P>
		* If no connection is available and the max number has been
		* reached, this method waits the specified time for one to be
		* checked in.
		*
		* @param timeout The timeout value in milliseconds
		*/
		public synchronized Connection getConnection(long timeout)
		{
			long startTime = new Date().getTime();
			Connection con;
			while ((con = getConnection()) == null)
			{
				try
				{
					wait(timeout);
				}
				catch (InterruptedException exp) {logException(exp);}
				if ((new Date().getTime() - startTime) >= timeout)
				{
					// Timeout has expired
					return null;
				}
			}
			return con;
		}

		/**
		* Closes all available connections.
		*/
		public synchronized void release()
		{
			Enumeration allConnections = freeConnections.elements();
			while (allConnections.hasMoreElements())
			{
				Connection con = (Connection) allConnections.nextElement();
				try
				{
					con.close();
					//logger.debug("Closed connection for pool " + name);
				}
				catch (SQLException e)
				{
					//logger.error("Can't close connection for pool " + name);
				}
			}
			freeConnections.removeAllElements();
		}

		/**
		* Creates a new connection, using a userid and password
		* if specified.
		*/
		private Connection newConnection()
		{
			Connection con = null;
			try
			{
				if (user == null)
				{
					con = DriverManager.getConnection(URL);
				}
				else
				{
					con = DriverManager.getConnection(URL, user, password);
				}
				//logger.debug("Created a new connection in pool " + name);
			}
			catch (SQLException e)
			{
				//logger.error("Can't create a new connection for " + URL);
				return null;
			}
			return con;
		}
	}
}