package com.sdp.billing;

import java.util.Properties;
import java.io.*;

public final class ConfigProperties
{
	private static final long serialVersionUID		= 7526472295622543147L;
	private static ConfigProperties instance		= null;
	private static Properties prop					= null;

	public static ConfigProperties getInstance(String filename) throws FileNotFoundException, IOException
	{
		if( instance == null )
		{
			if ( null == prop )
			{
				try
				{
					prop = new Properties();
					FileInputStream in = new FileInputStream(filename);
					prop.load( in );
					in.close();
					//prop.list(System.out);
				}
				catch( FileNotFoundException e )
				{
					throw new FileNotFoundException("File - " + filename + " not found in the working directory" );
				}
			}
		}
		return instance;
	}
	static public String getProperty( String key )
	{
		String str = prop.getProperty(key);
		if ( null == str )
		{
			System.out.println("Warning: attempt to get an non-existant value in property file: " + key );
		}
		return str;
	}
}
