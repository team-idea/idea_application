package in.spicedigital.yaari.util;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import in.spicedigital.yaari.controller.YaariMailer;

public class MailHelper {
	
	private static final Logger LOGGER=Logger.getLogger(MailHelper.class.getName());
	Properties prop=new Properties();
	private  String kmpid ="";
	private  String pwd = "";
	private  String to = "";
	private  String cc = "";
	private  String bcc = "";
	private  String from = "";
	private  String messageSubject = "";
	private  String messageBody = "";
	private  String smtpServ="";
	private String attachment="";
	private  String port=""; 


	public MailHelper(String circleCode)
	{
		try {
			PropertyConfigurator.configure("./log4j.properties");
			prop.load(new DataInputStream(new FileInputStream("./mailer.properties")));
		} catch (Exception ex) {
			ex.printStackTrace();
			LOGGER.error(LogStackTrace.getStackTrace(ex));
		}
		LOGGER.debug(circleCode+"------>");
		kmpid = prop.getProperty("mail_kmpid");
		pwd = prop.getProperty("mail_pwd");
		to = prop.getProperty(circleCode+"_mail_to");
		cc = prop.getProperty(circleCode+"_mail_cc");
		bcc = prop.getProperty(circleCode+"_mail_bcc");
		attachment = prop.getProperty(circleCode+"_attachment");
		from = prop.getProperty("mail_from");
		messageSubject = prop.getProperty("mail_messageSubject");
		messageBody = prop.getProperty("messageBody");
		smtpServ=prop.getProperty("smtpServ");
	}
	
	public void sendMailRequest(String date)
	{
		attachment=attachment.replaceAll("YYYYMMDD", date);
		LOGGER.debug("kmpis["+kmpid+"]pwd["+pwd+"]to["+to+"]cc["+cc+"]bcc["+bcc+"]from["+from+"]messageSubject["+messageSubject+"]messageBody["+messageBody+"]zipFileName["+attachment);
		boolean result = sendMail(kmpid, pwd, to, cc, bcc, from, messageSubject, messageBody,attachment);
		if (result == true) {
			System.out.println("mail send..");
		} else {
			System.out.println("mail not send..");
		}
	}
	
    public  boolean sendMail( String kmpid,  String pwd, String to, String cc, String bcc, String from, String messageSubject, String messageBody, String attachment) {
        try {
            Properties props = System.getProperties();
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", smtpServ);
			props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.port","465");
			props.put("mail.smtp.socketFactory.port", "465");
			props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
			// props.put("mail.smtp.auth", "false");
			Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {		
			protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
			return new javax.mail.PasswordAuthentication("sdpse.idea@spicedigital.in", "p@ss@4321");
			}
			});            
			//Store store=session.getStore();
			//Folder folderInbox = store.getFolder("INBOX");
            //folderInbox.open(Folder.READ_ONLY);
            
            
/*            String smtpServ = "webmail.spicedigital.in";
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", smtpServ);
            props.put("mail.smtp.auth", "true");
           // props.put("mail.smtp.auth", "false");
            Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {

                protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                    return new javax.mail.PasswordAuthentication(kmpid, pwd);
                }
          });*/// -- Create a new message --
 //           Session session = Session.getInstance(props);
            Message msg = new MimeMessage(session);
// -- Set the FROM and TO fields --
            msg.setFrom(new InternetAddress(from));
            String toList[] = to.split(",");
            InternetAddress[] addressTo = new InternetAddress[toList.length];
            for (int i = 0; i < toList.length; i++) {
                addressTo[i] = new InternetAddress(toList[i]);
            }
            msg.setRecipients(Message.RecipientType.TO, addressTo);
            String ccList[] = cc.split(",");
            InternetAddress[] addressCC = new InternetAddress[ccList.length];
            for (int i = 0; i < ccList.length; i++) {
                addressCC[i] = new InternetAddress(ccList[i]);
            }
            msg.setRecipients(Message.RecipientType.CC, addressCC);

            msg.setRecipients(Message.RecipientType.TO, addressTo);
            String bccList[] = bcc.split(",");
            InternetAddress[] addressBCC = new InternetAddress[bccList.length];
            for (int i = 0; i < bccList.length; i++) {
                addressBCC[i] = new InternetAddress(bccList[i]);
            }
            msg.setRecipients(Message.RecipientType.BCC, addressBCC);
            msg.setSubject(messageSubject);

            BodyPart messageBodyPart = new MimeBodyPart();

            Multipart multipart = new MimeMultipart();

            /////////////////
            
                messageBodyPart.setText(messageBody);
                multipart.addBodyPart(messageBodyPart);
                messageBodyPart = new MimeBodyPart();
 
                System.out.println("atatchement "+attachment);
                DataSource source = new FileDataSource(attachment);
                messageBodyPart.setDataHandler(new DataHandler(source));
                //messageBodyPart.setFileName(attachment.substring(attachment.substring(attachment.lastIndexOf("/")+1,attachment.length());
                messageBodyPart.setFileName(attachment.substring(attachment.lastIndexOf("/")+1, attachment.length()));
                multipart.addBodyPart(messageBodyPart);


            //multipart.addBodyPart(messageBodyPart);
            msg.setContent(multipart);

// -- Send the message --
            Transport.send(msg);
            ////////////////////

            for (int i = 0; i < toList.length; i++) {
                LOGGER.debug("Message sent to " + toList[i] + " OK.");
            }
            for (int i = 0; i < ccList.length; i++) {
            	LOGGER.debug("Message sent to[CC] " + ccList[i] + " OK.");
            }
            for (int i = 0; i < bccList.length; i++) {
            	LOGGER.debug("Message sent to[BCC] " + bccList[i] + " OK.");
            }

        } catch (Exception ex) {
            LOGGER.error(LogStackTrace.getStackTrace(ex));
//System.out.println("Exception " + ex);

        }
        LOGGER.debug("mail send");
        return true;
    }
}
