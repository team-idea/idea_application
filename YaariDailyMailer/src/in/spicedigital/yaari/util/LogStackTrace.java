package in.spicedigital.yaari.util;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.log4j.Logger;

public class LogStackTrace {
private static Logger logger = Logger.getLogger(LogStackTrace.class);
	
	public static synchronized String getStackTrace(Throwable t){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw, true);
		t.printStackTrace(pw);
		sw.flush();
		pw.flush();
		return sw.toString();
		
	}

}
