package in.spicedigital.yaari.controller;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Formatter;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import in.spicedigital.yaari.util.LogStackTrace;
import in.spicedigital.yaari.util.MailHelper;

public class YaariMailer {
	private static Logger LOGGER = Logger.getLogger(YaariMailer.class.getName());
	public static String todayDate="";
	private static int dateDiff=1;
	private static String misDate=null;
	private static int runTimeArg =1;
	private static String circle = null; 
	private Properties prop= null;
	
	public YaariMailer() {
		PropertyConfigurator.configure("./log4j.properties");
		prop = new Properties();
		try{
			prop.load(new DataInputStream(new FileInputStream("./mailer.properties")));
			circle = prop.getProperty("circle");
		}catch(Exception ex){
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public static void main(String[] args) {
		YaariMailer yaariMailer = new YaariMailer();
		try {
			if (yaariMailer.accessFile() && yaariMailer.getLock()) {
				LOGGER.info("program is started..");
				if (args.length == 1) {
					dateDiff = Integer.parseInt(args[0]);
					misDate=getdate(dateDiff);
				} else {
					misDate=getdate(1);
				}
				LOGGER.debug("Process going to execute for date [" + misDate + "]");
				String[] circles = circle.split("[,]+");
				for(String circleCode : circles){
					MailHelper mailHelper = new MailHelper(circleCode);
					mailHelper.sendMailRequest(misDate);
				}
				LOGGER.info("program is end..");									
				System.exit(0);
			} else {
				System.exit(1);				
			}
		} catch (Exception e) {
			LOGGER.error(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		}
	}
	
	public static String getdate(int dateArg){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -dateArg);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String strDate = formatter.format(cal.getTime());
		Formatter fmt = new Formatter();
		todayDate = strDate;
		return todayDate;
	}
	private boolean getLock() {
		boolean lock = false;
		try {
			RandomAccessFile file = new RandomAccessFile("./mailer.tmp", "rw");
			FileChannel fileChannel = file.getChannel();
			FileLock fileLock = fileChannel.tryLock();
			if (fileLock != null) {
				LOGGER.debug("File is locked. Starting the execution of the Application");
				lock = true;
			} else {
				LOGGER.error("Cannot create lock. Already another instance of the program is running.");
				LOGGER.error("System shutting down. Please wait...");
				try {
					Thread.sleep(1000 * 3);
				} catch (Exception exp) {
					exp.printStackTrace();
				}
				System.exit(0);
			}
		} catch (Exception exp) {
			exp.printStackTrace();
			LOGGER.debug(LogStackTrace.getStackTrace(exp));
		}
		return lock;
	}

	private boolean accessFile() {
		boolean retCode = false;
		try {
			String line = "";
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter("./mailer.tmp", false));
				bw.close();
			} catch (Exception exp) {
				exp.printStackTrace();
			}
			retCode = true;
		} catch (Exception e) {
			System.out.println(e);
			LOGGER.debug(LogStackTrace.getStackTrace(e));
		}
		LOGGER.debug("accessFile() = " + retCode);
		return retCode;
	}

}
