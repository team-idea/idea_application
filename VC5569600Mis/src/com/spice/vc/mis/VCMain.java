package com.spice.vc.mis;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class VCMain {
	private static Logger logger = Logger.getLogger(VCMain.class.getName());
	public static String dateParam = "1";

	VCMain() {
		PropertyConfigurator.configure("./log4j.properties");
	}

	public static void main(String[] args) {
		logger.debug("program is start..");
		int argsLength = args.length;
		try {
			if (argsLength == 0) {
				dateParam = getDate(1);
				logger.debug("no argument ::" + dateParam);
			} else if (argsLength == 1) {
				dateParam = getDate(Integer.parseInt(args[0]));
				logger.debug("one argument ::" + dateParam);
			} else {
				dateParam = getDate(1);
				logger.debug("multiple argument ::" + dateParam);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
		}

		int dateCounter=Integer.parseInt(dateParam.substring(6, 8));
		System.out.println("date--->"+dateCounter);
		String yearMonth=dateParam.substring(0, 6);
		System.out.println("ym---->"+yearMonth);
		VCMain vcMis = new VCMain();
		GenerateExcel generateExcel= new GenerateExcel();
		generateExcel.getExcelProcess(yearMonth,dateCounter);
		MailHelper mailHelper = new MailHelper();
		mailHelper.sendMailRequest("./misData/5569600_VCMIS_"+dateParam+".xlsx");
		logger.debug("Program is END..");
	}
	
	public static String getDate(int datediff) {
		// System.out.println("value of i = "+ i);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -datediff);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String strDate = formatter.format(cal.getTime());
		// System.out.println("Yesterday's date = "+ strDate);
		return strDate;

	}

}
