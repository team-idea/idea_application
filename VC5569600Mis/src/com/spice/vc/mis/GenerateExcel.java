package com.spice.vc.mis;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class GenerateExcel {
	private static Logger logger = Logger.getLogger(GenerateExcel.class.getName());
	private FileOutputStream fos = null;
	private String yearMonth = null;
	private int dateCounter = '1';
	private String mis_Date = null;
	private String sheetNames = null;
	private String circleColumnsNames = null;
	private String circleHeaderNames = null;
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	private String sqlQuery = null;
	private String sqlQueryMTD = null;
	private String workBookPath = "./MIS.xlsx";
	private Date MIS_DATE;

	private String[] circleColumnsName = null;
	private String[] sheetName = null;
	private String[] circleHeaderName = null;
	private String[] SummaryRowName = null;
	private String[] SummaryColName = null;
	private String SummaryRowNames = null;
	private String SummaryColNames = null;
	private XSSFWorkbook workBook = null;
	private XSSFRow circleSheetRow = null;
	private XSSFCell circleSheetCell = null;
	private XSSFSheet sheet = null;
	private Properties prop = null;
	private String tableColumns = null;
	private String[] tableColumn = null;
	private Properties propExcel = null;
	private int circleRowCounter = 0;
	private int mtdColCounter = 2;
	private int circleColCounter = 0;
	private String dbUrl = null;
	private String user = null;
	private String password = null;
	private String driverName = "";
	private XSSFCellStyle styleMerge = null;
	private XSSFCellStyle styleHeader = null;
	private XSSFCellStyle styleColumn = null;
	private String consolidateHeader = null;
	private String consolidateCol = null;
	private String consolidateCircle = null;
	private String[] consolidateCircleName = null;
	private String[] consolidateColName = null;
	private String consolidatetableColumns = null;
	private String[] tableColumnsName = null;
	private String[] Summarytablecol = null;
	private String finalDate = null;
	private String SummarytablecolName = null;
	private String revSheetCol = null;
	private String[] revSheetColName = null;
	private String revSheetTblCol = null;
	private String[] revTblCol = null;

	public GenerateExcel() {
		prop = new Properties();
		propExcel = new Properties();
		PropertyConfigurator.configure("./log4j.properties");

		try {
			workBook = new XSSFWorkbook();
			// this.connectionManager = DBConnectionManager.getInstance();
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			propExcel.load(new DataInputStream(new FileInputStream("./excel.properties")));
			sheetNames = propExcel.getProperty("sheetNames");
			revSheetTblCol = propExcel.getProperty("revSheetTblCol");
			// circleHeaderNames = propExcel.getProperty("circleHeaderNames");
			circleColumnsNames = propExcel.getProperty("circleColumnsNames");
			revSheetCol = propExcel.getProperty("revSheetCol");
			// SummaryRowNames = propExcel.getProperty("SummaryRowNames");
			// //SummaryColNames = propExcel.getProperty("SummaryColNames");
			tableColumns = propExcel.getProperty("tableColumns");
			// consolidateHeader = propExcel.getProperty("consolidateHeader");
			// //
			// consolidateCol = propExcel.getProperty("consolidateCol");
			consolidateCircle = propExcel.getProperty("consolidateCircle");
			// consolidatetableColumns=propExcel.getProperty("consolidatetableColumns");
			// //SummarytablecolName=propExcel.getProperty("SummarytablecolName");
			driverName = prop.getProperty("driverName");
			dbUrl = prop.getProperty("dbUrl");
			user = prop.getProperty("user");
			password = prop.getProperty("password");
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}

	}

	public void setStyle() {
		styleMerge = workBook.createCellStyle();
		// styleMerge.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		styleMerge.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleMerge.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		// styleMerge.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		styleMerge.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		styleMerge.setBorderTop(XSSFCellStyle.BORDER_THIN);
		styleMerge.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		styleMerge.setBorderRight(XSSFCellStyle.BORDER_THIN);
		XSSFFont mergeCellFont = workBook.createFont();
		mergeCellFont.setBoldweight(XSSFFont.BOLDWEIGHT_BOLD);
		styleMerge.setFont(mergeCellFont);

		styleHeader = workBook.createCellStyle();
		styleHeader.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		styleHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleHeader.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		styleHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		styleHeader.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderTop(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderRight(XSSFCellStyle.BORDER_THIN);

		styleColumn = workBook.createCellStyle();
		// styleColumn.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		styleColumn.setAlignment(XSSFCellStyle.ALIGN_CENTER);
		styleColumn.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		// styleColumn.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		styleColumn.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		styleColumn.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		styleColumn.setBorderTop(XSSFCellStyle.BORDER_THIN);
		styleColumn.setBorderRight(XSSFCellStyle.BORDER_THIN);

	}

	public void getExcelProcess(String yearMonth, int dateCounter) {
		sheetName = sheetNames.split("[,]+");
		circleColumnsName = circleColumnsNames.split("[,]+");
		// circleHeaderName = circleHeaderNames.split("[,]+");
		tableColumn = tableColumns.split("[,]+");
		finalDate = yearMonth + dateCounter;
		if (dateCounter < 10) {
			finalDate = yearMonth + "0" + dateCounter;
		} else {
			finalDate = yearMonth + dateCounter;
		}
		setStyle();
		this.yearMonth = yearMonth;
		this.dateCounter = dateCounter;
		try {
			Class.forName(driverName);
			con = DriverManager.getConnection(dbUrl, user, password);
			logger.debug("connect is [" + con + "]");
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
		}

		for (String sheetCode : sheetName) {
			sheet = workBook.createSheet(sheetCode);

			if (sheetCode.equalsIgnoreCase("PAN INDIA REVENUE REVIEW")) {
				getPanIndiaRevSheet(finalDate);
				continue;
			}

			if (sheetCode.equalsIgnoreCase("Circle-wise consolidated")) {
				getCircleWiseConsolidatedSheet(finalDate);
				continue;
			}

			for (circleRowCounter = 0; circleRowCounter < circleColumnsName.length; circleRowCounter++) {
				circleSheetRow = sheet.createRow(circleRowCounter);
			}

			circleRowCounter = 0;
			circleColCounter = 0;
			sheet.setColumnWidth(0, 8000);
			// sheet.setColumnWidth(circleColCounter, 8000);
			for (String circleColumnscode : circleColumnsName) {
				if (circleColumnscode.equalsIgnoreCase("circle")) {
					if (sheetCode.equalsIgnoreCase("all")) {
						circleColumnscode = "ALL CIRCLE";
					} else {
						circleColumnscode = sheetCode.toUpperCase();
					}
				}
				if (sheet.getRow(circleRowCounter) == null) {
					circleSheetRow = sheet.createRow(circleRowCounter);
					circleSheetCell = circleSheetRow.createCell(circleColCounter);
					circleSheetCell.setCellValue(circleColumnscode);
				} else {
					circleSheetRow = sheet.getRow(circleRowCounter);
					circleSheetCell = circleSheetRow.createCell(circleColCounter);
					circleSheetCell.setCellValue(circleColumnscode);
				}
				if (circleRowCounter == 0) {
					circleSheetCell.setCellStyle(styleHeader);
				} else {
					circleSheetCell.setCellStyle(styleColumn);
				}
				circleRowCounter++;
			}

			try {
				circleRowCounter = 0;
				circleColCounter = 2;
				for (int i = 1; i <= dateCounter; i++) {
					String misDate = "";
					if (i < 10) {
						misDate = yearMonth + "0" + i;
					} else {
						misDate = yearMonth + i;
					}
					logger.debug("mis_date is [" + misDate + "]");
					stmt = con.createStatement();

					circleRowCounter = 1;
					int mtdRowCounter = 1;
					sqlQuery = "select * from TBL_VC_5569600_MIS where circle='" + sheetCode
							+ "' and convert(varchar,MIS_DATE,112)='" + misDate + "'";
					rs = stmt.executeQuery(sqlQuery);
					logger.debug("sqlquery has been executed [" + sqlQuery + "]");
					while (rs.next()) {
						for (String colName : tableColumn) {
							if (colName.endsWith("_MTD")) {
								if (i == dateCounter) {
									if (mtdRowCounter == 1) {
										circleSheetRow = sheet.getRow(mtdRowCounter);
										circleSheetCell = circleSheetRow.createCell(1);
										circleSheetCell.setCellStyle(styleHeader);
										circleSheetCell.setCellValue("MTD");
										mtdRowCounter++;
									}
									int value = rs.getInt(colName);
									circleSheetRow = sheet.getRow(mtdRowCounter);
									circleSheetCell = circleSheetRow.createCell(1);
									circleSheetCell.setCellStyle(styleColumn);
									circleSheetCell.setCellValue(value);
									mtdRowCounter++;
								} else {
									continue;
								}
							} else {
								sheet.setColumnWidth(circleColCounter, 3000);
								if (colName.equalsIgnoreCase("MIS_DATE")) {
									MIS_DATE = rs.getDate("MIS_DATE");
									circleSheetRow = sheet.getRow(circleRowCounter);
									circleSheetCell = circleSheetRow.createCell(circleColCounter);
									circleSheetCell.setCellValue(MIS_DATE.toString());
									circleSheetCell.setCellStyle(styleHeader);
								} else {
									int value = rs.getInt(colName);
									circleSheetRow = sheet.getRow(circleRowCounter);
									circleSheetCell = circleSheetRow.createCell(circleColCounter);
									circleSheetCell.setCellStyle(styleColumn);
									circleSheetCell.setCellValue(value);
								}
								circleRowCounter++;
							}
						}
					}
					circleColCounter++;
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				logger.debug(LogStackTrace.getStackTrace(ex));
			} finally {
				try {
					if (stmt != null || !stmt.isClosed()) {
						stmt.close();
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					logger.debug(LogStackTrace.getStackTrace(ex));
				}

				try {
					if (rs != null || !rs.isClosed()) {
						rs.close();
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					logger.debug(LogStackTrace.getStackTrace(ex));
				}
			}
		}

		try {
			if (con != null || !con.isClosed()) {
				con.close();
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
		try {
			saveExcels("./misData/5569600_VCMIS_" + VCMain.dateParam + ".xlsx");
		} catch (IOException e) {
			e.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(e));
		}
	}

	public void getPanIndiaRevSheet(String lastDate) {
		try {
			revSheetColName = revSheetCol.split("[,]+");
			revTblCol = revSheetTblCol.split("[,]+");
			circleColCounter = 0;
			for (int revRowCounter = 0; revRowCounter < revSheetColName.length; revRowCounter++) {
				circleSheetRow = sheet.createRow(revRowCounter);
				circleSheetCell = circleSheetRow.createCell(circleColCounter);
				circleSheetCell.setCellValue(revSheetColName[revRowCounter]);
				circleSheetCell.setCellStyle(styleHeader);
			}

			try {
				try {
					con = DriverManager.getConnection(dbUrl, user, password);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				circleColCounter = 1;
				for (int i = 1; i <= dateCounter; i++) {
					String misDate = "";
					if (i < 10) {
						misDate = yearMonth + "0" + i;
					} else {
						misDate = yearMonth + i;
					}
					logger.debug("mis_date is [" + misDate + "]");
					stmt = con.createStatement();
					sqlQuery = "select top 1 * from TBL_VC56900_CircleWiseRev where convert(varchar,MIS_DATE,112)='"+ misDate + "'";
					rs = stmt.executeQuery(sqlQuery);
					circleRowCounter = 0;
					logger.debug("sqlquery has been executed [" + sqlQuery + "]");
					while (rs.next()) {
						for (String colName : revTblCol) {
							/*
							 * if (colName.endsWith("_MTD")) { if (i ==
							 * dateCounter) { if (mtdRowCounter == 1) {
							 * circleSheetRow = sheet.getRow(mtdRowCounter);
							 * circleSheetCell = circleSheetRow.createCell(1);
							 * circleSheetCell.setCellStyle(styleHeader);
							 * circleSheetCell.setCellValue("MTD");
							 * mtdRowCounter++; } int value =
							 * rs.getInt(colName); circleSheetRow =
							 * sheet.getRow(mtdRowCounter); circleSheetCell =
							 * circleSheetRow.createCell(1);
							 * circleSheetCell.setCellStyle(styleColumn);
							 * circleSheetCell.setCellValue(value);
							 * mtdRowCounter++; } else { continue; } } else {
							 */
							sheet.setColumnWidth(circleColCounter, 3000);
							if (colName.equalsIgnoreCase("MIS_DATE")) {
								MIS_DATE = rs.getDate("MIS_DATE");
								circleSheetRow = sheet.getRow(circleRowCounter);
								circleSheetCell = circleSheetRow.createCell(circleColCounter);
								circleSheetCell.setCellValue(MIS_DATE.toString());
								circleSheetCell.setCellStyle(styleHeader);
							} else {
								int value = rs.getInt(colName);
								circleSheetRow = sheet.getRow(circleRowCounter);
								circleSheetCell = circleSheetRow.createCell(circleColCounter);
								circleSheetCell.setCellStyle(styleColumn);
								circleSheetCell.setCellValue(value);
							}
							circleRowCounter++;
							// }
						}
					}
					circleColCounter++;
				}
				logger.debug("mis_date total is [" + lastDate + "]");
				stmt = con.createStatement();
				sqlQuery = "select top 1 * from TBL_VC56900_CircleWiseRevMTD where convert(varchar,MIS_DATE,112)='"+ lastDate + "'";
				rs = stmt.executeQuery(sqlQuery);
				circleRowCounter = 0;
				logger.debug("sqlquery has been executed [" + sqlQuery + "]");
				while (rs.next()) {
					for (String colName : revTblCol) {
						sheet.setColumnWidth(circleColCounter,2000);
						if (colName.equalsIgnoreCase("MIS_DATE")) {
							MIS_DATE = rs.getDate("MIS_DATE");
							circleSheetRow = sheet.getRow(circleRowCounter);
							circleSheetCell = circleSheetRow.createCell(circleColCounter);
							circleSheetCell.setCellValue("TOTAL");
							circleSheetCell.setCellStyle(styleHeader);
						} else {
							int value = rs.getInt(colName);
							circleSheetRow = sheet.getRow(circleRowCounter);
							circleSheetCell = circleSheetRow.createCell(circleColCounter);
							circleSheetCell.setCellStyle(styleColumn);
							circleSheetCell.setCellValue(value);
						}
						circleRowCounter++;
					}
				}
				circleColCounter++;
			
				
				con.close();
			} catch (Exception ex) {
				ex.printStackTrace();
				logger.debug(LogStackTrace.getStackTrace(ex));
			} finally {
				try {
					if (stmt != null || !stmt.isClosed()) {
						stmt.close();
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					logger.debug(LogStackTrace.getStackTrace(ex));
				}
				try {
					if (con != null || !con.isClosed()) {
						con.close();
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					logger.debug(LogStackTrace.getStackTrace(ex));
				}

				try {
					if (rs != null || !rs.isClosed()) {
						rs.close();
					}
				} catch (Exception ex) {
					ex.printStackTrace();
					logger.debug(LogStackTrace.getStackTrace(ex));
				}
			}

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
			;

		}
	}

	public void getCircleWiseConsolidatedSheet(String lastDate) {
		consolidateCircleName = consolidateCircle.split("[,]+");
		// consolidateColName=consolidateCol.split("[,]+");
		// tableColumnsName=consolidatetableColumns.split("[,]+");
		/*
		 * for (circleRowCounter = 0; circleRowCounter <
		 * consolidateCircleName.length+2; circleRowCounter++) { circleSheetRow
		 * = sheet.createRow(circleRowCounter); }
		 */ for (circleRowCounter = 0; circleRowCounter < circleColumnsName.length; circleRowCounter++) {
			circleSheetRow = sheet.createRow(circleRowCounter);
		}

		// circleSheetCell = circleSheetRow.createCell(circleColCounter);

		circleRowCounter = 0;
		circleColCounter = 0;
		sheet.setColumnWidth(0, 8000);
		// sheet.setColumnWidth(circleColCounter, 8000);
		for (String circleColumnscode : circleColumnsName) {
			if (sheet.getRow(circleRowCounter) == null) {
				circleSheetRow = sheet.createRow(circleRowCounter);
				circleSheetCell = circleSheetRow.createCell(circleColCounter);
				circleSheetCell.setCellValue(circleColumnscode);
			} else {
				circleSheetRow = sheet.getRow(circleRowCounter);
				circleSheetCell = circleSheetRow.createCell(circleColCounter);
				circleSheetCell.setCellValue(circleColumnscode);
			}
			if (circleRowCounter == 0) {
				circleSheetCell.setCellStyle(styleHeader);
			} else {
				circleSheetCell.setCellStyle(styleColumn);
			}
			circleRowCounter++;
		}

		// circleRowCounter++;
		try {
			con = DriverManager.getConnection(dbUrl, user, password);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			circleColCounter = 0;
			for (String circleCode : consolidateCircleName) {
				circleRowCounter = 1;
				circleColCounter++;
				String consolidateQuery = "select top 1 * from TBL_VC_5569600_MIS_MTD where circle='" + circleCode
						+ "' and convert(varchar,mis_date,112)='" + lastDate + "'";
				logger.debug("Query is going to execute[" + consolidateQuery + "]");
				stmt = con.createStatement();
				rs = stmt.executeQuery(consolidateQuery);
				logger.debug("sqlquery hase been executed [" + rs + "]");
				// circleColCounter=0;

				circleSheetRow = sheet.getRow(circleRowCounter);
				circleSheetCell = circleSheetRow.createCell(circleColCounter);
				circleSheetCell.setCellValue(circleCode);
				circleSheetCell.setCellStyle(styleHeader);
				circleRowCounter++;

				while (rs.next()) {
					// for (String tableColName : tableColumnsName) {
					// int value = rs.getInt("TOTAL_CALLS_69600_MTD");
					sheet.setColumnWidth(circleColCounter, 2000);
					circleSheetRow = sheet.getRow(circleRowCounter);
					circleSheetCell = circleSheetRow.createCell(circleColCounter);
					circleSheetCell.setCellValue(rs.getInt("TOTAL_CALLS_69600_MTD"));
					circleSheetCell.setCellStyle(styleColumn);
					circleRowCounter++;
					// value = rs.getInt("");
					circleSheetRow = sheet.getRow(circleRowCounter);
					circleSheetCell = circleSheetRow.createCell(circleColCounter);
					circleSheetCell.setCellValue(rs.getInt("TOTAL_UNIQUE_CALLERS_69600_MTD"));
					circleSheetCell.setCellStyle(styleColumn);
					circleRowCounter++;
					// value = rs.getInt("");
					circleSheetRow = sheet.getRow(circleRowCounter);
					circleSheetCell = circleSheetRow.createCell(circleColCounter);
					circleSheetCell.setCellValue(rs.getInt("TOTAL_MOU_69600_MTD"));
					circleSheetCell.setCellStyle(styleColumn);
					circleRowCounter++;
					// int value = rs.getInt("");
					circleSheetRow = sheet.getRow(circleRowCounter);
					circleSheetCell = circleSheetRow.createCell(circleColCounter);
					circleSheetCell.setCellValue(rs.getInt("OUTDIAL_MOU_69600_MTD"));
					circleSheetCell.setCellStyle(styleColumn);
					circleRowCounter++;
					// int value = rs.getInt("");
					circleSheetRow = sheet.getRow(circleRowCounter);
					circleSheetCell = circleSheetRow.createCell(circleColCounter);
					circleSheetCell.setCellValue(rs.getInt("TOTAL_PULSE_69600_MTD"));
					circleSheetCell.setCellStyle(styleColumn);
					circleRowCounter++;
					// int value = rs.getInt("");
					circleSheetRow = sheet.getRow(circleRowCounter);
					circleSheetCell = circleSheetRow.createCell(circleColCounter);
					circleSheetCell.setCellValue(rs.getInt("BROWSING_REVENUE_69600_MTD"));
					circleSheetCell.setCellStyle(styleColumn);
					circleRowCounter++;
					// }
				}
				rs.close();
				stmt.close();
				circleRowCounter++;
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
		} finally {
			try {
				if (rs != null || !rs.isClosed()) {
					rs.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.debug(LogStackTrace.getStackTrace(e));
			}
			try {
				if (stmt != null || !stmt.isClosed()) {
					stmt.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
				logger.debug(LogStackTrace.getStackTrace(e));
			}
			try {
				if (con != null || !con.isClosed()) {
					con.close();
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		/*
		 * circleRowCounter = 0; circleColCounter = 1;
		 * sheet.setColumnWidth(circleColCounter, 8000); for (String
		 * circleColumnscode : circleColumnsName) { if
		 * (sheet.getRow(circleRowCounter) == null) { circleSheetRow =
		 * sheet.createRow(circleRowCounter); circleSheetCell =
		 * circleSheetRow.createCell(circleColCounter);
		 * circleSheetCell.setCellValue(circleColumnscode); } else {
		 * circleSheetRow = sheet.getRow(circleRowCounter); circleSheetCell =
		 * circleSheetRow.createCell(circleColCounter);
		 * circleSheetCell.setCellValue(circleColumnscode); } if
		 * (circleRowCounter == 0) { circleSheetCell.setCellStyle(styleHeader);
		 * } else { circleSheetCell.setCellStyle(styleColumn); }
		 * circleRowCounter++; }
		 */
	}

	public void saveExcels(String workBookPath) throws IOException {
		this.workBookPath = workBookPath; // for saving file to the
		try {
			fos = new FileOutputStream(workBookPath); // saves xlsx
			workBook.write(fos);
			fos.close();
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			fos.close();
		}
	}
}
