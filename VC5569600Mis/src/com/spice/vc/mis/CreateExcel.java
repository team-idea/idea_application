package com.spice.vc.mis;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class CreateExcel {
	private static Logger logger = Logger.getLogger(CreateExcel.class.getName());
	private Properties prop = null;
	private FileOutputStream fos = null;
	private String excelColumn = null;
	private String[] column = null;
	private XSSFWorkbook workbook = null;
	private XSSFSheet sheet = null;
	private XSSFRow row = null;
	private XSSFCell cell = null;
	private int colIndex = 0;
	private XSSFCellStyle styleHeader = null;
	private XSSFCellStyle styleRow = null;
	private String workBookPath = null;

	public CreateExcel() {
		PropertyConfigurator.configure("./log4j.properties");
		prop = new Properties();
		workbook = new XSSFWorkbook();
		sheet = workbook.createSheet();
		setStyle();

		try {
			prop.load(new DataInputStream(new FileInputStream("./excel.properties")));
			excelColumn = prop.getProperty("excelColumn");
			workBookPath = prop.getProperty("workBookPath");
			logger.info(excelColumn);
			createExcelHeader();
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public void createExcelHeader() {
		colIndex = 0;
		column = excelColumn.split("[,]+");
		row = sheet.createRow(0);
		for (String columnData : column) {
			cell = row.createCell(colIndex);
			cell.setCellValue(columnData);
			cell.setCellStyle(styleHeader);
			colIndex++;
		}
		colIndex = 0;
	}

	public void getRow(int rowIndex, String excelRow) {
		column = excelRow.split("[#]+");
		row = sheet.createRow(rowIndex);
		colIndex = 0;
		for (String columnData : column) {
			cell = row.createCell(colIndex);
			if (colIndex > 4) {
				cell.setCellValue(Integer.parseInt(columnData));
			} else {
				cell.setCellValue(columnData);
			}
			cell.setCellStyle(styleRow);
			colIndex++;
		}
		colIndex = 0;
	}

	public String saveExcels(String misdate) throws IOException {
		try {
			workBookPath = workBookPath.replaceAll("YYMMDD", misdate);
			fos = new FileOutputStream(workBookPath);
			workbook.write(fos);
		} catch (FileNotFoundException e) {
			logger.debug(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		} catch (IOException e) {
			logger.debug(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		} finally {
			fos.close();
		}
		return workBookPath;
	}

	public void setStyle() {
		styleHeader = workbook.createCellStyle();
		styleHeader.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
		styleHeader.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
		styleHeader.setVerticalAlignment(XSSFCellStyle.VERTICAL_BOTTOM);
		styleHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		styleHeader.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderTop(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderRight(XSSFCellStyle.BORDER_THIN);

		styleRow = workbook.createCellStyle();
		styleRow.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
		styleRow.setVerticalAlignment(XSSFCellStyle.VERTICAL_BOTTOM);
		styleRow.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		styleRow.setBorderTop(XSSFCellStyle.BORDER_THIN);
		styleRow.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		styleRow.setBorderRight(XSSFCellStyle.BORDER_THIN);

	}

}
