package com.spice.obdMailer;

import java.io.IOException;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

public class OBDMailer {
	public static void main(String[] args) throws AddressException, MessagingException, IOException {
		try {
			System.out.println("start");
			String host = "smtp.gmail.com";
			String from = "ashish.guleria@spicedigital.com";
			String pass = "spice@1234";
			Properties props = System.getProperties();
			
			String smtpServ = "smtp.gmail.com";
			props.put("mail.transport.protocol", "smtp");
			props.put("mail.smtp.starttls.enable", "true");
			props.put("mail.smtp.host", smtpServ);
			//props.put("mail.smtp.auth", "true");
			props.put("mail.smtp.host", host);
			props.put("mail.smtp.port","465");
			props.put("mail.smtp.socketFactory.port", "465");
			props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
			
			
			
			////props.put("mail.smtp.starttls.enable", "true"); // added this line
			//props.put("mail.smtp.host", host);
			//props.put("mail.smtp.user", from);
			//props.put("mail.smtp.password", pass);
			//props.put("mail.smtp.port", "587");
			//props.put("mail.smtp.auth", "true");
			String[] to = {"ashish.guleria@spicedigital.com"}; // In thisarray
			Session session = Session.getDefaultInstance(props, null);
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			System.out.println("hello");
			InternetAddress[] toAddress = new InternetAddress[to.length];
			// To get the array of addresses
			for (int i = 0; i < to.length; i++) { // changed from a while loop
				toAddress[i] = new InternetAddress(to[i]);
			}
			System.out.println(Message.RecipientType.TO);
			for (int i = 0; i < toAddress.length; i++) {
				// changed from a while loop
				message.addRecipient(Message.RecipientType.TO, toAddress[i]);
			}
			message.setSubject("test mail");
			message.setText("Welcome to OBD");
			// set ur text message
			BodyPart messageBodyPart1 = new MimeBodyPart();
			messageBodyPart1.setText("This is message body");
			String filename[] = { "d:/temp/a.txt", "d:/temp/b.txt" };// change accordingly
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(messageBodyPart1);
			System.out.println(filename[1]);
			for (int i = 0; i < filename.length; i++) {
				System.out.println(filename[i]);
				MimeBodyPart messageBodyPart2 = new MimeBodyPart();
				DataSource source = new FileDataSource(filename[i]);
				messageBodyPart2.setDataHandler(new DataHandler(source));
				messageBodyPart2.setFileName(filename[i]);
				multipart.addBodyPart(messageBodyPart2);
			}
			// both part add into maulti part
			// set message content
			message.setContent(multipart);
			// Trans port the message
			Transport transport = session.getTransport("smtp");
			transport.connect(host, from, pass);
			transport.sendMessage(message, message.getAllRecipients());
			transport.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
}
