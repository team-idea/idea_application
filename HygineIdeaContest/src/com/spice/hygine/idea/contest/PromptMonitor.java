package com.spice.hygine.idea.contest;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.nio.file.Files;
import java.nio.file.attribute.BasicFileAttributes;
import java.text.SimpleDateFormat;
import java.util.Properties;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class PromptMonitor {
	private static Logger logger = Logger.getLogger(PromptMonitor.class.getName());
	private Properties prop = new Properties();
	private String circleName = null;
	private String circleIP = null;
	private String DriveNames = null;
	private String[] DriveName = null;
	private String lang = null;
	private String promptNames = null;
	private String[] promptName = null;
	private String pathName = null;
	private String sleepTime = null;
	private String mobileNumbers = null;
	private SimpleDateFormat sdf = null;
	private String insertResult = null;
	private String postResult = null;
	private String[] langName = null;
	private String promptLocation = null;

	private BasicFileAttributes basicFileAttributes = null;

	public PromptMonitor() {
		try {
			PropertyConfigurator.configure("./log4j.properties");
			// sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
			prop.load(new DataInputStream(new FileInputStream("./config.properties")));
			circleName = prop.getProperty("circleName");
			circleIP = prop.getProperty("circleIP");
			DriveNames = prop.getProperty("DriveNames");
			lang = prop.getProperty("lang");
			promptNames = prop.getProperty("promptNames");
			pathName = prop.getProperty("pathName");
			sleepTime = prop.getProperty("sleepTime");
			mobileNumbers = prop.getProperty("mobileNumbers");
			// System.out.println("pathName" + pathName);
			// System.out.println("DriveName" + DriveName);

		} catch (Exception ex) {
			ex.printStackTrace();
			LogStackTrace.getStackTrace(ex);
		}
	}

	public void promptMonitorProcess() {
		try {
			GenerateAlert alert = new GenerateAlert();
			logger.debug("Sending Http GET request for check..");
			HttpSendRequest httpSendRequest = new HttpSendRequest();
			insertResult = httpSendRequest.sendGet(
					"http://172.27.32.81:8080/HygineIdeaContestAPIs/CheckStatusUrl?circle=" + circleName, circleName,
					circleIP);
			logger.info("http://172.27.32.81:8080/HygineIdeaContestAPIs/CheckStatusUrl?circle=" + circleName);
			Thread.sleep(2000);
			DriveName = DriveNames.split("[,]+");
			for (String nameOfDrive : DriveName) {
				nameOfDrive = nameOfDrive.trim();
				File checkDrive = new File(nameOfDrive + ":");
				if (checkDrive.exists()) {
					logger.debug("[" + nameOfDrive + "]Drive found...");
					langName = lang.split("[,]");
					for (String nameOfLang : langName) {
						nameOfLang = nameOfLang.trim();
						promptName = promptNames.split("[,]");
						for (String nameOfPrompt : promptName) {
							nameOfPrompt = nameOfPrompt.trim();
							File fileIn = new File(
									nameOfDrive + ":/" + pathName + "/" + nameOfPrompt + nameOfLang + ".wav");
							promptLocation = fileIn.toString();
							if (fileIn.exists()) {
								long fileSize = getFileSize(fileIn);
								String fileCreationDate = getFileCreationDate(fileIn);
								String filemodification = getFilemodificationDate(fileIn);
								logger.info(
										"-----" + insertResult + "-------->fileSize[" + fileSize + "]fileCreationDate["
												+ fileCreationDate + "]filemodification[" + filemodification + "]");
								if (insertResult.equals("0")) {
									logger.debug(
											"http://172.27.32.81:8080/HygineIdeaContestAPIs/InsertPromptDatailUrl");
									postResult = httpSendRequest.sendPost(
											"http://172.27.32.81:8080/HygineIdeaContestAPIs/InsertPromptDatailUrl",
											circleName, nameOfPrompt + nameOfLang + ".wav", circleIP, promptLocation,
											fileSize, fileCreationDate, filemodification, "insert");
									Thread.sleep(2000);

								} else {
									postResult = httpSendRequest.sendPost(
											"http://172.27.32.81:8080/HygineIdeaContestAPIs/InsertPromptDatailUrl",
											circleName, nameOfPrompt + nameOfLang + ".wav", circleIP, promptLocation,
											fileSize, fileCreationDate, filemodification, "check");
									String[] postResults = postResult.split("['#']+");
									Thread.sleep(2000);
									if (postResults[1].equals("0") || postResults[2].equals("0")
											|| postResults[3].equals("0")) {
										logger.debug("-----------------------alert-----------------------------");
										logger.debug("name of prompt[" + fileIn.toString() + "]circleName[" + circleName
												+ "]");
										StringBuffer sendMsg = new StringBuffer(
												"Circle Name[" + circleName + "] Prompts Name[" + nameOfPrompt
														+ nameOfLang + ".wav] Drive Name[" + nameOfDrive + "]");
										if (postResults[1].equals("0")) {
											sendMsg.append(" Size not Matched from the center.");
										}
										if (postResults[2].equals("0")) {
											sendMsg.append(" Created Date not Matched from the center.");
										}
										if (postResults[3].equals("0")) {
											sendMsg.append("Modification Date not Matched from the center.");
										}
										sendMsg.append("Take Necessary Action ASAP.");

										System.out.println("sendMsg[" + sendMsg + "]");
										alert.sendAlert(sendMsg.toString(), circleName, circleIP);
										logger.debug("-----------------------alert-----------------------------");
									}
								}
							}
						}
					}
					if (insertResult.equals("0")) {
						postResult = httpSendRequest.sendPost(
								"http://172.27.32.81:8080/HygineIdeaContestAPIs/InsertPromptDatailUrl", circleName,
								"updateStatus", circleIP, promptLocation, 0, "now", "now", "update");
						Thread.sleep(2000);

					}
				} else {
					logger.debug("Drive not found..");
					alert.sendAlert(
							"Drive Name[" + nameOfDrive
									+ "] is not found Please check and take a necessary action ASAP.",
							circleName, circleIP);
				}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	public long getFileSize(File f) {
		long sizeOfFile = 0;
		try {
			basicFileAttributes = Files.readAttributes(f.toPath(), BasicFileAttributes.class);
			sizeOfFile = basicFileAttributes.size();
			// System.out.println("Size of file is : " + sizeOfFile);
			return sizeOfFile;
		} catch (Exception ex) {
			ex.printStackTrace();
			return sizeOfFile;
		}
	}

	public String getFileCreationDate(File f) {
		String dateOfCreation = null;
		try {
			basicFileAttributes = Files.readAttributes(f.toPath(), BasicFileAttributes.class);
			dateOfCreation = basicFileAttributes.creationTime().toString();
			// System.out.println("Creation date: " + dateOfCreation);
			return dateOfCreation;
		} catch (Exception ex) {
			ex.printStackTrace();
			return dateOfCreation;
		}
	}

	public String getFilemodificationDate(File f) {
		String dateOfModification = null;
		try {
			basicFileAttributes = Files.readAttributes(f.toPath(), BasicFileAttributes.class);
			dateOfModification = basicFileAttributes.lastModifiedTime().toString();
			// System.out.println("modification date: " + dateOfModification);
			return dateOfModification;
		} catch (Exception ex) {
			ex.printStackTrace();
			return dateOfModification;
		}
	}

}
