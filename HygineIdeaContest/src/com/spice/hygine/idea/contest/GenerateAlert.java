package com.spice.hygine.idea.contest;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Types;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class GenerateAlert {
	private static Logger logger = Logger.getLogger(GenerateAlert.class.getName());
	private Connection con = null;
	private CallableStatement callstmt = null;
	private ResultSet rs = null;
	private String driverName = "net.sourceforge.jtds.jdbc.Driver";
	private String sqlQuery;
	private String dbUrl = "jdbc:mysql://127.0.0.1:3306/national_contest";
	private String dbUserName = "root";
	private String dbPassword = "root@1234";
	private Properties prop = null;

	public GenerateAlert() {
		try {
			prop = new Properties();
			PropertyConfigurator.configure("./log4j.properties");
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			driverName = prop.getProperty("driverName");
			dbUrl = prop.getProperty("dbUrl");
			dbUserName = prop.getProperty("dbUserName");
			dbPassword = prop.getProperty("dbPassword");
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));			
		}
	}

	public void sendAlert(String message, String circleName, String circleIP) {
		try {
			Class.forName(driverName);
			logger.debug("message is ["+message+"]");
			con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
			logger.debug("connection for calling proc proc_ContestPromptAlert ::-->" + con);
			callstmt = con.prepareCall("{call proc_ContestPromptAlert(?,?,?)}");
			callstmt.setString(1, message);
			callstmt.setString(2, circleName);
			callstmt.setString(3, circleIP);
			callstmt.execute();
			callstmt.close();
			System.out.println("alert");
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}
	public void sendResponseErrorAlert(String message,String urlMethod, String circleName, String circleIP) {
		try {
			Class.forName(driverName);
			String sendMessage="Circle Name["+circleName+"]Circle IP["+circleIP+"] we are receving invalid response code["+message+"] for "+urlMethod+" from 172.27.32.81 server Please check ASAP. ";
			logger.debug("message is ["+message+"] and urlMethod["+urlMethod+"]circleName["+circleName+"]circleIP["+circleIP+"]");
			con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
			logger.debug("connection for calling proc proc_ContestPromptAlert ::-->" + con);
			callstmt = con.prepareCall("{call proc_ContestResponseAlert(?,?,?)}");
			callstmt.setString(1, sendMessage);
			callstmt.setString(2, circleName);
			callstmt.setString(3, circleIP);
			callstmt.execute();
			callstmt.close();
		System.out.println("alert");} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}

}
