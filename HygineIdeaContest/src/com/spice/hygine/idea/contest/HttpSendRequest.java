package com.spice.hygine.idea.contest;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.log4j.Logger;

public class HttpSendRequest {

	private static final String USER_AGENT = "Mozilla/5.0";
	private static Logger logger = Logger.getLogger(HttpSendRequest.class.getClass());
	private int responseCode=200;
	GenerateAlert alert = new GenerateAlert();
	
	public String sendGet(String getUrl,String circleName,String circleIP) throws Exception {
		//logger.info("get url is [" + getUrl + "]");
		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet(getUrl);
		request.addHeader("User-Agent", USER_AGENT);
		HttpResponse response = client.execute(request);
		//System.out.println("\nSending 'GET' request to URL : " + getUrl);
		logger.debug("Response Code : " + response.getStatusLine().getStatusCode());
		responseCode=response.getStatusLine().getStatusCode();
		if(responseCode!=200){
			alert.sendResponseErrorAlert(Integer.toString(responseCode),"UrlResponse for Get Method",circleName,circleIP);
		}
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		logger.debug(result.toString());
		String finalResult = result.toString();
		String[] sendResult = finalResult.split("[#]+");
		logger.debug("return response-->"+sendResult[0]);
		return sendResult[0];
	}

	public String sendPost(String postUrl, String circleName, String nameOfPrompt,String circleIP, String promptLocation, long fileSize,
		String fileCreationDate, String filemodification, String action) throws Exception {
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(postUrl);
		post.setHeader("User-Agent", USER_AGENT);
		List<NameValuePair> urlParameters = new ArrayList<NameValuePair>();
		urlParameters.add(new BasicNameValuePair("circle", circleName));
		urlParameters.add(new BasicNameValuePair("circleIP", circleIP));
		urlParameters.add(new BasicNameValuePair("promptLocation", promptLocation));
		urlParameters.add(new BasicNameValuePair("promptName", nameOfPrompt));
		urlParameters.add(new BasicNameValuePair("fileSize", String.valueOf(fileSize)));
		urlParameters.add(new BasicNameValuePair("fileCreationDate", fileCreationDate));
		urlParameters.add(new BasicNameValuePair("filemodificationDate", filemodification));
		urlParameters.add(new BasicNameValuePair("action", action));
		post.setEntity(new UrlEncodedFormEntity(urlParameters));
		HttpResponse response = client.execute(post);
		//System.out.println("\nSending 'POST' request to URL : " + postUrl);
		//System.out.println("Post parameters : " + post.getEntity());
		logger.debug("Response Code : " + response.getStatusLine().getStatusCode());
		responseCode=response.getStatusLine().getStatusCode();
			if(responseCode!=200){
				alert.sendResponseErrorAlert(Integer.toString(responseCode),"UrlResponse for Post Method",circleName,circleIP);
			}
		BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		logger.debug(result.toString());
		return result.toString();
	}

}
