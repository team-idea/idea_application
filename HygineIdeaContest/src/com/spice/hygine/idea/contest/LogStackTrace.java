package com.spice.hygine.idea.contest;

import java.io.PrintWriter;
import java.io.StringWriter;

public class LogStackTrace {
	
	public static synchronized String getStackTrace(Throwable t){
		StringWriter sw = new StringWriter();
		PrintWriter pw = new PrintWriter(sw,true);
		t.printStackTrace(pw);
		sw.flush();
		pw.flush();
		return sw.toString();
	}

}
