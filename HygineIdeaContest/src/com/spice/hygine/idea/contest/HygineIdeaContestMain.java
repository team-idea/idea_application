package com.spice.hygine.idea.contest;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class HygineIdeaContestMain {

	public static Logger logger = Logger.getLogger(HygineIdeaContestMain.class.getName());
	private Properties prop = null;
	private static int threadSleep = 1000;

	public HygineIdeaContestMain() {
		PropertyConfigurator.configure("./log4j.properties");
		prop = new Properties();
		try{
		prop.load(new DataInputStream(new FileInputStream("./config.properties")));
		threadSleep = Integer.parseInt(prop.getProperty("threadSleep"));
		}catch(Exception ex){
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));		
		}		
	}

	public static void main(String[] args) {
		HygineIdeaContestMain hygineIdeaContestMain = new HygineIdeaContestMain();
		try {
			if (hygineIdeaContestMain.accessFile() && hygineIdeaContestMain.getLock()) {
				logger.info("program is started..");
				PromptMonitor promptMonitor = new PromptMonitor();
				while (true) {
					promptMonitor.promptMonitorProcess();
					logger.debug("I am going to " + threadSleep + "min");
					Thread.sleep(threadSleep * 60 * 1000);
					
				}
			} else {
			}
		} catch (Exception e) {
			logger.error(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		}
	}

	private boolean getLock() {
		boolean lock = false;
		try {
			RandomAccessFile file = new RandomAccessFile("./promptMonitor.tmp", "rw");
			FileChannel fileChannel = file.getChannel();
			FileLock fileLock = fileChannel.tryLock();
			if (fileLock != null) {
				logger.debug("File is locked. Starting the execution of the Application");
				lock = true;
			} else {
				logger.error("Cannot create lock. Already another instance of the program is running.");
				logger.error("System shutting down. Please wait");
				try {
					Thread.sleep(1000 * 3);
				} catch (Exception exp) {
					exp.printStackTrace();
				}
				System.exit(0);
			}
		} catch (Exception exp) {
			exp.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(exp));
		}
		return lock;
	}

	private boolean accessFile() {
		boolean retCode = false;
		try {
			String line = "";
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter("./promptMonitor.tmp", false));
				bw.close();
			} catch (Exception exp) {
				exp.printStackTrace();
			}
			retCode = true;
		} catch (Exception e) {
			System.out.println(e);
			logger.debug(LogStackTrace.getStackTrace(e));
		}
		logger.debug("accessFile() = " + retCode);
		return retCode;
	}

}
