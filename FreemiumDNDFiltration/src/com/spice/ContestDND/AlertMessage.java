package com.spice.ContestDND;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class AlertMessage {
	private static final Logger LOGGER = Logger.getLogger(AlertMessage.class);
	private DBConnectionManager connectionManager = null;
	private Connection connection = null;
	private Statement statement = null;
	private Properties prop = null;
	private String query=null;
	
	public void sendAlert(String circle) throws SQLException{
		try{
		String message="Please check the contest dnd application which is occur the error for "+circle+" circle";	
		connectionManager = DBConnectionManager.getInstance();
		connection=connectionManager.getConnection("DND_POOL_ALERT");
		query = "INSERT INTO national_contest.tbl_sms_mt VALUES (uuid(), 'kk', '919743662027', '52256', '"+message+"', 'text', 1, now(), 0, NULL, NULL, NULL, NULL, ' ');";
		statement = connection.createStatement();
		statement.executeUpdate(query);
		
		query = "INSERT INTO smsmanager.tbl_sms_master_nonidea VALUES (uuid(),'DD','mailXXXXL1','IZ-Alert','Server Hygine Alert (Appl): CircleName : DD ["+message+" DND proces giving error at server IP 10.0.252.157. Take necessary action ]', 'text', 1, now(), 0, '');";
		statement = connection.createStatement();
		statement.executeUpdate(query);
		LOGGER.info("alert has been inserted...");
		}catch(Exception ex){
			ex.printStackTrace();
			LOGGER.info(LogStackTrace.getStackTrace(ex));
		}finally{
			if(null!=connection){
				connectionManager.freeConnection("DND_POOL_ALERT", connection);
			}
			if(null!=statement){
				statement.close();
			}				
		}
	}
	

}
