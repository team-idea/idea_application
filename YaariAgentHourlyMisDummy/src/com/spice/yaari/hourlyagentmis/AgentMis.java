package com.spice.yaari.hourlyagentmis;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class AgentMis {
	private static Logger logger = Logger.getLogger(AgentMis.class.getName());
	private static Properties prop = new Properties();
	private static String to = null;
    private static String cc = null;
    private static String bcc = null;
    private static String from = null;
    private static String smtpServer = "smtp.gmail.com";
    private static String mailSubject = null;
    private static String mailBody	=null;
    private static String dbDriverName=null;
    private static String sqlQuery = null;

	public AgentMis() {
		try{
		to = prop.getProperty("alert.mail.to");
        cc = prop.getProperty("alert.mail.cc");
        bcc = prop.getProperty("alert.mail.bcc");
        from = prop.getProperty("alert.mail.from");
        mailSubject = prop.getProperty("alert.mail.subject");
        mailBody = prop.getProperty("alert.mail.body");
        smtpServer = prop.getProperty("alert.mail.smtpServer");
		}catch(Exception ex){
			logger.debug(LogStackTrace.getStackTrace(ex));
		}				
	}

	public static void main(String[] args) {
	
		AgentMis agentMis = new AgentMis();
		DataBaseOperations dataBaseOperations = new DataBaseOperations();
				
	}

}
