package com.spice.yaari.hourlyagentmis;

import java.awt.List;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.log4j.Logger;

public class DataBaseOperations {
	private static Logger logger = Logger.getLogger(DataBaseOperations.class.getName());
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	private String dbUrl = null;
	private String dbPassword = null;
	private String dbUserName = null;
	private String dbDriverName = null;
	private String sqlQuery = null;
	private Properties prop = null;
	public ArrayList<AgentData> saveAgentData= new ArrayList<AgentData>();
			

	public DataBaseOperations() {
		try {
			prop = new Properties();
			prop.load(new DataInputStream(new FileInputStream("./config.properties")));
			dbUrl = prop.getProperty("dbUrl");
			dbPassword = prop.getProperty("dbPassword");
			dbDriverName = prop.getProperty("dbDriverName");
			sqlQuery = prop.getProperty("sqlQuery");
			dbUserName = prop.getProperty("dbUserName");
		} catch (Exception ex) {
			LogStackTrace.getStackTrace(ex);
		}
	}

	public void getAgentData() throws SQLException {
		try {
			Class.forName(dbDriverName);
			con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
			stmt = con.createStatement();
			rs = stmt.executeQuery(sqlQuery);
			while (rs.next()) {
				AgentData agentData = new AgentData();
				agentData.setCircle(rs.getString("circle"));
				agentData.setA_Party_Disconnect(rs.getInt("a_Party_Disconnect"));
				saveAgentData.add(agentData);
			}
			rs.close();
			stmt.close();
			con.close();
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		} finally {
			if (rs != null || !rs.isClosed()) {
				rs.close();
			}
			if (stmt != null || !stmt.isClosed()) {
				stmt.close();
			}
			if (rs != null || !rs.isClosed()) {
				rs.close();
			}
		}
	}

}
