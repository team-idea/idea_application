package com.spice.yaari.hourlyagentmis;

/**
 * @author CH-E01257
 *
 */

public class AgentData {
	private String circle=null;
	private String agent_location=null;
	private String curr_date = null;
	private int hour =0;
	private String login_logout_time=null;
	private String Last_call_Time=null;
	private String Last_AnsCall_time=null;
	private String chat_id=null;
	private String login_logout=null;
	private String ani=null;
	private String vendor=null;
	private int LoginTimeInMin=0;
	private int LogoutTimeInMin=0;
	private String current_status=null;
	private String Category=null;
	private int Priority=0;
	private int total_calls=0;
	private int success_calls=0;
	private int fail_calls=0;
	private int pulses=0;
	private int mou=0;
	private int A_Party_Disconnect=0;
	private int missed=0;
	private int unknownIssue=0;
	private int userBusy=0;
	private int noAnswer=0;
	private int noUserResponding=0;
	private int unallocatedNumber=0;
	private int resourceUnavailable=0;
	private int temporaryFailure=0;
	private int networkOutOfOrder=0;
	private int callRejected=0;
	private int normalCallClearing=0;
	private int interworking=0;
	private int protocolError=0;
	private int noCircuitAvailable=0;
	private int networkIssue=0;
	private int switchOff=0;
	private int Others_fail_reasons=0;
	private int call_Less_than_1min=0;
	private int call_1_3min=0;

	
	public String getCircle() {
		return circle;
	}
	public void setCircle(String circle) {
		this.circle = circle;
	}
	public String getAgent_location() {
		return agent_location;
	}
	public void setAgent_location(String agent_location) {
		this.agent_location = agent_location;
	}
	public String getCurr_date() {
		return curr_date;
	}
	public void setCurr_date(String curr_date) {
		this.curr_date = curr_date;
	}
	public int getHour() {
		return hour;
	}
	public void setHour(int hour) {
		this.hour = hour;
	}
	public String getLogin_logout_time() {
		return login_logout_time;
	}
	public void setLogin_logout_time(String login_logout_time) {
		this.login_logout_time = login_logout_time;
	}
	public String getLast_call_Time() {
		return Last_call_Time;
	}
	public void setLast_call_Time(String last_call_Time) {
		Last_call_Time = last_call_Time;
	}
	public String getLast_AnsCall_time() {
		return Last_AnsCall_time;
	}
	public void setLast_AnsCall_time(String last_AnsCall_time) {
		Last_AnsCall_time = last_AnsCall_time;
	}
	public String getChat_id() {
		return chat_id;
	}
	public void setChat_id(String chat_id) {
		this.chat_id = chat_id;
	}
	public String getLogin_logout() {
		return login_logout;
	}
	public void setLogin_logout(String login_logout) {
		this.login_logout = login_logout;
	}
	public String getAni() {
		return ani;
	}
	public void setAni(String ani) {
		this.ani = ani;
	}
	public String getVendor() {
		return vendor;
	}
	public void setVendor(String vendor) {
		this.vendor = vendor;
	}
	public int getLoginTimeInMin() {
		return LoginTimeInMin;
	}
	public void setLoginTimeInMin(int loginTimeInMin) {
		LoginTimeInMin = loginTimeInMin;
	}
	public int getLogoutTimeInMin() {
		return LogoutTimeInMin;
	}
	public void setLogoutTimeInMin(int logoutTimeInMin) {
		LogoutTimeInMin = logoutTimeInMin;
	}
	public String getCurrent_status() {
		return current_status;
	}
	public void setCurrent_status(String current_status) {
		this.current_status = current_status;
	}
	public String getCategory() {
		return Category;
	}
	public void setCategory(String category) {
		Category = category;
	}
	public int getPriority() {
		return Priority;
	}
	public void setPriority(int priority) {
		Priority = priority;
	}
	public int getTotal_calls() {
		return total_calls;
	}
	public void setTotal_calls(int total_calls) {
		this.total_calls = total_calls;
	}
	public int getSuccess_calls() {
		return success_calls;
	}
	public void setSuccess_calls(int success_calls) {
		this.success_calls = success_calls;
	}
	public int getFail_calls() {
		return fail_calls;
	}
	public void setFail_calls(int fail_calls) {
		this.fail_calls = fail_calls;
	}
	public int getPulses() {
		return pulses;
	}
	public void setPulses(int pulses) {
		this.pulses = pulses;
	}
	public int getMou() {
		return mou;
	}
	public void setMou(int mou) {
		this.mou = mou;
	}
	public int getA_Party_Disconnect() {
		return A_Party_Disconnect;
	}
	public void setA_Party_Disconnect(int a_Party_Disconnect) {
		A_Party_Disconnect = a_Party_Disconnect;
	}
	public int getMissed() {
		return missed;
	}
	public void setMissed(int missed) {
		this.missed = missed;
	}
	public int getUnknownIssue() {
		return unknownIssue;
	}
	public void setUnknownIssue(int unknownIssue) {
		this.unknownIssue = unknownIssue;
	}
	public int getUserBusy() {
		return userBusy;
	}
	public void setUserBusy(int userBusy) {
		this.userBusy = userBusy;
	}
	public int getNoAnswer() {
		return noAnswer;
	}
	public void setNoAnswer(int noAnswer) {
		this.noAnswer = noAnswer;
	}
	public int getNoUserResponding() {
		return noUserResponding;
	}
	public void setNoUserResponding(int noUserResponding) {
		this.noUserResponding = noUserResponding;
	}
	public int getUnallocatedNumber() {
		return unallocatedNumber;
	}
	public void setUnallocatedNumber(int unallocatedNumber) {
		this.unallocatedNumber = unallocatedNumber;
	}
	public int getResourceUnavailable() {
		return resourceUnavailable;
	}
	public void setResourceUnavailable(int resourceUnavailable) {
		this.resourceUnavailable = resourceUnavailable;
	}
	public int getTemporaryFailure() {
		return temporaryFailure;
	}
	public void setTemporaryFailure(int temporaryFailure) {
		this.temporaryFailure = temporaryFailure;
	}
	public int getNetworkOutOfOrder() {
		return networkOutOfOrder;
	}
	public void setNetworkOutOfOrder(int networkOutOfOrder) {
		this.networkOutOfOrder = networkOutOfOrder;
	}
	public int getCallRejected() {
		return callRejected;
	}
	public void setCallRejected(int callRejected) {
		this.callRejected = callRejected;
	}
	public int getNormalCallClearing() {
		return normalCallClearing;
	}
	public void setNormalCallClearing(int normalCallClearing) {
		this.normalCallClearing = normalCallClearing;
	}
	public int getInterworking() {
		return interworking;
	}
	public void setInterworking(int interworking) {
		this.interworking = interworking;
	}
	public int getProtocolError() {
		return protocolError;
	}
	public void setProtocolError(int protocolError) {
		this.protocolError = protocolError;
	}
	public int getNoCircuitAvailable() {
		return noCircuitAvailable;
	}
	public void setNoCircuitAvailable(int noCircuitAvailable) {
		this.noCircuitAvailable = noCircuitAvailable;
	}
	public int getNetworkIssue() {
		return networkIssue;
	}
	public void setNetworkIssue(int networkIssue) {
		this.networkIssue = networkIssue;
	}
	public int getSwitchOff() {
		return switchOff;
	}
	public void setSwitchOff(int switchOff) {
		this.switchOff = switchOff;
	}
	public int getOthers_fail_reasons() {
		return Others_fail_reasons;
	}
	public void setOthers_fail_reasons(int others_fail_reasons) {
		Others_fail_reasons = others_fail_reasons;
	}
	public int getCall_Less_than_1min() {
		return call_Less_than_1min;
	}
	public void setCall_Less_than_1min(int call_Less_than_1min) {
		this.call_Less_than_1min = call_Less_than_1min;
	}
	public int getCall_1_3min() {
		return call_1_3min;
	}
	public void setCall_1_3min(int call_1_3min) {
		this.call_1_3min = call_1_3min;
	}

	

}
