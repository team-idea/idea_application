package in.spicedigital.mis.bhakti.katha;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.Statement;
//import java.sql.Timestamp;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.IndexedColors;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DBConnection {
	private static Logger logger = Logger.getLogger(DBConnection.class.getName());
	private Properties prop = new Properties();
	private FileOutputStream fos = null;
	private String rowNames = null;
	private int combineSheetCol = 0;
	private String[] rowName = null;
	private String combineColNames = null;
	private String[] combineColName = null;
	private int REV_FTD = 0;
	private int REV_LFTD = 0;
	private int REV_MTD = 0;
	private int REV_LMTD = 0;
	private int REV_Growth_PLM = 0;
	private int SubsSubscribers_FTD = 0;
	private int SubsSubscribers_LFTD = 0;
	private int SubsSubscribers_MTD = 0;
	private int SubsSubscribers_LMTD = 0;
	private int SubsSubscribers_Growth_PLM = 0;
	private int Churn_FTD = 0;
	private int Churn_LFTD = 0;
	private int Churn_MTD = 0;
	private int Churn_LMTD = 0;
	private int Churn_Growth_PLM = 0;
	private int GrossAdd_FTD = 0;
	private int GrossAdd_LFTD = 0;
	private int GrossAdd_MTD = 0;
	private int GrossAdd_LMTD = 0;
	private int GrossAdd_Growth_PLM = 0;
	private String driverName = "";
	private Connection con = null;
	private Connection conn = null;
	private Statement stmt = null;
	private Statement combineStmt = null;
	private ResultSet combineRS = null;
	private ResultSet rs = null;
	private String dbUrl = null;
	private String user = null;
	private String password = null;
	private String sheetName = null;
	private String sqlQuery = null;
	private String sqlQueryMTD = null;
	private Date MIS_DATE;
	private String CIRCLE = "";
	private int Total_Calls = 0;
	private int TOTAL_LIVE_CALLS = 0;
	private int TOTAL_NONLIVE_CALLS = 0;
	private int Total_MOUs = 0;
	private int Total_MOU_LiveCallers = 0;
	private int Total_MOU_NonLiveCallers = 0;
	private int Total_Pulses = 0;
	private int Total_LivePulses = 0;
	private int Total_NonLivePulses = 0;
	private int Total_UniqueUsers = 0;
	private int Total_LiveUniqueUsers = 0;
	private int Total_NoLiveUniqueUsers = 0;
	private int Total_Subscription = 0;
	private int IVR_SuccesfullSubscription = 0;
	private int OBD_SuccesfullSubscription = 0;
	private int SMS_SuccesfullSubscription = 0;
	private int USSD_SuccesfullSubscription = 0;
	private int WAP_SuccesfullSubscription = 0;
	private int SIVR_SuccesfulSubscription = 0;
	private int Subscription_ThroughOtherMode = 0;
	private int Gross_Additions_45 = 0;
	private int Gross_Additions_30 = 0;
	private int Gross_Additions_15 = 0;
	private int Gross_Additions_7 = 0;
	private int Gross_Additions_4 = 0;
	private int Gross_Additions_2 = 0;
	private int Gross_Additions_1 = 0;
	private int Gross_Additions_0 = 0;
	private int Total_PaidGrossAdds = 0;
	private int Total_GrossAdds = 0;
	private int Net_Additions = 0;
	private int Total_Reactivation = 0;
	private int Renewals_45 = 0;
	private int Renewals_30 = 0;
	private int Renewals_15 = 0;
	private int Renewals_7 = 0;
	private int Renewals_4 = 0;
	private int Renewals_2 = 0;
	private int Renewals_1 = 0;
	private int SubscribersPendingRenewal = 0;
	private int TotalActiveBase = 0;
	private int PaidActiveBase = 0;
	private int FreeActiveBase = 0;
	private int SubscriberInGracePeriod = 0;
	private int SubscriberInSuspension = 0;
	private int MonthlySubscriber = 0;
	private int Subscriber_45 = 0;
	private int Subscriber_30 = 0;
	private int Subscriber_15 = 0;
	private int Subscriber_7 = 0;
	private int Subscriber_4 = 0;
	private int Subscriber_2 = 0;
	private int Subscriber_1 = 0;
	private int TotalChurn = 0;
	private int VoluntaryChurn = 0;
	private int VoluntaryPaidChurn = 0;
	private int VoluntaryFreeChurn = 0;
	private int InvoluntaryChurn = 0;
	private int InvoluntaryPaidChurn = 0;
	private int InvoluntaryFreeChurn = 0;
	private int DeactivationThroughIVR = 0;
	private int DeactivationThroughSMS = 0;
	private int DeactivationThroughOtherMode = 0;
	private int SubscriptionRevenue = 0;
	private int ResubscriptionRevenue = 0;
	private int TotalRevenue = 0;
	private int MTD_Total_Calls = 0;
	private int MTD_TOTAL_LIVE_CALLS = 0;
	private int MTD_TOTAL_NONLIVE_CALLS = 0;
	private int MTD_Total_MOUs = 0;
	private int MTD_Total_MOU_LiveCallers = 0;
	private int MTD_Total_MOU_NonLiveCallers = 0;
	private int MTD_Total_Pulses = 0;
	private int MTD_Total_LivePulses = 0;
	private int MTD_Total_NonLivePulses = 0;
	private int MTD_Total_UniqueUsers = 0;
	private int MTD_Total_LiveUniqueUsers = 0;
	private int MTD_Total_NoLiveUniqueUsers = 0;
	private int MTD_Total_Subscription = 0;
	private int MTD_IVR_SuccesfullSubscription = 0;
	private int MTD_OBD_SuccesfullSubscription = 0;
	private int MTD_SMS_SuccesfullSubscription = 0;
	private int MTD_USSD_SuccesfullSubscription = 0;
	private int MTD_WAP_SuccesfullSubscription = 0;
	private int MTD_SIVR_SuccesfulSubscription = 0;
	private int MTD_Subscription_ThroughOtherMode = 0;
	private int MTD_Gross_Additions_45 = 0;
	private int MTD_Gross_Additions_30 = 0;
	private int MTD_Gross_Additions_15 = 0;
	private int MTD_Gross_Additions_7 = 0;
	private int MTD_Gross_Additions_4 = 0;
	private int MTD_Gross_Additions_2 = 0;
	private int MTD_Gross_Additions_1 = 0;
	private int MTD_Gross_Additions_0 = 0;
	private int MTD_Total_PaidGrossAdds = 0;
	private int MTD_Total_GrossAdds = 0;
	private int MTD_Net_Additions = 0;
	private int MTD_Total_Reactivation = 0;
	private int MTD_Renewals_45 = 0;
	private int MTD_Renewals_30 = 0;
	private int MTD_Renewals_15 = 0;
	private int MTD_Renewals_7 = 0;
	private int MTD_Renewals_4 = 0;
	private int MTD_Renewals_2 = 0;
	private int MTD_Renewals_1 = 0;
	private int MTD_SubscribersPendingRenewal = 0;
	private int MTD_TotalActiveBase = 0;
	private int MTD_PaidActiveBase = 0;
	private int MTD_FreeActiveBase = 0;
	private int MTD_SubscriberInGracePeriod = 0;
	private int MTD_SubscriberInSuspension = 0;
	private int MTD_MonthlySubscriber = 0;
	private int MTD_Subscriber_45 = 0;
	private int MTD_Subscriber_30 = 0;
	private int MTD_Subscriber_15 = 0;
	private int MTD_Subscriber_7 = 0;
	private int MTD_Subscriber_4 = 0;
	private int MTD_Subscriber_2 = 0;
	private int MTD_Subscriber_1 = 0;
	private int MTD_TotalChurn = 0;
	private int MTD_VoluntaryChurn = 0;
	private int MTD_VoluntaryPaidChurn = 0;
	private int MTD_VoluntaryFreeChurn = 0;
	private int MTD_InvoluntaryChurn = 0;
	private int MTD_InvoluntaryPaidChurn = 0;
	private int MTD_InvoluntaryFreeChurn = 0;
	private int MTD_DeactivationThroughIVR = 0;
	private int MTD_DeactivationThroughSMS = 0;
	private int MTD_DeactivationThroughOtherMode = 0;
	private int MTD_SubscriptionRevenue = 0;
	private int MTD_ResubscriptionRevenue = 0;
	private int MTD_TotalRevenue = 0;

	public XSSFWorkbook xlsxwb = null;
	public XSSFSheet sheet = null;
	private String xlsxPath = null;
	private String columnNames = null;
	private XSSFRow row;
	private XSSFCell cell;
	private int rowCounter = 0;
	private int colCounter = 0;
	private String CombineMisDate = null;

	DBConnection() {
		PropertyConfigurator.configure("./log4j.properties");
		try {
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			driverName = prop.getProperty("driverName");
			dbUrl = prop.getProperty("dbUrl");
			user = prop.getProperty("user");
			password = prop.getProperty("password");
			sheetName = prop.getProperty("sheetName");
			columnNames = prop.getProperty("columnNames");
			rowNames = prop.getProperty("rowNames");
			combineSheetCol = Integer.parseInt(prop.getProperty("combineSheetCol"));
			combineColNames = prop.getProperty("combineColNames");
			xlsxwb = new XSSFWorkbook();
			/*
			 * System.out.println(dbUrl); System.out.println(user);
			 * System.out.println(password); System.out.println(sheetName);
			 * System.out.println(columnNames);
			 */

			// XSSFSheet sheet = xlsxwb.createSheet(Sheetname);
		} catch (Exception ex) {
				logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public void getDBConnection(String yearMonth, int dateCounter) {
		try {
			CombineMisDate = yearMonth + dateCounter;
			if (dateCounter < 10) {
				CombineMisDate = yearMonth + "0" + dateCounter;
			} else {
				CombineMisDate = yearMonth + dateCounter;
			}
			Class.forName(driverName);
			con = DriverManager.getConnection(dbUrl, user, password);
			logger.debug("connect is [" + con + "]");
			String[] sheetNames = sheetName.split("[,]+");
			String columnName[] = columnNames.split("[,]+");

			XSSFCellStyle styleO = xlsxwb.createCellStyle();
			styleO.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
			styleO.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			styleO.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
			styleO.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
			styleO.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);

			XSSFCellStyle styleMergeO = xlsxwb.createCellStyle();
			styleMergeO.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
			styleMergeO.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			styleMergeO.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
			styleMergeO.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			styleMergeO.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
			styleMergeO.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
			styleO.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);

			XSSFCellStyle styleMergeY = xlsxwb.createCellStyle();
			styleMergeY.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
			styleMergeY.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			styleMergeY.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
			styleMergeY.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			styleMergeY.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
			styleMergeY.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
			styleO.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);

			XSSFCellStyle styleY = xlsxwb.createCellStyle();
			styleY.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
			styleY.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			styleY.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
			styleY.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
			styleY.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);

			for (String sheetCode : sheetNames) {
				sheet = xlsxwb.createSheet(sheetCode);// sheer created
				if (sheetCode.equalsIgnoreCase("combine")) {
					createCombineSheet();
					continue;
				}

				for (int rowCreation = 0; rowCreation < columnName.length - 1; rowCreation++) {
					row = sheet.createRow(rowCreation);
				}

				row = sheet.getRow(0);
				cell = row.createCell(0);
				cell.setCellValue("Bhakti Katha MIS Idea");
				cell.setCellStyle(styleMergeO);
				sheet.setColumnWidth(0, 8000);
				sheet.setColumnWidth(1, 8000);
				// sheet.setDefaultColumnStyle(0, );

				row = sheet.getRow(1);
				cell = row.createCell(0);
				cell.setCellValue("Usage KPI's");
				sheet.addMergedRegion(CellRangeAddress.valueOf("A2:A13"));
				cell.setCellStyle(styleMergeY);

				row = sheet.getRow(13);
				cell = row.createCell(0);
				cell.setCellValue("Subscription detail");
				sheet.addMergedRegion(CellRangeAddress.valueOf("A14:A41"));
				cell.setCellStyle(styleMergeO);

				row = sheet.getRow(41);
				cell = row.createCell(0);
				cell.setCellValue("Base Details");
				sheet.addMergedRegion(CellRangeAddress.valueOf("A42:A54"));
				cell.setCellStyle(styleMergeY);

				row = sheet.getRow(54);
				cell = row.createCell(0);
				cell.setCellValue("Churn Details");
				sheet.addMergedRegion(CellRangeAddress.valueOf("A55:A64"));
				cell.setCellStyle(styleMergeO);

				row = sheet.getRow(64);
				cell = row.createCell(0);
				cell.setCellValue("Revenue Details");
				sheet.addMergedRegion(CellRangeAddress.valueOf("A65:A67"));
				cell.setCellStyle(styleMergeY);

				rowCounter = 0;
				colCounter = 1;
				for (String columncode : columnName) {

					if (sheet.getRow(rowCounter) == null) {
						row = sheet.createRow(rowCounter);
						XSSFCell cell = row.createCell(colCounter);
						cell.setCellValue(columncode);
						if (rowCounter > 0 && rowCounter <= 12 || rowCounter >= 41 && rowCounter <= 53
								|| rowCounter >= 64 && rowCounter <= 66) {
							cell.setCellStyle(styleY);
						} /*
							 * else if(rowCounter=>){ cell.setCellStyle(styleY);
							 * }
							 */else {
							cell.setCellStyle(styleO);
						}
					} else {
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(columncode);
						if (rowCounter > 0 && rowCounter <= 12 || rowCounter >= 41 && rowCounter <= 53
								|| rowCounter >= 64 && rowCounter <= 66) {
							// if(rowCounter >0 && rowCounter<13){
							cell.setCellStyle(styleY);
						} /*
							 * else if(rowCounter>53){
							 * cell.setCellStyle(styleY); }
							 */else {
							cell.setCellStyle(styleO);
						}
					}
					rowCounter++;
				}

				colCounter = 3;
				// int dataCount=25;
				for (int i = 1; i <= dateCounter; i++) {
					String misDate = "";
					if (i < 10) {
						misDate = yearMonth + "0" + i;
					} else {
						misDate = yearMonth + i;
					}
					logger.debug("mis_date is [" + misDate + "]");
					stmt = con.createStatement();
					rowCounter = 0;
					sqlQuery = "select * from TBL_BKATHA_DAILY_MIS where circle='" + sheetCode+ "' and convert(varchar,MIS_DATE,112)='" + misDate + "'";
					if (i == dateCounter) {
						sqlQueryMTD = "select * from TBL_BKATHA_DAILY_MIS_MTD where circle='" + sheetCode+ "' and convert(varchar,MIS_DATE,112)='" + misDate + "'";
						rs = stmt.executeQuery(sqlQueryMTD);
						logger.debug("sqlquery hase been executed ["+sqlQuery+"]");
						while (rs.next()) {
							int MTDMTDcolCounter = 2;
							MIS_DATE = rs.getDate("MIS_DATE");
							// String misDateTime = MIS_DATE.toString();
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue("MTD");
							cell.setCellStyle(styleO);
							// CIRCLE = rs.getString("CIRCLE");
							Total_Calls = rs.getInt("Total_Calls");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Total_Calls);
							cell.setCellStyle(styleY);

							TOTAL_LIVE_CALLS = rs.getInt("TOTAL_LIVE_CALLS");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellStyle(styleY);
							cell.setCellValue(TOTAL_LIVE_CALLS);

							TOTAL_NONLIVE_CALLS = rs.getInt("TOTAL_NONLIVE_CALLS");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellStyle(styleY);
							cell.setCellValue(TOTAL_NONLIVE_CALLS);

							Total_MOUs = rs.getInt("Total_MOUs");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Total_MOUs);
							cell.setCellStyle(styleY);

							Total_MOU_LiveCallers = rs.getInt("Total_MOU_LiveCallers");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Total_MOU_LiveCallers);
							cell.setCellStyle(styleY);

							Total_MOU_NonLiveCallers = rs.getInt("Total_MOU_NonLiveCallers");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Total_MOU_NonLiveCallers);
							cell.setCellStyle(styleY);

							Total_Pulses = rs.getInt("Total_Pulses");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Total_Pulses);
							cell.setCellStyle(styleY);

							Total_LivePulses = rs.getInt("Total_LivePulses");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Total_LivePulses);
							cell.setCellStyle(styleY);

							Total_NonLivePulses = rs.getInt("Total_NonLivePulses");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Total_NonLivePulses);
							cell.setCellStyle(styleY);

							Total_UniqueUsers = rs.getInt("Total_UniqueUsers");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Total_UniqueUsers);
							cell.setCellStyle(styleY);

							Total_LiveUniqueUsers = rs.getInt("Total_LiveUniqueUsers");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Total_LiveUniqueUsers);
							cell.setCellStyle(styleY);

							Total_NoLiveUniqueUsers = rs.getInt("Total_NoLiveUniqueUsers");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Total_NoLiveUniqueUsers);
							cell.setCellStyle(styleY);

							Total_Subscription = rs.getInt("Total_Subscription");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Total_Subscription);
							cell.setCellStyle(styleO);

							IVR_SuccesfullSubscription = rs.getInt("IVR_SuccesfullSubscription");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(IVR_SuccesfullSubscription);
							cell.setCellStyle(styleO);

							OBD_SuccesfullSubscription = rs.getInt("OBD_SuccesfullSubscription");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(OBD_SuccesfullSubscription);
							cell.setCellStyle(styleO);

							SMS_SuccesfullSubscription = rs.getInt("SMS_SuccesfullSubscription");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(SMS_SuccesfullSubscription);
							cell.setCellStyle(styleO);

							USSD_SuccesfullSubscription = rs.getInt("USSD_SuccesfullSubscription");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(USSD_SuccesfullSubscription);
							cell.setCellStyle(styleO);

							WAP_SuccesfullSubscription = rs.getInt("WAP_SuccesfullSubscription");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(WAP_SuccesfullSubscription);
							cell.setCellStyle(styleO);

							SIVR_SuccesfulSubscription = rs.getInt("SIVR_SuccesfulSubscription");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(SIVR_SuccesfulSubscription);
							cell.setCellStyle(styleO);

							Subscription_ThroughOtherMode = rs.getInt("Subscription_ThroughOtherMode");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Subscription_ThroughOtherMode);
							cell.setCellStyle(styleO);

							Gross_Additions_45 = rs.getInt("Gross_Additions_45");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Gross_Additions_45);
							cell.setCellStyle(styleO);

							Gross_Additions_30 = rs.getInt("Gross_Additions_30");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Gross_Additions_30);
							cell.setCellStyle(styleO);

							Gross_Additions_15 = rs.getInt("Gross_Additions_15");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Gross_Additions_15);
							cell.setCellStyle(styleO);

							Gross_Additions_7 = rs.getInt("Gross_Additions_7");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Gross_Additions_7);
							cell.setCellStyle(styleO);

							Gross_Additions_4 = rs.getInt("Gross_Additions_4");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Gross_Additions_4);
							cell.setCellStyle(styleO);

							Gross_Additions_2 = rs.getInt("Gross_Additions_2");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Gross_Additions_2);
							cell.setCellStyle(styleO);

							Gross_Additions_1 = rs.getInt("Gross_Additions_1");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Gross_Additions_1);
							cell.setCellStyle(styleO);

							Gross_Additions_0 = rs.getInt("Gross_Additions_0");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Gross_Additions_0);
							cell.setCellStyle(styleO);

							Total_PaidGrossAdds = rs.getInt("Total_PaidGrossAdds");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Total_PaidGrossAdds);
							cell.setCellStyle(styleO);

							Total_GrossAdds = rs.getInt("Total_GrossAdds");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Total_GrossAdds);
							cell.setCellStyle(styleO);

							Net_Additions = rs.getInt("Net_Additions");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Net_Additions);
							cell.setCellStyle(styleO);

							Total_Reactivation = rs.getInt("Total_Reactivation");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Total_Reactivation);
							cell.setCellStyle(styleO);

							Renewals_45 = rs.getInt("Renewals_45");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Renewals_45);
							cell.setCellStyle(styleO);

							Renewals_30 = rs.getInt("Renewals_30");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Renewals_30);
							cell.setCellStyle(styleO);

							Renewals_15 = rs.getInt("Renewals_15");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Renewals_15);
							cell.setCellStyle(styleO);

							Renewals_7 = rs.getInt("Renewals_7");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Renewals_7);
							cell.setCellStyle(styleO);

							Renewals_4 = rs.getInt("Renewals_4");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Renewals_4);
							cell.setCellStyle(styleO);

							Renewals_2 = rs.getInt("Renewals_2");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Renewals_2);
							cell.setCellStyle(styleO);

							Renewals_1 = rs.getInt("Renewals_1");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Renewals_1);
							cell.setCellStyle(styleO);

							SubscribersPendingRenewal = rs.getInt("SubscribersPendingRenewal");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(SubscribersPendingRenewal);
							cell.setCellStyle(styleO);

							TotalActiveBase = rs.getInt("TotalActiveBase");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(TotalActiveBase);
							cell.setCellStyle(styleY);

							PaidActiveBase = rs.getInt("PaidActiveBase");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(PaidActiveBase);
							cell.setCellStyle(styleY);

							FreeActiveBase = rs.getInt("FreeActiveBase");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(FreeActiveBase);
							cell.setCellStyle(styleY);

							SubscriberInGracePeriod = rs.getInt("SubscriberInGracePeriod");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(SubscriberInGracePeriod);
							cell.setCellStyle(styleY);

							SubscriberInSuspension = rs.getInt("SubscriberInSuspension");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(SubscriberInSuspension);
							cell.setCellStyle(styleY);

							MonthlySubscriber = rs.getInt("MonthlySubscriber");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(MonthlySubscriber);
							cell.setCellStyle(styleY);

							Subscriber_45 = rs.getInt("Subscriber_45");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Subscriber_45);
							cell.setCellStyle(styleY);

							Subscriber_30 = rs.getInt("Subscriber_30");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Subscriber_30);
							cell.setCellStyle(styleY);

							Subscriber_15 = rs.getInt("Subscriber_15");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Subscriber_15);
							cell.setCellStyle(styleY);

							Subscriber_7 = rs.getInt("Subscriber_7");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Subscriber_7);
							cell.setCellStyle(styleY);

							Subscriber_4 = rs.getInt("Subscriber_4");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Subscriber_4);
							cell.setCellStyle(styleY);

							Subscriber_2 = rs.getInt("Subscriber_2");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Subscriber_2);
							cell.setCellStyle(styleY);

							Subscriber_1 = rs.getInt("Subscriber_1");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(Subscriber_1);
							cell.setCellStyle(styleY);

							TotalChurn = rs.getInt("TotalChurn");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(TotalChurn);
							cell.setCellStyle(styleO);

							VoluntaryChurn = rs.getInt("VoluntaryChurn");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(VoluntaryChurn);
							cell.setCellStyle(styleO);

							VoluntaryPaidChurn = rs.getInt("VoluntaryPaidChurn");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(VoluntaryPaidChurn);
							cell.setCellStyle(styleO);

							VoluntaryFreeChurn = rs.getInt("VoluntaryFreeChurn");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(VoluntaryFreeChurn);
							cell.setCellStyle(styleO);

							InvoluntaryChurn = rs.getInt("InvoluntaryChurn");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(InvoluntaryChurn);
							cell.setCellStyle(styleO);

							InvoluntaryPaidChurn = rs.getInt("InvoluntaryPaidChurn");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(InvoluntaryPaidChurn);
							cell.setCellStyle(styleO);

							InvoluntaryFreeChurn = rs.getInt("InvoluntaryFreeChurn");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(InvoluntaryFreeChurn);
							cell.setCellStyle(styleO);

							DeactivationThroughIVR = rs.getInt("DeactivationThroughIVR");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(DeactivationThroughIVR);
							cell.setCellStyle(styleO);

							DeactivationThroughSMS = rs.getInt("DeactivationThroughSMS");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(DeactivationThroughSMS);
							cell.setCellStyle(styleO);

							DeactivationThroughOtherMode = rs.getInt("DeactivationThroughOtherMode");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(DeactivationThroughOtherMode);
							cell.setCellStyle(styleO);

							SubscriptionRevenue = rs.getInt("SubscriptionRevenue");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(SubscriptionRevenue);
							cell.setCellStyle(styleY);

							ResubscriptionRevenue = rs.getInt("ResubscriptionRevenue");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(ResubscriptionRevenue);
							cell.setCellStyle(styleY);

							TotalRevenue = rs.getInt("TotalRevenue");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(MTDMTDcolCounter);
							cell.setCellValue(TotalRevenue);
							cell.setCellStyle(styleY);

						}
					}
					rowCounter = 0;
					rs = stmt.executeQuery(sqlQuery);
					logger.debug("sqlquery hase been executed ["+sqlQuery+"]");
					
					while (rs.next()) {
						// sheet.setColumnWidth(colCounter, 5000);

						MIS_DATE = rs.getDate("MIS_DATE");
						// String misDateTime = MIS_DATE.toString();
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						sheet.setColumnWidth(colCounter, 3000);
						cell.setCellValue(MIS_DATE.toString());
						cell.setCellStyle(styleO);
						// CIRCLE = rs.getString("CIRCLE");

						Total_Calls = rs.getInt("Total_Calls");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Total_Calls);
						cell.setCellStyle(styleY);

						TOTAL_LIVE_CALLS = rs.getInt("TOTAL_LIVE_CALLS");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(TOTAL_LIVE_CALLS);
						cell.setCellStyle(styleY);

						TOTAL_NONLIVE_CALLS = rs.getInt("TOTAL_NONLIVE_CALLS");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(TOTAL_NONLIVE_CALLS);
						cell.setCellStyle(styleY);

						Total_MOUs = rs.getInt("Total_MOUs");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Total_MOUs);
						cell.setCellStyle(styleY);

						Total_MOU_LiveCallers = rs.getInt("Total_MOU_LiveCallers");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Total_MOU_LiveCallers);
						cell.setCellStyle(styleY);

						Total_MOU_NonLiveCallers = rs.getInt("Total_MOU_NonLiveCallers");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Total_MOU_NonLiveCallers);
						cell.setCellStyle(styleY);

						Total_Pulses = rs.getInt("Total_Pulses");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Total_Pulses);
						cell.setCellStyle(styleY);

						Total_LivePulses = rs.getInt("Total_LivePulses");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Total_LivePulses);
						cell.setCellStyle(styleY);

						Total_NonLivePulses = rs.getInt("Total_NonLivePulses");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Total_NonLivePulses);
						cell.setCellStyle(styleY);

						Total_UniqueUsers = rs.getInt("Total_UniqueUsers");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Total_UniqueUsers);
						cell.setCellStyle(styleY);

						Total_LiveUniqueUsers = rs.getInt("Total_LiveUniqueUsers");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Total_LiveUniqueUsers);
						cell.setCellStyle(styleY);

						Total_NoLiveUniqueUsers = rs.getInt("Total_NoLiveUniqueUsers");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Total_NoLiveUniqueUsers);
						cell.setCellStyle(styleY);

						Total_Subscription = rs.getInt("Total_Subscription");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Total_Subscription);
						cell.setCellStyle(styleO);

						IVR_SuccesfullSubscription = rs.getInt("IVR_SuccesfullSubscription");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(IVR_SuccesfullSubscription);
						cell.setCellStyle(styleO);

						OBD_SuccesfullSubscription = rs.getInt("OBD_SuccesfullSubscription");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(OBD_SuccesfullSubscription);
						cell.setCellStyle(styleO);

						SMS_SuccesfullSubscription = rs.getInt("SMS_SuccesfullSubscription");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(SMS_SuccesfullSubscription);
						cell.setCellStyle(styleO);

						USSD_SuccesfullSubscription = rs.getInt("USSD_SuccesfullSubscription");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(USSD_SuccesfullSubscription);
						cell.setCellStyle(styleO);

						WAP_SuccesfullSubscription = rs.getInt("WAP_SuccesfullSubscription");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(WAP_SuccesfullSubscription);
						cell.setCellStyle(styleO);

						SIVR_SuccesfulSubscription = rs.getInt("SIVR_SuccesfulSubscription");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(SIVR_SuccesfulSubscription);
						cell.setCellStyle(styleO);

						Subscription_ThroughOtherMode = rs.getInt("Subscription_ThroughOtherMode");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Subscription_ThroughOtherMode);
						cell.setCellStyle(styleO);

						Gross_Additions_45 = rs.getInt("Gross_Additions_45");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Gross_Additions_45);
						cell.setCellStyle(styleO);

						Gross_Additions_30 = rs.getInt("Gross_Additions_30");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Gross_Additions_30);
						cell.setCellStyle(styleO);

						Gross_Additions_15 = rs.getInt("Gross_Additions_15");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Gross_Additions_15);
						cell.setCellStyle(styleO);

						Gross_Additions_7 = rs.getInt("Gross_Additions_7");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Gross_Additions_7);
						cell.setCellStyle(styleO);

						Gross_Additions_4 = rs.getInt("Gross_Additions_4");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Gross_Additions_4);
						cell.setCellStyle(styleO);

						Gross_Additions_2 = rs.getInt("Gross_Additions_2");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Gross_Additions_2);
						cell.setCellStyle(styleO);

						Gross_Additions_1 = rs.getInt("Gross_Additions_1");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Gross_Additions_1);
						cell.setCellStyle(styleO);

						Gross_Additions_0 = rs.getInt("Gross_Additions_0");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Gross_Additions_0);
						cell.setCellStyle(styleO);

						Total_PaidGrossAdds = rs.getInt("Total_PaidGrossAdds");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Total_PaidGrossAdds);
						cell.setCellStyle(styleO);

						Total_GrossAdds = rs.getInt("Total_GrossAdds");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Total_GrossAdds);
						cell.setCellStyle(styleO);

						Net_Additions = rs.getInt("Net_Additions");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Net_Additions);
						cell.setCellStyle(styleO);

						Total_Reactivation = rs.getInt("Total_Reactivation");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Total_Reactivation);
						cell.setCellStyle(styleO);

						Renewals_45 = rs.getInt("Renewals_45");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Renewals_45);
						cell.setCellStyle(styleO);

						Renewals_30 = rs.getInt("Renewals_30");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Renewals_30);
						cell.setCellStyle(styleO);

						Renewals_15 = rs.getInt("Renewals_15");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Renewals_15);
						cell.setCellStyle(styleO);

						Renewals_7 = rs.getInt("Renewals_7");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Renewals_7);
						cell.setCellStyle(styleO);

						Renewals_4 = rs.getInt("Renewals_4");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Renewals_4);
						cell.setCellStyle(styleO);

						Renewals_2 = rs.getInt("Renewals_2");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Renewals_2);
						cell.setCellStyle(styleO);

						Renewals_1 = rs.getInt("Renewals_1");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Renewals_1);
						cell.setCellStyle(styleO);

						SubscribersPendingRenewal = rs.getInt("SubscribersPendingRenewal");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(SubscribersPendingRenewal);
						cell.setCellStyle(styleO);

						TotalActiveBase = rs.getInt("TotalActiveBase");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(TotalActiveBase);
						cell.setCellStyle(styleY);

						PaidActiveBase = rs.getInt("PaidActiveBase");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(PaidActiveBase);
						cell.setCellStyle(styleY);

						FreeActiveBase = rs.getInt("FreeActiveBase");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(FreeActiveBase);
						cell.setCellStyle(styleY);

						SubscriberInGracePeriod = rs.getInt("SubscriberInGracePeriod");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(SubscriberInGracePeriod);
						cell.setCellStyle(styleY);

						SubscriberInSuspension = rs.getInt("SubscriberInSuspension");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(SubscriberInSuspension);
						cell.setCellStyle(styleY);

						MonthlySubscriber = rs.getInt("MonthlySubscriber");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(MonthlySubscriber);
						cell.setCellStyle(styleY);

						Subscriber_45 = rs.getInt("Subscriber_45");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Subscriber_45);
						cell.setCellStyle(styleY);

						Subscriber_30 = rs.getInt("Subscriber_30");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Subscriber_30);
						cell.setCellStyle(styleY);

						Subscriber_15 = rs.getInt("Subscriber_15");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Subscriber_15);
						cell.setCellStyle(styleY);

						Subscriber_7 = rs.getInt("Subscriber_7");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Subscriber_7);
						cell.setCellStyle(styleY);

						Subscriber_4 = rs.getInt("Subscriber_4");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Subscriber_4);
						cell.setCellStyle(styleY);

						Subscriber_2 = rs.getInt("Subscriber_2");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Subscriber_2);
						cell.setCellStyle(styleY);

						Subscriber_1 = rs.getInt("Subscriber_1");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(Subscriber_1);
						cell.setCellStyle(styleY);

						TotalChurn = rs.getInt("TotalChurn");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(TotalChurn);
						cell.setCellStyle(styleO);

						VoluntaryChurn = rs.getInt("VoluntaryChurn");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(VoluntaryChurn);
						cell.setCellStyle(styleO);

						VoluntaryPaidChurn = rs.getInt("VoluntaryPaidChurn");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(VoluntaryPaidChurn);
						cell.setCellStyle(styleO);

						VoluntaryFreeChurn = rs.getInt("VoluntaryFreeChurn");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(VoluntaryFreeChurn);
						cell.setCellStyle(styleO);

						InvoluntaryChurn = rs.getInt("InvoluntaryChurn");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(InvoluntaryChurn);
						cell.setCellStyle(styleO);

						InvoluntaryPaidChurn = rs.getInt("InvoluntaryPaidChurn");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(InvoluntaryPaidChurn);
						cell.setCellStyle(styleO);

						InvoluntaryFreeChurn = rs.getInt("InvoluntaryFreeChurn");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(InvoluntaryFreeChurn);
						cell.setCellStyle(styleO);

						DeactivationThroughIVR = rs.getInt("DeactivationThroughIVR");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(DeactivationThroughIVR);
						cell.setCellStyle(styleO);

						DeactivationThroughSMS = rs.getInt("DeactivationThroughSMS");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(DeactivationThroughSMS);
						cell.setCellStyle(styleO);

						DeactivationThroughOtherMode = rs.getInt("DeactivationThroughOtherMode");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(DeactivationThroughOtherMode);
						cell.setCellStyle(styleO);

						SubscriptionRevenue = rs.getInt("SubscriptionRevenue");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(SubscriptionRevenue);
						cell.setCellStyle(styleY);

						ResubscriptionRevenue = rs.getInt("ResubscriptionRevenue");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(ResubscriptionRevenue);
						cell.setCellStyle(styleY);

						TotalRevenue = rs.getInt("TotalRevenue");
						rowCounter++;
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(TotalRevenue);
						cell.setCellStyle(styleY);

					}
					colCounter++;
				}

			}
			
			saveExcels("./misData/BHAKTI_KATHA_MIS_"+BhaktikathaMisMain.dateParam+".xlsx");

		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
		} finally {
			try {
				if (con != null || !con.isClosed()) {
					con.close();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				logger.debug(LogStackTrace.getStackTrace(ex));
			}

			try {
				if (stmt != null || !stmt.isClosed()) {
					stmt.close();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				logger.debug(LogStackTrace.getStackTrace(ex));
			}

			try {
				if (rs != null || !rs.isClosed()) {
					rs.close();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
				logger.debug(LogStackTrace.getStackTrace(ex));
			}

		}

	}

	public void saveExcels(String xlsxPath) throws IOException {
		this.xlsxPath = xlsxPath; // for saving file to the
		try {
			fos = new FileOutputStream(xlsxPath); // saves xlsx
			xlsxwb.write(fos);

			// fos.close();
		} catch (FileNotFoundException e) {
			logger.debug(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		} catch (IOException e) {
			logger.debug(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		} finally {
			fos.close();
		}

	}

	public void createCombineSheet() {
		rowName = rowNames.split("[,]+");
		combineColName = combineColNames.split("[,]+");
		XSSFCellStyle styleY = xlsxwb.createCellStyle();
		// styleY.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		// styleY.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		// styleY.setBorderTop(XSSFCellStyle.BORDER_THICK);
		styleY.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
		styleY.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
		styleY.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);

		XSSFCellStyle styleOfColoumn = xlsxwb.createCellStyle();
		styleOfColoumn.setFillForegroundColor(IndexedColors.LIGHT_ORANGE.getIndex());
		styleOfColoumn.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		styleOfColoumn.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
		styleOfColoumn.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
		styleOfColoumn.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
		styleOfColoumn.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);

		XSSFCellStyle styleMergeColoumn = xlsxwb.createCellStyle();
		styleMergeColoumn.setFillForegroundColor(IndexedColors.PINK.getIndex());
		styleMergeColoumn.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		// styleY.setBorderTop(XSSFCellStyle.BORDER_THICK);
		styleMergeColoumn.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
		styleMergeColoumn.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
		styleMergeColoumn.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);

		// for(int rowCreation=0; rowCreation<rowName.length-1; rowCreation++){
		// row = dBConnection.sheet.createRow(rowCreation);
		// }
		int rowCreation = 0;
		for (String rowCode : rowName) {
			row = sheet.createRow(rowCreation);
			//System.out.println("row-->" + rowCode);
			for (int colCounter = 0; colCounter < combineSheetCol; colCounter++) {
				cell = row.createCell(colCounter);
			}
			colCounter = 0;
			if (rowCreation == 0) {
				cell = row.getCell(0);
				cell.setCellValue("Categories");
				cell.setCellStyle(styleMergeColoumn);

				cell = row.getCell(1);
				cell.setCellValue("Revenue");
				sheet.addMergedRegion(new CellRangeAddress(0, 0, 1, 5));
				cell.setCellStyle(styleMergeColoumn);

				cell = row.getCell(6);
				cell.setCellValue("Subscribers");
				sheet.addMergedRegion(new CellRangeAddress(0, 0, 6, 10));
				cell.setCellStyle(styleMergeColoumn);

				cell = row.getCell(11);
				cell.setCellValue("Churn");
				sheet.addMergedRegion(new CellRangeAddress(0, 0, 11, 15));
				cell.setCellStyle(styleMergeColoumn);

				cell = row.getCell(16);
				cell.setCellValue("Gross Additions");
				sheet.addMergedRegion(new CellRangeAddress(0, 0, 16, 20));
				cell.setCellStyle(styleMergeColoumn);

			} else if (rowCreation == 1) {
				for (String combineColCode : combineColName) {
					cell = row.getCell(colCounter);
					cell.setCellValue(combineColCode);
					cell.setCellStyle(styleOfColoumn);
					colCounter++;
				}
			} else {
				String combineSheetQuery = "select * from TBL_BKATHA_DAILY_COMBINE_MIS where circle='" + rowCode
						+ "' and convert(varchar,mis_date,112)='" + CombineMisDate + "'";
				logger.debug("combineQuery is going to execute[" + combineSheetQuery+"]");
				try {
					conn = DriverManager.getConnection(dbUrl, user, password);
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
				try {
					combineStmt = conn.createStatement();
					combineRS = combineStmt.executeQuery(combineSheetQuery);
					logger.debug("sqlquery hase been executed ["+combineRS+"]");
					
					while (combineRS.next()) {
						CIRCLE = combineRS.getString("CIRCLE");
						cell = row.getCell(colCounter);
						cell.setCellValue(CIRCLE);
						cell.setCellStyle(styleMergeColoumn);
						colCounter++;

						REV_FTD = combineRS.getInt("REV_FTD");
						cell = row.getCell(colCounter);
						cell.setCellValue(REV_FTD);
						cell.setCellStyle(styleY);
						colCounter++;

						REV_LFTD = combineRS.getInt("REV_LFTD");
						cell = row.getCell(colCounter);
						cell.setCellValue(REV_LFTD);
						cell.setCellStyle(styleY);
						colCounter++;

						REV_MTD = combineRS.getInt("REV_MTD");
						cell = row.getCell(colCounter);
						cell.setCellValue(REV_MTD);
						cell.setCellStyle(styleY);
						colCounter++;

						REV_LMTD = combineRS.getInt("REV_LMTD");
						cell = row.getCell(colCounter);
						cell.setCellValue(REV_LMTD);
						cell.setCellStyle(styleY);
						colCounter++;

						REV_Growth_PLM = combineRS.getInt("REV_Growth_PLM");
						cell = row.getCell(colCounter);
						cell.setCellValue(REV_Growth_PLM);
						// sheet.setColumnWidth(0, 8000);
						cell.setCellStyle(styleY);
						colCounter++;

						SubsSubscribers_FTD = combineRS.getInt("SubsSubscribers_FTD");
						cell = row.getCell(colCounter);
						cell.setCellValue(SubsSubscribers_FTD);
						cell.setCellStyle(styleY);
						colCounter++;

						SubsSubscribers_LFTD = combineRS.getInt("SubsSubscribers_LFTD");
						cell = row.getCell(colCounter);
						cell.setCellValue(SubsSubscribers_LFTD);
						cell.setCellStyle(styleY);
						colCounter++;

						SubsSubscribers_MTD = combineRS.getInt("SubsSubscribers_MTD");
						cell = row.getCell(colCounter);
						cell.setCellValue(SubsSubscribers_MTD);
						cell.setCellStyle(styleY);
						colCounter++;

						SubsSubscribers_LMTD = combineRS.getInt("SubsSubscribers_LMTD");
						cell = row.getCell(colCounter);
						cell.setCellValue(SubsSubscribers_LMTD);
						cell.setCellStyle(styleY);
						colCounter++;

						SubsSubscribers_Growth_PLM = combineRS.getInt("SubsSubscribers_Growth_PLM");
						cell = row.getCell(colCounter);
						cell.setCellValue(SubsSubscribers_Growth_PLM);
						// sheet.setColumnWidth(0, 8000);
						cell.setCellStyle(styleY);
						colCounter++;

						Churn_FTD = combineRS.getInt("Churn_FTD");
						cell = row.getCell(colCounter);
						cell.setCellValue(Churn_FTD);
						cell.setCellStyle(styleY);
						colCounter++;

						Churn_LFTD = combineRS.getInt("Churn_LFTD");
						cell = row.getCell(colCounter);
						cell.setCellValue(Churn_LFTD);
						cell.setCellStyle(styleY);
						colCounter++;

						Churn_MTD = combineRS.getInt("Churn_MTD");
						cell = row.getCell(colCounter);
						cell.setCellValue(Churn_MTD);
						cell.setCellStyle(styleY);
						colCounter++;

						Churn_LMTD = combineRS.getInt("Churn_LMTD");
						cell = row.getCell(colCounter);
						cell.setCellValue(Churn_LMTD);
						cell.setCellStyle(styleY);
						colCounter++;

						Churn_Growth_PLM = combineRS.getInt("Churn_Growth_PLM");
						cell = row.getCell(colCounter);
						cell.setCellValue(Churn_Growth_PLM);
						// sheet.setColumnWidth(0, 8000);
						cell.setCellStyle(styleY);
						colCounter++;

						Churn_FTD = combineRS.getInt("GrossAdd_FTD");
						cell = row.getCell(colCounter);
						cell.setCellValue(Churn_FTD);
						cell.setCellStyle(styleY);
						colCounter++;

						Churn_LFTD = combineRS.getInt("GrossAdd_LFTD");
						cell = row.getCell(colCounter);
						cell.setCellValue(Churn_LFTD);
						cell.setCellStyle(styleY);
						colCounter++;

						Churn_MTD = combineRS.getInt("GrossAdd_MTD");
						cell = row.getCell(colCounter);
						cell.setCellValue(Churn_MTD);
						cell.setCellStyle(styleY);
						colCounter++;

						Churn_LMTD = combineRS.getInt("GrossAdd_LMTD");
						cell = row.getCell(colCounter);
						cell.setCellValue(Churn_LMTD);
						cell.setCellStyle(styleY);
						colCounter++;

						Churn_Growth_PLM = combineRS.getInt("GrossAdd_Growth_PLM");
						cell = row.getCell(colCounter);
						cell.setCellValue(Churn_Growth_PLM);
						// sheet.setColumnWidth(0, 8000);
						cell.setCellStyle(styleY);
						colCounter++;
					}

					combineRS.close();
					combineStmt.close();
					conn.close();
				} catch (Exception e) {
					e.printStackTrace();
					logger.debug(LogStackTrace.getStackTrace(e));
				} finally {
					try {
						if (combineRS != null || !combineRS.isClosed()) {
							combineRS.close();
						}
					} catch (SQLException e) {
						e.printStackTrace();
						logger.debug(LogStackTrace.getStackTrace(e));
					}
					try {
						if (combineStmt != null || !combineStmt.isClosed()) {
							combineStmt.close();
						}
					} catch (SQLException e) {
						e.printStackTrace();
						logger.debug(LogStackTrace.getStackTrace(e));
					}
					try {
						if (conn != null || !conn.isClosed()) {
							conn.close();
						}
					} catch (SQLException e) {
						e.printStackTrace();
						
					}
				}

			}
			rowCreation++;
		}

	}

}
