package in.spicedigital.mis.bhakti.katha;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class GenerateExcel {
	
	
	private Logger logger= Logger.getLogger(GenerateExcel.class.getName());
	XSSFWorkbook xlsxwb;
	XSSFSheet sheet;
	XSSFRow row;
	//XSSFCell cell;
	private FileOutputStream fos;
	private String xlsxPath="envalidFileName";


	GenerateExcel() {
		PropertyConfigurator.configure("./log4j.properties");
		XSSFWorkbook xlsxwb = new XSSFWorkbook();
		//row.setRowStyle(style1);
        //sheet.autoSizeColumn(0);

		
	}
	GenerateExcel(String Sheetname) {
		//xlsxwb = new XSSFWorkbook();
		//XSSFCellStyle style1= xlsxwb.createCellStyle();
		//style1.setFillForegroundColor(IndexedColors.YELLOW.getIndex());
		XSSFSheet sheet = xlsxwb.createSheet(Sheetname);
		row = sheet.createRow((short)0);
		XSSFCell cellA = row.createCell(0);
		//cellA.setCellStyle(style1);
		cellA.setCellValue("Bhakti Katha MIS Idea");
		XSSFCell cellB = row.createCell(0);
		XSSFCell cellC = row.createCell(0);
		cellC.setCellValue("Total MTD");
		XSSFCell cell = row.createCell(0);
		sheet.addMergedRegion(CellRangeAddress.valueOf("A1:B1"));
		//row.setRowStyle(style1);
        //sheet.autoSizeColumn(0);

		
	}

	
	public void createExcel(){
		System.out.println("heloo");
		try {
			saveExcels("./misData/abc.xlsx");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void saveExcels(String xlsxPath) throws IOException {
		 this.xlsxPath = xlsxPath; // for saving file to the
		try {
			 fos = new FileOutputStream(xlsxPath); // saves xlsx
			xlsxwb.write(fos);
			//fos.close();
		} catch (FileNotFoundException e) {
			logger.debug(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		} catch (IOException e) {
			logger.debug(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		}
		finally {
			fos.close();
		}
			
		
	}
}
