package in.spicedigital.mis.bhakti.katha;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.apache.log4j.*;


public class BhaktikathaMisMain {
	private static Logger logger = Logger.getLogger(BhaktikathaMisMain.class.getName());
	public static String dateParam = "1";
	private Properties prop = new Properties();
	private int misCounter=1;
	
	
	public BhaktikathaMisMain() {
		PropertyConfigurator.configure("./log4j.properties");
		try{
		prop.load(new DataInputStream(new FileInputStream("./config.properties")));
		}catch(Exception ex){
			ex.printStackTrace();
			logger.error(LogStackTrace.getStackTrace(ex));
		}
		
	}

	public static void main(String[] args) {
		logger.debug("program is start..");
		int argsLength = args.length;
		try {
			if (argsLength == 0) {
				dateParam = getDate(1);
				logger.debug("no argument ::" + dateParam);
			} else if (argsLength == 1) {
				dateParam = getDate(Integer.parseInt(args[0]));
				logger.debug("one argument ::" + dateParam);
			} else {
				dateParam = getDate(1);
				logger.debug("multiple argument ::" + dateParam);
			}
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
		}

		int dateCounter=Integer.parseInt(dateParam.substring(6, 8));
		System.out.println("date--->"+dateCounter);
		String yearMonth=dateParam.substring(0, 6);
		System.out.println("ym---->"+yearMonth);
		logger.debug("date["+dateCounter+"] ym["+yearMonth+"]");
		
		BhaktikathaMisMain bhaktikathaMisMain = new BhaktikathaMisMain();
		DBConnection dbConnection= new DBConnection();
		dbConnection.getDBConnection(yearMonth,dateCounter);
		//GenerateExcel excelCreation = new GenerateExcel();
		//excelCreation.createExcel();sendMailRequest
		MailHelper mailHelper = new MailHelper();
		mailHelper.sendMailRequest("./misData/BHAKTI_KATHA_MIS_"+dateParam+".xlsx");
		logger.debug("program is end..");
	}
	
	public static String getDate(int datediff) {
		// System.out.println("value of i = "+ i);
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -datediff);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String strDate = formatter.format(cal.getTime());
		// System.out.println("Yesterday's date = "+ strDate);
		return strDate;

	}

}
