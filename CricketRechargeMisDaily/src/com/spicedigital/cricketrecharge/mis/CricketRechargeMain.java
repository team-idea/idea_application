package com.spicedigital.cricketrecharge.mis;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Formatter;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.spicedigital.cricketrecharge.dbo.DataBaseOperations;
import com.spicedigital.cricketrecharge.helper.LogStackTrace;
import com.spicedigital.cricketrecharge.helper.Mailer;

public class CricketRechargeMain {
	private static Logger logger = Logger.getLogger(CricketRechargeMain.class.getName());
	private Properties prop = null;
	private static int dateDiff=1;
	public static String misDate=null;
	public static String date=null;
	private static String attachmentPath=null;
	
	public CricketRechargeMain() {
		prop = new Properties();
		PropertyConfigurator.configure("./log4j.properties");
		try{
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			dateDiff = Integer.parseInt(prop.getProperty("dateDiff"));						
		}catch(Exception ex){
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
		
	}

	public static void main(String[] args) {
		logger.debug("program is start");
		CricketRechargeMain cricketRechargeMain = new CricketRechargeMain();
		misDate=getDate(dateDiff);
		logger.debug("mis date is ["+misDate+"]");
		if(cricketRechargeMain.accessFile() && cricketRechargeMain.getLock()){
			DataBaseOperations dataBaseOperations = new DataBaseOperations();
			attachmentPath=dataBaseOperations.doDbOperations(dateDiff);
			Mailer mailer = new Mailer();
			mailer.sendMail(attachmentPath);
			
		}else{
			logger.debug("condition flase going to die.");
			System.exit(0);
		}
		logger.debug("program End");		
	}
	
    private boolean getLock() {
    	boolean lock=false;
        try {
            RandomAccessFile file = new RandomAccessFile("./contestRechargeMis.tmp", "rw");
            FileChannel fileChannel = file.getChannel();
            FileLock fileLock = fileChannel.tryLock();
            if (fileLock != null) {
                logger.debug("File is locked. Starting the execution of the Application");
                lock = true;
              } else {
            	  logger.error("Cannot create lock. Already another instance of the program is running.");
            	  logger.error("System shutting down. Please wait");
                try {
                    Thread.sleep(1000 * 3);
                } catch (Exception exp) {
                }
                System.exit(0);
            }
        } catch (Exception exp) {
            exp.printStackTrace();
        }
        return lock;
    }

    private boolean accessFile() {
        boolean retCode = false;
        try {
            String line = "";
            try {
                BufferedWriter bw = new BufferedWriter(new FileWriter("./contestRechargeMis", false));
                bw.close();
            } catch (Exception exp) {
            }
            retCode = true;
        } catch (Exception e) {
            System.out.println(e);
        }
        logger.debug("accessFile() = " + retCode);
        return retCode;
    }		
    
    public static String getDate(int datediff) {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -datediff);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String strDate = formatter.format(cal.getTime());
		//Formatter fmt = new Formatter();
		date = strDate;
		//monthName = fmt.format("%tb", cal).toString();
		//yearOfDate = cal.get(cal.YEAR);
		//logger.debug("date [" + date + "] and month [" + monthName + "] and year [" + yearOfDate + "]");
		return strDate;
	}

}
