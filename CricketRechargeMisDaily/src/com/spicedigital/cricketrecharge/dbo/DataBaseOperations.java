package com.spicedigital.cricketrecharge.dbo;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.security.auth.Subject;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.dom4j.xpath.SubstringTest;

import com.spicedigital.cricketrecharge.helper.CreateExcel;
import com.spicedigital.cricketrecharge.helper.LogStackTrace;
import com.spicedigital.cricketrecharge.mis.CricketRechargeMain;

public class DataBaseOperations {
	private static Logger logger = Logger.getLogger(DataBaseOperations.class.getName());
	private Properties prop = null;
	private String dbUrl = null;
	private String dbUserName = null;
	private String dbPassword = null;
	private String dataBaseName = null;
	private String tableName = null;
	private String excelDataQuery = null;
	private String rsColumn = null;
	private int misDateCounter = 1;
	private String attachmentPath = null;
	private String procCall = null;
	private boolean proc_status = true;

	public DataBaseOperations() {
		PropertyConfigurator.configure("./log4j.properties");
		try {
			prop = new Properties();
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			dbUrl = prop.getProperty("dbUrl").trim();
			dbUserName = prop.getProperty("dbUserName").trim();
			dbPassword = prop.getProperty("dbPassword").trim();
			// dataBaseName = prop.getProperty("dataBaseName").trim();
			// tableName = prop.getProperty("tableName").trim();
			excelDataQuery = prop.getProperty("excelDataQuery").trim();
			excelDataQuery = prop.getProperty("excelDataQuery").trim();
			procCall = prop.getProperty("callProcedure").trim();
			if (procCall.equalsIgnoreCase("true")) {
				proc_status = true;
			} else {
				proc_status = false;
			}
			// rsColumn = prop.getProperty("rsColumn");
			Class.forName(prop.getProperty("driverName").trim());
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public String doDbOperations(int misDateCounter) {
		this.misDateCounter = misDateCounter;
		try {
			if(proc_status){
				callproc();	
			}			
			String attachmentPath = fetchData();
		} catch (Exception e) {
			logger.debug(LogStackTrace.getStackTrace(e));
		}
		return attachmentPath;
	}

	public void callproc() throws SQLException {
		Connection con = null;
		CallableStatement callableStmt = null;
		String procQuery = "{call MIS_CRICKET_RECHARGE_NEW(?)}";
		try {
			con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
			callableStmt = con.prepareCall(procQuery);
			callableStmt.setInt(1, misDateCounter);
			callableStmt.executeUpdate();
			callableStmt.close();
			con.close();
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		} finally {
			/* 
			 * if (!callableStmt.isClosed() || callableStmt != null) {
			 * callableStmt.close(); }
			 */
			if (!con.isClosed() || con != null) {
				con.close();
			}
		}
	}

	public String fetchData() throws SQLException {
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		CreateExcel createExcel = new CreateExcel();
		try {
			con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
			stmt = con.createStatement();
			excelDataQuery = excelDataQuery.replaceAll("YYYYMMDD", CricketRechargeMain.date);
			excelDataQuery = excelDataQuery.replaceAll("YYYYMM", CricketRechargeMain.date.substring(0, 6));
			System.out.println(excelDataQuery);
			rs = stmt.executeQuery(excelDataQuery);
			// String[] rsColumns = rsColumn.split("[,]+");
			int rowIndex = 1;
			while (rs.next()) {
				String mis_date = rs.getString("mis_date");
				String circle = rs.getString("circle_name");				
				String total_msgsent = rs.getString("total_msgsent");
				String total_msgsentsameday = rs.getString("total_msgsentsameday");
				String total_msgsentdday = rs.getString("total_msgsentdday");
				String total_msgnotsent = rs.getString("total_msgnotsent");
				String total_hits = rs.getString("total_hits");
				String total_uniqueHits = rs.getString("total_uniqueHits");
				String total_succhits_s1 = rs.getString("total_succhits_s1");
				String total_succhits_s0 = rs.getString("total_succhits_s0");
				String total_succhits_sm1 = rs.getString("total_succhits_sm1");
				String total_succhits_sm2 = rs.getString("total_succhits_sm2");

				String ExcelData = mis_date + "#"+circle+"#" + total_msgsent + "#" + total_msgsentsameday + "#" + total_msgsentdday
						+ "#" + total_msgnotsent + "#" + total_hits + "#" + total_uniqueHits + "#" + total_succhits_s1
						+ "#" + total_succhits_s0 + "#" + total_succhits_sm1 + "#" + total_succhits_sm2;
				logger.debug(ExcelData);
				createExcel.setRow(rowIndex, ExcelData);
				rowIndex++;
			}
			attachmentPath = createExcel.saveExcels();
			con.close();
			stmt.close();
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		} finally {
			// if(!stmt.isClosed() || stmt !=null ){
			// stmt.close();
			// }
			if (!con.isClosed() || con != null) {
				con.close();
			}
		}
		return attachmentPath;

	}

}
