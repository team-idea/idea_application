package com.spice.hygine.idea.contest;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.SQLType;
import java.sql.Statement;
import java.sql.Types;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

//@WebServlet("/InsertPromptDatail")
public class InsertPromptDatail extends HttpServlet {
	private static Logger logger= Logger.getLogger(InsertPromptDatail.class.getName());
	private static final long serialVersionUID = 1L;

	private String driverName = "com.mysql.jdbc.Driver";	
	private String dbUrl = "jdbc:mysql://172.27.32.81:3306/National_Contest";
	private String dbUserName = "root";
	private String dbPassword = "root@1234";
	private Properties prop=null;
	
       
    public InsertPromptDatail() {
    	try {
			prop = new Properties();
			PropertyConfigurator.configure("ContestHygine_log4j.properties");
			prop.load(new DataInputStream(new FileInputStream("contestDB.properties")));
			driverName = prop.getProperty("driverName");
			dbUrl = prop.getProperty("dbUrl");
			dbUserName = prop.getProperty("dbUserName");
			dbPassword = prop.getProperty("dbPassword");
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/HTML");
		String description=null;
		String circle=request.getParameter("circle");
		String circleIP=request.getParameter("circleIP");
		String promptLocation=request.getParameter("promptLocation");		
		String promptName=request.getParameter("promptName");
		String fileSize=request.getParameter("fileSize");
		String fileCreationDate=request.getParameter("fileCreationDate");
		String filemodificationDate=request.getParameter("filemodificationDate");
		String action=request.getParameter("action");
		Connection con = null;
		Statement stmt=null;
		ResultSet rs= null;
		CallableStatement callstmt=null;
		int insert=0;
		String procResponse=null;		
		String sqlQuery=null;
		PrintWriter out=response.getWriter();
		String promptDrive= promptLocation.substring(0,promptLocation.indexOf(":"));
		//out.println(circle+","+promptName+","+fileSize+","+fileCreationDate+","+filemodificationDate+","+action);
		logger.info(circle+","+promptName+","+fileSize+","+fileCreationDate+","+filemodificationDate+","+action);
		try{
		Class.forName(driverName);
		con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
		logger.debug("connection::-->"+con);
		if(action.equalsIgnoreCase("insert") || action.equalsIgnoreCase("insert#")){
			sqlQuery="insert into tbl_contestPromptDetails(promptName,promptSize,circle,circleIP,driveName,promptLocation,fileCreationDate,filemodification,insertDateTime) values ('"+promptName+"','"+fileSize+"','"+circle+"','"+circleIP+"','"+promptDrive+"','"+promptLocation+"','"+fileCreationDate+"','"+filemodificationDate+"',now())";
		}else if(action.equalsIgnoreCase("update") || action.equalsIgnoreCase("update#")){
			sqlQuery="update tbl_contest_status_check set status='1' where date(contestStartDate)=date(now()) and circle='"+circle+"';";	
		}else{
			sqlQuery="call proc";
		}
		
		if(sqlQuery.equalsIgnoreCase("call proc")){
			logger.debug("calling proc..");
			logger.debug("[location of prompt:-->]"+promptLocation);
			System.out.println("calling procedure with below given parameter");
			System.out.println("circle["+circle+"] promptName["+promptName+"] fileSize["+fileSize+"] fileCreationDate["+fileCreationDate+"] filemodificationDate["+filemodificationDate+"] promptLocation["+promptLocation+"] promptDrive["+promptDrive+"]");
			logger.debug("calling procedure with below given parameter");
			logger.debug("circle["+circle+"] promptName["+promptName+"] fileSize["+fileSize+"] fileCreationDate["+fileCreationDate+"] filemodificationDate["+filemodificationDate+"] promptLocation["+promptLocation+"] promptDrive["+promptDrive+"]");
			logger.debug("version 0.1");
			System.out.println("version 0.1");
			//System.out.println("[location of prompt:-->]"+promptLocation+"promptDrive"+promptDrive);
			 callstmt = con.prepareCall("{call proc_ContestPromptChecking(?,?,?,?,?,?,?,?)}");
			 callstmt.setString(1,circle);
			 callstmt.setString(2,promptName);
			 callstmt.setLong(3,Long.parseLong(fileSize));
			 callstmt.setString(4,fileCreationDate);
			 callstmt.setString(5,filemodificationDate);
			 callstmt.setString(6,promptLocation);
			 callstmt.setString(7,promptDrive);
			 callstmt.registerOutParameter(8,java.sql.Types.VARCHAR);
			 callstmt.execute();
			 procResponse=callstmt.getString(8);
			 callstmt.close();
		}else{
			stmt= con.createStatement();
			insert=stmt.executeUpdate(sqlQuery);
			stmt.close();
		}
		
		con.close();
		}catch(Exception ex){
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
			try {
				if(callstmt!=null || !callstmt.isClosed()){
					try {
						callstmt.close();
					} catch (Exception e1) {
						logger.debug(LogStackTrace.getStackTrace(e1));
					}
				}				
				if( stmt!=null || !stmt.isClosed()){
					try {
						stmt.close();
					} catch (Exception e2) {
						logger.debug(LogStackTrace.getStackTrace(e2));
					}
				}
				if( con!=null || !con.isClosed()){
					try {
						con.close();
					} catch (Exception e3) {
						logger.debug(LogStackTrace.getStackTrace(e3));
					}
				}
			} catch (SQLException e) {
				e.printStackTrace();
				logger.debug(LogStackTrace.getStackTrace(e));
			}
		}
		if(action.equalsIgnoreCase("insert")){
			description=action+"#["+insert+"] Record Inserted";
		}else if(action.equalsIgnoreCase("update")){
			description=action+"#["+insert+"] Record updated";
		}else{
			description=action+"#"+procResponse;
		}				
		out.println(description);
		logger.info(description);
	}

}
