package com.spice.hygine.idea.contest;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;



//@WebServlet("/CheckStatus")
	public class CheckStatus extends HttpServlet {
		
	private static Logger logger= Logger.getLogger(CheckStatus.class.getName()); 	
	private static final long serialVersionUID = 1L;
	private Connection con = null;
	private Statement stmt=null;
	private ResultSet rs= null;
	private String driverName = "com.mysql.jdbc.Driver";
	private String sqlQuery;
	private String dbUrl = "jdbc:mysql://172.27.32.81:3306/National_Contest";
	private String dbUserName = "root";
	private String dbPassword = "root@1234";
	private Properties prop=null;
	
	
    public CheckStatus() {
    	try {
			prop = new Properties();
			PropertyConfigurator.configure("ContestHygine_log4j.properties");
			prop.load(new DataInputStream(new FileInputStream("contestDB.properties")));
			driverName = prop.getProperty("driverName");
			dbUrl = prop.getProperty("dbUrl");
			dbUserName = prop.getProperty("dbUserName");
			dbPassword = prop.getProperty("dbPassword");
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
    }
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/HTML");
		String description=null;
		String circle=null;
		int status=1;
		circle=request.getParameter("circle");
		System.out.println(circle);
		try{
		Class.forName(driverName);
		sqlQuery = "select status from tbl_contest_status_check where date(contestStartDate)=date(now()) and circle='"+circle+"';";
		con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
		System.out.println("connection::-->"+con);
		System.out.println("circle["+circle+"]Query::----------------->"+sqlQuery);
		stmt= con.createStatement();
		rs=stmt.executeQuery(sqlQuery);
		while(rs.next()){
			 status = rs.getInt(1);
			 System.out.println("status is ["+status+"]");
		}
		rs.close();
		stmt.close();
		con.close();
		PrintWriter out=response.getWriter();
		//if(status. null){{
		if(status == 0 ){
			description="0#"+"Need to be insert the data.";
		}else if(status == 1){
			description="1#"+"No Need to insert the data.";
		}else{
			description="1#"+"No Need to insert the data.";
		}
		out.println(description);
		logger.debug(description);
		}catch(Exception ex){
			logger.debug(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		}finally{
			try {
				if( rs!=null || !rs.isClosed()){
					rs.close();
				}
				if( stmt!=null || !stmt.isClosed()){
					stmt.close();
				}
				if( con!=null || !con.isClosed()){
					con.close();
				}
			} catch (SQLException e) {
				e.printStackTrace();
				logger.debug(LogStackTrace.getStackTrace(e));
			}
		}
		//response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
