package com.spice.ping.alert;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import javax.xml.crypto.Data;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.xml.Log4jEntityResolver;

public class NonIdeaSubmitAlert {

	private static Logger logger = Logger.getLogger(NonIdeaSubmitAlert.class.getName());

	// private String nonIdeaNumber = null;
	// private String ip = null;
	// private String hostIP = null;
	private final String USER_AGENT = "Mozilla/5.0";
	private Properties prop = null;
	private String Username = "IdeaAlerts";
	private String Password = "IdeaAlerts";
	private String MessageType = "txt";
	/// private String Mobile = null;
	private String SenderID = "SMSSDL";
	private String Message = null;

	NonIdeaSubmitAlert() {
		PropertyConfigurator.configure("./log4j.properties");
		try {
			prop = new Properties();
			prop.load(new DataInputStream(new FileInputStream("./config.properties")));
			Username = prop.getProperty("Username");
			Password = prop.getProperty("Password");
			MessageType = prop.getProperty("MessageType");
			SenderID = prop.getProperty("SenderID");
			// System.out.println("Username["+Username+"]Password["+Password+"]MessageType["+MessageType+"]SenderID["+SenderID);
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(LogStackTrace.getStackTrace(ex));
		}
	}

	public void sendNonIdeaMessage(String nonIdeaNumber, String ip, String hostIP) {
		Message = "serverIP:" + ip + " is not pinging from serverIp:" + hostIP
				+ ". Please check server is up or down ASAP.";
		Message = Message.replaceAll(" ", "%20");
		if (nonIdeaNumber.length() < 12) {
			nonIdeaNumber = 91 + nonIdeaNumber;
		}
		String url = "http://103.15.179.45:8085/MessagingGateway/SendTransSMS?Username=" + Username + "&Password="
				+ Password + "&MessageType=" + MessageType + "&Mobile=" + nonIdeaNumber + "&SenderID=" + SenderID
				+ "&Message=" + Message;
		HttpClient client = new DefaultHttpClient();
		HttpGet request = new HttpGet(url); // add request header
		request.addHeader("User-Agent", USER_AGENT);
		try {
			HttpResponse response = client.execute(request);
			logger.info("Sending 'GET' request to URL : " + url);
			logger.info("Response Code : " + response.getStatusLine().getStatusCode());
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			StringBuffer result = new StringBuffer();
			String line = "";
			while ((line = rd.readLine()) != null) {
				result.append(line);
			}
			// System.out.println(result.toString());
			logger.info(result.toString());
		} catch (Exception ex) {
			ex.printStackTrace();
			logger.error(LogStackTrace.getStackTrace(ex));
		}
	}
}
