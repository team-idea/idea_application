package com.spice.ping.alert;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class Mailer {
	private static final Logger logger = Logger.getLogger(Mailer.class.getName());
	private Properties prop = null;
	private Connection con = null;
	private Statement stmt = null;
	private String driverName = "com.mysql.jdbc.Driver";
	private String sqlQuery = null;
	private String dbUrl = "jdbc:mysql://127.0.0.1:3306/national_contest";
	private String dbUserName = "root";
	private String dbPassword = "root@1234";

	public Mailer() {
		try {
			prop = new Properties();
			PropertyConfigurator.configure("./log4j.properties");
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			driverName = prop.getProperty("driverName");
			dbUrl = prop.getProperty("dbUrl");
			dbUserName = prop.getProperty("dbUserName");
			dbPassword = prop.getProperty("dbPassword");
		} catch (Exception ex) {
			logger.error(ex);
		}
	}

	public void sendMailer(String msg,String circle) {
		try{
			Class.forName(driverName);
			con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
			logger.info("connection::-->"+con);		
			sqlQuery="INSERT INTO tbl_sms_master_nonidea VALUES (uuid(), '"+circle+"', 'mailXXXXL1', 'IZ-Alert', ' <center> Server Hygine Alert(APP) CircleName :"+circle+" <br> "+msg+"</center>', 'text', 1, now(), 0, '');";
			logger.info("sqlQuery-->["+sqlQuery+"]");
			stmt= con.createStatement();
			int i=stmt.executeUpdate(sqlQuery);
			logger.info("["+i+"] record inserted.");
			sqlQuery="INSERT INTO tbl_sms_master_nonidea VALUES (uuid(), '"+circle+"', 'mailXXXXL2', 'IZ-Alert', ' <center> Server Hygine Alert(APP) CircleName :"+circle+" <br> "+msg+"</center>', 'text', 1, now(), 0, '');";
			logger.info("sqlQuery-->["+sqlQuery+"]");
			i=stmt.executeUpdate(sqlQuery);
			logger.info("["+i+"] record inserted.");
			stmt.close();
			con.close();
		}catch(Exception ex){
			try{
				stmt.close();
			}catch(Exception e){
				e.printStackTrace();
				logger.info(LogStackTrace.getStackTrace(e));
			}
			try{
				stmt.close();
			}catch(Exception e1){
				e1.printStackTrace();
				logger.info(LogStackTrace.getStackTrace(e1));
			}
			ex.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
			
	}

}
