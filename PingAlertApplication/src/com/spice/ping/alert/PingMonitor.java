package com.spice.ping.alert;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class PingMonitor {
	private static final Logger logger = Logger.getLogger(PingMonitor.class.getName());
	private Properties prop = null;
	private String IpAddresses = "10.0.252.103";
	private boolean status = false;
	private String msisdnLevel1 = null;
	private String msisdnLevel2 = null;
	private String msisdnLevel3 = null;
	private String hostIP = null;
	private String mailerFlag = "false";
	private String mailerMessage = "ping is down";
	private String nonIdeaMsisdn = null;
	private String nonIdeaAlertFlag = null;

	PingMonitor() {
		try {
			prop = new Properties();
			PropertyConfigurator.configure("./log4j.properties");
			prop.load(new DataInputStream(new FileInputStream("./config.properties")));
			IpAddresses = prop.getProperty("IpAddresses");
			msisdnLevel1 = prop.getProperty("msisdnLevel1");
			nonIdeaMsisdn = prop.getProperty("nonIdeaMsisdn");
			msisdnLevel1 = prop.getProperty("msisdnLevel1");
			msisdnLevel2 = prop.getProperty("msisdnLevel2");
			msisdnLevel3 = prop.getProperty("msisdnLevel3");
			hostIP = prop.getProperty("HostIP");
			mailerFlag = prop.getProperty("mailerFlag");
			nonIdeaAlertFlag = prop.getProperty("nonIdeaAlertFlag");
			System.out.println(IpAddresses);
		} catch (Exception ex) {
			logger.error(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		}
	}

	public void generatIPpings() {
		MessageSubmit messageSubmit = new MessageSubmit();
		NonIdeaSubmitAlert nonIdeaSubmitAlert = new NonIdeaSubmitAlert();
		Mailer mailer = new Mailer();
		String[] IpAddress = IpAddresses.split("[,]+");
		String[] level1Msisdn = msisdnLevel1.split("[,]+");
		String[] nonIdeaNumber = nonIdeaMsisdn.split("[,]+");
		while (true) {
			try {
				for (int i = 0; i < IpAddress.length; i++) {
					try {
						String ip = IpAddress[i];
						ip = ip.trim();
						status = PingIP(ip);
						if (status) {
							logger.info("server [" + ip + "] is reachable from server [" + hostIP + "]");
						} else {
							logger.info("server [" + ip + "] is not reachable from server [" + hostIP
									+ "]......please check ASAP");
							for (int j = 0; j < level1Msisdn.length; j++) {
								mailerMessage = "SERVER IP:" + ip + " is not pinging  from SERVER IP:" + hostIP
										+ ". Please check server is up or down and TAKE NECESSARY ACTION ASAP.";
								messageSubmit.sendMessage(level1Msisdn[j], ip, hostIP);
							}
							if (mailerFlag.equalsIgnoreCase("true")) {
								if (ip.equals("172.27.32.81")) {
									mailer.sendMailer(mailerMessage, "PB");
								} else {
									mailer.sendMailer(mailerMessage, "MH");
								}
							}
							for (int j = 0; j < nonIdeaNumber.length; j++) {
								nonIdeaSubmitAlert.sendNonIdeaMessage(nonIdeaNumber[j], ip, hostIP);
							}
						}
					} catch (Exception e) {
						e.printStackTrace();
						logger.error(LogStackTrace.getStackTrace(e));
					}
				}
				logger.info("Going to sleep for 2 min.");
				Thread.sleep(2 * 60 * 1000);
				System.gc();
			} catch (Exception ex) {
				logger.debug(LogStackTrace.getStackTrace(ex));
			}
		}
	}

	private boolean PingIP(String ip) {
		String pingResult = "";
		boolean Reachable = false;
		String pingCmd = "ping " + ip;
		try {
			Runtime r = Runtime.getRuntime();
			Process p = r.exec(pingCmd);
			BufferedReader in = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String inputLine=null;
			while ((inputLine = in.readLine()) != null) {
				pingResult += inputLine;
			}
			in.close();
			Thread.sleep(5000);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error(LogStackTrace.getStackTrace(e));
		}
		if (pingResult.contains("Request timed out.Request timed out.Request timed out")
				|| pingResult.contains("unreachable")) {
			Reachable = false;
		} else {
			Reachable = true;
		}
		return Reachable;
	}

}
