package com.spice.ping.alert;

import java.util.*;
import java.io.*;
import java.net.BindException;
import java.net.ConnectException;
import java.nio.channels.*;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Timer;
import java.sql.*;
import java.text.SimpleDateFormat;
import java.security.*;
import javax.net.ssl.*;
import com.sun.net.ssl.*;
import com.sun.net.ssl.internal.ssl.Provider;
import java.security.Security;

import org.apache.log4j.*;

public class MessageSubmit {
	private static Logger logger = Logger.getLogger(MessageSubmit.class.getName());
	// private String[] circles = {"ap","as","bh","dl","gj","hp","hr","jk","kk","kl","kr","mh","mp","mu","ne","or","pb","rj","tn","ue","uw","wb","xx"};
	private static String instance = "smsmanager";
	private static String[] workers = null;
	private String strServerName = "";
	private String serverUrl = "/";
	private int intSSLport = 0;
	private String msisdn = null;
	private String hostIP = null;
	private String destinationIP = "";
	// private String circle="kk";
	private String cli = "IZ-Alert";
	private String adjCode = "";
	private String authString = "Q2VsbHRyOkV0bEAwMWR3bQ==";
	private String connectionPort="64002";
	
	public MessageSubmit() {
		try {
			PropertyConfigurator.configure("log4j.properties");
		} catch (Exception exp) {
		}
		try {
			Properties prop = new Properties();
			prop.load(new DataInputStream(new FileInputStream("sms.properties")));
			instance = prop.getProperty("sms.application.instance");
			String trustStore = prop.getProperty("sdp.sms.trustStore");
			String trustStorePwd = prop.getProperty("sdp.sms.trustStorePwd");
			String keyStore = prop.getProperty("sdp.sms.keyStore");
			String keyStorePwd = prop.getProperty("sdp.sms.keyStorePwd");
			String debugFlag = prop.getProperty("sdp.sms.debugFlag");
			strServerName = prop.getProperty("sdp.sms.server.ip");
			cli = prop.getProperty("sdp.sms.cli");
			adjCode = prop.getProperty("sdp.sms.adjCode");
			authString = prop.getProperty("sdp.sms.auth.String");
			connectionPort=prop.getProperty("sdp.sms.server.port");
			// String circle=prop.getProperty("circle");
			// workers = Arrays.copyOf(threads, threads.length);
			// String[] threads =
			// prop.getProperty("sms.application.circle.tps").split(",");
			// circle = prop.getProperty("sdp.sms.circle");
						
			try {
				registerSSLSecurity(trustStore, trustStorePwd, keyStore, keyStorePwd, debugFlag);
			} catch (Exception exp) {
				exp.printStackTrace();
			}
		} catch (Exception exp) {
		}
	}

	private void logException(final String threadName, Exception exp) {
		logger.error(threadName + LogStackTrace.getStackTrace(exp));
	}

	private String generateUID() {
		UUID id = UUID.randomUUID();
		return String.valueOf(id);
	}

	public void sendMessage(String msisdn, String ip, String hostIP) {
		String ref_id = generateUID();
		String message = "Ping is down";
		this.msisdn = msisdn;
		String type = "text";
		// String msisdn = "919743662027";
		// this.circle = circle;
		// this.cli = cli;		
		// String priority = "1";
		// String insert_time = "2016-10-04 10:10:10";
		// this.adjCode = "";
		// this.authString ="Q2VsbHRyOkV0bEAwMWR3bQ==";
		// this.strServerName = "172.30.80.16";
		this.hostIP = hostIP;
		try {
			this.intSSLport						= Integer.parseInt(connectionPort);
			//this.intSSLport = 64002;
		} catch (Exception exp) {
			intSSLport = -1;
		}
		final String threadName = String.format("[%-21s] ->(res :) ", Thread.currentThread().getName());
		this.destinationIP = ip;
		message = "serverIP:" + destinationIP + " is not pinging  from serverIp:" + hostIP
				+ ". Please check server is up or down ASAP.";
		
		msisdn=msisdn.trim();
		if (msisdn.length() == 10) {
			msisdn = "91" + msisdn;
		}
		String dcs = "", udhi = "";
		if (type.equalsIgnoreCase("text")) {
			dcs = "";
			udhi = "";
		} else if (type.equalsIgnoreCase("unicode")) {
			dcs = "08";
			udhi = "";
		} else if (type.equalsIgnoreCase("tone")) {
			dcs = "F5";
			udhi = "06050415811581";
		} else if (type.equalsIgnoreCase("logo")) {
			dcs = "F5";
			udhi = "06050415821582";
		} else if (type.equalsIgnoreCase("pic")) {
			dcs = "F5";
			udhi = "060504158A0000";
		} else if (type.equalsIgnoreCase("flash")) {
			dcs = "F0";
			udhi = "";
		}

		int ret_code = 0;

		String AdjCode = "text";

		if (type.equalsIgnoreCase("charge")) {
			AdjCode = adjCode;
		}
		
		logger.info(threadName+"[buildMessage(" + cli + "," + msisdn + "," + message+","+type+","+dcs+","+udhi+","+ref_id+","+AdjCode+","+authString+"]");
		String xml_msg = buildMessage(cli, msisdn, message, type, dcs, udhi, ref_id, AdjCode, "spice", "text",
				authString);
		logger.trace(threadName + "buildMessage(" + cli + "," + msisdn + "," + message + "," + type + "," + dcs + ","
				+ udhi + "," + ref_id + "," + AdjCode + ",spice,text," + authString + ") = " + xml_msg);
		logger.info(threadName+"[SubmitMessage(" + cli + "," + msisdn + "," + message+"]");
		String res_url = submitMessage(threadName, xml_msg);
		logger.trace(threadName + "submitMessage(threadName,xml_msg) = " + res_url.replace("\n", " "));
		/*
		 * if(res_url.indexOf("ok")>0 || res_url.indexOf("limit_")>0) { ret_code
		 * = updateRecord(threadName,ref_id,priority,res_url,view_sms_mt);
		 * logger.trace(threadName+"updateRecord(threadName,"+ref_id+","+res_url
		 * +","+view_sms_mt+") = "+ret_code); }else{ret_code = -3;} if(ret_code
		 * == 1) logger.info(threadName+"Message sent   to "+msisdn+
		 * ", response = "+res_url+", success, ref_id = "+ref_id); else
		 * if(ret_code == -1) logger.info(threadName+"Message failed to "
		 * +msisdn+", response = "+res_url+", due to TPS overload, ref_id = "
		 * +ref_id); else if(ret_code == -2) logger.info(threadName+
		 * "Message failed to "+msisdn+", response = "+res_url+
		 * ", due to DB error, ref_id = "+ref_id); else if(ret_code == -3)
		 * logger.info(threadName+"Message failed to "+msisdn+", response = "
		 * +res_url+", due to Submit error, ref_id = "+ref_id+
		 * ", retry will be done"); else logger.info(threadName+
		 * "Message failed to "+msisdn+", response = "+res_url+
		 * ", due to unknown error, ref_id = "+ref_id);
		 * 
		 * employer.returnResult("Task Completed! "+info); return null;
		 */

	}

	private String generateDateTime() {
		java.util.Date date = new java.util.Date();
		SimpleDateFormat SQL_DATE_FORMAT = new SimpleDateFormat("yyyyMMdd HH:mm:ss.SSS");
		return SQL_DATE_FORMAT.format(date.getTime());
	}

	private String buildMessage(final String cli, final String msisdn, final String message, final String messageType,
			final String dcs, final String udhi, final String unique_id, final String mo_keyword,
			final String content_description, final String content_type, final String authString) {
		String retStr = "";
		String xmlhdr = "", xmlStr = "";

		xmlhdr = "POST " + serverUrl + " HTTP/1.1\r\n";
		xmlhdr += "Host: " + strServerName + ":" + intSSLport + "\n";
		xmlhdr += "Connection: closed\n";
		xmlhdr += "Content-length: <len>\n";
		xmlhdr += "Content-Type: text/xml\n";
		xmlhdr += "Authorization: Basic " + authString + "\n";
		xmlhdr += "Date " + generateDateTime() + " MEST\n";
		xmlhdr += "User-Agent: Spice/2.0\n";

		xmlStr += "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
		xmlStr += "<message><sms type=\"mt\">";
		xmlStr += "<destination><address><number type=\"international\">";
		xmlStr += msisdn;
		xmlStr += "</number></address></destination>";
		// xmlStr += "<source><address><number type=\"abbreviated\">";
		xmlStr += "<source><address><alphanumeric>";
		xmlStr += cli;
		xmlStr += "</alphanumeric></address></source>";
		// xmlStr += "</number></address></source>";

		if (messageType.equalsIgnoreCase("unicode")) {
			xmlStr += "<dcs>" + dcs + "</dcs>";
		} /*
			 * Only data coding scheme (DCS) is required in case of unicode
			 * messages.
			 */
		else if (!messageType.equalsIgnoreCase("text")) {
			xmlStr += "<dcs>" + dcs + "</dcs>" + "<udh>" + udhi + "</udh>";
		} /*
			 * Headers (UDHI) and data coding scheme (DCS) are required for binary
			 * messages.
			 */

		xmlStr += "<ud type=\""; /* Message Type - text,binary */
		if (messageType.equalsIgnoreCase("unicode")) {
			xmlStr += "text\" encoding=\"unicode\">";
		} else {
			xmlStr += messageType + "\" encoding=\"default\">";
		}

		xmlStr += convertXMLMessage(message)
				+ "</ud>"; /* Message Written here */

		/*
		 * if(messageType.equalsIgnoreCase("charge")) { xmlStr +=
		 * "<vp type=\"relative\"><date><minute>2</minute></date></vp>"; xmlStr
		 * += "<rsr type=\"success_failure\"/>"; }
		 */
		if (unique_id.length() > 0) {
			xmlStr += "<param name=\"unique_id\" value=\"" + unique_id + "\"/>";
		}
		if (mo_keyword.length() > 0) {
			xmlStr += "<param name=\"mo_keyword\" value=\"" + mo_keyword + "\"/>";
		}
		if ("SPICE".length() > 0) {
			xmlStr += "<param name=\"developer_content_id\" value=\"SPICE\"/>";
		}
		if (content_description.length() > 0) {
			xmlStr += "<param name=\"content_description\" value=\"" + content_description + "\"/>";
		}
		if (content_type.length() > 0) {
			xmlStr += "<param name=\"content_type\" value=\"" + content_type + "\"/>";
		}

		xmlStr += "</sms></message>";
		xmlhdr = xmlhdr.replace("<len>", String.valueOf(xmlStr.length()));
		return xmlhdr + "\n" + xmlStr;
	}

	private String convertXMLMessage(String str) {
		String retStr = "";
		byte[] bytStr = str.getBytes();
		for (int i = 0; i < bytStr.length; i++) {
			if (Integer.parseInt(String.valueOf(bytStr[i])) > 0)
				retStr += "&#" + String.valueOf(bytStr[i]) + ";";
		}
		return retStr;
	}

	private void registerSSLSecurity(String trustStore, String trustStorePwd, String keyStore, String keyStorePwd,
			String debugFlag) {
		// Registering the JSSE provider
		Security.addProvider(new Provider());
		/*********************************************************************
		 *
		 * Specifying the Keystore details
		 *
		 **********************************************************************/
		System.setProperty("javax.net.ssl.trustStore", trustStore);
		System.setProperty("javax.net.ssl.trustStorePassword", trustStorePwd);
		System.setProperty("javax.net.ssl.keyStore", keyStore);
		System.setProperty("javax.net.ssl.keyStorePassword", keyStorePwd);
		if (debugFlag.equalsIgnoreCase("true")) {
			// Enable debugging to view the handshake and communication which
			// happens between the SSLClient and the SSLServer
			System.setProperty("java.protocol.handler.pkgs", "javax.net.ssl");
			System.setProperty("sun.security.ssl.allowUnsafeRenegotiation", "true");
			System.setProperty("javax.net.debug", "all");
		}
	}

	private String submitMessage(final String appender, final String sdp_message) {
		String ret_str = "failed";
		PrintWriter out = null;
		BufferedReader in = null;

		try {
			int ctr = 0;
			// Creating Client Sockets
			logger.trace(appender + "Trying to connect to " + strServerName + " on port " + intSSLport);
			final SSLSocketFactory sslsocketfactory = (SSLSocketFactory) SSLSocketFactory.getDefault();
			final SSLSocket sslSocket = (SSLSocket) sslsocketfactory.createSocket(strServerName, intSSLport);

			try {
				if (sslSocket != null) {

					sslSocket.setEnableSessionCreation(true);
					logger.trace(
							appender + "sslSocket.setEnableSessionCreation(true) called.  sslSocket.getEnableSessionCreation() = "
									+ sslSocket.getEnableSessionCreation());
					sslSocket.setUseClientMode(true);
					logger.trace(
							appender + "sslSocket.setUseClientMode(true) called.          sslSocket.getUseClientMode()         = "
									+ sslSocket.getUseClientMode());
					sslSocket.setReuseAddress(true);
					logger.trace(
							appender + "sslSocket.setReuseAddress(true) called.           sslSocket.getReuseAddress()          = "
									+ sslSocket.getReuseAddress());
					sslSocket.setSoLinger(true, 10);
					logger.trace(
							appender + "sslSocket.setSoLinger(true, 10) called.           sslSocket.getSoLinger()              = "
									+ sslSocket.getSoLinger());
					sslSocket.setSoTimeout(60000);
					logger.trace(
							appender + "sslSocket.setSoTimeout(60000) called.             sslSocket.getSoTimeout()             = "
									+ sslSocket.getSoTimeout());
					sslSocket.setTcpNoDelay(true);
					logger.trace(
							appender + "sslSocket.setTcpNoDelay(true) called.             sslSocket.getTcpNoDelay()            = "
									+ sslSocket.getTcpNoDelay());
					sslSocket.setKeepAlive(true);
					logger.trace(
							appender + "sslSocket.setKeepAlive(true) called.              sslSocket.getKeepAlive()             = "
									+ sslSocket.getKeepAlive());

					try {
						sslSocket.setNeedClientAuth(true);
						sslSocket.startHandshake();
					} catch (Exception exp) {
						exp.printStackTrace();
						logger.trace(exp);
						logException(appender, exp);
						return "error";
					}

					String tempBuff = "";
					String getServerResp = "", strpost = "";

					// Initializing the streams for Communication with the
					// Server
					out = new PrintWriter(sslSocket.getOutputStream(), true);
					in = new BufferedReader(new InputStreamReader(sslSocket.getInputStream()));

					out.println(sdp_message);
					out.flush();

					try {
						while ((tempBuff = in.readLine()) != null) {
							getServerResp += tempBuff + " ";
							logger.trace(appender + tempBuff);
							try {
								Thread.sleep(3);
							} catch (Exception exp) {
								logException(appender, exp);
							}
						}
					} catch (Exception expSocket) {
						logger.error(appender + "Error occured during reading message packet " + ctr
								+ " sslSocket.isConnected() = " + sslSocket.isConnected() + " for message packet: "
								+ sdp_message);
						logException(appender, expSocket);
						try {
							Thread.sleep(1000 * 1);
						} catch (Exception exp) {
							logException(appender, exp);
						}
						expSocket.getStackTrace();
						ret_str = "error_read";
					}
					ret_str = getServerResp.trim();
				}
			} catch (Exception exp) {
				logException(appender, exp);
				try {
					Thread.sleep(1000);
				} catch (Exception expT) {
					logException(appender, expT);
				}
			} finally {
				try {
					if (out != null)
						out.close(); // Closing the Streams and the Socket
					if (in != null)
						in.close(); // Closing the Streams and the Socket
					if (sslSocket != null)
						sslSocket.close(); // Closing the Streams and the Socket
				} catch (Exception expClose) {
					logException(appender, expClose);
				}
			}
		}
		// catch(BindException bind_exp){logger.error("Cannot bind to SDP-MG
		// server. total threads in system "+Thread.activeCount()+" Transaction
		// will be re-tried."); ret_str = "error_bind";}
		
		catch (BindException bind_exp) {
			bind_exp.printStackTrace();
			logger.error("Cannot bind to SDP-MG server. total threads in system " + Thread.activeCount()
					+ " Transaction will be re-tried.");
			ret_str = "error_bind";
		} catch (ConnectException conn_exp) {
			conn_exp.printStackTrace();
			logger.error("Cannot connect to SDP-MG server. Transaction will be re-tried.");
			ret_str = "error_connect";
		} catch (Exception exp) {
			logException(appender, exp);
			try {
				Thread.sleep(1000);
			} catch (Exception expT) {
				logException(appender, expT);
			}
			ret_str = "error";
		}
		return ret_str.toLowerCase();
	}

}
