package com.spice.ping.alert;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
//import java.util.Properties;
import org.apache.log4j.*;

public class PingAlertMain {

	private static final Logger logger = Logger.getLogger(PingAlertMain.class.getName());

	PingAlertMain() {
		try {
			PropertyConfigurator.configure("./log4j.properties");
		} catch (Exception e) {
			e.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(e));
		}

	}

	public static void main(String[] args) {
		PingAlertMain pingAlertMain = new PingAlertMain();
		try {
			if (pingAlertMain.accessFile() && pingAlertMain.getLock()) {
				logger.info("program is started..");
				PingMonitor pingMonitor = new PingMonitor();
				pingMonitor.generatIPpings();
			} else {

			}
		} catch (Exception e) {
			logger.error(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		}

	}

	private boolean getLock() {
		boolean lock = false;
		try {
			RandomAccessFile file = new RandomAccessFile("./pingAlerts.tmp", "rw");
			FileChannel fileChannel = file.getChannel();
			FileLock fileLock = fileChannel.tryLock();
			if (fileLock != null) {
				logger.debug("File is locked. Starting the execution of the Application");
				lock = true;
			} else {
				logger.error("Cannot create lock. Already another instance of the program is running.");
				logger.error("System shutting down. Please wait");
				try {
					Thread.sleep(1000 * 3);
				} catch (Exception exp) {
					exp.printStackTrace();
				}
				System.exit(0);
			}
		} catch (Exception exp) {
			exp.printStackTrace();
		}
		return lock;
	}

	private boolean accessFile() {
		boolean retCode = false;
		try {
			String line = "";
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter("./pingAlerts.tmp", false));
				bw.close();
			} catch (Exception exp) {
				exp.printStackTrace();
			}
			retCode = true;
		} catch (Exception e) {
			System.out.println(e);
		}
		logger.debug("accessFile() = " + retCode);
		return retCode;
	}

}
