package com.spice.missCall.outdial;

import java.util.Vector;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.util.*;
import java.lang.*;
import java.io.*;
import java.sql.*;
import java.security.*;

//import java.nio.*;



public class OutDial extends Thread
{
	private static  Logger logger = Logger.getLogger(OutdialFileUpload.class.getName());
	public static String[] out_parameters 			= null;
	public static Vector<Object> base				= new Vector<Object>();
	public static int outdial_counter				= 0,i=0;
	public static int testing_counter				= 0;
	public static java.util.Date dtRef				= new java.util.Date();
	public static PrintStream cdr_file			= null;
	
	private Connection con= null;
	private String dbUserName =null;
	private String driverName =null;
	private String dbPassword =null;
	private Statement stmt=null;
	private ResultSet rs=null;
	private String circle=null;
	private String dbUrl =null;
	private String dataBaseName=null;
	private String tableName=null;
	private String columnName=null;
	private String tableNameForCheck=null;
	
	public OutDial()
	{
		//String strFile = "E:\\MAPP_TEST\\IMS\\Outdial\\Base_list.txt";
		//checkCheckSum();
		Properties prop = new Properties();
		try{
			PropertyConfigurator.configure("./log4j.properties");
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
		}catch(Exception ex){
			ex.printStackTrace();
			LogStackTrace.getStackTrace(ex);
		}
		
		 driverName = prop.getProperty("driverName");
		 dbUrl = prop.getProperty("dbUrl");
		 dbUserName = prop.getProperty("dbUserName");
		 dbPassword = prop.getProperty("dbPassword");
		 circle=prop.getProperty("circle");
		 dataBaseName=prop.getProperty("dataBaseName");
		 tableName=prop.getProperty("tableName");
		 tableNameForCheck=prop.getProperty("tableNameForCheck");
		 columnName=prop.getProperty("columnName");
		 
		 	
		System.out.println("+-----------------------------------------------------------------------+");
		System.out.println("|   Starting the OutDial File Generation Program for OBD promotions.    |");
		System.out.println("|                                            Release Version : 1.07.    |");
		System.out.println("+-----------------------------------------------------------------------+");
			try
		{
			Class.forName(driverName);	
			ConfigFile cfg 		= loadConfig();
			checkSystem(cfg);
			//cdr_file 			= new BufferedWriter(new FileWriter(cfg.CDR_DIRECTORY+"OUT_LOG_"+formatOutput(((new java.util.Date().getDate())),2)+""+formatOutput(((new java.util.Date().getMonth())),2)+"20"+formatOutput(((new java.util.Date().getYear())-100),2)+".txt",true));
			cdr_file 			= new PrintStream(new FileOutputStream(cfg.CDR_DIRECTORY+"OUT_LOG_"+formatOutput(((new java.util.Date().getDate())),2)+""+formatOutput(((new java.util.Date().getMonth())),2)+"20"+formatOutput(((new java.util.Date().getYear())-100),2)+".txt",true));
			OutdialFileUpload out_File		= new OutdialFileUpload();
			System.out.println("|      Base List Loaded For OutDialing Successfully                     |");
			System.out.println("+-----------------------------------------------------------------------+");
			base 	= out_File.fileUpload();
		}
		catch(Exception e)
		{
			System.out.println("|      Loading of Base List failed                                      |");
			System.out.println("+-----------------------------------------------------------------------+");
			//e.printStackTrace();
			error(e);
		}
	}

	public void checkSystem(ConfigFile cfg)
	{

		File fd		= new File(cfg.LOG_DIRECTORY+"out.st");
	}

	public String formatOutput(int iVar,int pos)
	{
		String pad 			= "";
		int len				= 0;
		String str			= ""+iVar;
		len 				= str.length();
		if (len >= pos)		pad = str;
		else
		{
			for(int i=0; i<pos-len; i++)	pad = pad + "0";
			pad = pad + str;
		}
		return pad;
	}

	private ConfigFile loadConfig() throws Exception
	{
		try
		{
			ConfigFile cfg 		= new ConfigFile();
			System.out.println("|      Configuration Loaded For OutDialing Successfully                 |");
			System.out.println("+-----------------------------------------------------------------------+");
			return cfg;
		}
		catch(Exception e)
		{
			System.out.println("|      Loading of Configuration failed from OutDial.cfg                 |");
			System.out.println("+-----------------------------------------------------------------------+");
			throw e;
		}
	}
	public void run()
	{
		try
		{
			ConfigFile cfg = new ConfigFile();
			System.out.println("|       Starting File Generation Process.                               |");
			if((new java.util.Date()).getHours()>=Integer.parseInt(cfg.START_TIME)&&(new java.util.Date()).getHours()<Integer.parseInt(cfg.END_TIME))
			{
				channelFileGeneration(base);
			}
			else
			{
				System.out.println("|      Time is not appropriate to start the Outdial Process             |");
				System.out.println("+-----------------------------------------------------------------------+");
				System.exit(2);
			}
		}
		catch(Exception e)
		{
			error(e);
		}
	}

	public void channelFileGeneration(Vector<Object> base_list)
	{
		try
		{
			String MSISDN	= "";
			ConfigFile cfg 	= new ConfigFile();
			int ret_val		= 0;
			int ctr_vector	= outdial_counter;
			if(ctr_vector>base_list.size())
			{
				System.out.println("|      Outdial is complete for the current file. Update out.st for new. |");
				System.out.println("+-----------------------------------------------------------------------+");
				System.exit(0);
			}
			while(ctr_vector<=base_list.size())
			{
				System.out.println("ctr_vector = "+ctr_vector+" base_list.size() = "+base_list.size());
				int channel_found		= findFreeChannel(Integer.parseInt(cfg.START_CHANNEL),Integer.parseInt(cfg.END_CHANNEL),cfg);
				if((new java.util.Date()).getHours()>=Integer.parseInt(cfg.START_TIME)&&(new java.util.Date()).getHours()<Integer.parseInt(cfg.END_TIME))
				{

					try
					{
						if(channel_found >= 0 )
						{
							//System.out.println("channel_found = "+channel_found+" testing_counter = "+testing_counter+" cfg.TEST_FREQUENCY = "+cfg.TEST_FREQUENCY);
							if(testing_counter>cfg.TEST_FREQUENCY){MSISDN	= cfg.TEST_NUMBER;ctr_vector--;testing_counter=0;}
							else try{MSISDN	= (base_list.elementAt(ctr_vector)).toString();}catch(Exception ex){ex.toString();}
							ret_val											= writeFileToChannel(channel_found,MSISDN,cfg);
						}
						else
						{

						}
					}
					catch(Exception exp){exp.printStackTrace();}
				}
				else
				{
					System.out.println("|      Time is not appropriate to start the Outdial Process             |");
					System.out.println("+-----------------------------------------------------------------------+");
					System.exit(2);
				}
				ctr_vector++;testing_counter++;
				System.out.println("File Written ret_val = "+ret_val+" ctr_vector = "+ctr_vector);
			}
		}
		catch(Exception exp)
		{
			error(exp);
		}
	}

	public int findFreeChannel(int Start,int End,ConfigFile cfg)
	{
		File chn_file		= null;
		int ret_val			= -1;
		int i				= 0;
		int fail_counter	= 0;
		while(ret_val<0)
		{
			for(i = Start;i<=End;i++)
			{
				if(fail_counter>End)
				{
					PrintInfo pInfo			= new PrintInfo();
					pInfo.Total_channels	= Integer.parseInt(cfg.END_CHANNEL) - Integer.parseInt(cfg.START_CHANNEL);
					pInfo.Busy_channels		= (Integer.parseInt(cfg.END_CHANNEL) - Integer.parseInt(cfg.START_CHANNEL))+1;
					pInfo.Idle_channels		= 0;
					pInfo.Total_Calls		= outdial_counter;
					pInfo.MSISDN			= cfg.TEST_NUMBER;
					pInfo.frequency			= cfg.TEST_FREQUENCY;
					pInfo.info();
					try{Thread.sleep(2000);fail_counter = 0;}catch(Exception e){error(e);}
				}
				chn_file = new File(cfg.IN_DIRECTORY+"chnl"+i+".lck");
				if(chn_file.exists()) {ret_val = -1; fail_counter++;}
				else {ret_val = i; break;}
			}
		}
		return i;
	}

	public int writeFileToChannel(int Channel,String MSISDN,ConfigFile cfg) throws Exception
	{
		try
		{
			int totchnlpercli = 0,tot_Channel=0;
			String CLI_ARR[]=cfg.CLI_LIST.split("~");

			tot_Channel = (Integer.parseInt(cfg.END_CHANNEL)-Integer.parseInt(cfg.START_CHANNEL));
			totchnlpercli = tot_Channel/cfg.TOTAL_CLI;
			//System.out.println(Channel+"totchnlpercli : "+totchnlpercli+" Usage"+Channel%cfg.TOTAL_CLI);
			i = (Channel%cfg.TOTAL_CLI);
			//System.out.println("i is "+i+",,CLI: "+CLI_ARR[i]);
			String cli=CLI_ARR[i];

			PrintStream text_file =new PrintStream(new FileOutputStream(cfg.IN_DIRECTORY+"chnl"+Channel+".txt"),false);
			text_file.println(MSISDN+"F "+cli);
			text_file.close();
			Connection conn= null;
			Statement stmtt =null;
			String sql="insert into "+dataBaseName+".dbo."+tableNameForCheck+" values('"+MSISDN+"',getdate());";
			System.out.println(sql);
			conn = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
			System.out.println(conn);
			stmtt= conn.createStatement();
			stmtt.executeUpdate(sql);
			stmtt.close();
			conn.close();			
			outdial_counter++;
			PrintStream stat_file =new PrintStream(new FileOutputStream(cfg.LOG_DIRECTORY+"out.st"),false);
			stat_file.println(outdial_counter);
			stat_file.close();

			PrintStream lock_file =new PrintStream(new FileOutputStream(cfg.IN_DIRECTORY+"chnl"+Channel+".lck"),false);
			lock_file.close();

			generateCDR(MSISDN,cfg,Channel);
		}
		catch(Exception e)
		{
			logger.debug(LogStackTrace.getStackTrace(e));
			throw e;
		}
		return 0;
	}
	public void generateCDR(String MSISDN,ConfigFile cfg,int Channel)
	{
			try
			{
				cdr_file.println("OUTDIAL LOG#"+formatOutput(Channel,3)+"#"+MSISDN+"#"+formatOutput((new java.util.Date().getDate()),2)+""+formatOutput((new java.util.Date().getMonth()),2)+"20"+formatOutput((new java.util.Date().getYear()),2)+" "+formatOutput((new java.util.Date().getHours()),2)+":"+formatOutput((new java.util.Date().getMinutes()),2)+":"+formatOutput((new java.util.Date().getSeconds()),2)+"#");
			}
			catch(Exception ex){error(ex);}
	}
	public static void main(String argv[])
	{
		OutDial out		= new OutDial();
		out.start();

	}

	public void error(Exception e)
	{
		try
		{
			Calendar today 				= Calendar.getInstance();
			String strerrorfile 		= "err_"+formatOutput(today.get(Calendar.YEAR),4) + formatOutput((today.get(Calendar.MONTH)+1),2) + formatOutput(today.get(Calendar.DATE),2);
			PrintStream outprint 		= new PrintStream(new FileOutputStream("./error/" + strerrorfile + ".txt",true));
			outprint.println("Exception:" +e.toString());
			outprint.close();
			//outfile.close();
			//System.out.println(e.toString());
			e.printStackTrace();
			//System.exit(0);
		}
		catch(Exception ee)
		{
			System.out.println("Exception:" + ee.toString());
			ee.printStackTrace();
			//sendAlert(1,"Err in " + this.getClass().getName());
		}
	}
	class ConfigFile
	{
		String OUTDIAL_CLI			= "";
		String IN_DIRECTORY			= "";
		String OUT_DIRECTORY 		= "";
		String CDR_DIRECTORY		= "";
		String LOG_DIRECTORY		= "";
		String DATABASE_USR			= "";
		String DATABASE_PWD			= "";
		String DATABASE_DSN			= "";
		String BASE_FILE			= "";
		String DND_LIST				= "";
		String START_TIME			= "";
		String END_TIME				= "";
		String START_CHANNEL		= "";
		String END_CHANNEL			= "";
		String TEST_NUMBER			= "";
		String CLI_LIST				= "";
		int   	TEST_FREQUENCY		= 0;
		int 	TOTAL_CLI			= 0;
		String info					= "";
		String var_info				= "";
		String var_val				= "";

		int 	idx					= 0;

		BufferedReader cfgFile		= null;
		public ConfigFile() throws Exception
		{
			try
			{
				cfgFile = new BufferedReader( new FileReader(new File("OutDial.cfg")));
				while((info = cfgFile.readLine())!=null)
				{
					if((info.equals(""))||info.substring(0,1).equals("[")||info.substring(0,1).equals("#")||info.substring(0,1).equals("*")){continue;}
					idx			= info.indexOf("=");
					var_info 	= info.substring(0,idx).trim();
					var_val		= info.substring(idx+1).trim().replaceAll("\"","").replaceAll(";","");
					if (var_info.equals("OUTDIAL_CLI"))	OUTDIAL_CLI = var_val;
					else if (var_info.equals("OUTDIAL_CLI"))	OUTDIAL_CLI 		= var_val;
					else if (var_info.equals("IN_DIRECTORY"))	IN_DIRECTORY 		= var_val;
					else if (var_info.equals("OUT_DIRECTORY"))	OUT_DIRECTORY 		= var_val;
					else if (var_info.equals("CDR_DIRECTORY"))	CDR_DIRECTORY 		= var_val;
					else if (var_info.equals("LOG_DIRECTORY"))	LOG_DIRECTORY 		= var_val;
					else if (var_info.equals("DATABASE_USR"))	DATABASE_USR 		= var_val;
					else if (var_info.equals("DATABASE_PWD"))	DATABASE_PWD 		= var_val;
					else if (var_info.equals("DATABASE_DSN"))	DATABASE_DSN 		= var_val;
					else if (var_info.equals("BASE_FILE"))		BASE_FILE 			= var_val;
					else if (var_info.equals("DND_LIST"))		DND_LIST 			= var_val;
					else if (var_info.equals("START_TIME"))		START_TIME 			= var_val;
					else if (var_info.equals("END_TIME"))		END_TIME 			= var_val;
					else if (var_info.equals("START_CHANNEL"))	START_CHANNEL 		= var_val;
					else if (var_info.equals("END_CHANNEL"))	END_CHANNEL			= var_val;
					else if (var_info.equals("TEST_NUMBER"))	TEST_NUMBER			= var_val;
					else if (var_info.equals("TEST_FREQUENCY"))	TEST_FREQUENCY		= Integer.parseInt(var_val);
					else if (var_info.equals("TOTAL_CLI"))		TOTAL_CLI			= Integer.parseInt(var_val);
					else if (var_info.equals("CLI_LIST"))		CLI_LIST			= var_val;//.split("~");

				}
				cfgFile.close();
				try{Thread.sleep(2000);}catch(Exception e){error(e);}
				try
				{
					cfgFile 			= new BufferedReader( new FileReader(new File(LOG_DIRECTORY+"Out.st")));
					info 				= cfgFile.readLine().toString();
					cfgFile.close();
					outdial_counter		= Integer.parseInt(info);
				}
				catch(Exception exp)
				{
					outdial_counter = 1;
					try
					{

						PrintStream stat_file =new PrintStream(new FileOutputStream(LOG_DIRECTORY+"out.st"),false);
						stat_file.println(outdial_counter);
						stat_file.close();
					}
					catch(Exception ex)
					{
						throw ex;
					}
				}
			}
			catch(Exception e)
			{
				throw e;
			}
		}
	}

	 class OutdialFileUpload
	{
		private  final Logger logger = Logger.getLogger(OutdialFileUpload.class.getName());
		
		BufferedReader upFile		= null;
		String info					= "";
		Vector<Object> base_list 	= new Vector<Object>();
		//PropertyConfigurator.configure("./log4j.properties");
		public Vector<Object> fileUpload() throws Exception
		{
			PropertyConfigurator.configure("./log4j.properties");
		
			try
			{	
				String msisdnStatus = null;
				
				String sql="select top 500 ani as msisdn from "+dataBaseName+".dbo."+tableName+" where status='1' and convert(varchar,"+columnName+",112) >= convert(varchar,getdate(),112);";
				System.out.println(sql);
				//TBL_VOICECHAT
				//String sql="select ani as msisdn from "+dataBaseName+".dbo."+tableName+" where status='1' and convert(varchar,renew_date,112) >= convert(varchar,getdate(),112);";
				
				con = DriverManager.getConnection(dbUrl, dbUserName, dbPassword);
				System.out.println(con);
				stmt= con.createStatement();
				rs= stmt.executeQuery(sql);
				while(rs.next()){
					String msisdn=rs.getString("msisdn");
					msisdn=msisdn.trim();
					HttpSendRequest httpSendRequest = new HttpSendRequest();
					msisdnStatus = httpSendRequest.sendGet("http://10.0.252.103/CheckDndNumber/checkDndStatusUrl?msisdn="+msisdn+"&circle="+circle);
					logger.debug("http://10.0.252.103/CheckDndNumber/checkDndStatusUrl?msisdn="+msisdn+"&circle="+circle+"----->response["+msisdnStatus+"]");
					if(msisdnStatus.equals("dndNotActive")){
						logger.debug("add number in list ["+msisdn+"]msisdnStatus["+msisdnStatus+"]");
						base_list.add(msisdn.trim());
						Thread.sleep(500);
					}					
				}
				rs.close();
				stmt.close();
				con.close();
				/*ConfigFile cfg 	= new ConfigFile();
				upFile 			= new BufferedReader(new FileReader(new File(cfg.BASE_FILE)));
				while((info = upFile.readLine())!=null)
				{
					if((info.equals(""))||info.substring(0,1).equals("[")||info.substring(0,1).equals("#")||info.substring(0,1).equals("*")){continue;}
					base_list.add(info.trim());
				}
				upFile.close();*/
				return base_list;
			}
			catch(Exception e){
				if (stmt != null || !stmt.isClosed()) {
					stmt.close();
				}
				if (rs != null || !rs.isClosed()) {
					rs.close();
				}
				if (con != null || !con.isClosed()) {
					con.close();
				}
				e.printStackTrace();
				throw e;
				
			}
		}
	}

	class PrintInfo
	{
		int Total_channels	= 0;
		int Busy_channels	= 0;
		int Idle_channels	= 0;
		int Total_Calls		= 0;
		String MSISDN		= "";
		int frequency		= 0;
		int time			= 0;
		int diff			= 0;
		String pad			= "";

		java.util.Date dtCurr = new java.util.Date();

		void info()
		{
			diff = ((dtCurr.getHours()+1)*24+(dtCurr.getMinutes()+1)*60+dtCurr.getSeconds())-(((dtRef.getHours()+1)*24+(dtRef.getMinutes()+1)*60+dtRef.getSeconds()));
			System.out.println("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n");

			System.out.println("+-----------------------------------------------------------------------+");
			System.out.println("|              OutDial File Generation Program Version 1.07             |");
			System.out.println("+-----------------------------------------------------------------------+");

			System.out.println("\n\n\n\n\n\n");

			System.out.println("+-----------------------------------------------------------------------+");
			System.out.println("|                OutDial File Formation Info    "+formatOutput((new java.util.Date().getHours()),2)+":"+formatOutput((new java.util.Date().getMinutes()),2)+":"+formatOutput((new java.util.Date().getSeconds()),2)+"                |");
			System.out.println("+-----------------------------------------------------------------------+");
			System.out.println("|            Total Available Channels           = "+formatOutput((Total_channels+1),3)+"                   |");
			System.out.println("|            Total Busy Channels                = "+formatOutput(Busy_channels,3)+"                   |");
			System.out.println("|            Total Idle Channels                = "+formatOutput(Idle_channels,3)+"                   |");
			System.out.println("|            Total Calls Attepted till now      = "+formatOutput(Total_Calls,6)+"                |");
			System.out.println("|            Testing OutDial Number             = "+MSISDN+"            |");
			System.out.println("|            Frequency of Test Call             = "+formatOutput(frequency,3)+"                   |");
			//System.out.println("|            Total Elapsed Time                 = "+formatOutput(diff/60,3)+" mins.             |");
			System.out.println("+-----------------------------------------------------------------------+");
		}
	}



public static void count(String filename) throws IOException {
    InputStream is = new BufferedInputStream(new FileInputStream(filename));
    /*try {
        byte[] c = new byte[1024];
        int count = 0;
        int readChars = 0;
        while ((readChars = is.read(c)) != -1) {
            for (int i = 0; i < readChars; ++i) {
                if (c[i] == '\n')
                    ++count;
            }
        }
        //return count;
        System.out.println("Total Logs in File"+count);
    } //finally {       System.out.println("no Logs ");//is.close();    }
    catch (Exception e){e.printStackTrace();};*/

}


	public static void checkCheckSum()
	{
		String checkSumfromFile = "";
		String checkSumCalculated = "";
		PrintWriter pw1=null, errPw1 = null;
		String r_fileOutList = "/root/OBD/tcp/Outdial/base_list.txt";
		checkSumfromFile = removileLastLine(r_fileOutList);
		checkSumCalculated = getChecksum (r_fileOutList);
		System.out.println("CheckSum from File"+checkSumfromFile);
		System.out.println("Calculated CheckSum"+checkSumCalculated);
		if(!checkSumfromFile.equals(checkSumCalculated))
		{

			System.out.println("Check sum not valid");
			System.exit(0);
		}
		/*String checkSumFinal=getChecksum(r_fileOutList);
		try
		{
			pw1 = new PrintWriter(new FileOutputStream(r_fileOutList,true));
			pw1.println(checkSumFinal);
			pw1.close();
		}
		catch(Exception e)
		{
			e.printStackTrace();
			errPw1.println(e.getMessage());
		}*/

	}

	public static String removileLastLine(String fileName)
	{
		String chkSm = null;
		try
		{
			String msg, prevline;
			msg = prevline = null;
			BufferedReader in=new BufferedReader(new FileReader(fileName));
			while((msg = in.readLine()) != null)
			{
				prevline = msg;
			}
			chkSm = prevline;

			System.out.println("prevline:"+prevline);
			in.close();

			RandomAccessFile raf = new RandomAccessFile(fileName, "rw");

			long length = raf.length();
			System.out.println("File Length="+raf.length());
			//raf.setLength(length - (prevline.length()+1));
			raf.setLength(length - (prevline.length()+1));
			System.out.println("File Length="+raf.length());
			raf.close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		return chkSm;
	}

	public static String getChecksum ( String strFilepath)
	{
		String strChecksum="";
		try
		{
			MessageDigest md = MessageDigest.getInstance("MD5");
			FileInputStream fis = new FileInputStream(strFilepath);
			byte[] dataBytes = new byte[1024];
			int nread = 0;
			while ((nread = fis.read(dataBytes)) != -1) {
				md.update(dataBytes, 0, nread);
			};
			byte[] mdbytes = md.digest();
			//convert the byte to hex format
			StringBuffer sb = new StringBuffer("");
			for (int i = 0; i < mdbytes.length; i++) {
				sb.append(Integer.toString(mdbytes[i]));
			// sb.append(Integer.toString((mdbytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			strChecksum=sb.toString();
			return sb.toString();
				//System.out.println("Digest(in hex format):: " + sb.toString());
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return strChecksum;
	}
}



