package in.spicedigital.auditMailer.controller;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import in.spicedigital.auditMailer.model.AuditDataModel;
import in.spicedigital.auditMailer.util.LogStackTrace;

public class CreateAuditDataExcel {
	private static final Logger LOGGER = Logger.getLogger(CreateAuditDataExcel.class);

	private Properties prop = null;
	private FileOutputStream fos = null;
	private String excelColumn = null;
	private String[] column = null;
	private XSSFWorkbook workbook = null;
	private XSSFSheet sheet = null;
	private XSSFRow row = null;
	private XSSFCell cell = null;
	private int colIndex = 0;
	private static int rowIndex = 0;
	private XSSFCellStyle styleHeader = null;
	private XSSFCellStyle styleRow = null;
	private String workBookPath = null;

	public CreateAuditDataExcel() {
		PropertyConfigurator.configure("./log4j.properties");
		prop = new Properties();
		workbook = new XSSFWorkbook();
		sheet = workbook.createSheet();
		setStyle();

		try {
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			excelColumn = prop.getProperty("excelColumn");
			workBookPath = prop.getProperty("workBookPath");
			LOGGER.info(excelColumn);
			createExcelHeader();
		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public void createExcelHeader() {
		colIndex = 0;
		column = excelColumn.split("[,]+");
		row = sheet.createRow(0);
		for (String columnData : column) {
			cell = row.createCell(colIndex);
			cell.setCellValue(columnData);
			cell.setCellStyle(styleHeader);
			colIndex++;
		}
		colIndex = 0;
	}

	public void insertExcelRow(List<AuditDataModel> collectAuditDataList) {
		for (AuditDataModel auditDataRow : collectAuditDataList) {
			colIndex = 0;
			rowIndex++;
			row = sheet.createRow(rowIndex);		
					
			try {
				String auditMisDate = auditDataRow.getAuditMisDate();
				cell = row.createCell(colIndex);
				sheet.setColumnWidth(colIndex, 3000);				
				cell.setCellValue(auditMisDate);
				cell.setCellStyle(styleRow);
				colIndex++;

				String circle = auditDataRow.getCircle();
				cell = row.createCell(colIndex);				
				cell.setCellValue(circle);
				cell.setCellStyle(styleRow);
				colIndex++;

				String operator = auditDataRow.getOperator();
				cell = row.createCell(colIndex);				
				cell.setCellValue(operator);
				cell.setCellStyle(styleRow);
				colIndex++;

				String shortCode = auditDataRow.getShortCode();
				cell = row.createCell(colIndex);				
				cell.setCellValue(shortCode);
				cell.setCellStyle(styleRow);
				colIndex++;

				int mtdDuration = auditDataRow.getDurationMTD();
				cell = row.createCell(colIndex);
				sheet.setColumnWidth(colIndex, 3000);				
				cell.setCellValue(mtdDuration);
				cell.setCellStyle(styleRow);
				colIndex++;	
				
				} catch (Exception ex) {
				LOGGER.debug(LogStackTrace.getStackTrace(ex));
				}
		}
			
	}

	public String saveExcels(String misdate) throws IOException {
		try {
			workBookPath = workBookPath.replaceAll("YYYYMMDD", misdate);
			fos = new FileOutputStream(workBookPath);
			workbook.write(fos);
		} catch (FileNotFoundException e) {
			LOGGER.debug(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		} catch (IOException e) {
			LOGGER.debug(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		} finally {
			fos.close();
		}
		return workBookPath;
	}

	public void setStyle() {
		styleHeader = workbook.createCellStyle();
		styleHeader.setFillForegroundColor(IndexedColors.GREY_40_PERCENT.getIndex());
		styleHeader.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
		styleHeader.setVerticalAlignment(XSSFCellStyle.VERTICAL_BOTTOM);
		styleHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
		styleHeader.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderTop(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		styleHeader.setBorderRight(XSSFCellStyle.BORDER_THIN);

		styleRow = workbook.createCellStyle();
		styleRow.setAlignment(XSSFCellStyle.ALIGN_RIGHT);
		styleRow.setVerticalAlignment(XSSFCellStyle.VERTICAL_BOTTOM);
		styleRow.setBorderBottom(XSSFCellStyle.BORDER_THIN);
		styleRow.setBorderTop(XSSFCellStyle.BORDER_THIN);
		styleRow.setBorderLeft(XSSFCellStyle.BORDER_THIN);
		styleRow.setBorderRight(XSSFCellStyle.BORDER_THIN);

	}

}
