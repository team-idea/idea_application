package in.spicedigital.auditMailer.controller;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Formatter;
import java.util.List;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import in.spicedigital.auditMailer.model.AuditDataModel;
import in.spicedigital.auditMailer.util.LogStackTrace;
import in.spicedigital.auditMailer.util.MailHelper;

public class AuditMis {
	private static final Logger LOGGER = Logger.getLogger(AuditMis.class);
	private static String dateDiff = null;
	public static String misDate= null;

	public AuditMis() {
		PropertyConfigurator.configure("./log4j.properties");
	}

	public static void main(String[] args) {
		AuditMis yaariAudit = new AuditMis();
		try {
			if (yaariAudit.accessFile() && yaariAudit.getLock()) {
				LOGGER.info("program is started..");
				if (args.length == 1) {
					dateDiff = args[0];
					if(dateDiff.equalsIgnoreCase("monthly")){
						misDate=getdate("month");
					}else if(dateDiff.equalsIgnoreCase("weekly")){
						misDate=getdate("week");
					}
					else{
						misDate=getdate("week");
					}					
				} else {
					misDate=getdate("month");
				}
				//misDate = getdate(dateDiff);
				LOGGER.debug("Process going to execute for date [" + misDate + "]");
				CollectAuditData collectYaariAuditData = new CollectAuditData();
				List<AuditDataModel> collectAuditDataList = collectYaariAuditData.arrangeData();
				CreateAuditDataExcel createAuditData = new CreateAuditDataExcel();
				createAuditData.insertExcelRow(collectAuditDataList);
				String excelPath = createAuditData.saveExcels(misDate);
				MailHelper mailHelper = new MailHelper();
				mailHelper.sendMailRequest(excelPath);
				LOGGER.info("program is end..");
				System.exit(0);
			} else {
			}
		} catch (Exception e) {
			LOGGER.error(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		}

	}
	private boolean getLock() {
		boolean lock = false;
		try {
			RandomAccessFile file = new RandomAccessFile("./yaariAuditMis1.tmp", "rw");
			FileChannel fileChannel = file.getChannel();
			FileLock fileLock = fileChannel.tryLock();
			if (fileLock != null) {
				LOGGER.debug("File is locked. Starting the execution of the Application");
				lock = true;
			} else {
				LOGGER.error("Cannot create lock. Already another instance of the program is running.");
				LOGGER.error("System shutting down. Please wait...");
				try {
					Thread.sleep(1000 * 3);
				} catch (Exception exp) {
					exp.printStackTrace();
				}
				System.exit(0);
			}
		} catch (Exception exp) {
			exp.printStackTrace();
			LOGGER.debug(LogStackTrace.getStackTrace(exp));
		}
		return lock;
	}

	private boolean accessFile() {
		boolean retCode = false;
		try {
			String line = "";
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter("./yaariAuditMis1.tmp", false));
				bw.close();
			} catch (Exception exp) {
				exp.printStackTrace();
			}
			retCode = true;
		} catch (Exception e) {
			System.out.println(e);
			LOGGER.debug(LogStackTrace.getStackTrace(e));
		}
		LOGGER.debug("accessFile() = " + retCode);
		return retCode;
	}

	public static String getdate(String type) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");		
		String strDate=null;
		if(type.equalsIgnoreCase("month")){
			cal.set(Calendar.DATE, 1);					
			cal.add(Calendar.DAY_OF_MONTH, -1);
			strDate = formatter.format(cal.getTime());		
		}else if(type.equalsIgnoreCase("week")){
			cal.add(Calendar.DATE, -1);
			strDate = formatter.format(cal.getTime());
		}
		else{
			cal.add(Calendar.DATE, -1);
			strDate = formatter.format(cal.getTime());
		}
		return strDate;
	}


}
