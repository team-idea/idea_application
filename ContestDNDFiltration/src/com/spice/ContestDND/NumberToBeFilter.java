package com.spice.ContestDND;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class NumberToBeFilter {

	private static final Logger LOGGER = Logger.getLogger(NumberToBeFilter.class);
	private DBConnectionManager connectionManager = null;
	private Connection connection = null;
	private Statement statement = null;
	private ResultSet resultSet = null;
	private String query = null;
	private BufferedWriter writer = null;
	private String path = null;
	private String filePath = null;
	private String loadBaseFile = null;
	private Properties prop = null;
	private String circle = null;

	public NumberToBeFilter() {
		PropertyConfigurator.configure("./log4j.properties");
		prop = new Properties();
		try {
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			filePath = prop.getProperty("filePath");
		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		}
		connectionManager = DBConnectionManager.getInstance();
	}

	public void getContestNumber(String circle, String date) throws SQLException, IOException {
		this.circle = circle;
		try {
			writer = new BufferedWriter(new FileWriter(filePath + circle + "_" + date + ".csv"));
			LOGGER.info("file is created at-->["+filePath + circle + "_" + date + ".csv"+"]");
			//connection = connectionManager.getConnection("DND_POOL_" + circle);
			connection = connectionManager.getConnection("DND_POOL");
			LOGGER.info("Connection with -->[DND_POOL] and connection id ["+connection+"]");
			query = "select msisdn from national_contest_" + circle + ".tbl_user_info where status='1' and engagement_level not in (1,2,3,6,7,-100,-200,-300);";
			statement = connection.createStatement();
			resultSet = statement.executeQuery(query);
			int i = 1;
			while (resultSet.next()) {
				String msisdn = resultSet.getString("msisdn");
				//System.out.println("circleName [" + circle + "]line number [" + i + "] msisdn is [" + msisdn + "]");
				writer.write(msisdn);
				// if(msisdn){
				writer.newLine();
				// }
				i++;
			}
			LOGGER.info("process write ["+i+"] lines");
			writer.close();
			
			FileLoadDndSystem FileLoadDndSystem = new FileLoadDndSystem();
			String loadBaseFile = FileLoadDndSystem.fileLoaderInDb(circle, filePath + circle + "_" + date + ".csv",date);

			query = "LOAD DATA LOCAL INFILE '" + loadBaseFile + "' IGNORE INTO TABLE national_contest_" + circle + ".tbl_dnd_" + circle + " LINES TERMINATED BY '\n' (msisdn)";
			LOGGER.info("file["+loadBaseFile+"] is loaded in [national_contest_" + circle + ".tbl_dnd_" + circle + "] table");
			int loadLines=statement.executeUpdate(query);
			LOGGER.info("["+loadLines+"] row loaded");

							
			query = "update national_contest_" + circle + ".tbl_user_info set engagement_level='7' where msisdn in (select msisdn from national_contest_"+ circle + ".tbl_dnd_" + circle + ");";
			int updateNumberWithSeven=statement.executeUpdate(query);
			LOGGER.info(query +"has been executed and ["+updateNumberWithSeven+"] rows affected");
			
			query = "update national_contest_" + circle + ".tbl_user_info set engagement_level='0' where engagement_level='10';";			
			int updateNumber=statement.executeUpdate(query);
			LOGGER.info(query +"has been executed and ["+updateNumber+"] rows affected");
			

			query = "truncate table national_contest_" + circle + ".tbl_dnd_" + circle + ";";
			statement.execute(query);
			LOGGER.info(query +"has been executed");
			

			System.out.println("END... ");
		} catch (Exception ex) {
			AlertMessage alertMessage= new AlertMessage();
			alertMessage.sendAlert(circle);
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		} finally {
			if(null!=connection){
				connectionManager.freeConnection("DND_POOL", connection);
			}
			if(null!=statement){
				statement.close();
			}
			if(null!=resultSet){
				resultSet.close();
			}
			if(null!=writer){
				writer.close();
			}
		}
	}
}
