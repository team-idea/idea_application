package com.spice.ContestDND;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class FileLoadDndSystem {
	private static final Logger LOGGER=Logger.getLogger(NumberToBeFilter.class);
	private DBConnectionManager connectionManager=null;
	private Connection conn=null;
	private Statement stmt=null;
	private ResultSet rSet=null;
	private String query=null;
	private BufferedWriter writer =null;
	private String path=null;
	private String filePath=null;
	private String loadBaseFile=null;
	private Properties prop = null;
	private String circle=null;
	private String fileProcessedPath=null;
	
	public FileLoadDndSystem() {
		PropertyConfigurator.configure("./log4j.properties");
		prop = new Properties();
		try {
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			fileProcessedPath = prop.getProperty("fileProcessedPath");
		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
			ex.printStackTrace();
		}
		connectionManager = DBConnectionManager.getInstance();
	}
	
	public String fileLoaderInDb(String circle,String loadBaseFile,String date) throws SQLException, IOException{
	try{
		this.circle=circle;
//		System.out.println("circle in filter class-->"+circle);
	//	System.out.println("filePath is -->"+filePath);
		//writer=new BufferedWriter(new FileWriter(filePath+circle+"_"+date+".csv"));		
		//loadBaseFile=baseFileFilterModel.getUploadFilePath()+baseFileFilterModel.getUploadFileName();
		//LOGGER.debug("Table name is ["+tableName+"] loadBaseFile ["+loadBaseFile+"]");
		//try{
		conn=connectionManager.getConnection("DND_POOL_MAIN");
		stmt=conn.createStatement();
		query="truncate table dnd.tbl_dnd_"+circle+"";
		stmt.execute(query);
		LOGGER.info(query +"has been executed");
		
		
		query="LOAD DATA LOCAL INFILE '"+loadBaseFile+"' IGNORE INTO TABLE dnd.tbl_dnd_"+circle+" LINES TERMINATED BY '\n' (msisdn)";
		LOGGER.info("file["+loadBaseFile+"] is loaded in [dnd.tbl_dnd_"+circle + "] table");
		int loadLines=stmt.executeUpdate(query);
		LOGGER.info("["+loadLines+"] row loaded");

		writer=new BufferedWriter(new FileWriter(fileProcessedPath+circle+"_"+date+".csv"));		
		query="select msisdn from dnd.tbl_dnd_"+circle+" where msisdn in(select msisdn from dnd.tbl_dnd_base where opstype='A')";
		rSet=stmt.executeQuery(query);
		
		int i=1;
		while(rSet.next()){
			String msisdn=rSet.getString("msisdn");
			System.out.println("circleName ["+circle+"]line number ["+i+"] msisdn is ["+msisdn+"]");
			writer.write(msisdn);
			//if(msisdn){
			writer.newLine();
			//}
			i++;
		}
		LOGGER.info("dnd count ["+loadLines+"] in ["+circle+" circle from DND Server]");
		writer.close();
		
	}catch(Exception ex){
		AlertMessage alertMessage= new AlertMessage();
		alertMessage.sendAlert(circle);
		ex.printStackTrace();
		LOGGER.debug(LogStackTrace.getStackTrace(ex));
	}finally{
		if(null!=conn){
			connectionManager.freeConnection("DND_POOL_MAIN", conn);
		}
		if(null!=stmt){
			stmt.close();
		}			
		if(null!=rSet){
			rSet.close();
		}
		if(null!=writer){
			writer.close();
		}
	}
	return fileProcessedPath+circle+"_"+date+".csv";
  }
}