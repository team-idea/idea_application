package com.spice.ContestDND;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


public class ContestDNDFiltrationMain {
	private static final Logger LOGGER = Logger.getLogger(ContestDNDFiltrationMain.class);
	private static String[] circleName = null;
	private static String circleNames = null;

	public ContestDNDFiltrationMain() {
		PropertyConfigurator.configure("./log4j.properties");
		Properties prop = new Properties();
		try {
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			circleNames = prop.getProperty("circleNames");
			System.out.println("circleNames is circleNames[" + circleNames + "]");
		} catch (Exception ex) {
			System.out.println("exception");
			ex.printStackTrace();
		}

	}

	public static void main(String[] args) throws Exception {
		ContestDNDFiltrationMain contestDNDFiltrationMain = new ContestDNDFiltrationMain();
		try {
			if (contestDNDFiltrationMain.accessFile() && contestDNDFiltrationMain.getLock()) {
				LOGGER.info("program is started..");
					NumberToBeFilter numberToBeFilter = new NumberToBeFilter();
					circleName = circleNames.split("[,]+");
					for (String circle : circleName) {
						numberToBeFilter.getContestNumber(circle, getDate());
					}
					Thread.sleep(1000);					
			} else {
			}
		} catch (Exception e) {
			LOGGER.error(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		}
	}

	public static String getDate() {
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -0);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String strDate = formatter.format(cal.getTime());
		LOGGER.debug("date return[" + strDate.toString() + "]");
		return strDate;
	}

	private boolean getLock() {
		boolean lock = false;
		try {
			RandomAccessFile file = new RandomAccessFile("./contestDND.tmp", "rw");
			FileChannel fileChannel = file.getChannel();
			FileLock fileLock = fileChannel.tryLock();
			if (fileLock != null) {
				LOGGER.debug("File is locked. Starting the execution of the Application");
				lock = true;
			} else {
				LOGGER.error("Cannot create lock. Already another instance of the program is running.");
				LOGGER.error("System shutting down. Please wait...");
				try {
					Thread.sleep(1000 * 3);
				} catch (Exception exp) {
					exp.printStackTrace();
				}
				System.exit(0);
			}
		} catch (Exception exp) {
			exp.printStackTrace();
			LOGGER.debug(LogStackTrace.getStackTrace(exp));
		}
		return lock;
	}

	private boolean accessFile() {
		boolean retCode = false;
		try {
			String line = "";
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter("./contestDND.tmp", false));
				bw.close();
			} catch (Exception exp) {
				exp.printStackTrace();
			}
			retCode = true;
		} catch (Exception e) {
			System.out.println(e);
			LOGGER.debug(LogStackTrace.getStackTrace(e));
		}
		LOGGER.debug("accessFile() = " + retCode);
		return retCode;
	}

}
