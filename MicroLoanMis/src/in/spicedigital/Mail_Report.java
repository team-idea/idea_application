package in.spicedigital;
import java.io.*;
import javax.mail.*;
import javax.mail.event.*;
import javax.mail.internet.*;
import java.util.*;
import javax.activation.*;
import java.net.InetAddress;
import java.util.Properties;
import java.util.Date;
import java.text.SimpleDateFormat;
import org.apache.log4j.*;
import java.sql.*;
import java.net.*;
import java.lang.*;

public class Mail_Report
{

static Logger logger = Logger.getLogger(Mail_Report.class.getName());
	static Properties props = System.getProperties();
	static Properties objProperty =null;
	static int noOfDaysBack = 0;

	File fileDel = null, delFile = null;
	static {

try{
		objProperty = new Properties();
				 FileInputStream in = new FileInputStream(new File(".\\config\\config.properties"));
				logger.info("Reading the configuration file");
		         	 objProperty.load(in);

	}catch(Exception e){}
}


public static boolean sendMessage(String subject,String attachment,String SendMsg,int noOfDaysBack)
	{
		String mailhost ="",mailer ="";
		String protocol = null, host = null, user = null;

		String sender = objProperty.getProperty("sender").trim();
		//String receiver = ".org";
		String receiver =  objProperty.getProperty("recevierTo").trim();
		String receiver1 =  objProperty.getProperty("recevierCc").trim();
		String receiverBcc =  objProperty.getProperty("recevierBcc").trim();

		String message = SendMsg;
		//System.out.println(SendMsg);
		Date date = new Date();
       	date.setDate(date.getDate()-noOfDaysBack);
       	SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");
		String strDate = sdf.format(date);
		message =SendMsg.replace("???",strDate);

		System.out.println(strDate);

		/**** Connections for Mail ****/
		mailhost = "smtp.gmail.com";   /********* This id our Mail Ip **********/
		//mailhost="172.21.13.48";
		mailer = "msgsend";
		String port="587";
		/********* This id our Mail Ip **********/
		props.setProperty("mail.host", mailhost);
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", port);
		props.put("mail.smtp.starttls.enable", "true");
		final String kmpid =objProperty.getProperty("kmpid").trim();
		final String password = objProperty.getProperty("password").trim();

		Session session			= Session.getDefaultInstance(props,new javax.mail.Authenticator()
						  {
								protected javax.mail.PasswordAuthentication getPasswordAuthentication()
								{
									return new javax.mail.PasswordAuthentication(kmpid,password);
								}
					   });



		//Session session = Session.getDefaultInstance(props, null);

		try
		{
			Message msg = new MimeMessage(session);
			System.out.println("SEND MAIL:\tStarting Send Mail Program");
			System.out.println("SEND MAIL:\tSender: "+sender);
			System.out.println("SEND MAIL:\tTo: "+receiver);
			System.out.println("SEND MAIL:\tCC: "+receiver1);
			System.out.println("SEND MAIL:\tCC: "+receiverBcc);
			//System.out.println("SEND MAIL:\tCC: "+receiver2);

			System.out.println("SEND MAIL:\tSubject: "+subject);
			InternetAddress from = new InternetAddress(sender);
			msg.setFrom(from);


			subject = subject ;

			java.util.StringTokenizer stzr=new java.util.StringTokenizer(receiver,"~");
			java.util.StringTokenizer stzr1=new java.util.StringTokenizer(receiver1,"~");
			java.util.StringTokenizer stzr2=new java.util.StringTokenizer(receiverBcc,"~");
			//java.util.StringTokenizer stzr2=new java.util.StringTokenizer(receiver2,",");

			while(stzr.hasMoreTokens())
			{
				InternetAddress[] address ={new InternetAddress(stzr.nextToken())};
				msg.addRecipients(Message.RecipientType.TO, address);
				//msg.addRecipients(Message.RecipientType.CC, address);
			}

			while(stzr1.hasMoreTokens())
			{
				InternetAddress[] address1 ={new InternetAddress(stzr1.nextToken())};
				msg.addRecipients(Message.RecipientType.CC, address1);
				//msg.addRecipients(Message.RecipientType.CC, address);
			}
			while(stzr2.hasMoreTokens())
				{
				InternetAddress[] address2 ={new InternetAddress(stzr2.nextToken())};
				msg.addRecipients(Message.RecipientType.BCC, address2);
							//msg.addRecipients(Message.RecipientType.CC, address);
				}

			/*while(stzr2.hasMoreTokens())
			{
				InternetAddress[] address2 ={new InternetAddress(stzr2.nextToken())};
				//msg.addRecipients(Message.RecipientType.TO, address1);
				msg.addRecipients(Message.RecipientType.CC, address2);
			}*/
			while(stzr.hasMoreTokens())
			{
				InternetAddress[] address1 ={new InternetAddress(stzr.nextToken())};
				msg.addRecipients(Message.RecipientType.CC, address1);
			}


			msg.setSubject(subject);
			msg.setContent(message, "text/plain");

			MimeMultipart mp = new MimeMultipart();

			MimeBodyPart text = new MimeBodyPart();
			text.setDisposition(Part.INLINE);
			text.setContent(message, "text/html");
			mp.addBodyPart(text);




			String line="";

			stzr=new java.util.StringTokenizer(attachment,",");
			while(stzr.hasMoreTokens())
			{
				line=stzr.nextToken();
				mp=attachFile(mp,line);
			}

			msg.setContent(mp);
 			Transport transport = session.getTransport("smtp");
            transport.connect(mailhost, kmpid,password);
			Transport.send(msg);
			System.out.println("SEND MAIL:\tFinish Mail");
			return true;
		}
		catch (MessagingException mex)
		{
			System.out.println("SEND MAIL:\t------------------ Error -------------");
			mex.printStackTrace();
			System.out.println("SEND MAIL:\tFinish Mail");

			return false;
		}
   	}
	public static MimeMultipart attachFile(MimeMultipart mp,String path)
	{
		try
		{
			File file=new File(path);
			if(!file.exists())
			{
				System.out.println("SEND MAIL:\tFile doesnot exist: "+path);
				System.out.println("SEND MAIL:\tmp:"+mp);
				System.exit(0);
				//Alert if file does'nt exist
				//sendAlert("Error When file doesnt exist","919814013147");

				return mp;
			}
			else

			System.out.println("SEND MAIL:\tAttaching file: "+path);
			MimeBodyPart file_part = new MimeBodyPart();
			FileDataSource fds = new FileDataSource(file);
			DataHandler dh = new DataHandler(fds);
			file_part.setFileName(file.getName());
			file_part.setDisposition(Part.ATTACHMENT);
			file_part.setDescription("SEND MAIL:\tAttached file: " + file.getName());
			file_part.setDataHandler(dh);
			mp.addBodyPart(file_part);
			return mp;
		}
		catch(Exception e)
		{
			System.out.println("SEND MAIL:\tError while attaching File : "+path);

			return mp;
		}
	}

	/*public static void Main(String args[])

	{

		noOfDaysBack = Integer.parseInt(args[0]);
		try
		{
			Mail_Report objsend = new Mail_Report();
			objsend.sendMessage(objProperty.getProperty("subject").trim(),generateFileName(),objProperty.getProperty("body").trim(),noOfDaysBack);

		}
		catch(Exception es)
		{
			System.out.println("ERROR :"+es.toString());
		}
	}
*/	public static String generateFileName(){
		Calendar cal = null;
		String month = null;

		try{			String[] monthName = {"Jan", "Feb","Mar", "Apr", "May", "June", "July","Aug", "Sep", "Oct", "Nov","Dec"};
						java.util.Date d = new java.util.Date();
						d.setDate(d.getDate()-noOfDaysBack);
						 cal = Calendar.getInstance();
						cal.setTime(d);
						month = monthName[cal.get(Calendar.MONTH)];
						System.out.println("Month name: " + month);
						System.out.println(cal.get(Calendar.YEAR));

							return objProperty.getProperty("fileName").trim()+"_"+month+"\'"+cal.get(Calendar.YEAR)+".xls";
		}catch(Exception e){
					return objProperty.getProperty("fileName").trim()+"_"+month+"\'"+cal.get(Calendar.YEAR)+".xls";

								}
}



}

