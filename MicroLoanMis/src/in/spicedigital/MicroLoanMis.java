package in.spicedigital;

import java.io.*;
import java.util.*;
import java.text.*;
import jxl.*;
import jxl.format.*;
import jxl.write.Label;
import jxl.write.*;
import jxl.write.Number;
import java.sql.*;
import java.util.Date;
import org.apache.log4j.*;

public class MicroLoanMis {

    static Logger logger = Logger.getLogger(MicroLoanMis.class.getName());
    static int noOfDaysBack = 1;
    static int totalCallsCount = 0;
    static int totalSuccCallsCount = 0;
    static String[] monthName = {"Jan", "Feb", "Mar", "Apr", "May", "June", "July", "Aug", "Sep", "Oct", "Nov", "Dec"};
    static HashSet hs = new HashSet();
    public static WritableCellFormat timesBoldHeading = null;
    public static Properties objProperty = null;
    public static WritableSheet sheet = null;
    public static WritableWorkbook workbook = null;
    public static jxl.format.CellFormat cfnum = null;
    public static WritableCellFormat headerCellFormat = null;
    public static WritableCellFormat headerCellFormat1 = null;
    public static WritableCellFormat headerCellFormat2 = null;
    public static WritableCellFormat headerCellFormat3 = null;
    public static WritableCellFormat dataCellFormat = null;
    public static WritableCellFormat dateFormat = null;
    public static String xlFileName = null;
    public static Date date;
    public static boolean data = false;
    static {

        try {
            objProperty = new Properties();
            FileInputStream in = new FileInputStream(new File(".\\config\\config.properties"));
            logger.info("Reading the configuration file");
            objProperty.load(in);
        } catch (Exception e) {
        }
    }

    //@SuppressWarnings("deprecation")
	public static void main(String args[]) throws Exception {
        if(args.length<1)
        {
        	noOfDaysBack = 1;
           // date = new java.util.Date();
            //date.setDate(date.getDate() - noOfDaysBack);	
        }
        else
        {
	    	noOfDaysBack = Integer.parseInt(args[0].trim());
	        //date = new java.util.Date();
	        //date.setDate(date.getDate() - noOfDaysBack);
        }
        String query = "SELECT COUNT(1) CNT FROM [loan_ivr].[DBO].[tbl_mis_daily_loan_new] WITH(NOLOCK) WHERE STATUS=0 AND CONVERT(VARCHAR,MIS_DATE,112)=CONVERT(VARCHAR,GETDATE()-" + noOfDaysBack + ",112)";
        logger.info("query is:"+query);
        System.out.println(query);
        ResultSet rs = Connections.GetConnection("P2A").createStatement().executeQuery(query);
        int cnt = 0;
        while (rs.next()) {
            cnt = rs.getInt("cnt");
            logger.info("Count is --" + cnt);
        }
        if (cnt != 0) {
            GenerateMis();
            if (data) {
                if (sendMail()) {
                    logger.info("Update query");
                    query = "UPDATE [loan_ivr].[DBO].[tbl_mis_daily_loan_new] SET STATUS=1 WHERE CONVERT(VARCHAR,MIS_DATE,112)=CONVERT(VARCHAR,GETDATE()-" + noOfDaysBack + ",112)";
                    Connections.GetConnection("P2A").createStatement().executeUpdate(query);
                }
            }
        } else {
            System.exit(0);
        }

    }

    public static boolean sendMail() {
        Mail_Report mr = new Mail_Report();
        return mr.sendMessage(objProperty.getProperty("subject").trim(), xlFileName, objProperty.getProperty("body").trim(), noOfDaysBack);

    }

    public static void GenerateMis() throws Exception {

//####################Writing data to XL sheet ########################
        generateXLSheet();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        int column = 1;
        int row = 1;

        sheet.setColumnView(1, 15);
        sheet.setColumnView(2, 18);
        sheet.setColumnView(3, 22);
        sheet.setColumnView(4, 10);
        sheet.setColumnView(5, 10);
        sheet.setColumnView(6, 10);
        sheet.setColumnView(7, 10);
        sheet.setColumnView(8, 10);
        sheet.setColumnView(9, 10);
        sheet.setColumnView(10, 10);
        sheet.setColumnView(11, 10);
        sheet.setColumnView(12, 10);
        sheet.setColumnView(13, 10);
        
        sheet.setColumnView(14, 10);
        sheet.setColumnView(15, 10);
        sheet.setColumnView(16, 10);
        sheet.setColumnView(17, 10);
        
        sheet.mergeCells(1, 1, 14, 1);
        sheet.mergeCells(2, 2, 5, 2);
        sheet.mergeCells(11, 2, 12, 2);
        sheet.mergeCells(13, 2, 14, 2);
        
        sheet.mergeCells(15, 2, 16, 2);
        sheet.mergeCells(17, 2, 18, 2);
        //sheet.mergeCells(19, 2, 20, 2);
        //sheet.setRowView(2,20);
        Label head = new Label(column, row++, "IVR Loan-PB MIS NEW (" + monthName[cal.get(Calendar.MONTH)] + "-" + cal.get(Calendar.YEAR) + ")", headerCellFormat3);
        sheet.addCell(head);
        head = new Label(column++, row, " ", headerCellFormat1);
        sheet.addCell(head);
        head = new Label(column++, row, "Circle", headerCellFormat1);
        sheet.addCell(head);
        column++;
        column++;
        column++;
        head = new Label(column++, row, "AON Criteria Failed", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, "Balance  Failed", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, "IPP Failed", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, "Other Fail", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, "Total Fail", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, "Spice Loan 5",headerCellFormat2);
        sheet.addCell(head);
        column++;
        head = new Label(column++, row, "Spice Loan 10",headerCellFormat2);
		sheet.addCell(head);
        column++;
        head = new Label(column++, row, "Spice Loan 20",headerCellFormat2);
		sheet.addCell(head);
        column++;
        head = new Label(column++, row, "SMS Loan",headerCellFormat2);
		sheet.addCell(head);
        column++;
        //head = new Label(column++, row, "Data Loan",headerCellFormat2);
		//sheet.addCell(head);
        //column++;
        row++;
        column = 1;
        head = new Label(column++, row, "Date ", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, "Total Calls", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, "Total Unique Calls", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, "Total Success", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, "AHT", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, "Act Info Failed", headerCellFormat2);
		sheet.addCell(head);
		head = new Label(column++, row, " ", headerCellFormat2);
		sheet.addCell(head);
		head = new Label(column++, row, " ", headerCellFormat2);
		sheet.addCell(head);
		head = new Label(column++, row, " ", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, " ", headerCellFormat2);
        sheet.addCell(head);
       	head = new Label(column++, row, "Request", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, "Success", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, "Request", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, "Success", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, "Request", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, "Success", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, "Request", headerCellFormat2);
        sheet.addCell(head);
        head = new Label(column++, row, "Success", headerCellFormat2);
        //sheet.addCell(head);
        //head = new Label(column++, row, "Request", headerCellFormat2);
		//sheet.addCell(head);
		//head = new Label(column++, row, "Success", headerCellFormat2);
        sheet.addCell(head);
        row++;
        column = 1;
        jxl.write.Number num = null;
        String query = "SELECT REPLACE(CONVERT(VARCHAR(11), MIS_DATE, 106), ' ', '-')  AS [MIS_DATE],[TOTAL_CALLS],[TOTAL_UNIQUE_CALLERS],[TOTAL_SUCCESS],round(total_aht,2) as TOTAL_AHT,TOTAL_AONFAILED,TOTAL_BALFAILED,TOTAL_IPPFAILED,TOTAL_OTHERFAILED,TOTAL_FAILED,TOTAL_LOAN5REQUEST,TOTAL_LOAN5SUCC,TOTAL_LOAN10REQUEST,TOTAL_LOAN10SUCC,TOTAL_LOAN20REQUEST,TOTAL_LOAN20SUCC,TOTAL_LOANSMSREQUEST,TOTAL_LOANSMSSUCC FROM [loan_ivr].[DBO].[tbl_mis_daily_loan_new] with(nolock) where datepart(year,mis_date)=datepart(year,getdate()-" + noOfDaysBack + ") AND datepart(month,mis_date)=datepart(month,getdate()-" + noOfDaysBack + ") and convert(varchar,mis_date,112)<=convert(varchar,getdate()-" + noOfDaysBack + ",112) order by mis_date asc";
        logger.info(query);

        ResultSet rs = Connections.GetConnection("P2A").createStatement().executeQuery(query);
        while (rs.next()) {
            data = true;
            logger.info("mis_date" + rs.getString("mis_date"));
            head = new Label(column++, row, rs.getString("mis_date"), headerCellFormat2);
            sheet.addCell(head);
            logger.info("TotalCalls");
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("TOTAL_CALLS")), headerCellFormat);
            sheet.addCell(num);
            logger.info("Total_unique_calls");
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("TOTAL_UNIQUE_CALLERS")), headerCellFormat);
            sheet.addCell(num);
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("TOTAL_SUCCESS")), headerCellFormat);
            sheet.addCell(num);
            //NumberFormat twodps = new NumberFormat("#.##");
            //WritableCellFormat cf2 = new WritableCellFormat(twodps);
            num = new jxl.write.Number(column++, row, Double.parseDouble(rs.getString("TOTAL_AHT")), headerCellFormat);
            sheet.addCell(num);
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("TOTAL_AONFAILED")), headerCellFormat);
            sheet.addCell(num);
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("TOTAL_BALFAILED")), headerCellFormat);
            sheet.addCell(num);
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("TOTAL_IPPFAILED")), headerCellFormat);
            sheet.addCell(num);
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("TOTAL_OTHERFAILED")), headerCellFormat);
            sheet.addCell(num);
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("TOTAL_FAILED")), headerCellFormat);
            sheet.addCell(num);
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("TOTAL_LOAN5REQUEST")), headerCellFormat);
            sheet.addCell(num);
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("TOTAL_LOAN5SUCC")), headerCellFormat);
            sheet.addCell(num);
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("TOTAL_LOAN10REQUEST")), headerCellFormat);
            sheet.addCell(num);
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("TOTAL_LOAN10SUCC")), headerCellFormat);
            sheet.addCell(num);
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("TOTAL_LOAN20REQUEST")), headerCellFormat);
            sheet.addCell(num);
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("TOTAL_LOAN20SUCC")), headerCellFormat);
            sheet.addCell(num);
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("TOTAL_LOANSMSREQUEST")), headerCellFormat);
            sheet.addCell(num);
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("TOTAL_LOANSMSSUCC")), headerCellFormat);
            sheet.addCell(num);

            /*num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("total_request_20")), headerCellFormat);
            sheet.addCell(num);
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("total_success_20")), headerCellFormat);
            sheet.addCell(num);

            logger.info("SET_IND_FAIL");
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("total_request_sms")), headerCellFormat);
            sheet.addCell(num);
            logger.info("SET_IND_SUCC");
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("total_success_sms")), headerCellFormat);
            sheet.addCell(num);
            logger.info("DEBT_AMT_FAIL");
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("total_request_data")), headerCellFormat);
            sheet.addCell(num);
            logger.info("BAL_UPDATE_FAIL");
            num = new jxl.write.Number(column++, row, Long.parseLong(rs.getString("total_success_data")), headerCellFormat);
            sheet.addCell(num);*/

            column = 1;
            row++;

        }

        workbook.write();
        workbook.close();
    }

    public static void generateXLSheet() {
        WritableSheet s = null;

        java.util.Date d = new java.util.Date();
        d.setDate(d.getDate() - noOfDaysBack);

        WorkbookSettings ws = new WorkbookSettings();
        ws.setLocale(new Locale("en", "EN"));
        SimpleDateFormat sd = new SimpleDateFormat("MMM-yy");
        xlFileName = objProperty.getProperty("MisFileName").trim() + " " + sd.format(d) + ".xls";
        File xlSheetName = new File(xlFileName);
        logger.info(xlSheetName);
        Workbook wb = null;
        //creating the sheet if not exist's
        if (!xlSheetName.exists()) {
            logger.info("creating the work book");
            try {
                workbook = Workbook.createWorkbook(xlSheetName, ws);
            } catch (Exception e) {
                logger.info("Exception while createing workbook");
            }
        }

        sheet = workbook.createSheet(monthName[d.getMonth()], 0);

        try {
            WritableFont boldFont = new WritableFont(WritableFont.createFont("Arial"), 10, WritableFont.NO_BOLD);
            headerCellFormat = new WritableCellFormat(boldFont);
            headerCellFormat.setAlignment(jxl.format.Alignment.CENTRE);
            headerCellFormat.setShrinkToFit(true);
            headerCellFormat.setWrap(false);
            headerCellFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);

        } catch (Exception e) {
        }

        try {
            WritableFont boldFont = new WritableFont(WritableFont.createFont("Arial"), 10, WritableFont.BOLD);
            headerCellFormat1 = new WritableCellFormat(boldFont);
            headerCellFormat1.setAlignment(jxl.format.Alignment.CENTRE);
            headerCellFormat1.setShrinkToFit(false);
            headerCellFormat1.setWrap(true);
            headerCellFormat1.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
            headerCellFormat1.setBackground(jxl.format.Colour.YELLOW);

        } catch (Exception e) {
        }


        try {
            WritableFont boldFont = new WritableFont(WritableFont.createFont("Arial"), 10, WritableFont.BOLD);
            headerCellFormat2 = new WritableCellFormat(boldFont);
            headerCellFormat2.setAlignment(jxl.format.Alignment.CENTRE);
            headerCellFormat2.setShrinkToFit(true);
            headerCellFormat2.setWrap(true);
            headerCellFormat2.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
            headerCellFormat2.setBackground(jxl.format.Colour.YELLOW);

        } catch (Exception e) {
        }
        try {
            WritableFont boldFont = new WritableFont(WritableFont.createFont("Arial"), 15, WritableFont.BOLD);
            headerCellFormat3 = new WritableCellFormat(boldFont);
            headerCellFormat3.setAlignment(jxl.format.Alignment.CENTRE);
            headerCellFormat3.setShrinkToFit(true);
            headerCellFormat3.setWrap(false);
            headerCellFormat3.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);

        } catch (Exception e) {
        }

        try {
            WritableFont boldFont = new WritableFont(WritableFont.createFont("Arial"), 10, WritableFont.BOLD);
            dataCellFormat = new WritableCellFormat(boldFont);
            dataCellFormat.setAlignment(jxl.format.Alignment.CENTRE);
            dataCellFormat.setShrinkToFit(true);
            dataCellFormat.setWrap(false);
            dataCellFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
            dataCellFormat.setBackground(jxl.format.Colour.WHITE);
        } catch (Exception e) {
        }
        try {
            jxl.write.DateFormat customDateFormat = new jxl.write.DateFormat("dd-MMM-yy");
            dateFormat = new WritableCellFormat(customDateFormat);
            dateFormat.setAlignment(jxl.format.Alignment.CENTRE);
            dateFormat.setShrinkToFit(true);
            dateFormat.setWrap(true);
            dateFormat.setBorder(jxl.format.Border.ALL, jxl.format.BorderLineStyle.THIN);
        } catch (Exception e) {
        }

    }
}