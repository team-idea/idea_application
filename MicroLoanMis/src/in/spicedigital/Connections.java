package in.spicedigital;
import java.sql.*;
import java.io.*;
import java.net.*;
import java.util.*;
import java.util.logging.Logger;

import org.apache.log4j.*;

public class Connections extends Thread
{
	static final int max=20;
	static String strConServiceName[] =new String[max];
	static String strConIP[]	= new String[max];
	static String strPort[]		= new String[max];
	static String strConUser[]	= new String[max];
	static String strConPwd[]	= new String[max];
	static String strConSID[]	= new String[max];
	static Connection Conns[]	= new Connection[max];
	static String  DB_TYPE[]	= new String[max];
	static int actualConCount=0;
	static String configFile=".//Config//Configuration.cfg";
	static Logger logger = Logger.getLogger(Connections.class.getName());

	Connections(String cfgSrc)
	{
		configFile=cfgSrc;

	}
	Connections()
	{
		configFile=".//Config//Configuration.cfg";
	}
	static
	{

		try
				{
					logger.info("Initalizing connections...");
					Connections cfg=new Connections();
					cfg.ReadConfig();
					logger.info("***************************************************************************\nConnections set.\n***************************************************************************");

				}
				catch(Exception ex)
				{
					logger.info("Exception while running Config thread."+ex);
		}





	}
	public void run()
	//public static void main(String ar[])
	{
		try
		{
			logger.info("Initalizing connections...");
			Connections cfg=new Connections();
			cfg.ReadConfig();
			logger.info("***************************************************************************\nConnections set.\n***************************************************************************");

		}
		catch(Exception ex)
		{
			logger.info("Exception while running Config thread."+ex);
		}
	}

	static void ReadConfig()
	{
		try
		{
			//Read "Connections.cfg" file from the "Config" Folder placed in the current Folder
			File conFile = new File(configFile);
			BufferedReader brCon = new BufferedReader(new FileReader(conFile));
			String line="";

			//To count number of connections
			int iConCount=0;
			boolean conn_flag=false;

			while((line=brCon.readLine())!=null)
			{
				line=line.trim();

				if(line.startsWith("*")||line.startsWith("//")||line.length()<5)
				{
					//Skip the line in case of comment or length<5
					//logger.info("*");
					continue;
				}

				if(line.toUpperCase().equals("CONNECTION_SETTING"))
				{
					logger.info("*~");
					conn_flag=true;
					continue;
				}
				else if(line.toUpperCase().equals("END_CONNECTION_SETTING"))
				{
					logger.info("~*");
					conn_flag=false;
					break;
				}

				if(conn_flag)
				{
					StringTokenizer tokenizerString = new StringTokenizer (line,"~");
					//while(tokenizerString.hasMoreTokens())

					int nTkn=tokenizerString.countTokens();

					logger.info("No. of tokens are: "+nTkn);
					if(nTkn==7)
					{
						if(iConCount>max-1)
						{
							logger.info("Maximum limit exceeded :check \"Config\\Configuration.cfg\" file");
							System.exit(1);
						}
						strConServiceName[iConCount]=tokenizerString.nextToken().trim();
						strConIP[iConCount] 	= tokenizerString.nextToken().trim();
						strPort[iConCount] 		= tokenizerString.nextToken().trim();
						strConUser[iConCount]	= tokenizerString.nextToken().trim();
						strConPwd[iConCount] 	= tokenizerString.nextToken().trim();
						strConSID[iConCount] 	= tokenizerString.nextToken().trim();
						DB_TYPE[iConCount] 		= tokenizerString.nextToken().trim();

						logger.info(strConServiceName[iConCount]+"\t"+strConIP[iConCount] +"\t"+strPort[iConCount]+"\t"+strConUser[iConCount] +"\t"+strConPwd[iConCount]+"\t"+strConSID[iConCount]);
						iConCount++;
						actualConCount=iConCount;
					}
					else
					{
						logger.info("Number of tokens in  \"Configuration.cfg\" file are not correct");
					}
				}
				else
				{
					logger.info("You are not in connection setting block");
				}
			}//End of file reading while block

			//Initializes all the connections
			initConn();
		}
		catch(Exception ex)
		{
			logger.info("Exception while reading Config file."+ex);
		}
	}

	static void initConn()
	{
		for(int c=0;c<actualConCount;c++)
		{
			Connect(c);
		}
	}

	static void Connect(int conSeq)
	{
		try
		{
			/**if(DB_TYPE[conSeq].equalsIgnoreCase("ORACLE"))
			{
				DriverManager.registerDriver(new oracle.jdbc.driver.OracleDriver());

				logger.info("Creating connection for: "+strConIP[conSeq]+":"+strPort[conSeq]+":"+strConSID[conSeq]+","+strConUser[conSeq]+","+strConPwd[conSeq]);
				Conns[conSeq] = DriverManager.getConnection("jdbc:oracle:thin:@"+strConIP[conSeq]+":"+strPort[conSeq]+":"+strConSID[conSeq],strConUser[conSeq],strConPwd[conSeq]);
				logger.info("Connection up with : "+strConIP[conSeq]+":"+strPort[conSeq]+":"+strConSID[conSeq]+","+strConUser[conSeq]+","+strConPwd[conSeq]);
			}
			else if(DB_TYPE[conSeq].equalsIgnoreCase("MYSQL"))
			{
				Class.forName("com.mysql.jdbc.Driver");

				String connectionURL = "jdbc:mysql://"+strConIP[conSeq]+":"+strPort[conSeq]+"/"+strConSID[conSeq];
				//Conns[conSeq] = DriverManager.getConnection(connectionURL, strConUser[conSeq], strConPwd[conSeq]);
				Conns[conSeq] = DriverManager.getConnection(connectionURL+"?noAccessToProcedureBodies=true", strConUser[conSeq], strConPwd[conSeq]);

			logger.info("Connection Done with MYSQL "+strConIP[conSeq]+":"+strPort[conSeq]);
			}*/
			if(DB_TYPE[conSeq].equalsIgnoreCase("SQLSERVER"))
			{
				//Class.forName("net.sourceforge.jtds.jdbc.Driver");
						//objConn	= DriverManager.getConnection("jdbc:jtds:sqlserver://172.26.184.221:1433:BGM","bgm","bgm");

				DriverManager.registerDriver(new net.sourceforge.jtds.jdbc.Driver());
				//Connection connection = DriverManager.getConnection("jdbc:sqlserver://"+strConIP[conSeq]+":"+strPort[conSeq],"mobile_radio","mobile");

				logger.info("Creating connection for: "+strConIP[conSeq]+":"+strPort[conSeq]+":"+strConSID[conSeq]+","+strConUser[conSeq]+","+strConPwd[conSeq]);
				Conns[conSeq] = DriverManager.getConnection("jdbc:jtds:sqlserver://"+strConIP[conSeq]+":"+strPort[conSeq],strConUser[conSeq],strConPwd[conSeq]);
				logger.info("Connection up with : "+strConIP[conSeq]+":"+strPort[conSeq]+":"+strConSID[conSeq]+","+strConUser[conSeq]+","+strConPwd[conSeq]);
			}
			else
			{
				logger.info("Invalid DB type. Only ORACLE or MYSQL can be handled. Going to Exit.");
				System.exit(1);
			}
		}
		catch(Exception ee)
		{
			String message="Exception while initializing the connections for "+strConIP[conSeq]+"\nError :"+ee.toString();
			logger.info(message);
			System.exit(1);
		}
	}

	static Connection GetConnection(String ConnectTo)
	{
		//logger.info("Checking for the connection name= "+ConnectTo);
		ConnectTo =ConnectTo.trim();
		for(int c=0;c<actualConCount;c++)
		{
			try
			{
				if(strConServiceName[c].equalsIgnoreCase(ConnectTo))
				{
					//logger.info("Received the connection for connection name= "+ConnectTo);
					//return Conns[c];
					for(int i=0;i<3;i++)
					{
						if(Conns[c]==null||Conns[c].isClosed())
						{
							//ReConnects the closed connection only
							Connect(c);
							if(i>0)
							{
								switch(i)
								{
									case 1:
										Thread.sleep(2000);
										break;
									case 2:
										Thread.sleep(5000);
										break;
									default:
										Thread.sleep(5000);
										break;
								}
							}
						}
						else
						{
							if(i>0)
								logger.info(i+"Reconnected for connection name= "+ConnectTo);
							return Conns[c];
						}
					}
					return Conns[c];
				}
			}
			catch(Exception exc)
			{
				logger.info("Exception for connection name= "+ConnectTo);
			}
		}
		logger.info("No connection available for connection name= "+ConnectTo);
		return null;
	}
}