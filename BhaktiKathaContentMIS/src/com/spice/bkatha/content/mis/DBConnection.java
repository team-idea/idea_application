package com.spice.bkatha.content.mis;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Properties;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.Statement;
//import java.sql.Timestamp;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.IndexedColors;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFColor;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;



public class DBConnection {
	private static Logger logger = Logger.getLogger(DBConnection.class.getName());
	private Properties prop = new Properties();
	private FileOutputStream fos = null;
	private String rowNames = null;
	private int combineSheetCol = 0;
	private String[] rowName = null;
	private String combineColNames = null;
	private String[] combineColName = null;
	private String driverName = "";
	private Connection con = null;
	private Connection conn = null;
	private Statement stmt = null;
	private Statement combineStmt = null;
	private ResultSet combineRS = null;
	private ResultSet rs = null;
	private String dbUrl = null;
	private String user = null;
	private String password = null;
	private String sheetName = null;
	private String sqlQuery = null;
	private String sqlQueryMTD = null;
	private Date MIS_DATE;
	private String CIRCLE = "";
	private int CallsPUUsubs = 0;
	private int CallsPUUnonsubs = 0;
	private int MonthlyUU = 0;
	private int AverageCallDuration = 0;
	private int UniqueLiveCallersSubs = 0;
	private int OneTimeCallersPerWeek = 0;
	private int TwoTimeCallersPerWeek = 0;
	private int DormantUandRBaseCallers = 0;
	private int MousPerUUsubs = 0;
	private int MouPerUUnonsubs = 0;
	private int Mins1to5 = 0;
	private int Mins6to10Mins = 0;
	private int Mins11to20Mins = 0;
	private int Mins21to30Mins = 0;
	private int Mins31toAboveMins = 0;
	private int Ramayana_UU = 0;
	private int Ramayana_MOUs = 0;
	private int Ramayana_Pulses = 0;
	private int ShriHanuman_UU = 0;
	private int ShriHanuman_MOUs = 0;
	private int ShriHanuman_Pulses = 0;
	private int ShriSaiBaba_UU = 0;
	private int ShriSaiBaba_MOUs = 0;
	private int ShriSaiBaba_Pulses = 0;
	private int BhagwadGeeta_UU = 0;
	private int BhagwadGeeta_MOUs = 0;
	private int BhagwadGeeta_Pulses = 0;
	private int Mahabharat_UU = 0;
	private int Mahabharat_MOUs = 0;
	private int Mahabharat_Pulses = 0;
	private int MaaDurga_UU = 0;
	private int MaaDurga_MOUs = 0;
	private int MaaDurga_Pulses = 0;
	private int BalGopal_UU = 0;
	private int BalGopal_MOUs = 0;
	private int BalGopal_Pulses = 0;
	private int ShriGanesh_UU = 0;
	private int ShriGanesh_MOUs = 0;
	private int ShriGanesh_Pulses = 0;

	public XSSFWorkbook xlsxwb = null;
	public XSSFSheet sheet = null;
	private String xlsxPath = null;
	private String columnNames = null;
	private XSSFRow row;
	private XSSFCell cell;
	private int rowCounter = 0;
	private int colCounter = 0;
	private String CombineMisDate = null;

	DBConnection() {
		PropertyConfigurator.configure("./log4j.properties");
		try {
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			driverName = prop.getProperty("driverName");
			dbUrl = prop.getProperty("dbUrl");
			user = prop.getProperty("user");
			password = prop.getProperty("password");
			sheetName = prop.getProperty("sheetName");
			columnNames = prop.getProperty("columnNames");
			rowNames = prop.getProperty("rowNames");
			combineSheetCol = Integer.parseInt(prop.getProperty("combineSheetCol"));
			combineColNames = prop.getProperty("combineColNames");
			xlsxwb = new XSSFWorkbook();
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public void getDBConnection(String yearMonth, int dateCounter) {
		try {
			try {
				Class.forName(driverName);
				con = DriverManager.getConnection(dbUrl, user, password);
				logger.debug("connect is [" + con + "]");
			} catch (Exception e1) {
				e1.printStackTrace();
				logger.debug(LogStackTrace.getStackTrace(e1));
			}
			String[] sheetNames = sheetName.split("[,]+");
			String columnName[] = columnNames.split("[,]+");

			XSSFCellStyle styleSB = xlsxwb.createCellStyle();
			styleSB.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
			styleSB.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			styleSB.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
			styleSB.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
			styleSB.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
			styleSB.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);

			XSSFCellStyle styleTopHeader = xlsxwb.createCellStyle();
			styleTopHeader.setFillForegroundColor(IndexedColors.GREY_50_PERCENT.getIndex());
			styleTopHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			styleTopHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			styleTopHeader.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
			styleTopHeader.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
			styleTopHeader.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
			styleTopHeader.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
			styleTopHeader.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);

			XSSFCellStyle styleHeader = xlsxwb.createCellStyle();
			styleHeader.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
			styleHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			styleHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			styleHeader.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
			styleHeader.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
			styleHeader.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
			styleHeader.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
			styleHeader.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);

			XSSFCellStyle styleWHeader = xlsxwb.createCellStyle();
			styleWHeader.setFillForegroundColor(IndexedColors.WHITE.getIndex());
			styleWHeader.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			styleWHeader.setAlignment(XSSFCellStyle.ALIGN_CENTER);
			styleWHeader.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
			styleWHeader.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
			styleWHeader.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
			styleWHeader.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
			styleWHeader.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
			
			XSSFCellStyle styleW = xlsxwb.createCellStyle();
			styleW.setFillForegroundColor(IndexedColors.WHITE.getIndex());
			styleW.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			styleW.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
			styleW.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
			styleW.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
			styleW.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);
			
			XSSFCellStyle styleY = xlsxwb.createCellStyle();
			styleY.setFillForegroundColor(IndexedColors.LIGHT_BLUE.getIndex());
			styleY.setFillPattern(XSSFCellStyle.SOLID_FOREGROUND);
			styleY.setBorderTop(XSSFCellStyle.BORDER_MEDIUM);
			styleY.setBorderBottom(XSSFCellStyle.BORDER_MEDIUM);
			styleY.setBorderLeft(XSSFCellStyle.BORDER_MEDIUM);
			styleY.setBorderRight(XSSFCellStyle.BORDER_MEDIUM);

			for (String sheetCode : sheetNames) {
				sheet = xlsxwb.createSheet(sheetCode);
				logger.debug("["+sheetCode+"] sheet has been created.");

				for (int rowCreation = 0; rowCreation < columnName.length - 1; rowCreation++) {
					row = sheet.createRow(rowCreation);
				}

				row = sheet.getRow(0);
				cell = row.createCell(0);
				cell.setCellValue("Idea LA MPACK Daily MIS");
				cell.setCellStyle(styleTopHeader);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A1:B1"));

				sheet.setColumnWidth(0, 8000);
				sheet.setColumnWidth(1, 8000);
				// sheet.setDefaultColumnStyle(0, );

				row = sheet.getRow(1);
				cell = row.createCell(0);
				cell.setCellValue("Calls Analysis");
				cell.setCellStyle(styleHeader);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A2:A9"));

				row = sheet.getRow(9);
				cell = row.createCell(0);
				cell.setCellValue("MOU Analysis");
				cell.setCellStyle(styleWHeader);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A10:A11"));

				row = sheet.getRow(11);
				cell = row.createCell(0);
				cell.setCellValue("MOU BucketMoUs/Day");
				cell.setCellStyle(styleHeader);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A12:A16"));

				row = sheet.getRow(16);
				cell = row.createCell(0);
				cell.setCellValue("Category Wise Details");
				cell.setCellStyle(styleTopHeader);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A17:B17"));

				row = sheet.getRow(17);
				cell = row.createCell(0);
				cell.setCellValue("Ramayana");
				cell.setCellStyle(styleHeader);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A18:A20"));

				row = sheet.getRow(20);
				cell = row.createCell(0);
				cell.setCellValue("Shri Hanuman");
				cell.setCellStyle(styleWHeader);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A21:A23"));

				row = sheet.getRow(23);
				cell = row.createCell(0);
				cell.setCellValue("Shri Sai Baba");
				cell.setCellStyle(styleHeader);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A24:A26"));

				row = sheet.getRow(26);
				cell = row.createCell(0);
				cell.setCellValue("Bhagwad Geeta");
				cell.setCellStyle(styleWHeader);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A27:A29"));

				row = sheet.getRow(29);
				cell = row.createCell(0);
				cell.setCellValue("Mahabharat");
				cell.setCellStyle(styleHeader);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A30:A32"));

				row = sheet.getRow(32);
				cell = row.createCell(0);
				cell.setCellValue("Maa Durga");
				cell.setCellStyle(styleWHeader);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A33:A35"));

				row = sheet.getRow(35);
				cell = row.createCell(0);
				cell.setCellValue("Bal Gopal");
				cell.setCellStyle(styleHeader);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A36:A38"));

				row = sheet.getRow(38);
				cell = row.createCell(0);
				cell.setCellValue("Shri Ganesh");
				cell.setCellStyle(styleWHeader);
				sheet.addMergedRegion(CellRangeAddress.valueOf("A39:A40"));

				rowCounter = 0;
				colCounter = 1;
				sheet.setColumnWidth(1, 8000);
				
				for (String columncode : columnName) {
					if (sheet.getRow(rowCounter) == null) {
						row = sheet.createRow(rowCounter);
						XSSFCell cell = row.createCell(colCounter);
						cell.setCellValue(columncode);
						if(rowCounter==0||rowCounter==16){
							cell.setCellStyle(styleTopHeader);
						}else if(rowCounter >=1 && rowCounter<=8 || rowCounter >=11 && rowCounter<=15 || rowCounter >=17 && rowCounter<=19||rowCounter >=23 && rowCounter<=25 ||rowCounter >=29 && rowCounter<=31 ||rowCounter >=35 && rowCounter<=37){
							cell.setCellStyle(styleSB);
						}else{
							cell.setCellStyle(styleW);
						}
					} else {
						row = sheet.getRow(rowCounter);
						cell = row.createCell(colCounter);
						cell.setCellValue(columncode);
						if(rowCounter==0||rowCounter==16){
							cell.setCellStyle(styleTopHeader);
						}else if(rowCounter >=1 && rowCounter<=8 || rowCounter >=11 && rowCounter<=15 || rowCounter >=17 && rowCounter<=19||rowCounter >=23 && rowCounter<=25 ||rowCounter >=29 && rowCounter<=31 ||rowCounter >=35 && rowCounter<=37){
							cell.setCellStyle(styleSB);
						}else{
							cell.setCellStyle(styleW);
						}
					}
					rowCounter++;
				}
				colCounter = 3;
				for (int i = 1; i <= dateCounter; i++) {
					String misDate = "";
					if (i < 10) {
						misDate = yearMonth + "0" + i;
					} else {
						misDate = yearMonth + i;
					}
					//System.out.println("mis_date is ---->" + misDate);
					//System.out.println("sqlQuery");
					try {
						stmt = con.createStatement();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					rowCounter = 0;
					sqlQuery = "select * from TBL_BKATHA_DAILY_CONTENT_MIS where circle='" + sheetCode
							+ "' and convert(varchar,MIS_DATE,112)='" + misDate + "'";
					if (i == dateCounter) {
						sqlQueryMTD = "select * from TBL_BKATHA_DAILY_CONTENT_MIS_MTD where circle='" + sheetCode
								+ "' and convert(varchar,MIS_DATE,112)='" + misDate + "'";
						try {
							rs = stmt.executeQuery(sqlQueryMTD);
							logger.debug("sqlQuery ["+sqlQueryMTD+"] has been executed..");
						} catch (SQLException e) {
							logger.debug(LogStackTrace.getStackTrace(e));
							e.printStackTrace();
						}
						try {
							while (rs.next()) {
								int MTDMTDcolCounter = 2;
								MIS_DATE = rs.getDate("MIS_DATE");
								// String misDateTime = MIS_DATE.toString();
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue("MTD");
								cell.setCellStyle(styleTopHeader);
								// CIRCLE = rs.getString("CIRCLE");
								CallsPUUsubs = rs.getInt("CallsPUUsubs");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(CallsPUUsubs);
								cell.setCellStyle(styleSB);

								CallsPUUnonsubs = rs.getInt("CallsPUUnonsubs");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(CallsPUUnonsubs);
								cell.setCellStyle(styleSB);

								MonthlyUU = rs.getInt("MonthlyUU");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(MonthlyUU);
								cell.setCellStyle(styleSB);

								AverageCallDuration = rs.getInt("AverageCallDuration");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(AverageCallDuration);
								cell.setCellStyle(styleSB);
								
								UniqueLiveCallersSubs = rs.getInt("UniqueLiveCallersSubs");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(UniqueLiveCallersSubs);
								cell.setCellStyle(styleSB);
								
								OneTimeCallersPerWeek = rs.getInt("OneTimeCallersPerWeek");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(OneTimeCallersPerWeek);
								cell.setCellStyle(styleSB);
								
								TwoTimeCallersPerWeek = rs.getInt("TwoTimeCallersPerWeek");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(TwoTimeCallersPerWeek);
								cell.setCellStyle(styleSB);

								DormantUandRBaseCallers = rs.getInt("DormantUandRBaseCallers");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(DormantUandRBaseCallers);
								cell.setCellStyle(styleSB);

								MousPerUUsubs = rs.getInt("MousPerUUsubs");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(MousPerUUsubs);
								cell.setCellStyle(styleW);

								MouPerUUnonsubs = rs.getInt("MouPerUUnonsubs");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(MouPerUUnonsubs);
								cell.setCellStyle(styleW);

								Mins1to5 = rs.getInt("Mins1to5");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(Mins1to5);
								cell.setCellStyle(styleSB);

								Mins6to10Mins = rs.getInt("Mins6to10");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(Mins6to10Mins);
								cell.setCellStyle(styleSB);

								Mins11to20Mins = rs.getInt("Mins11to20");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(Mins11to20Mins);
								cell.setCellStyle(styleSB);

								Mins21to30Mins = rs.getInt("Mins21to30");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(Mins21to30Mins);
								cell.setCellStyle(styleSB);

								Mins31toAboveMins = rs.getInt("Mins31toAbove");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(Mins31toAboveMins);
								cell.setCellStyle(styleSB);

								// SMS_SuccesfullSubscription =
								// rs.getInt("SMS_SuccesfullSubscription");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellStyle(styleTopHeader);
								// cell.setCellValue(SMS_SuccesfullSubscription);

								Ramayana_UU = rs.getInt("Ramayana_UU");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(Ramayana_UU);
								cell.setCellStyle(styleSB);

								Ramayana_MOUs = rs.getInt("Ramayana_MOUs");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(Ramayana_MOUs);
								cell.setCellStyle(styleSB);

								Ramayana_Pulses = rs.getInt("Ramayana_Pulses");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(Ramayana_Pulses);
								cell.setCellStyle(styleSB);

								ShriHanuman_UU = rs.getInt("ShriHanuman_UU");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(ShriHanuman_UU);
								cell.setCellStyle(styleW);

								ShriHanuman_MOUs = rs.getInt("ShriHanuman_MOUs");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(ShriHanuman_MOUs);
								cell.setCellStyle(styleW);

								ShriHanuman_Pulses = rs.getInt("ShriHanuman_Pulses");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(ShriHanuman_Pulses);
								cell.setCellStyle(styleW);

								ShriSaiBaba_UU = rs.getInt("ShriSaiBaba_UU");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(ShriSaiBaba_UU);
								cell.setCellStyle(styleSB);

								ShriSaiBaba_MOUs = rs.getInt("ShriSaiBaba_MOUs");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(ShriSaiBaba_MOUs);
								cell.setCellStyle(styleSB);

								ShriSaiBaba_Pulses = rs.getInt("ShriSaiBaba_Pulses");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(ShriSaiBaba_Pulses);
								cell.setCellStyle(styleSB);

								BhagwadGeeta_UU = rs.getInt("BhagwadGeeta_UU");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(BhagwadGeeta_UU);
								cell.setCellStyle(styleW);

								BhagwadGeeta_MOUs = rs.getInt("BhagwadGeeta_MOUs");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(BhagwadGeeta_MOUs);
								cell.setCellStyle(styleW);

								BhagwadGeeta_Pulses = rs.getInt("BhagwadGeeta_Pulses");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(BhagwadGeeta_Pulses);
								cell.setCellStyle(styleW);

								Mahabharat_UU = rs.getInt("Mahabharat_UU");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(Mahabharat_UU);
								cell.setCellStyle(styleSB);

								Mahabharat_MOUs = rs.getInt("Mahabharat_MOUs");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(Mahabharat_MOUs);
								cell.setCellStyle(styleSB);

								Mahabharat_Pulses = rs.getInt("Mahabharat_Pulses");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(Mahabharat_Pulses);
								cell.setCellStyle(styleSB);

								MaaDurga_UU = rs.getInt("MaaDurga_UU");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(MaaDurga_UU);
								cell.setCellStyle(styleW);

								MaaDurga_MOUs = rs.getInt("MaaDurga_MOUs");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(MaaDurga_MOUs);
								cell.setCellStyle(styleW);

								MaaDurga_Pulses = rs.getInt("MaaDurga_Pulses");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(MaaDurga_Pulses);
								cell.setCellStyle(styleW);

								BalGopal_UU = rs.getInt("BalGopal_UU");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(BalGopal_UU);
								cell.setCellStyle(styleSB);

								BalGopal_MOUs = rs.getInt("BalGopal_MOUs");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(BalGopal_MOUs);
								cell.setCellStyle(styleSB);

								BalGopal_Pulses = rs.getInt("BalGopal_Pulses");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(BalGopal_Pulses);
								cell.setCellStyle(styleSB);

								ShriGanesh_UU = rs.getInt("ShriGanesh_UU");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(ShriGanesh_UU);
								cell.setCellStyle(styleW);

								ShriGanesh_MOUs = rs.getInt("ShriGanesh_MOUs");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(ShriGanesh_MOUs);
								cell.setCellStyle(styleW);

								ShriGanesh_Pulses = rs.getInt("ShriGanesh_Pulses");
								rowCounter++;
								row = sheet.getRow(rowCounter);
								cell = row.createCell(MTDMTDcolCounter);
								cell.setCellValue(ShriGanesh_Pulses);
								cell.setCellStyle(styleW);
							}
						} catch (SQLException e) {
							e.printStackTrace();
							logger.debug(LogStackTrace.getStackTrace(e));
						}
					}
					rowCounter = 0;
					try {
						rs = stmt.executeQuery(sqlQuery);
						logger.debug("sqlQuery ["+sqlQuery+"] has been executed..");
					} catch (SQLException e) {
						logger.debug(LogStackTrace.getStackTrace(e));
						e.printStackTrace();
					}
					try {
						while (rs.next()) {
							sheet.setColumnWidth(colCounter, 5000);
							MIS_DATE = rs.getDate("MIS_DATE");
							// String misDateTime = MIS_DATE.toString();
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							sheet.setColumnWidth(colCounter, 3000);
							cell.setCellValue(MIS_DATE.toString());
							cell.setCellStyle(styleTopHeader);

							// CIRCLE = rs.getString("CIRCLE");
							CallsPUUsubs = rs.getInt("CallsPUUsubs");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(CallsPUUsubs);
							cell.setCellStyle(styleSB);

							CallsPUUnonsubs = rs.getInt("CallsPUUnonsubs");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellStyle(styleSB);
							cell.setCellValue(CallsPUUnonsubs);

							MonthlyUU = rs.getInt("MonthlyUU");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellStyle(styleSB);
							cell.setCellValue(MonthlyUU);

							AverageCallDuration = rs.getInt("AverageCallDuration");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(AverageCallDuration);
							cell.setCellStyle(styleSB);

							UniqueLiveCallersSubs = rs.getInt("UniqueLiveCallersSubs");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(UniqueLiveCallersSubs);
							cell.setCellStyle(styleSB);

							OneTimeCallersPerWeek = rs.getInt("OneTimeCallersPerWeek");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(OneTimeCallersPerWeek);
							cell.setCellStyle(styleSB);

							TwoTimeCallersPerWeek = rs.getInt("TwoTimeCallersPerWeek");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(TwoTimeCallersPerWeek);
							cell.setCellStyle(styleSB);

							DormantUandRBaseCallers = rs.getInt("DormantUandRBaseCallers");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(DormantUandRBaseCallers);
							cell.setCellStyle(styleSB);

							MousPerUUsubs = rs.getInt("MousPerUUsubs");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(MousPerUUsubs);
							cell.setCellStyle(styleW);

							MouPerUUnonsubs = rs.getInt("MouPerUUnonsubs");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(MouPerUUnonsubs);
							cell.setCellStyle(styleW);

							Mins1to5 = rs.getInt("Mins1to5");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(Mins1to5);
							cell.setCellStyle(styleSB);

							Mins6to10Mins = rs.getInt("Mins6to10");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(Mins6to10Mins);
							cell.setCellStyle(styleSB);

							Mins11to20Mins = rs.getInt("Mins11to20");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(Mins11to20Mins);
							cell.setCellStyle(styleSB);


							Mins21to30Mins = rs.getInt("Mins21to30");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(Mins21to30Mins);
							cell.setCellStyle(styleSB);


							Mins31toAboveMins = rs.getInt("Mins31toAbove");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(Mins31toAboveMins);
							cell.setCellStyle(styleSB);


							// SMS_SuccesfullSubscription =
							// rs.getInt("SMS_SuccesfullSubscription");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellStyle(styleTopHeader);
							// row = sheet.getRow(rowCounter);
							// cell = row.createCell(colCounter);
							// cell.setCellValue(SMS_SuccesfullSubscription);

							Ramayana_UU = rs.getInt("Ramayana_UU");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(Ramayana_UU);
							cell.setCellStyle(styleSB);


							Ramayana_MOUs = rs.getInt("Ramayana_MOUs");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(Ramayana_MOUs);
							cell.setCellStyle(styleSB);


							Ramayana_Pulses = rs.getInt("Ramayana_Pulses");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(Ramayana_Pulses);
							cell.setCellStyle(styleSB);


							ShriHanuman_UU = rs.getInt("ShriHanuman_UU");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(ShriHanuman_UU);
							cell.setCellStyle(styleW);


							ShriHanuman_MOUs = rs.getInt("ShriHanuman_MOUs");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(ShriHanuman_MOUs);
							cell.setCellStyle(styleW);


							ShriHanuman_Pulses = rs.getInt("ShriHanuman_Pulses");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(ShriHanuman_Pulses);
							cell.setCellStyle(styleW);


							ShriSaiBaba_UU = rs.getInt("ShriSaiBaba_UU");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(ShriSaiBaba_UU);
							cell.setCellStyle(styleSB);


							ShriSaiBaba_MOUs = rs.getInt("ShriSaiBaba_MOUs");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(ShriSaiBaba_MOUs);
							cell.setCellStyle(styleSB);


							ShriSaiBaba_Pulses = rs.getInt("ShriSaiBaba_Pulses");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(ShriSaiBaba_Pulses);
							cell.setCellStyle(styleSB);


							BhagwadGeeta_UU = rs.getInt("BhagwadGeeta_UU");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(BhagwadGeeta_UU);
							cell.setCellStyle(styleW);


							BhagwadGeeta_MOUs = rs.getInt("BhagwadGeeta_MOUs");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(BhagwadGeeta_MOUs);
							cell.setCellStyle(styleW);


							BhagwadGeeta_Pulses = rs.getInt("BhagwadGeeta_Pulses");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(BhagwadGeeta_Pulses);
							cell.setCellStyle(styleW);


							Mahabharat_UU = rs.getInt("Mahabharat_UU");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(Mahabharat_UU);
							cell.setCellStyle(styleSB);


							Mahabharat_MOUs = rs.getInt("Mahabharat_MOUs");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(Mahabharat_MOUs);
							cell.setCellStyle(styleSB);


							Mahabharat_Pulses = rs.getInt("Mahabharat_Pulses");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(Mahabharat_Pulses);
							cell.setCellStyle(styleSB);


							MaaDurga_UU = rs.getInt("MaaDurga_UU");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(MaaDurga_UU);
							cell.setCellStyle(styleW);


							MaaDurga_MOUs = rs.getInt("MaaDurga_MOUs");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellStyle(styleW);

							cell.setCellValue(MaaDurga_MOUs);

							MaaDurga_Pulses = rs.getInt("MaaDurga_Pulses");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(MaaDurga_Pulses);
							cell.setCellStyle(styleW);


							BalGopal_UU = rs.getInt("BalGopal_UU");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(BalGopal_UU);
							cell.setCellStyle(styleSB);


							BalGopal_MOUs = rs.getInt("BalGopal_MOUs");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(BalGopal_MOUs);
							cell.setCellStyle(styleSB);


							BalGopal_Pulses = rs.getInt("BalGopal_Pulses");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(BalGopal_Pulses);
							cell.setCellStyle(styleSB);


							ShriGanesh_UU = rs.getInt("ShriGanesh_UU");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(ShriGanesh_UU);
							cell.setCellStyle(styleW);


							ShriGanesh_MOUs = rs.getInt("ShriGanesh_MOUs");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(ShriGanesh_MOUs);
							cell.setCellStyle(styleW);


							ShriGanesh_Pulses = rs.getInt("ShriGanesh_Pulses");
							rowCounter++;
							row = sheet.getRow(rowCounter);
							cell = row.createCell(colCounter);
							cell.setCellValue(ShriGanesh_Pulses);
							cell.setCellStyle(styleW);


						}
					} catch (SQLException e) {
						logger.debug(LogStackTrace.getStackTrace(e));
						e.printStackTrace();
					}
					colCounter++;
				}

			}
			//saveExcels("./mis.xlsx");
			saveExcels("./misData/BHAKTI_KATHA_CONTENT_MIS_"+BkathaContentMIS.dateParam+".xlsx");
			
		} catch (IOException e) {
			e.printStackTrace();
			logger.debug(LogStackTrace.getStackTrace(e));
		} finally {

			try {
				if (con != null || !con.isClosed()) {
					con.close();
				}
			} catch (Exception ex) {
				ex.printStackTrace();
			}

			try {
				if (stmt != null || !stmt.isClosed()) {
					stmt.close();
				}
			} catch (Exception ex) {
				logger.debug(LogStackTrace.getStackTrace(ex));
				ex.printStackTrace();
			}

			try {
				if (rs != null || !rs.isClosed()) {
					rs.close();
				}
			} catch (Exception ex) {
				logger.debug(LogStackTrace.getStackTrace(ex));
				ex.printStackTrace();
			}

		}

	}

	public void saveExcels(String xlsxPath) throws IOException {
		this.xlsxPath = xlsxPath; // for saving file to the
		try {
			fos = new FileOutputStream(xlsxPath); // saves xlsx
			xlsxwb.write(fos);

			// fos.close();
		} catch (FileNotFoundException e) {
			logger.debug(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		} catch (IOException e) {
			logger.debug(LogStackTrace.getStackTrace(e));
			e.printStackTrace();
		} finally {
			fos.close();
		}

	}

}
