import java.io.*;
import java.util.*;
import java.net.*;
import java.sql.*;
import javax.naming.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.log4j.*;

import java.security.*;
import javax.net.ssl.*;

public class cgengine extends HttpServlet
{
	private static final long serialVersionUID	= 7526892295622776147L;
	private static Logger logger 				= Logger.getLogger(cgengine.class.getName());
	private DBConnectionManager connMgr			= null;
	private String conPool						= "csdb";

	public void init(ServletConfig config) throws ServletException
	{
		try{PropertyConfigurator.configure("log4j.properties");}catch(Exception exp){logException(exp);}
		try{connMgr= DBConnectionManager.getInstance();}catch(Exception exp){logException(exp);}
		try{trustAllHttpsCertificates();}catch(Exception tmr){}
		logger.info("cgengine servlet started...");
	}

	private static void trustAllHttpsCertificates() throws Exception
	{
		javax.net.ssl.TrustManager[] trustAllCerts  = new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm 				= new miTM();

		trustAllCerts[0] 							= tm;
		javax.net.ssl.SSLContext sc 				= javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(
		sc.getSocketFactory());
	}

	public void doGet (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{
		PrintWriter	out 			= res.getWriter();
		String str_req				= req.getQueryString();
		String retStr				= callHttps("http://10.0.106.95:16101/cgengine/cgengine?"+str_req);
		out.println(retStr);
		req.getSession(true).invalidate();
	}


 	private String callHttps(final String url)
 	{
 		String retStr 								= "";
 		String header 								= "";
 		try
 		{
			HostnameVerifier hv 					= new HostnameVerifier(){public boolean verify(String urlHostName, SSLSession session){return true;}};
			HttpsURLConnection.setDefaultHostnameVerifier(hv);

 			URL Url 								= new URL(url);
 			URLConnection conn 						= Url.openConnection();
 			conn.setReadTimeout(1000*20);
 			header 									= String.valueOf(conn.getHeaderFields());
 			//logger.debug("["+threadName+"] ->(sdp   :"+circle+") responseHeader: "+header);
 			if(header.indexOf("200 OK")>0) header 	= "[HTTP/1.1 200 OK]"; else retStr = header;
 			String str 								= null;
 			BufferedReader in 						= new BufferedReader(new InputStreamReader(conn.getInputStream()));
 			while ((str = in.readLine()) != null) retStr += str;
 			in.close();
 			//if(retStr.length()<2) if(header.equalsIgnoreCase("[HTTP/1.1 200 OK]")){retStr="success";logger.debug(retStr);}
 		}catch(Exception exp){retStr="error";logger.error("OffnetError: ",exp);}
 		logger.trace(url+" :"+retStr);
 		return retStr;
 	}


	private void logException(Exception exp)
	{
		logger.error(getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}
	public void destroy()
	{
		logger.debug("destroy() function called for cgengine...");
		super.destroy();
	}
	public static class miTM implements javax.net.ssl.TrustManager,javax.net.ssl.X509TrustManager
	{
		public java.security.cert.X509Certificate[] getAcceptedIssuers()			{return null;}
		public boolean isServerTrusted(java.security.cert.X509Certificate[] certs)	{return true;}
		public boolean isClientTrusted(java.security.cert.X509Certificate[] certs)	{return true;}
		public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)	throws java.security.cert.CertificateException{return;}
		public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) throws java.security.cert.CertificateException{return;}
	}
}

