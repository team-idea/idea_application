import java.io.*;
import java.util.*;
import java.net.*;
import java.sql.*;
import javax.naming.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.log4j.*;

import java.security.*;
import javax.net.ssl.*;

public class PushUssd extends HttpServlet
{
	private static final long serialVersionUID	= 7526892295622776147L;
	private static Logger logger 				= Logger.getLogger(PushUssd.class.getName());
	private DBConnectionManager connMgr			= null;
	private String conPool						= "csdb";

	public void init(ServletConfig config) throws ServletException
	{
		try{PropertyConfigurator.configure("log4j.properties");}catch(Exception exp){logException(exp);}
		try{connMgr= DBConnectionManager.getInstance();}catch(Exception exp){logException(exp);}
		logger.info("PushUssd servlet started...");
	}

	public void doGet (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException
	{
		PrintWriter	out 			= res.getWriter();
		String str_req				= req.getQueryString();
		String retStr				= callHttp("http://125.19.68.45:2014/UssdPushRequest/ProcessPushRequest.jsp?&"+str_req);
		out.println(retStr);
		req.getSession(true).invalidate();
	}

	private String callHttp(final String url)
	{
		String retStr = "";
		try
		{
			URL Url 			= new URL(url);
			URLConnection conn 	= Url.openConnection();
			conn.setReadTimeout(1000*50);
			String header = String.valueOf(conn.getHeaderFields());
			if(header.indexOf("200 OK")>0) header = "[HTTP/1.1 200 OK]"; else header = "error";
			String str = null;
			BufferedReader in 	= new BufferedReader(new InputStreamReader(conn.getInputStream()));
			while ((str = in.readLine()) != null)  retStr += str;
			in.close();
		}catch(Exception exp){/*logException(exp);*/retStr = exp.toString();}
		logger.debug("URL "+url+" Response return: "+retStr);
		return retStr;
	}
	private void logException(Exception exp)
	{
		logger.error(getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}
	public void destroy()
	{
		logger.debug("destroy() function called for PushUssd...");
		super.destroy();
	}
}

