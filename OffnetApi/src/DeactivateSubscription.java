import java.io.*;
import java.util.*;
import java.net.*;
import java.sql.*;
import javax.naming.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.log4j.*;

import java.security.*;
import javax.net.ssl.*;

public class DeactivateSubscription extends HttpServlet
{
	private static final long serialVersionUID	= 7526892295622776147L;
	private static Logger logger 				= Logger.getLogger(DeactivateSubscription.class.getName());
	private DBConnectionManager connMgr			= null;

	private String conPool_deact				= "sdp_81";
	private String conPool_retry				= "offnet";

	public void init(ServletConfig config) throws ServletException
	{
		initlogger("log4j.properties");
		logger.info("DeactivateSubscription servlet started...");
		connMgr 				= DBConnectionManager.getInstance();
	}

	private void initlogger(String loggerfile)
	{
		try{PropertyConfigurator.configure( "log4j.properties" );}catch(Exception exp){logException(exp);}
	}

	public void doGet (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException			// http://10.0.252.103/csdp/DeactivateSubscription?msisdn=9218514140&contentid=0120&contentDesc=test&price=100&circle=hp
	{
		PrintWriter	out 			= res.getWriter();

		String retStr				= "";
		String remoteIp				= req.getRemoteAddr();
		String str_req				= req.getQueryString();
		logger.trace(str_req);
		String circle				= req.getParameter("circle");
		String msisdn				= req.getParameter("msisdn");
		String srvKey				= req.getParameter("srvKey");
		String mode					= req.getParameter("mode");
		retStr						= deactivateSubscriber(remoteIp,circle,msisdn,srvKey,mode);	logger.debug("deactivateSubscriber("+remoteIp+","+circle+","+msisdn+","+srvKey+","+mode+") = "+retStr);
		String checkRetryQueue		= deleteFromRetryQueue(remoteIp,circle,msisdn,srvKey,mode);	logger.debug("deleteFromRetryQueue("+remoteIp+","+circle+","+msisdn+","+srvKey+","+mode+") = "+checkRetryQueue);
		out.print(retStr);
		req.getSession(true).invalidate();
	}

	private String deactivateSubscriber(final String remoteIp,final String circle,final String msisdn,final String srvKey,final String mode)
	{
		String retStr					= "0|fail|UnknownError";
		final Connection conn			= connMgr.getConnection(conPool_deact);
		try
		{
			CallableStatement cstmt		= null;
			try
			{
				//refid,msisdn,content_gid,content_id,content_desc,price,validDays,srvkey,content_type
				cstmt 	= conn.prepareCall("{call sdpCentral_ss.dbo.proc_OffnetDeactivate(?,?,?,?,?,?)}");
				cstmt.setString(1,remoteIp);
				cstmt.setString(2,circle);
				cstmt.setString(3,msisdn);
				cstmt.setString(4,srvKey);
				cstmt.setString(5,mode);
				cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);
				cstmt.execute();
				retStr	= cstmt.getString(6);
				logger.info("{call sdpCentral_ss.dbo.proc_OffnetDeactivate('"+remoteIp+"','"+circle+"','"+msisdn+"','"+srvKey+"','"+mode+"',@res)} = "+retStr);
			}catch(Exception exp){logException(exp);}
			finally{try{cstmt.close();cstmt = null;}catch(Exception exp){logException(exp);}}
		}catch(Exception exp){logException(exp);}
		finally{connMgr.freeConnection(conPool_deact,conn);}
		return retStr;
	}

	private String deleteFromRetryQueue(final String remoteIp,final String circle,final String msisdn,final String srvkey,final String mode)
	{
		String retStr					= "0|fail|UnknownError";
		final Connection conn			= connMgr.getConnection(conPool_retry);
		try
		{
			int chkCntr					= 0;
			Statement stmt				= conn.createStatement();
			String stm_chkReq			= "select count(1) cnt from tbl_offnet_retry where circle = '"+circle+"' and msisdn = '"+msisdn+"' and srvkey = '"+srvkey+"'";
			String stm_moveReq			= "insert into tbl_offnet_retry_fail select * from tbl_offnet_retry where circle = '"+circle+"' and msisdn = '"+msisdn+"' and srvkey = '"+srvkey+"'";
			String stm_delReq			= "delete from tbl_offnet_retry where circle = '"+circle+"' and msisdn = '"+msisdn+"' and srvkey = '"+srvkey+"'";
			ResultSet rs				= stmt.executeQuery(stm_chkReq);
			if(rs.next()){chkCntr		= rs.getInt("cnt"); try{rs.close();}catch(Exception exp_rs){logException(exp_rs);}}
			if(chkCntr>0)
			{
				logger.debug(stm_moveReq+" : "+stmt.executeUpdate(stm_moveReq));
				logger.debug(stm_delReq+" : "+stmt.executeUpdate(stm_delReq));
				retStr						= "1|Success|Subscriber deleted from retry queue";
			}else retStr					= "0|Success|Subscriber not present in retry queue";
		}catch(Exception exp){logException(exp);}
		finally{connMgr.freeConnection(conPool_retry,conn);}
		return retStr;
	}

	private void logException(Exception exp)
	{
		logger.error(getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}
	public void destroy()
	{
		logger.debug("destroy() function called for DeactivateSubscription...");
		super.destroy();
	}
}

