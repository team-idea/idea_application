import java.io.*;
import java.util.*;
import java.net.*;
import java.sql.*;
import javax.naming.*;
import javax.servlet.*;
import javax.servlet.http.*;
import org.apache.log4j.*;

import java.security.*;
import javax.net.ssl.*;

public class OffnetSubscription extends HttpServlet
{
	private static final long serialVersionUID	= 7526892295622776147L;
	private static Logger logger 				= Logger.getLogger(OffnetSubscription.class.getName());
	private DBConnectionManager connMgr			= null;
	private String conPool_offnet				= "offnet";
	private String conPool_sdp					= "sdp";
	private String conPool_80					= "sdp_80";
	private String conPool_81					= "sdp_81";

	public void init(ServletConfig config) throws ServletException
	{
		initlogger("log4j.properties");
		try{trustAllHttpsCertificates();}catch(Exception tmr){}
		logger.info("OffnetSubscription servlet started...");
		connMgr 				= DBConnectionManager.getInstance();
	}

	private void initlogger(String loggerfile)
	{
		try{PropertyConfigurator.configure( "log4j.properties" );}catch(Exception exp){logException(exp);}
	}

	private static void trustAllHttpsCertificates() throws Exception
	{
		javax.net.ssl.TrustManager[] trustAllCerts  = new javax.net.ssl.TrustManager[1];
		javax.net.ssl.TrustManager tm 				= new miTM();

		trustAllCerts[0] 							= tm;
		javax.net.ssl.SSLContext sc 				= javax.net.ssl.SSLContext.getInstance("SSL");
		sc.init(null, trustAllCerts, null);
		javax.net.ssl.HttpsURLConnection.setDefaultSSLSocketFactory(
		sc.getSocketFactory());
	}

	public void doGet (HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException			// http://10.0.252.103/csdp/OffnetSubscription?msisdn=9218514140&contentid=0120&contentDesc=test&price=100&circle=hp
	{
		PrintWriter	out 			= res.getWriter();
		String retStr				= "";

		String res_id="",result="fail",ret_url="900|fail|Unknown Error|NA",content_partner="A7",content_gid="",contentDesc="",price="0.00",srvKey="",contentType="",sub_info="act",fallback_list="",succ_msg=null,offnet_user = "SPICEDIGITAL_01",offnet_password="U1BJQ0VESUdJVEFMQDEy";
		int days = 0,service_validDays=0,service_retryCount=0,service_retryDays=0;
		String remoteIp				= req.getRemoteAddr();
		String str_req				= req.getQueryString();
		try
		{
			logger.trace(str_req);
			String circle				= req.getParameter("circle");
			String msisdn				= req.getParameter("msisdn");
			if(isNumeric(msisdn))
			{
				String contentid			= String.format("%7s", req.getParameter("contentid")).replace(' ', '0');
				String mode					= req.getParameter("mode").toLowerCase(); if(mode.indexOf("_offnet")<0) mode = mode+"_offnet"; mode = mode.replace("_offnet_offnet","_offnet");
				try{sub_info				= req.getParameter("subflag").toLowerCase();}catch(Exception exp){sub_info="act";}
				String proc_res				= checkServiceInfo(remoteIp,circle,msisdn,contentid,mode);			//1|success|content_gid,contentType,contentDesc,price,srvKey,service_validDays,service_retryCount,service_retryDays
				String[] response			= proc_res.split("[|]");
				final String req_id			= response[1];
				result						= response[2];
				String[] proc_response		= response[3].split(",");
				try{content_gid				= proc_response[0];}catch(Exception exp){content_gid	= "error";}
				try{contentType				= proc_response[1];}catch(Exception exp){contentType	= "error";}
				try{contentDesc				= proc_response[2];}catch(Exception exp){contentDesc	= "error";}
				try{price					= proc_response[3];}catch(Exception exp){price			= "0";}
				try{srvKey					= proc_response[4];}catch(Exception exp){srvKey			= "error";}
				try{service_validDays		= Integer.parseInt(proc_response[5]);}catch(Exception exp){service_validDays	= 1;}
				try{fallback_list			= proc_response[6];}catch(Exception exp){fallback_list	= price;}
				try{content_partner			= proc_response[7];}catch(Exception exp){content_partner= "A7";}
				try{succ_msg				= proc_response[8];}catch(Exception exp){succ_msg		= null;}

				if(content_partner.equalsIgnoreCase("A7"))		{offnet_user = "SPICEDIGITAL_01";	offnet_password = "IxBcNl9m0LYnG/j4+ZTwAA==";} 	//	U1BJQ0VESUdJVEFMQDEy
				else if(content_partner.equalsIgnoreCase("F5"))	{offnet_user = "DIVINEVISTAAS_01";	offnet_password = "08vv1q+g6XpPO7cGM0Ahjg==";}	//	bWFubmF0QGY1
				else											{offnet_user = "SPICEDIGITAL_01";	offnet_password = "IxBcNl9m0LYnG/j4+ZTwAA==";}	//	U1BJQ0VESUdJVEFMQDEy
				String content_id			= contentid;
				String price_local			= price;
				if(response[0].equalsIgnoreCase("1"))
				{
					logger.info("start of the offnet check sub info, fallback info = "+fallback_list+" for content_id = "+content_id);
					String sub_flag					= checkSubInfo(circle,msisdn,content_gid,srvKey,sub_info);
					logger.trace("checkSubInfo('"+circle+"','"+msisdn+"','"+content_gid+"','"+srvKey+"','"+sub_info+") = "+sub_flag);
					if(sub_flag.startsWith("0"))
					{
						String[] arr_fallback				= fallback_list.split("-");
						for(int i=0;i<arr_fallback.length;i++)
						{
							String[] fallback_detail		= arr_fallback[i].split(":");
							content_id						= contentid.substring(0,3);
							price_local						= String.format("%2s",fallback_detail[0]).replace(' ', '0');
							String validity	 				= String.format("%2s",fallback_detail[1]).replace(' ', '0');
							try{service_validDays			= Integer.parseInt(proc_response[5]);}catch(Exception exp){service_validDays	= 1;}
							content_id						= content_id+price_local+validity;

							if(content_id.length()>7)		{content_id = content_id.substring(0,7);}
							if(contentDesc.length()>25)		{contentDesc= contentDesc.substring(0,25);}
							if(msisdn.length() == 10)		{msisdn		= "91"+msisdn;}
							price_local						= price_local+"00";
							logger.info("msisdn:"+msisdn+", content_id:"+content_id+", price:"+price_local+", validity: "+validity+", service_validDays: "+service_validDays+", contentDesc: "+contentDesc);

							//final String str_url			= "https://172.30.196.127:7776/offnet/offnet?msisdn=+"+msisdn+"&cpid="+content_partner+"&aggregatorid=3&contenttype="+contentType+"&contentid="+content_id+"&contentdesc="+URLEncoder.encode(contentDesc, "ISO-8859-1")+"&copyrightid=0&smskeyword=&eup="+price_local+"&contenturl=&accounttype=1&devicemodel=&contentsize=240&contentMimeType=&cptransid="+req_id+"&category=&producttype=SONG&Userid="+offnet_user+"&Password="+offnet_password+"";
							// port Chnaged on 16th Sept 2016
							final String str_url			= "https://172.30.196.127:7777/offnet/offnet?msisdn=+"+msisdn+"&cpid="+content_partner+"&aggregatorid=3&contenttype="+contentType+"&contentid="+content_id+"&contentdesc="+URLEncoder.encode(contentDesc, "ISO-8859-1")+"&copyrightid=0&smskeyword=&eup="+price_local+"&contenturl=&accounttype=1&devicemodel=&contentsize=240&contentMimeType=&cptransid="+req_id+"&category=&producttype=SONG&Userid="+offnet_user+"&Password="+offnet_password+"";
							result							= callHttps(str_url).toLowerCase();
							try{res_id 						= result.split(",")[1];}catch(Exception exp){}
							if(result.indexOf("success")>0)
							{
								try{service_validDays		= Integer.parseInt(validity);}catch(Exception exp){service_validDays	= 1;}
								ret_url 					= "200|success|Charging request for "+contentDesc+" successfull. Amount deducted Rs. "+String.valueOf(Integer.parseInt(price_local)/100)+"|"+req_id;
								result 						= "success";
								contentid 					= content_id;
								sub_info					= "act";
								break;
							}
							else if(result.indexOf("err_allocation_failed")>0)	{ret_url = "901|fail|Charging request for "+contentDesc+" failed due to low balance."+"|"+req_id;result = "fail";}
							else if(result.indexOf("invalid_subscriber")>0)		{ret_url = "904|fail|Subscriber not present."+"|"+req_id;result = "fail";}
							else 												{ret_url = "900|fail|Unknown System Error. Please try again later."+"|"+req_id;result = "fail";}
							logger.info("Charging not successfull for "+msisdn+" for pricepoint "+price_local+" and content_id "+content_id+" fallback_detail[0] = "+fallback_detail[0]);
						}
					}
					else
					{
						try{price_local				= String.valueOf(Integer.parseInt(price_local)/100);}catch(Exception exp){price_local="0.0";}
						if(sub_flag.indexOf("RetryActive")>=0)				{ret_url = "902|fail|Charging Request is already in Queue for "+contentDesc+" @ Rs. "+price_local+"|"+req_id;}
						else if(sub_flag.indexOf("ActiveActive")>=0)		{ret_url = "903|fail|Charging already done for "+contentDesc+"|"+req_id;}
						else if(sub_flag.indexOf("GraceActive")>=0)			{ret_url = "903|fail|Charging already done for "+contentDesc+"|"+req_id;}
						else if(sub_flag.indexOf("CallbackActive")>=0)		{ret_url = "906|fail|Number pending in charging queue "+contentDesc+"|"+req_id;}
						else 												{ret_url = "900|fail|Unknown System Error. Please try again later."+"|"+req_id;}
						result = "fail";
					}
				}
				else if(response[0].equalsIgnoreCase("0"))
				{
					logger.trace("User charging information cannot be fetched from the database. Sending DCT command");
					ret_url = "999|fail|Not a valid charging request.|NA";
				}
				else if(response[0].equalsIgnoreCase("909"))
				{
					ret_url = "909|fail|IP Authentication Error.|NA";
				}
				else{ret_url  = proc_res;}
				updateRecord(circle,remoteIp,msisdn,srvKey,content_gid,contentid,contentDesc,contentType,service_validDays,price_local,req_id,res_id,result,ret_url,mode,sub_info);
			}else ret_url = "905|fail|Invalid MSISDN. Not a Number|NA";
		}catch(Exception exp){logException(exp);ret_url = "900|fail|Unknown System Error. Please try again later.";}
		out.println(ret_url);
		req.getSession(true).invalidate();
	}

	public boolean isNumeric(String str) {return str.matches("[-+]?\\d*\\.?\\d+");}

	private String checkServiceInfo(final String remoteIp,final String circle,final String msisdn,final String content_id,final String mode)
	{
		String retStr					= "0|fail|na";
		final Connection conn			= connMgr.getConnection(conPool_offnet);
		try
		{
			logger.trace("CONN: "+conn);
			CallableStatement cstmt		= null;
			try
			{
				//refid,msisdn,content_gid,content_id,content_desc,price,validDays,srvkey,content_type
				cstmt 	= conn.prepareCall("{call proc_OffnetCheck(?,?,?,?,?,?)}");
				cstmt.setString(1,remoteIp);
				cstmt.setString(2,circle);
				cstmt.setString(3,msisdn);
				cstmt.setString(4,content_id);
				cstmt.setString(5,mode);
				cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);
				cstmt.execute();
				retStr	= cstmt.getString(6);
				logger.info("{call proc_OffnetCheck('"+remoteIp+"','"+circle+"','"+msisdn+"','"+content_id+"','"+mode+"',@res)} = "+retStr);
			}catch(Exception exp){logger.error(exp);}
			finally{try{cstmt.close();cstmt = null;}catch(Exception exp){logger.error(exp);}}
		}catch(Exception exp){}
		finally{connMgr.freeConnection(conPool_offnet,conn);}
		return retStr;
	}

	private String checkSubInfo(final String circle,final String msisdn,final String srv_gid,final String srvKey,final String sub_info)
	{
		String retStr			= "";
		try
		{
			final Connection conn	= connMgr.getConnection(conPool_81);
			try
			{
				if(conn != null)
				{
					CallableStatement cstmt		= null;
					try
					{
						cstmt 	= conn.prepareCall("{call sdpCentral_SS.dbo.proc_checkSubsciberStatus(?,?,?,?,?,?)}");
						cstmt.setString(1,circle);
						cstmt.setString(2,srv_gid);
						cstmt.setString(3,srvKey);
						cstmt.setString(4,msisdn);
						cstmt.setString(5,sub_info);
						cstmt.registerOutParameter(6, java.sql.Types.VARCHAR);
						cstmt.execute();
						retStr	= cstmt.getString(6);
						logger.info("{call sdpCentral_SS.dbo.proc_checkSubsciberStatus('"+circle+"','"+srv_gid+"','"+srvKey+"','"+msisdn+"','"+sub_info+"',@res)} = "+retStr);
					}catch(Exception exp){logger.error(exp);}
					finally{try{cstmt.close();cstmt = null;}catch(Exception exp){logger.error(exp);}}
				}else{logger.info("Cannot connect to the MSSQL "+conPool_81+" Database...");}
			}catch(Exception exp){logger.error(exp);}
			finally{connMgr.freeConnection(conPool_81,conn);}
		}catch(Exception exp){logger.error(exp);}
		return retStr;
	}

	private String updateRecord(final String circle,final String remoteIp,final String msisdn,final String srvKey,final String content_gid,final String content_id,final String contentDesc,final String contentType,final int days,final String price,final String req_id,final String res_id,final String result,final String res_desc,final String mode,final String sub_info)
	{
		String retStr			= "nok",res="fail";
		String resid			= res_id;
		String[] arr_result		= result.split(",");
		String[] arr_url		= res_desc.split("[|]");
		final String err_code	= arr_url[0].trim();
		try{if(arr_url.length==2)res= arr_url[1];}catch(Exception exp){/*TODO: Handle the exception in future*/}
		Connection conn_offnet	= connMgr.getConnection(conPool_offnet);
		Connection conn_ins_sdp	= connMgr.getConnection(conPool_81);
		float price_flt			= 0;
		try{price_flt			= Integer.parseInt(price)/100;}catch(Exception exp){logException(exp);price_flt=0;}
		if(arr_result.length > 0)
		{
			//try{resid = arr_result[1];}catch(Exception exp){}
			try{if(arr_result.length > 1)res = arr_result[2];}catch(Exception exp){}
		}
		if(result.indexOf("success")>=0) res="success";

		logger.info("err_code: "+err_code+", Result: "+result+", Length: "+arr_result.length+", resid = "+resid+", res "+res+", price: "+price_flt+"("+price+")");

		try
		{
			String sdpStatus		= "act";
			String sdp_retStr		= "bal-low";
			if(err_code.equalsIgnoreCase("200"))		{sdpStatus = "act";      sdp_retStr="success";}
			else if(err_code.equalsIgnoreCase("903"))	{sdpStatus = "act";      price_flt=0;sdp_retStr="success";}
			else if(err_code.equalsIgnoreCase("999"))	{sdpStatus = "dct";      price_flt=0;sdp_retStr="success";}
			else if(err_code.equalsIgnoreCase("906"))	{sdpStatus = "act";      price_flt=0;sdp_retStr="success";}
			else 										{sdpStatus = "act_retry";price_flt=0;sdp_retStr="bal-low";}

			if(conn_ins_sdp != null && sub_info.equalsIgnoreCase("act") && !err_code.equalsIgnoreCase("902"))
			{
				Statement stm_ins_sdp	= conn_ins_sdp.createStatement();
				String str_ins			= "insert into sdpCentral_ss.dbo.tbl_callback(callbackInsertDateTime,RefId, retry_num, circle, msisdn, prepost, srvkey, mode, price, sdpStatus, status, action, precharge, startDateTime, endDateTime, originator, cp_user, cp_password, info) values(getdate(),'"+req_id+"',0,'"+circle+"','"+ msisdn+"','U','"+ srvKey+"','"+ mode+"','"+ price_flt+"','"+sdp_retStr+"',0,'"+ sdpStatus+"','N',getDate(),getdate()+"+days+",'"+content_gid+"','spice','spice_internal','"+content_id+"')";
				int counter				= stm_ins_sdp.executeUpdate(str_ins);
				logger.trace(str_ins+" : "+counter);
			}
			if(conn_offnet != null)
			{
				try
				{
					CallableStatement cstmt		= null;
					try
					{
						//refid,msisdn,content_gid,content_id,content_desc,price,validDays,srvkey,content_type
						cstmt 	= conn_offnet.prepareCall("{call proc_OffnetResponse(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}");
						cstmt.setString(1,req_id);
						cstmt.setString(2,resid);
						cstmt.setString(3,remoteIp);
						cstmt.setString(4,circle);
						cstmt.setString(5,msisdn);
						cstmt.setString(6,content_gid);
						cstmt.setString(7,content_id);
						cstmt.setString(8,contentDesc);
						cstmt.setString(9,contentType);
						cstmt.setFloat(10,price_flt);
						cstmt.setString(11,srvKey);
						cstmt.setInt(12,days);
						cstmt.setString(13,res);
						cstmt.setString(14,res_desc);
						cstmt.setString(15,mode);
						cstmt.setString(16,sdpStatus);
						cstmt.setString(17,sdp_retStr);
						cstmt.registerOutParameter(18, java.sql.Types.VARCHAR);
						cstmt.execute();
						retStr	= cstmt.getString(18);
						logger.info("{call proc_OffnetResponse('"+req_id+"','"+resid+"','"+remoteIp+"','"+circle+"','"+msisdn+"','"+content_gid+"','"+content_id+"','"+contentDesc+"','"+contentType+"','"+price+"','"+srvKey+"','"+days+"','"+result+"','"+res_desc+"','"+mode+"','"+sdpStatus+"','"+sdp_retStr+"',@res)} = "+retStr);
					}catch(Exception exp){logger.info("{call proc_OffnetRecord('"+req_id+"','"+resid+"','"+remoteIp+"','"+circle+"','"+msisdn+"','"+content_gid+"','"+content_id+"','"+contentDesc+"','"+contentType+"','"+price+"','"+srvKey+"','"+days+"','"+result+"','"+res_desc+"','"+mode+"','"+sdpStatus+"','"+sdp_retStr+"',@res)} = error");logException(exp);}
					finally{try{cstmt.close();cstmt = null;}catch(Exception exp){logger.error(exp);}}
				}catch(Exception exp){logger.error(exp);}
			}else{logger.error("Cannot connect to the Local "+conPool_offnet+" Database...");}
		}catch(Exception exp){logger.error("OffnetError: ",exp);}
		finally
		{
			if(conn_offnet != null){connMgr.freeConnection(conPool_offnet,conn_offnet);retStr	= "ok";}
			if(conn_ins_sdp != null){connMgr.freeConnection(conPool_81,conn_ins_sdp);retStr	= "ok";}
		}
		return retStr;
	}

 	private String callHttps(final String url)
 	{
 		String retStr 								= "";
 		String header 								= "";
 		try
 		{
			HostnameVerifier hv 					= new HostnameVerifier(){public boolean verify(String urlHostName, SSLSession session){return true;}};
			HttpsURLConnection.setDefaultHostnameVerifier(hv);

 			URL Url 								= new URL(url);
 			URLConnection conn 						= Url.openConnection();
 			conn.setReadTimeout(1000*20);
 			header 									= String.valueOf(conn.getHeaderFields());
 			//logger.debug("["+threadName+"] ->(sdp   :"+circle+") responseHeader: "+header);
 			if(header.indexOf("200 OK")>0) header 	= "[HTTP/1.1 200 OK]"; else retStr = header;
 			String str 								= null;
 			BufferedReader in 						= new BufferedReader(new InputStreamReader(conn.getInputStream()));
 			while ((str = in.readLine()) != null) retStr += str;
 			in.close();
 			//if(retStr.length()<2) if(header.equalsIgnoreCase("[HTTP/1.1 200 OK]")){retStr="success";logger.debug(retStr);}
 		}catch(Exception exp){retStr="error,error,fail";logger.error("OffnetError: ",exp);}
 		logger.trace(url+" :"+retStr);
 		return retStr;
 	}
	private String getId()
	{
		String retStr	= "";
		UUID uid		= UUID.randomUUID();
		retStr			= String.valueOf(uid);
		return retStr;
	}

	private void logException(Exception exp)
	{
		logger.error(getStackTrace(exp));
	}

	private synchronized String getStackTrace(Throwable t)
	{
			StringWriter sw 	= new StringWriter();
			PrintWriter pw 		= new PrintWriter(sw, true);
			t.printStackTrace(pw);
			pw.flush();
			sw.flush();
			return sw.toString();
	}
	public void destroy()
	{
		logger.debug("destroy() function called for OffnetSubscription...");
		super.destroy();
	}

	public static class miTM implements javax.net.ssl.TrustManager,javax.net.ssl.X509TrustManager
	{
		public java.security.cert.X509Certificate[] getAcceptedIssuers()			{return null;}
		public boolean isServerTrusted(java.security.cert.X509Certificate[] certs)	{return true;}
		public boolean isClientTrusted(java.security.cert.X509Certificate[] certs)	{return true;}
		public void checkServerTrusted(java.security.cert.X509Certificate[] certs, String authType)	throws java.security.cert.CertificateException{return;}
		public void checkClientTrusted(java.security.cert.X509Certificate[] certs, String authType) throws java.security.cert.CertificateException{return;}
	}
}

