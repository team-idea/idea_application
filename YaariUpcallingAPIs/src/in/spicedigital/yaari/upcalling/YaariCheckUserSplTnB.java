package in.spicedigital.yaari.upcalling;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import in.spicedigital.yaari.util.DBConnectionManager;
import in.spicedigital.yaari.util.LogStackTrace;

public class YaariCheckUserSplTnB extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(YaariCheckUserSplTnB.class);
	private DBConnectionManager connectionManager=null;   
	  
    public YaariCheckUserSplTnB() {
        super();
    }
	public void init(ServletConfig config) throws ServletException {
		try{
			PropertyConfigurator.configure("yaariUpcalling_log4j.properties");
			LOGGER.debug("Configuration file loaded successfullly");
		}catch(Exception exception){
			LOGGER.error("yaariUpcalling_log4j.properties file not found");
			LOGGER.error(LogStackTrace.getStackTrace(exception));
		}
		
		try{
			connectionManager=DBConnectionManager.getInstance();
			LOGGER.debug("Got instance of DBConnectionManager ["+connectionManager+"]");
		}catch(Exception exception){
			exception.printStackTrace();
			LOGGER.error("Exception while getting instance of DBConnectionManager");
			LOGGER.error(LogStackTrace.getStackTrace(exception));
		}
	}



	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ani=null;
		String circle=null;
		
		if(request.getQueryString()!=null){
			try{
				LOGGER.debug((Object) "Query String got as ["+request.getQueryString()+"]");				
				ani= request.getParameter("ani").trim();
				circle= request.getParameter("circle").trim();								
				String dbResponse=null;
				CallableStatement callableStatement=null;
				Connection connMaria=null;
				PrintWriter out=response.getWriter();

				try{
						connMaria=connectionManager.getConnection("mariadb");
						LOGGER.debug("Connection got as ["+connMaria+"]");
						callableStatement=connMaria.prepareCall("{call proc_YaariCheckUserSplTnB(?,?,?)}");
						callableStatement.setString(1,ani);
						callableStatement.setString(2,circle);						
						callableStatement.registerOutParameter(3, java.sql.Types.VARCHAR);
						callableStatement.execute();
						dbResponse=callableStatement.getString(3);
						out.print(dbResponse);
						LOGGER.debug("DB Response returned is ["+dbResponse+"]");
						//out.print(callableStatement.getString(5));
					}catch(Exception ex){
						response.getWriter().println("ERROR");
						LOGGER.error((Object) "Error while calling procedure");
						LOGGER.error(LogStackTrace.getStackTrace(ex));
					}finally{
						if(null!=connMaria){
							connectionManager.freeConnection("mariadb", connMaria);
						}
						if(null!=callableStatement){
							try {
								callableStatement.close();
							} catch (SQLException ex) {
								response.getWriter().println("ERROR");
								LOGGER.error(LogStackTrace.getStackTrace(ex));
							}
							if(null!=out){
								out.flush();
								out.close();
							}
						}
					}
			}catch(Exception ex){
				response.getWriter().println("ERROR");
				LOGGER.error("Exception while processing Query String");
				LOGGER.error((Object) "ani parametes is null [ani] ["+ani+"]" );
				LOGGER.error(LogStackTrace.getStackTrace(ex));			
			}
		}else{
			LOGGER.error("Query String got as null request.getQueryString() ["+request.getQueryString()+"]");
		}		
	
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}
	
}
