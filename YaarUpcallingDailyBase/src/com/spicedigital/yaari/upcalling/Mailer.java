
package com.spicedigital.yaari.upcalling;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Properties;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.log4j.Logger;

public class Mailer {

    private boolean html = true;
    private static Logger logger = Logger.getLogger(Mailer.class.getName());

    private static String _to = null;
    private static String _cc = null;
    private static String _bcc = null;
    private static String from = null;
    private static String smtpServer = "smtp.gmail.com";
    private static String accountUserName = null;
    private static String accountUserPwd = null;
    private static String attachment = null;
    private static String messageSubject=null;
    private static String message=null;

    public Mailer() {
        init();
    }

    public void init() {
        try {
            Properties prop = new Properties();
            prop.load(new DataInputStream(new FileInputStream("./mail.properties")));
            _to = prop.getProperty("alert.mail.to");
            _cc = prop.getProperty("alert.mail.cc");
            _bcc = prop.getProperty("alert.mail.bcc");
            from = prop.getProperty("alert.mail.from");
            smtpServer = prop.getProperty("alert.mail.smtpServer");
            messageSubject=prop.getProperty("alert.mail.messageSubject");
            message=prop.getProperty("alert.mail.message");            
            //accountUserName = prop.getProperty("alert.mail.accountUserName");
            //accountUserPwd = prop.getProperty("alert.mail.accountUserPwd");
            attachment = prop.getProperty("alert.mail.attachment");
            /*try {
                html = Boolean.parseBoolean(prop.getProperty("alert.mail.html"));
            } catch (Exception exp) {
                html = false;
            }*/
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public boolean sendMail() {
    	attachment=attachment.replaceAll("YYYYMMDD", UpcallingMain.todayDate);
        return sendMail(_to, _cc, _bcc, from, smtpServer,accountUserPwd,messageSubject, message, attachment, accountUserName, accountUserPwd);
    }

    public boolean sendMail(final String listTo, final String listCc, final String listBcc, String From, final String messageSubject, final String message, final String Attachment) {
        System.out.println(smtpServer+accountUserPwd+Attachment);
        return sendMail(listTo, listCc, listBcc, From, smtpServer, accountUserPwd, messageSubject, message, Attachment, accountUserName, accountUserPwd);
    }
    
    public boolean sendMail(final String listTo, final String listCc, final String listBcc, String From, final String messageSubject, final String message) {
    	String Attachment="";
        System.out.println(smtpServer+accountUserPwd+Attachment);
        return sendMail(listTo, listCc, listBcc, From, smtpServer, accountUserPwd, messageSubject, message, Attachment, accountUserName, accountUserPwd);
    }
    private boolean sendMail(final String to, final String cc, final String bcc, final String from, final String host, final String pwd, final String messageSubject, final String message,
            final String attachment, final String username, final String password) {

        try {
            Properties props = System.getProperties();
            //String smtpServ = "74.125.130.108";
            String smtpServ =smtpServer;
            props.put("mail.transport.protocol", "smtp");
            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", smtpServ);
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port","465");
            props.put("mail.smtp.socketFactory.port", "465");
            props.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");
            Session session = Session.getDefaultInstance(props, new javax.mail.Authenticator() {
            protected javax.mail.PasswordAuthentication getPasswordAuthentication()
            {
                    return new javax.mail.PasswordAuthentication("sdpse.idea@spicedigital.in", "p@ss@4321");
                }
          });
            Transport bus = session.getTransport("smtp"); // Get a Transport
            bus.connect(host, from, pwd); // Connect only once here

            Message msg = new MimeMessage(session); // Instantiate a message

            msg.setFrom(new InternetAddress(from, "Idea SDP SE")); // Set


            String addressListTo[] = to.split(",");
            InternetAddress[] addressTo = new InternetAddress[addressListTo.length];
            for (int i = 0; i < addressListTo.length; i++) {
                addressTo[i] = new InternetAddress(addressListTo[i]);
            }

            String addressListCc[] = cc.split(",");
            InternetAddress[] addressCc = new InternetAddress[addressListCc.length];
            for (int i = 0; i < addressListCc.length; i++) {
                addressCc[i] = new InternetAddress(addressListCc[i]);
            }

            String addressListBcc[] = bcc.split(",");
            InternetAddress[] addressBcc = new InternetAddress[addressListBcc.length];
            for (int i = 0; i < addressListBcc.length; i++) {
                addressBcc[i] = new InternetAddress(addressListBcc[i]);
            }

            msg.setRecipients(Message.RecipientType.TO, addressTo);

            msg.setRecipients(Message.RecipientType.CC, addressCc);
            msg.setRecipients(Message.RecipientType.BCC, addressBcc);

            msg.setSubject(messageSubject);
            msg.setSentDate(new java.util.Date());

            BodyPart messageBodyPart = new MimeBodyPart();

            Multipart multipart = new MimeMultipart();

            if (html) {
                messageBodyPart.setContent(message, "text/html");
            } else {
                messageBodyPart.setText(message);
            }

            multipart.addBodyPart(messageBodyPart);
            
            if(!attachment.isEmpty()){
            String[] attachementFinal = attachment.split(",");
            for (int i = 0; i < attachementFinal.length; i++) {
                try {
                    //attachementFinal[i] = attachementFinal[i].replace("yyyymmdd", generateDate());
                	//attachementFinal[i] = "/home/Apps/VCHAT/BILLING_DATA/"+attachementFinal[i];
                	attachementFinal[i] = attachementFinal[i];
                    
                    File file = new File(attachementFinal[i]);
                    boolean exists = file.exists();
                    if (exists) {
                        String fileNameActual = new java.io.File(attachementFinal[i]).getName();
                        logger.debug("attachementFinal[" + i + "] = " + attachementFinal[i] + " fileNameActual = " + fileNameActual);
                        messageBodyPart = new MimeBodyPart();
                        DataSource source = new FileDataSource(attachementFinal[i]);
                        messageBodyPart.setDataHandler(new DataHandler(source));
                        messageBodyPart.setFileName(fileNameActual);
                        multipart.addBodyPart(messageBodyPart);
                    } else {
                        logger.debug("File " + attachementFinal[i] + " not present");
                    }
                } catch (Exception exp) {
                    logException(exp);
                    return false;
                }
             }
            }

            // Put parts in message
            msg.setContent(multipart);

            msg.saveChanges();
            Transport.send(msg);
            // bus.sendMessage(msg,address);
            bus.close();
            return true;
        } catch (MessagingException mex) {

            mex.printStackTrace(); // Prints all nested (chained) exceptions as
            // well

            while (mex.getNextException() != null) // How to access nested
            // exceptions
            {
                Exception ex = mex.getNextException(); // Get next exception in
                // chain
                ex.printStackTrace();
                if (!(ex instanceof MessagingException)) {
                    break;
                } else {
                    mex = (MessagingException) ex;
                }
            }
            return false;
        } catch (Exception exp) {
            logException(exp);
            return false;
        }
    }

    private String generateDate() {
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        cal.add(Calendar.DATE, -1);
        return String.valueOf(dateFormat.format(cal.getTime()));

    }

    private void logException(Exception exp) {
        logger.error(getStackTrace(exp));
    }

    private synchronized String getStackTrace(Throwable t) {
        StringWriter sw = new StringWriter();
        PrintWriter pw = new PrintWriter(sw, true);
        t.printStackTrace(pw);
        pw.flush();
        sw.flush();
        return sw.toString();
    }
}