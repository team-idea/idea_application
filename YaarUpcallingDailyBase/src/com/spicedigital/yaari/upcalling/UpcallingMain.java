package com.spicedigital.yaari.upcalling;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Formatter;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.spicedigital.yaari.helper.LogStackTrace;

public class UpcallingMain {
	private static Logger logger = Logger.getLogger(UpcallingMain.class.getName());
	public static String todayDate="";
	private static int runTimeArg =1;
	

	public UpcallingMain() {
		PropertyConfigurator.configure("./log4j.properties");
	}

	public static void main(String[] args) {
		try{
			new UpcallingMain();
		System.out.println("start");
		if(args.length == 1){
			runTimeArg=Integer.parseInt(args[0]);
			todayDate=getdate(runTimeArg);
		}else{
			runTimeArg=1;	
			todayDate=getdate(runTimeArg);
		}
		logger.debug("this date is a date of specific day["+todayDate+"]");
		Mailer mailer= new Mailer();
		mailer.sendMail();
		}catch(Exception ex){
			LogStackTrace.getStackTrace(ex);
		}
	}

	public static String getdate(int dateArg){
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, -dateArg);
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
		String strDate = formatter.format(cal.getTime());
		Formatter fmt = new Formatter();
		todayDate = strDate;
		//monthName = fmt.format("%tb", cal).toString();
		//yearOfDate = cal.get(cal.YEAR);
		//logger.debug("date [" + date + "] and month [" + monthName + "] and year [" + yearOfDate + "]")		
		return todayDate;
	}
	
}
