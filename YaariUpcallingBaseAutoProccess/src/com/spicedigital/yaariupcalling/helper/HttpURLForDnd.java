package com.spicedigital.yaariupcalling.helper;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;


public class HttpURLForDnd {
	private final static Logger logger = Logger.getLogger(HttpURLForDnd.class);
	private String USER_AGENT = "";
	private Properties prop;
//	private String url = null;

	public HttpURLForDnd(){
		//prop = new Properties();
		try{
		PropertyConfigurator.configure("./log4j.properties");
		//prop.load(new DataInputStream(new FileInputStream("./config.properties")));
		//url = prop.getProperty(url);
		}catch(Exception ex){
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public String sendGet(String ani) throws Exception {
		String url = "http://192.168.9.99:8181/DndPlatform/api/checkSingleMsisdn?msisdn="+ani+"&authKey=98a7e03751f811e7a6c2e4115baaccae";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestMethod("GET");
		con.setRequestProperty("User-Agent", USER_AGENT);
		int responseCode = con.getResponseCode();
		logger.debug("\nSending 'GET' request to URL : " + url);
		logger.debug("Response Code : " + responseCode);
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		while ((inputLine = in.readLine()) != null) {
			response.append(inputLine);
		}
		in.close();
		logger.debug(response.toString());
		if(responseCode == 200){
			return response.toString();
		}else{
			return "Error";
		}
	}
}
