package com.spicedigital.yaariupcalling.dbo;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.LinkedList;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.spicedigital.yaariupcalling.helper.LogStackTrace;

public class YaariIvrData {
	private static Logger logger = Logger.getLogger(YaariIvrData.class.getName());
	private DBConnectionManager connMgr = null;
	private String dbpool_ivrYaari = "ivrYaari";
	private Properties prop;
	private String ivrYaariInsertNewUser=null;
	private String ivrYaariInsertDormantUser= null;
	private String appIvrDataMapping=null;
	private String noCallonIvr=null;
	private LinkedList<String> dormantUserList = new LinkedList<String>();
	

	public YaariIvrData() {
		prop = new Properties();
		try {
			PropertyConfigurator.configure("./log4j.properties");
			connMgr = DBConnectionManager.getInstance();
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			ivrYaariInsertNewUser = prop.getProperty("ivrYaariInsertNewUser");		
			ivrYaariInsertDormantUser =prop.getProperty("ivrYaariInsertDormantUser");
			appIvrDataMapping =prop.getProperty("appIvrDataMapping");
			noCallonIvr=prop.getProperty("noCallonIvr");
			
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}
	
	private Timestamp getCurrentDatetime() {
	    java.util.Date today = new java.util.Date();
	    return new java.sql.Timestamp(today.getTime());
	}

	public void insertUpcallingBase(String ani, String status) throws SQLException{
		Connection con = null;
		PreparedStatement pstmt=null;		
		int dndstatus=0;
		try {
			if(status.equalsIgnoreCase("INACTIVE")){
				dndstatus=0;
			}else{
				dndstatus=5;
			}
			con = connMgr.getConnection(dbpool_ivrYaari);
			if (con != null) {
				try {
					pstmt = con.prepareStatement(ivrYaariInsertNewUser);
					pstmt.setString(1, ani);
					pstmt.setInt(2,dndstatus);
					Timestamp date = getCurrentDatetime();
					pstmt.setTimestamp(3, date);
					int record=pstmt.executeUpdate();
					logger.debug("["+record+"] record inserted.");
					pstmt.close();
				} catch (Exception ex) {
					logger.debug(LogStackTrace.getStackTrace(ex));
					if (pstmt != null || !pstmt.isClosed()) {
						pstmt.close();
					}					
				}
			} else {
				logger.debug("database Connection is not found with dbpool_appYaari");
			}
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		} finally {
			if (con != null || !con.isClosed()) {
				connMgr.freeConnection(dbpool_ivrYaari, con);
			}
		}	
	}
	
	public void insertAppDormantUser(String ani, String status) throws SQLException{
		Connection con = null;
		PreparedStatement pstmt=null;		
		int dndstatus=0;
		try {
			if(status.equalsIgnoreCase("INACTIVE")){
				dndstatus=0;
			}else{
				dndstatus=5;
			}
			con = connMgr.getConnection(dbpool_ivrYaari);
			if (con != null) {
				try {
					pstmt = con.prepareStatement(ivrYaariInsertDormantUser);
					pstmt.setString(1, ani);
					pstmt.setInt(2,dndstatus);
					Timestamp date = getCurrentDatetime();
					pstmt.setTimestamp(3, date);
					int record=pstmt.executeUpdate();
					logger.debug("["+record+"] record inserted.");
					pstmt.close();
				} catch (Exception ex) {
					logger.debug(LogStackTrace.getStackTrace(ex));
					if (pstmt != null || !pstmt.isClosed()) {
						pstmt.close();
					}					
				}
			} else {
				logger.debug("database Connection is not found with dbpool_appYaari");
			}
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		} finally {
			if (con != null || !con.isClosed()) {
				connMgr.freeConnection(dbpool_ivrYaari, con);
			}
		}
	
	}
	
	public LinkedList<String> getMappedData() throws SQLException{
		Connection con=null;
		Statement stmt =null;
		ResultSet rs =null;					
		try {
			con = connMgr.getConnection(dbpool_ivrYaari);
			if (con != null) {
				try {
					stmt = con.createStatement();
					rs = stmt.executeQuery(appIvrDataMapping);
					while (rs.next()) {
						String ani = rs.getString("ani").trim();
						dormantUserList.add(ani);
					}
					
					//stmt = con.createStatement();
					rs = stmt.executeQuery(noCallonIvr);
					while (rs.next()) {
						String ani = rs.getString("ani").trim();
						dormantUserList.add(ani);
					}					
					rs.close();
					stmt.close();
				} catch (Exception ex) {
					logger.debug(LogStackTrace.getStackTrace(ex));
					if (stmt != null || !stmt.isClosed()) {
						stmt.close();
					}
					if (rs != null || !rs.isClosed()) {
						rs.close();
					}
				}
				
			} else {
				logger.debug("database Connection is not found with dbpool_appYaari");
			}
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		} finally {
			if (con != null || !con.isClosed()) {
				connMgr.freeConnection(dbpool_ivrYaari, con);
			}
		}
		logger.debug(dormantUserList.size());
		return dormantUserList;
	}

}
