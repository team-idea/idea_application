package com.spicedigital.yaariupcalling.dbo;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import javax.net.ssl.HttpsURLConnection;

import org.apache.log4j.Logger;

import com.spicedigital.yaariupcalling.helper.HttpURLForDnd;
import com.spicedigital.yaariupcalling.helper.LogStackTrace;

public class YaariAppData {
	private static Logger logger = Logger.getLogger(YaariAppData.class.getName());
	private Properties prop;
	private String yaariAppUserName;
	private String yaariAppDbUrl;
	private String yaariAppPassword;
	private String driverName;
	private String appYaariDormantUserQuery;
	private String appYaariNewUserQuery;
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;

	private DBConnectionManager connMgr = null;
	private String dbpool_appYaari = "appYaari";

	private ArrayList<String> YaarriAgentOutCallingNewUser = new ArrayList<String>();
	private ArrayList<String> YaarriAgentOutCallingDormantUser = new ArrayList<String>();

	public YaariAppData() {
		prop = new Properties();
		try {
			connMgr = DBConnectionManager.getInstance();
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			appYaariNewUserQuery = prop.getProperty("appYaariNewUserQuery");
			appYaariDormantUserQuery = prop.getProperty("appYaariDormantUserQuery");
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public void fetchRequiredData() {
		HttpURLForDnd httpURLForDnd = new HttpURLForDnd();
		try {
			getYaarriAgentOutCallingNewUser();
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
		for (String ani : YaarriAgentOutCallingNewUser) {
			try {
				String result = httpURLForDnd.sendGet(ani);
				Thread.sleep(500);
				String[] resultFilter = result.split("[:]+");
				String msisdn = resultFilter[0];
				String dndStatus = resultFilter[1];
				logger.debug("msisdn[" + msisdn + "] status[" + dndStatus + "]");
				YaariIvrData yaariIvrData = new YaariIvrData();
				yaariIvrData.insertUpcallingBase(ani, dndStatus);
			} catch (Exception ex) {
				logger.debug(LogStackTrace.getStackTrace(ex));
			}
		}
		try {
			getyaarriAgentOutCallingDormantUser();
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		}
		for (String ani : YaarriAgentOutCallingDormantUser) {
			try {
				String result = httpURLForDnd.sendGet(ani);
				Thread.sleep(500);
				String[] resultFilter = result.split("[:]+");
				String msisdn = resultFilter[0];
				String dndStatus = resultFilter[1];
				logger.debug("msisdn[" + msisdn + "] status[" + dndStatus + "]");
				YaariIvrData yaariIvrData = new YaariIvrData();
				yaariIvrData.insertAppDormantUser(ani, dndStatus);
			} catch (Exception ex) {
				logger.debug(LogStackTrace.getStackTrace(ex));
			}
		}
	}

	public void getYaarriAgentOutCallingNewUser() throws SQLException {
		try {
			con = connMgr.getConnection(dbpool_appYaari);
			if (con != null) {
				try {
					stmt = con.createStatement();
					rs = stmt.executeQuery(appYaariNewUserQuery);
					while (rs.next()) {
						String ani = rs.getString("ani").trim();
						YaarriAgentOutCallingNewUser.add(ani);
					}
					rs.close();
					stmt.close();
				} catch (Exception ex) {
					logger.debug(LogStackTrace.getStackTrace(ex));
					if (stmt != null || !stmt.isClosed()) {
						stmt.close();
					}
					if (rs != null || !rs.isClosed()) {
						rs.close();
					}
				}
			} else {
				logger.debug("database Connection is not found with dbpool_appYaari");
			}
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		} finally {
			if (con != null || !con.isClosed()) {
				connMgr.freeConnection(dbpool_appYaari, con);
			}
		}
	}

	public void getyaarriAgentOutCallingDormantUser() throws SQLException {
		try {
			con = connMgr.getConnection(dbpool_appYaari);
			if (con != null) {
				try {
					stmt = con.createStatement();
					rs = stmt.executeQuery(appYaariDormantUserQuery);
					while (rs.next()) {
						String ani = rs.getString("ani").trim();
						YaarriAgentOutCallingDormantUser.add(ani);
					}
					rs.close();
					stmt.close();
				} catch (Exception ex) {
					logger.debug(LogStackTrace.getStackTrace(ex));
					if (stmt != null || !stmt.isClosed()) {
						stmt.close();
					}
					if (rs != null || !rs.isClosed()) {
						rs.close();
					}
				}
			} else {
				logger.debug("database Connection is not found with dbpool_appYaari");
			}
		} catch (Exception ex) {
			logger.debug(LogStackTrace.getStackTrace(ex));
		} finally {
			if (con != null || !con.isClosed()) {
				connMgr.freeConnection(dbpool_appYaari, con);
			}
		}
	}

}
