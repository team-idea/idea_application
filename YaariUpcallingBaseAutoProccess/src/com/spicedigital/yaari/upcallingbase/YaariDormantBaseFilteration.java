package com.spicedigital.yaari.upcallingbase;

import java.sql.SQLException;
import java.util.LinkedList;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.spicedigital.yaariupcalling.dbo.YaariIvrData;

public class YaariDormantBaseFilteration {
	private static Logger logger = Logger.getLogger(YaariDormantBaseFilteration.class);
	private LinkedList<String> dormantUpcallingBase = new LinkedList<String>();

	public YaariDormantBaseFilteration() {
		PropertyConfigurator.configure("./log4j.properties");
	}
	
	public void appDataMappingwithIvr() throws SQLException{
		YaariIvrData yaariIvrData = new YaariIvrData();
		dormantUpcallingBase.addAll(yaariIvrData.getMappedData());
		for(String ani:dormantUpcallingBase){
			yaariIvrData.insertUpcallingBase(ani, "INACTIVE");			
		}
		logger.debug(dormantUpcallingBase.size());
	}

}
