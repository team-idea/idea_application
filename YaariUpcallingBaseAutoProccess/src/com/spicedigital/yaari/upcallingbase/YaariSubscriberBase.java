package com.spicedigital.yaari.upcallingbase;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.spicedigital.yaariupcalling.dbo.DBConnectionManager;
import com.spicedigital.yaariupcalling.dbo.YaariAppData;
import com.spicedigital.yaariupcalling.dbo.YaariIvrData;
import com.spicedigital.yaariupcalling.helper.HttpURLForDnd;
import com.spicedigital.yaariupcalling.helper.LogStackTrace;

public class YaariSubscriberBase {
	private static final Logger LOGGER = Logger.getLogger(YaariSubscriberBase.class.getName());
	private ArrayList<String> YaarriAgentOutCallingSubUser = new ArrayList<String>();
	private DBConnectionManager connMgr = null;
	private Connection con = null;
	private Statement stmt = null;
	private ResultSet rs = null;
	private String yaariSubUserQuery = null;
	private String dbpool_ivrYaari = "ivrYaari";
	// private YaariAppData yaariAppData = null;
	// yaariAppData = new YaariAppData();
	private Properties prop = null;

	public YaariSubscriberBase() {
		PropertyConfigurator.configure("./log4j.properties");
		prop = new Properties();
		try {
			connMgr = DBConnectionManager.getInstance();
			prop.load(new DataInputStream(new FileInputStream("./db.properties")));
			yaariSubUserQuery = prop.getProperty("yaariSubUserQuery");
		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		}
	}

	public void yaariSubscriberBase() {
		HttpURLForDnd httpURLForDnd = new HttpURLForDnd();
		YaariIvrData yaariIvrData = new YaariIvrData();
		try {
			getYaariSubscriberBase();
		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		}
		for (String ani : YaarriAgentOutCallingSubUser) {
			try {
				String result = httpURLForDnd.sendGet(ani);
				Thread.sleep(500);
				String[] resultFilter = result.split("[:]+");
				String msisdn = resultFilter[0];
				String dndStatus = resultFilter[1];
				LOGGER.debug("msisdn[" + msisdn + "] status[" + dndStatus + "] [yaari subscriber user]");				
				yaariIvrData.insertUpcallingBase(ani, dndStatus);
			} catch (Exception ex) {
				LOGGER.debug(LogStackTrace.getStackTrace(ex));
			}
		}
	}

	public void getYaariSubscriberBase() throws SQLException {
		try {
			con = connMgr.getConnection(dbpool_ivrYaari);
			if (con != null) {
				try {
					stmt = con.createStatement();
					rs = stmt.executeQuery(yaariSubUserQuery);
					while (rs.next()) {
						String ani = rs.getString("ani").trim();
						YaarriAgentOutCallingSubUser.add(ani);
					}
					rs.close();
					stmt.close();
				} catch (Exception ex) {
					LOGGER.debug(LogStackTrace.getStackTrace(ex));
					if (stmt != null || !stmt.isClosed()) {
						stmt.close();
					}
					if (rs != null || !rs.isClosed()) {
						rs.close();
					}
				}
			} else {
				LOGGER.debug("database Connection is not found with dbpool_appYaari");
			}
		} catch (Exception ex) {
			LOGGER.debug(LogStackTrace.getStackTrace(ex));
		} finally {
			if (con != null || !con.isClosed()) {
				connMgr.freeConnection(dbpool_ivrYaari, con);
			}
		}
	}
}
