package com.spicedigital.yaari.upcallingbase;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.RandomAccessFile;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.spicedigital.yaariupcalling.dbo.YaariAppData;
import com.spicedigital.yaariupcalling.dbo.YaariIvrData;
import com.spicedigital.yaariupcalling.helper.LogStackTrace;

public class YaariUpCallingBase {
	private static Logger logger = Logger.getLogger(YaariUpCallingBase.class.getName());

	public YaariUpCallingBase() {
		super();
	}

	public static void main(String[] args) {
		PropertyConfigurator.configure("./log4j.properties");
		try{
			if (YaariUpCallingBase.accessFile() && YaariUpCallingBase.getLock()) {
				logger.info("program is started..");	
				YaariAppData yaariAppData = new YaariAppData();
				yaariAppData.fetchRequiredData();
				YaariSubscriberBase yaariSubscriberBase = new YaariSubscriberBase();
				yaariSubscriberBase.yaariSubscriberBase();
				YaariDormantBaseFilteration yaariDormantBaseFilteration = new YaariDormantBaseFilteration();
				yaariDormantBaseFilteration.appDataMappingwithIvr();				
				logger.info("program is end..");
			} else {
				System.exit(1);
			}			
		}catch(Exception ex){
			LogStackTrace.getStackTrace(ex);
		}
	}

	private static boolean getLock() {
		boolean lock = false;
		try {
			RandomAccessFile file = new RandomAccessFile("./yaariUpCallingBase.tmp", "rw");
			FileChannel fileChannel = file.getChannel();
			FileLock fileLock = fileChannel.tryLock();
			if (fileLock != null) {
				logger.debug("File is locked. Starting the execution of the Application");
				lock = true;
			} else {
				logger.error("Cannot create lock. Already another instance of the program is running.");
				logger.error("System shutting down. Please wait");
				try {
					Thread.sleep(1000 * 3);
				} catch (Exception exp) {
					exp.printStackTrace();
				}
				System.exit(0);
			}
		} catch (Exception exp) {
			logger.debug(LogStackTrace.getStackTrace(exp));
		}
		return lock;
	}

	private static boolean accessFile() {
		boolean retCode = false;
		try {
			String line = "";
			try {
				BufferedWriter bw = new BufferedWriter(new FileWriter("./yaariUpCallingBase.tmp", false));
				bw.close();
			} catch (Exception exp) {
				exp.printStackTrace();
			}
			retCode = true;
		} catch (Exception e) {
			System.out.println(e);
			logger.debug(LogStackTrace.getStackTrace(e));
		}
		logger.debug("accessFile() = " + retCode);
		return retCode;
	}
}
